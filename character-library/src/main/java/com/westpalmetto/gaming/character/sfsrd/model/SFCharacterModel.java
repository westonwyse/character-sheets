package com.westpalmetto.gaming.character.sfsrd.model;

import java.io.File;

import com.westpalmetto.gaming.character.dd3e.Advancement;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.ThemeWrapper;

import lombok.Getter;
import lombok.Setter;

/**
 * A character class for PSRD characters.
 */
@Getter
@Setter
public class SFCharacterModel extends DD3EModel {
	private ThemeWrapper theme;
	private int credits;

	public SFCharacterModel(File resourceRoot) {
		super(resourceRoot);
	}

	@Override
	public Advancement getAdvancement() {
		return ADVANCEMENT;
	}

	/**
	 * The advancement tables for PSRD characters.
	 */
	public static class AdvancementTable implements Advancement {
		private static final int[] TABLE = new int[] { 0, 1300, 3300, 6000, 10000, 15000, 23000, 34000, 50000, 71000,
				105000, 145000, 210000, 295000, 425000, 600000, 850000, 1200000, 1700000, 2400000 };

		@Override
		public int getLevel(int experience) {
			int level;
			for (level = 0; level < TABLE.length; level++) {
				if (experience < TABLE[level]) {
					break;
				}
			}
			return level;
		}

		@Override
		public int getExperience(int level) {
			return TABLE[level - 1];
		}

		@Override
		public int getNextLevel(int experience) {
			return TABLE[getLevel(experience)];
		}
	}

	private static final Advancement ADVANCEMENT = new AdvancementTable();
}
