/**
 * PrimaryWeaponPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Feb 16, 2016
 */
package com.westpalmetto.gaming.character.model.predicate;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment.Hand;


/**
 * Predicate to match a weapon equipped in a given hand.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class HeldWeaponPredicate implements Predicate {
	private static Map<Hand, HeldWeaponPredicate> instances;
	
	private final Hand hand;
	
	private HeldWeaponPredicate(Hand hand){
		this.hand = hand;
	}
	
	public static HeldWeaponPredicate getInstance(Hand hand){
		//Create the map, if needed.
		if(instances == null){
			instances = new HashMap<>();
		}
		
		//Create the instance, if needed.
		if(!instances.containsKey(hand)){
			instances.put(hand, new HeldWeaponPredicate(hand));
		}
		
		//Return the instance.
		return instances.get(hand);
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Weapon){
			Weapon weapon = (Weapon)object;
			matches = StringUtils.equalsIgnoreCase(AbstractEquipment.LOCATION_EQUIPPED, weapon.getLocation())
					&& getHand().equals(weapon.getHand());
		}
		
		return matches;
	}

	/**
	 * @return the hand
	 */
	public Hand getHand(){
		return hand;
	}
}
