/**
 * DataLoader.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.util.loading;

import java.util.Map;


/**
 * Interface to define utilities to allow an object to be created from data
 * loaded at runtime.  Data should be cached after loading and returned by
 * implementations of this class.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface DataLoader<V> {
	/**
	 * For caching purposes, an application-scope map of key to object should
	 * be available, and should contain objects previously loaded.
	 * 
	 * @return The map of previously loaded objects.
	 */
	public Map<String, V> getCache();
	
	/**
	 * Determines if an object exists in the cache.
	 * 
	 * @param id The ID of the item.
	 * @return TRUE if the item is in the cache.
	 */
	public boolean contains(String id);
	
	/**
	 * Retrieves a specific object from the cache.
	 * 
	 * @param id The ID of the item to be returned.
	 * @return The parsed object
	 */
	public V getData(String id);
	
	/**
	 * Determines what the cache key should be for the given ID.
	 * 
	 * @param id The ID of the item to be returned.
	 * @return The expected key for the ID
	 */
	public String getKey(String id);
	
	/**
	 * Null-safe duplication of a given object.
	 * 
	 * @param original The original object
	 * @return A duplicate of the original object; null if original is null.
	 */
	public V getClonedObject(V original);
}
