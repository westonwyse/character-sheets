package com.westpalmetto.gaming.character.bonuses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment.Size;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.KeyedObject;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableObject;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper methods for Bonus operations.
 */
@Slf4j
public class BonusHelper{
    private BonusHelper(){
        throw new UnsupportedOperationException("Cannot instantiate helper class.");
    }

    /**
     * Calculates the finalized bonuses for the given ModifiableBonus collection
     * and Modifiers.
     * 
     * @param bonuses The bonuses to calculate.
     * @param modifiers The modifiers to apply.
     * @return The finalized bonus.
     */
    public static Collection<UnmodifiableBonus> calculateBonuses(Collection<ModifiableBonus> bonuses,
            Collection<Modifier> modifiers){
        Collection<UnmodifiableBonus> finalized = new HashSet<UnmodifiableBonus>();

        for (ModifiableBonus bonus : bonuses) {
            finalized.add(calculateBonus(bonus, modifiers));
        }

        return finalized;
    }

    /**
     * Calculates the finalized bonus for the given ModifiableBonus and
     * Modifiers.
     * 
     * @param bonus The bonus to calculate.
     * @param modifiers The modifiers to apply.
     * @return The finalized bonus.
     */
    public static UnmodifiableBonus calculateBonus(ModifiableBonus bonus, Collection<Modifier> modifiers){
        UnmodifiableBonus unmodifiableBonus = null;

        if (applies(bonus, modifiers)) {
            ModifiableBonus finalizedBonus = new ModifiableBonus(bonus);
            log.trace("Modifiers: {}", modifiers);

            // If there are adjustments to be made, make them
            if (bonus.getIncreases().size() > 0) {
                // Determine number of steps met.
                int steps = determineSteps(bonus, modifiers);
                log.trace("Steps: {}", steps);

                // Adjust value, if exists
                if (bonus.getValue() != 0) {
                    // There can be only one value, so take only the first
                    // increase; others are ignored
                    finalizedBonus.setValue(bonus.getValue() + steps * bonus.getIncreases().get(0));
                }

                // Adjust condition, if exists
                if (bonus.getConditionValues().size() > 0) {
                    List<Integer> conditionValues = new ArrayList<Integer>();
                    for (int i = 0; (i < bonus.getConditionValues().size()) && (i < bonus.getIncreases().size()); i++) {
                        conditionValues.add(bonus.getConditionValues().get(i) + steps * bonus.getIncreases().get(i));
                    }
                    log.trace("Condition values: {}", conditionValues);
                    finalizedBonus.setCondition(String.format(bonus.getCondition(), conditionValues.toArray()));
                }
            }

            try {
                unmodifiableBonus = new UnmodifiableBonus(finalizedBonus);
            } catch (Exception e) {
                // TODO: Remove try
                throw new RuntimeException("Exception while modifying " + finalizedBonus, e);
            }
        }
        return unmodifiableBonus;
    }

    private static int determineSteps(ModifiableBonus bonus, Collection<Modifier> modifiers){

        // Sum the appropriate modifiers
        Collection<Modifier> filtered = filterModifiers(modifiers, bonus);
        int total = sumModifiers(filtered);

        // Find the appropriate step
        int steps = 0;
        for (steps = 0; (steps < bonus.getSteps().size()) && (bonus.getSteps().get(steps) <= total); steps++)
            ;
        return steps;
    }

    /**
     * Determine if a ModifiableObject should be applied with the given
     * modifiers.
     * 
     * @param obj The object to inspect.
     * @param modifiers The modifiers with which to inspect.
     * @return TRUE is the object should be applied; FALSE if not.
     */
    public static boolean applies(Object obj, Collection<Modifier> modifiers){
        boolean applies;
        if (obj instanceof ModifiableObject) {
            ModifiableObject modifiable = (ModifiableObject) obj;

            // Only need to check if applies > 0
            applies = (modifiable.getApplies() < 1);

            if (!applies) {
                // Filter for the appropriate modifiers
                Collection<Modifier> filtered = filterModifiers(modifiers, modifiable);

                // Compare the total to the threshold
                applies = sumModifiers(filtered) >= modifiable.getApplies();
            }
        } else {
            // If the object does not have an applies level, return true;
            applies = true;
        }

        return applies;
    }

    public static Collection<Modifier> filterModifiers(Collection<Modifier> modifiers, ModifiableObject modifiable){
        log.trace("Modifiable object: {}", modifiable);
        // Filter by type
        Collection<Modifier> typed = filterModifiersByType(modifiers, modifiable.getAppliesType());
        log.trace("Typed modifiers: {}", typed);

        // Filter by keys/name/appliesKey
        Collection<Modifier> filtered = filterObjectsByKeys(typed, modifiable.getItemKeys());
        filterObjectsByNonEmptyKey(typed, modifiable.getName(), filtered);
        filterObjectsByNonEmptyKey(typed, modifiable.getAppliesKey(), filtered);
        log.trace("Filtered: {}", filtered);
        return filtered;
    }

    /**
     * For a given collection of Bonus objects, returns the collection updated
     * to add the given default name to any bonus that has no name.
     * 
     * @param bonuses The bonuses to update.
     * @param defaultName The name to add.
     * @return The modified collection.
     */
    public static Collection<ModifiableBonus> nameBonuses(Collection<ModifiableBonus> bonuses, String defaultName){
        Collection<ModifiableBonus> namedBonuses = new ArrayList<ModifiableBonus>();
        for (ModifiableBonus bonus : bonuses) {
            if (bonus != null) {
                ModifiableBonus duplicate = new ModifiableBonus(bonus);

                // Add name, if it doesn't exist
                if (StringUtils.isBlank(duplicate.getName())) {
                    duplicate.setName(defaultName);
                }

                namedBonuses.add(duplicate);
            }
        }
        return namedBonuses;
    }

    /**
     * Validates that the object has a value of itemKey, if it is required.
     * 
     * @param keyed The object to validate
     * @return The valid itemKey.
     * @throws IllegalArgumentException if the required itemKey is missing.
     */
    public static Collection<String> validateItemKeys(KeyedObject keyed){
        if (CollectionUtils.isEmpty(keyed.getItemKeys()) && keyed.isRequireItemKey()) {
            throw new IllegalArgumentException("Item key was not set for " + keyed);
        }

        return keyed.getItemKeys();
    }

    /**
     * Filters a collection of bonuses by BonusType.
     * 
     * @param bonuses The collection to filter
     * @param type The BonusType by which to filter
     * @return The filtered bonuses
     */
    public static <T extends Bonus> Collection<T> filterBonusesByType(Collection<T> bonuses, BonusType type){
        Collection<T> filteredBonuses = new HashSet<T>();
        CollectionUtils.select(bonuses, BonusTypePredicate.getInstance(type), filteredBonuses);
        return filteredBonuses;
    }

    /**
     * Filters a collection of modifiable objects by name and/or item key.
     * 
     * @param objects The collection to filter
     * @param key The keys by which to filter
     * @return The filtered bonuses
     */
    public static <T extends KeyedObject> Collection<T> filterObjectsByKey(Collection<T> objects, String key){
        return filterObjectsByKeys(objects, Sets.newHashSet(key));
    }

    /**
     * Filters a collection of modifiable objects by string key, if the key is
     * not blank.
     * 
     * @param objects The collection to filter
     * @param key The keys by which to filter
     * @param filtered The collection to contain the filtered bonuses
     */
    public static <T extends KeyedObject> void filterObjectsByNonEmptyKey(Collection<T> objects,
            String key,
            Collection<T> filtered){
        if (StringUtils.isNotBlank(key)) {
            filtered.addAll(filterObjectsByKey(objects, key));
        }
    }

    /**
     * Filters a collection of modifiable objects by name and/or item key.
     * 
     * @param objects The collection to filter
     * @param keys The keys by which to filter
     * @return The filtered bonuses
     */
    public static <T extends KeyedObject> Collection<T> filterObjectsByKeys(Collection<T> objects,
            Collection<String> keys){
        Collection<T> filtered = new ArrayList<T>();
        CollectionUtils.select(objects, KeyedObjectKeyPredicate.getInstance(keys), filtered);
        return filtered;
    }

    /**
     * @param bonuses The bonuses to sum.
     * @return The sum of all values of the given bonuses.
     */
    public static int sumBonuses(Collection<? extends Bonus> bonuses){
        int total = 0;

        for (Bonus bonus : bonuses) {
            total += bonus.getValue();
        }

        return total;
    }

    /**
     * Filters a collection of modifiers by ModifierType.
     * 
     * @param modifiers The collection to filter
     * @param type The ModifierType by which to filter
     * @return The filtered modifiers
     */
    public static Collection<Modifier> filterModifiersByType(Collection<Modifier> modifiers, ModifierType type){
        Collection<Modifier> filteredModifiers = new HashSet<Modifier>();
        CollectionUtils.select(modifiers, ModifierTypePredicate.getInstance(type), filteredModifiers);
        return filteredModifiers;
    }

    /**
     * @param modifiers The modifiers to sum.
     * @return The sum of all values of the given modifiers.
     */
    public static int sumModifiers(Collection<Modifier> modifiers){
        int total = 0;

        for (Modifier modifier : modifiers) {
            total += modifier.getValue();
        }

        return total;
    }

    /**
     * @param provider The provider.
     * @param bonuses The raw bonuses from the provider.
     * @param modifiers The modifiers.
     * @return The bonuses that apply.
     */
    public static <T extends BonusProvider & ModifiableObject> Collection<ModifiableBonus> filterByApplies(T provider,
            Collection<ModifiableBonus> bonuses,
            Collection<Modifier> modifiers){
        Collection<ModifiableBonus> applies = new HashSet<ModifiableBonus>();

        // Verify the object should be applied.
        if (BonusHelper.applies(provider, modifiers) && (bonuses != null)) {
            // Ensure each bonus has necessary fields.
            for (ModifiableBonus bonus : bonuses) {
                if (bonus != null) {
                    ModifiableBonus copy = new ModifiableBonus(bonus);
                    if (copy.getAppliesType() == null) {
                        copy.setAppliesType(provider.getAppliesType());
                    }
                    copy.getItemKeys().addAll(provider.getItemKeys());
                    if (StringUtils.isBlank(copy.getName())) {
                        copy.setName(provider.getName());
                    } else if (!StringUtils.equals(provider.getName(), copy.getName())) {
                        copy.getItemKeys().add(provider.getName());
                    }
                    applies.add(copy);
                }
            }
        }

        return applies;
    }

    /**
     * Collects bonuses from a given bonus provider where the bonus contains a
     * bonus die.
     * 
     * @param weapon The weapon for which to search for bonuses.
     * @param providers The providers.
     * @param modifiers The modifiers to apply.
     * @return All discovered matching bonuses with bonus dice.
     */
    public static Collection<UnmodifiableBonus>
            getDieBonuses(Weapon weapon, Collection<BonusProvider> providers, Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = new HashSet<ModifiableBonus>();

        // Collect all bonuses
        for (BonusProvider provider : providers) {
            if (provider != null) {
                bonuses.addAll(provider.getBonuses(modifiers));
            }
        }

        // Add any bonus abilities
        bonuses.addAll(weapon.getBonusAbilities(bonuses).getBonuses(modifiers));

        // Calculate the bonuses
        Collection<UnmodifiableBonus> calculated = calculateBonuses(bonuses, modifiers);

        // Filter to only bonuses with dice
        Collection<UnmodifiableBonus> filtered = new HashSet<UnmodifiableBonus>();
        CollectionUtils.select(calculated, BonusDiePredicate.getInstance(), filtered);

        // Filter to only bonuses applicable to this weapon
        Collection<UnmodifiableBonus> weaponSpecific = new HashSet<UnmodifiableBonus>();
        CollectionUtils.select(filtered,
                KeyedObjectKeyPredicate.getInstance(Sets.newHashSet(weapon.getName(), Weapon.KEY_DEFAULT)),
                weaponSpecific);
        return weaponSpecific;
    }

    /**
     * For the given bonuses, get the bonus dice descriptions.
     * 
     * @param bonuses The bonuses.
     * @return The descriptions of the dice bonuses.
     */
    public static String getDieDescriptions(Collection<UnmodifiableBonus> bonuses){
        Collection<String> bonusDescriptions = new HashSet<String>();

        for (UnmodifiableBonus bonus : bonuses) {
            if (bonus.getValueDie() != null) {
                String description = bonus.getValueDie().getForSize(Size.M).getDisplay();
                if (StringUtils.isNotBlank(bonus.getValueType())) {
                    description = description + "(" + bonus.getValueType() + ")";
                }
                bonusDescriptions.add(description);
            }
        }

        String descriptions = StringUtils.join(bonusDescriptions, "+");
        if (StringUtils.isNotBlank(descriptions)) {
            descriptions = "+" + descriptions;
        }

        return descriptions;
    }

    /**
     * Clones the given bonuses & adds the given keys.
     * 
     * @param bonuses The bonuses to modify
     * @param keys The keys to add
     * @return The cloned, keyed bonuses.
     */
    public static Collection<ModifiableBonus> keyBonuses(Collection<ModifiableBonus> bonuses, Collection<String> keys){
        Collection<ModifiableBonus> keyed = new HashSet<>();

        for (ModifiableBonus bonus : bonuses) {
            if (bonus != null) {
                ModifiableBonus newBonus = new ModifiableBonus(bonus);
                newBonus.getItemKeys().addAll(keys);
                keyed.add(newBonus);
            }
        }

        return keyed;
    }

    /**
     * For a given collection of bonus providers, extracts all providers from
     * all sets, to create a shallow collection of only the final providers.
     * 
     * @param providers The providers to flatten.
     * @return The shallow collection of providers.
     */
    public static Collection<BonusProvider> flattenProviders(Collection<? extends BonusProvider> providers){
        Collection<BonusProvider> flattened = new HashSet<>();

        // For each provider, determine if it is a set or not
        for (BonusProvider provider : providers) {
            if (provider instanceof BonusProviderSet) {
                // Extract the providers within the set
                flattened.addAll(flattenProviders((Collection<? extends BonusProvider>) provider));
            } else {
                // Just add the provider
                flattened.add(provider);
            }
        }

        return flattened;
    }

    /**
     * Sums all of the values in the given bonuses.
     * 
     * @param bonuses The bonuses to sum.
     * @return The sum of the bonuses.
     */
    public static int sumValues(Collection<UnmodifiableBonus> bonuses){
        int total = 0;

        for (UnmodifiableBonus bonus : bonuses) {
            total += bonus.getValue();
        }

        return total;
    }

    /**
     * For a given collection of bonuses, return all bonus types found.
     * 
     * @param bonuses The bonuses to inspect.
     * @return All types contained in the collection.
     */
    public static Collection<BonusType> getTypes(Collection<? extends Bonus> bonuses){
        Collection<BonusType> types = new HashSet<>();

        for (Bonus bonus : bonuses) {
            types.add(bonus.getType());
        }

        return ImmutableSet.copyOf(types);
    }
}
