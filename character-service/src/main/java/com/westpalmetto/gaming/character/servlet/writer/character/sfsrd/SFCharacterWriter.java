/**
 * SFCharacterMapper.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.sfsrd;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Character3E.CarryingCapacity;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment.Slot;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.RaceWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SchoolWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.servlet.writer.character.character3e.Character3EWriter;
import com.westpalmetto.gaming.character.sfsrd.SFCharacter;
import com.westpalmetto.gaming.character.sfsrd.model.SFCharacterModel;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.ThemeWrapper;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.equipment.SFArmor;

import lombok.extern.slf4j.Slf4j;

/**
 * Handling of response writing for SFSRD characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class SFCharacterWriter extends Character3EWriter<SFCharacter>{

    @Override
    protected void writeBasics(PrintWriter writer, SFCharacter character){
        long start = System.currentTimeMillis();

        SFCharacterModel data = character.getSFData();

        // Gather class names
        Iterator<ClassWrapper> classes = data.getClasses().iterator();
        StringBuilder classNames = new StringBuilder();
        while (classes.hasNext()) {
            ClassWrapper cls = classes.next();
            classNames.append(cls.getData().getName() + " " + cls.getLevels());
            if (classes.hasNext()) {
                classNames.append(" / ");
            }
        }

        // Open section
        writer.println("<div id='basics'>");

        // Write data
        writeLabelValuePair(writer, "name", "Character Name", data.getName());
        writeLabelValuePair(writer, "alignment", "Alignment", data.getAlignment().toString());
        writeLabelValuePair(writer, "class", "Class", classNames.toString());
        writeLabelValuePair(writer, "race", "Race", data.getRace().getData().getName());
        writeLabelValuePair(writer, "theme", "Theme", data.getTheme().getData().getName());
        writeLabelValuePair(writer, "deity", "Deity", data.getDeity());
        writeLabelValuePair(writer,
                "experience",
                "Experience",
                Character.FORMATTER_THOUSANDS.format(data.getExperience()));
        writeLabelValuePair(writer,
                "nextLevel",
                "Next Level",
                Character.FORMATTER_THOUSANDS
                        .format(data.getAdvancement().getNextLevel(data.getExperience().intValue())));
        writeLabelValuePair(writer, "size", "Size", character.getSize().getData().getName());
        writeLabelValuePair(writer, "gender", "Gender", data.getGender());
        writeLabelValuePair(writer, "age", "Age", data.getAge());
        writeLabelValuePair(writer, "height", "Height", data.getHeight());
        writeLabelValuePair(writer, "weight", "Weight", data.getWeight());
        writeLabelValuePair(writer, "hair", "Hair", data.getHair());
        writeLabelValuePair(writer, "eyes", "Eyes", data.getEyes());

        // Close section
        writer.println("</div>");

        log.trace("writeBasics: {}ms", System.currentTimeMillis() - start);
    }

    @Override
    protected void writeSkills(PrintWriter writer, SFCharacter character){
        // Open section
        writer.println("<div id='skills'>");
        writer.println("<h2>Skills</h2>");

        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='classSkill'>&nbsp;</th>");
        writer.println("<th class='name'>Skill Name</th>");
        writer.println("<th class='bonus'>Total Bonus</th>");
        writer.println("<th class='ability'>Ability</th>");
        writer.println("<th class='abilityMod'>Ability Mod.</th>");
        writer.println("<th class='ranks'>Ranks</th>");
        writer.println("<th class='classSkill'>Class Bonus</th>");
        writer.println("<th class='misc'>Misc.</th>");
        writer.println("</tr>");

        // Write skills
        int count = 1;
        NamedObjectSet<DD3ESkill> skills = character.getSkills();
        for (DD3ESkill skill : skills) {
            AttributeName attributeName = skill.getAttributeName();
            String classSkill = (skill.isClassSkill() || character.isThemeSkill(skill)) ? "classSkill" : "";
            String classSkillMarker = (skill.isClassSkill()) ? "&raquo;" : "";
            String trainedOnly = (skill.isTrainedOnly()) ? "trainedOnly" : "";
            String trainedOnlyMarker = (skill.isTrainedOnly()) ? " &Dagger;" : "";
            String totalBonus = (skill.isTrainedOnly() && (skill.getRanks() == 0)) ? "&mdash;"
                    : Character.FORMATTER_MODIFIER.format(character.getSkillBonus(skill));
            String marker = (count % 3 == 0) ? "marker" : "";
            count++;

            // Write name
            writer.println("<tr class='" + classSkill + " " + trainedOnly + " " + marker + "'>");
            writer.println("<td class='classSkill'>" + classSkillMarker + "</td>");
            writer.println("<td class='name'>" + skill.getName() + trainedOnlyMarker + "</td>");
            writer.println("<td class='bonus'>" + totalBonus + "</td>");
            writer.println("<td class='ability'>" + attributeName.getAbbreviation() + "</td>");
            writer.println("<td class='abilityMod'>" + character.getAttributeBonus(attributeName) + "</td>");
            writer.println("<td class='ranks'>" + skill.getRanks() + "</td>");
            writer.println("<td class='classSkill'>" + character.getSkillClassBonus(skill) + "</td>");
            writer.println("<td class='misc'>" + character.getSkillMiscModifiers(skill) + "</td>");
            writer.println("</tr>");
        }
        writer.println("</table>");
        writer.println("<div class='legend'>&raquo; - Class skill</div>");
        writer.println("<div class='legend'>&Dagger; - Trained only</div>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SKILL);
        writeConditionals(writer, conditionals);

        // Validate skill points
        writer.println("<ul>");
        writer.println("<li>Earned skill ranks: " + character.getEarnedSkillRanks() + "</li>");
        writer.println("<li>Used skill ranks: " + character.getUsedSkillRanks() + "</li>");
        writer.println("</ul>");

        // Close section
        writer.println("</div>");
    }

    @Override
    protected void writeHP(PrintWriter writer, SFCharacter character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<div id='hp'>");

        // Write HP
        writer.println("<table class='hp'><tr>");
        writer.println("<th class='name'>SP</th>");
        writer.println("<th class='name'>HP</th>");
        writer.println("<th class='name'>Resolve</th>");
        writer.println("</tr><tr>");
        writer.println("<td class='value'>" + character.getStaminaPoints() + "</td>");
        writer.println("<td class='value'>" + character.getHitPoints() + "</td>");
        writer.println("<td class='value'>" + character.getResolvePoints() + "</td>");
        writer.println("</tr></table>");
        writer.println("<div class='wounds'>");
        writer.println("<span class='name'>Wounds</span>");
        writer.println("<span class='value'>&nbsp;</span>");
        writer.println("</div>");
        writer.println("<div class='nonlethal'>");
        writer.println("<span class='name'>Nonlethal Damage</span>");
        writer.println("<span class='value'>&nbsp;</span>");
        writer.println("</div>");

        // Close section
        writer.println("</div>");

        log.trace("writeHP: {}ms", System.currentTimeMillis() - start);
    }

    @Override
    protected void writeAC(PrintWriter writer, SFCharacter character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<table id='ac'>");

        // Write EAC
        writer.println("<tr class='ac'>");
        writeLabelValuePair(writer, "header", null, "EAC", "th");
        writeLabelValuePair(writer, "total", "Total", character.getEACBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "armor", "Armor", character.getEACArmorBonus(), "td");
        writeLabelValuePair(writer, "dex", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX, true), "td");
        // writeLabelValuePair(writer, "size", "Size",
        // character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getACMiscBonus(), "td");
        writer.println("</tr>");

        // Write KAC
        writer.println("<tr class='ac'>");
        writeLabelValuePair(writer, "header", null, "KAC", "th");
        writeLabelValuePair(writer, "total", "Total", character.getKACBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "armor", "Armor", character.getKACArmorBonus(), "td");
        writeLabelValuePair(writer, "dex", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX, true), "td");
        // writeLabelValuePair(writer, "size", "Size",
        // character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getACMiscBonus(), "td");
        writer.println("</tr>");

        // Close section
        writer.println("</table>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.ARMOR_CLASS);
        writeConditionals(writer, conditionals);

        log.trace("writeAC: {}ms", System.currentTimeMillis() - start);
    }

    @Override
    protected void writeCombat(PrintWriter writer, SFCharacter character){
        long start = System.currentTimeMillis();

        writer.println("<h2>Combat</h2>");
        writer.println("<table id='attacks'>");

        // Write Melee
        writer.println("<tr class='melee'>");
        writeLabelValuePair(writer, "header", null, "Melee", "th");
        writeLabelValuePair(writer,
                "total",
                "Total",
                Character.FORMATTER_MODIFIER.format(character.getMeleeAttackTotalBonus()),
                "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Str Bonus", character.getAttributeBonus(AttributeName.STR), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getMeleeAttackMiscBonus(), "td");
        writer.println("</tr>");

        // Write Ranged
        writer.println("<tr class='ranged'>");
        writeLabelValuePair(writer, "header", null, "Ranged", "th");
        writeLabelValuePair(writer,
                "total",
                "Total",
                Character.FORMATTER_MODIFIER.format(character.getRangedAttackTotalBonus()),
                "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getRangedAttackMiscBonus(), "td");
        writer.println("</tr>");

        // Close section
        writer.println("</table>");

        // Write Two-Weapon Fighting, if applicable
        if (character.hasTwoWeapons()) {
            writer.println("<table id='twoweapon'>");

            writer.println("<tr class='standard'>");
            writeLabelValuePair(writer, "header", "", "Two-Weapon Fighting", "th");
            writeLabelValuePair(writer,
                    "primary",
                    "Primary Hand",
                    Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackPrimaryBonus()),
                    "td");
            writeLabelValuePair(writer,
                    "offhand",
                    "Off Hand",
                    Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackOffHandBonus()),
                    "td");
            writer.println("</tr>");

            writer.println("</table>");
        }

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.ATTACK);
        CollectionUtils.addAll(conditionals, character.getConditionalModifiers(BonusType.COMBAT_MANEUVER).iterator());
        CollectionUtils.addAll(conditionals, character.getConditionalModifiers(BonusType.COMBAT_DEFENSE).iterator());
        writeConditionals(writer, conditionals);

        log.trace("writeCombat: {}ms", System.currentTimeMillis() - start);
    }

    @Override
    protected void writeSpecialAbilities(PrintWriter writer, SFCharacter character){
        SFCharacterModel data = character.getSFData();
        Collection<Modifier> modifiers = character.getModifiers();

        // Write feats
        writer.println("<h3>Feats</h3>");
        writeSpecialAbilityList(character.getFeats(), writer, modifiers);

        // Validate feat count
        writer.println("<ul>");
        writer.println("<li>Earned feats: " + character.getEarnedFeats() + "</li>");
        writer.println("<li>Used feats: " + character.getUsedFeats() + "</li>");
        writer.println("</ul>");

        // Write racial abilities
        RaceWrapper race = data.getRace();
        if (race != null) {
            writer.println("<h3>" + race.getData().getName() + "</h3>");
            writeSpecialAbilityList(race.getData().getAbilities(), writer, modifiers);
        }

        // Write theme abilities
        ThemeWrapper theme = data.getTheme();
        if (theme != null) {
            writer.println("<h3>" + theme.getData().getName() + "</h3>");
            writeSpecialAbilityList(theme.getData().getAbilities(), writer, modifiers);
        }

        // Write class abilities
        for (ClassWrapper clz : data.getClasses()) {
            Map<String, String> options = clz.getOptions();
            writer.println("<h3>" + clz.getData().getName() + "</h3>");
            // Write standard class abilities
            writeSpecialAbilityList(clz.getAbilities(), writer, modifiers, options);

            // Check for talents
            if (!clz.getTalents().isEmpty()) {
                if (StringUtils.isNotBlank(clz.getData().getTalentsName())) {
                    writer.println("<h3>" + clz.getData().getTalentsName() + "</h3>");
                }
                // Write standard class abilities
                writeSpecialAbilityList(clz.getTalents(), writer, modifiers, options);
            }

            // Check for schools
            if (!clz.getSchools().isEmpty()) {
                for (SchoolWrapper school : clz.getSchools()) {
                    writer.println("<h3>" + school.getName() + "</h3>");
                    // Write school abilities
                    writeSpecialAbilityList(school.getData().getAbilities(), writer, modifiers, options);
                }
            }
        }

        // Write other special abilities, if applicable
        Collection<SpecialAbility> abilities = new HashSet<SpecialAbility>();
        abilities.addAll(data.getBoons());
        abilities.addAll(character.getData().getQualities());
        if (CollectionUtils.isNotEmpty(abilities)) {
            writer.println("<h3>Other Abilities</h3>");
            writeSpecialAbilityList(abilities, writer, modifiers);
        }
    }

    @Override
    protected void writeWeapons(PrintWriter writer, SFCharacter character){
        Set<Weapon> weaponSet = character.getWeaponSet();

        if (CollectionUtils.isNotEmpty(weaponSet)) {
            // Open section
            writer.println("<div id='weapons'>");
            writer.println("<h2>Weapons/Attacks</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>Weapon</th>");
            writer.println("<th class='bonus'>Atk. Bonus</th>");
            writer.println("<th class='damage'>Dam.</th>");
            writer.println("<th class='critical'>Critical</th>");
            writer.println("<th class='type'>Dam. Type</th>");
            writer.println("<th class='range'>Range</th>");
            writer.println("<th class='size'>Size</th>");
            writer.println("<th class='weight'>Bulk</th>");
            writer.println("<th class='level'>Level</th>");
            writer.println("<th class='category'>Class</th>");
            writer.println("<th class='notes'>Special</th>");
            writer.println("</tr>");

            // Write weapons
            for (Weapon weapon : weaponSet) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + weapon.getDisplayName() + "</td>");
                writer.println("<td class='bonus'>"
                        + Character.FORMATTER_MODIFIER.format(character.getWeaponAttackBonus(weapon))
                        + "</td>");
                writer.println("<td class='damage'>" + character.getWeaponDamage(weapon) + "</td>");
                writer.println("<td class='critical'>" + character.getThreatRange(weapon)
                        + "/"
                        + weapon.getData().getThreatMultiplier()
                        + "</td>");
                writer.println("<td class='type'>" + weapon.getData().getDamageType() + "</td>");
                writer.println("<td class='range'>" + formatNullable(weapon.getData().getRangeIncrement()) + "</td>");
                writer.println("<td class='size'>" + formatNullable(weapon.getSize()) + "</td>");
                writer.println("<td class='weight'>" + formatNullable(weapon.getWeight()) + "</td>");
                writer.println("<td class='level'>" + formatNullable(weapon.getData().getLevel()) + "</td>");
                writer.println("<td class='category'>" + weapon.getData().getProficiency().getDisplayName()
                        + ", "
                        + weapon.getData().getHeft().getAbbreviation()
                        + "</td>");
                writer.println("<td class='notes'>" + formatSpecialAbilities(weapon, character) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    @Override
    protected void writeArmor(PrintWriter writer, SFCharacter character){
        Set<SFArmor> armorSet = character.getSFArmorSet();

        if (CollectionUtils.isNotEmpty(armorSet)) {
            // Open section
            writer.println("<div id='armor'>");
            writer.println("<h2>Armor</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>AC Item</th>");
            writer.println("<th class='eac'>EAC Bonus</th>");
            writer.println("<th class='kac'>KAC Bonus</th>");
            writer.println("<th class='type'>Armor Type</th>");
            writer.println("<th class='dex'>Max. Dex.</th>");
            writer.println("<th class='upgrades'>Upgrade Slots</th>");
            writer.println("<th class='weight'>Bulk</th>");
            writer.println("<th class='level'>Level</th>");
            writer.println("<th class='notes'>Special</th>");
            writer.println("</tr>");

            // Write armor
            for (SFArmor armor : armorSet) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + armor.getDisplayName() + "</td>");
                writer.println("<td class='bonus'>" + Character.FORMATTER_MODIFIER.format(armor.getModifiedEacBonus())
                        + "</td>");
                writer.println("<td class='bonus'>" + Character.FORMATTER_MODIFIER.format(armor.getModifiedKacBonus())
                        + "</td>");
                writer.println("<td class='type'>" + armor.getData().getProficiency().getAbbreviation() + "</td>");
                writer.println("<td class='dex'>" + formatNullable(armor.getData().getMaxDex()) + "</td>");
                writer.println("<td class='upgrades'>" + formatNullable(armor.getData().getUpgradeSlots()) + "</td>");
                writer.println("<td class='weight'>" + armor.getWeight() + "</td>");
                writer.println("<td class='level'>" + formatNullable(armor.getData().getLevel()) + "</td>");
                writer.println("<td class='notes'>" + formatSpecialAbilities(armor, character) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    @Override
    protected void writeCoins(PrintWriter writer, SFCharacter character){
        writer.println("<h2>Money</h2>");
        writer.println("<div id='coins'>");
        writeLabelValuePair(writer,
                "credits",
                "Credits",
                Character.FORMATTER_THOUSANDS.format(character.getSFData().getCredits()));
        writer.println("</div>");
    }

    @Override
    protected void writeLoad(PrintWriter writer, SFCharacter character){
        CarryingCapacity current = character.getCarryingCapacity();
        String lightClass = "light";
        String mediumClass = "medium";
        String heavyClass = "heavy";
        switch (current) {
        case LIGHT:
            lightClass = "light load";
            break;
        case MEDIUM:
            mediumClass = "medium load";
            break;
        case HEAVY:
            heavyClass = "heavy load";
            break;
        }

        // Write carrying capacity
        writer.println("<h2>Carrying Capacity (In Bulk)</h2>");
        writer.println("<div id='carryingCapacity'>");
        writeLabelValuePair(writer, lightClass, "Unencumbered", character.getCarryingCapacityLight());
        writeLabelValuePair(writer, mediumClass, "Encumbered", character.getCarryingCapacityMedium());
        writeLabelValuePair(writer, heavyClass, "Overburdened", "> " + character.getCarryingCapacityMedium());
        writer.println("</div>");
    }

    /**
     * Write the details of equipped equipment.
     * 
     * @param writer The writer.
     * @param locationsMap The map of equipment by location.
     */
    private void writeEquipmentEquipped(PrintWriter writer, Map<String, Set<AbstractEquipment<?>>> locationsMap){
        writer.println("<div class='location' id='equipped'>");
        writer.println("<h3>" + AbstractEquipment.LOCATION_EQUIPPED + "</h3>");
        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='slot'>Slot</th>");
        writer.println("<th class='name'>Item</th>");
        writer.println("<th class='level'>Level</th>");
        writer.println("<th class='quantity'>Qty</th>");
        writer.println("<th class='weight'>Bulk</th>");
        writer.println("<th class='totalWeight'>Total Bulk</th>");
        writer.println("</tr>");

        // Iterate through items in the location
        Map<Slot, Set<AbstractEquipment<?>>> bySlot =
                EquipmentSet.bySlot(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED));
        for (Entry<Slot, Set<AbstractEquipment<?>>> slot : bySlot.entrySet()) {
            List<AbstractEquipment<?>> items = new ArrayList<AbstractEquipment<?>>(slot.getValue());
            int size = items.size();
            for (int i = 0; i < size; i++) {
                AbstractEquipment<?> item = items.get(i);

                if (i == 0) {
                    writer.println("<tr>");
                    writer.println(
                            "<th class='slot' rowspan='" + size + "'>" + item.getData().getSlot().getName() + "</th>");
                    writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
                    writer.println("<td class='level'>" + formatNullable(item.getData().getLevel()) + "</td>");
                    writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
                    writer.println("<td class='weight'>" + item.getWeight() + "</td>");
                    writer.println(
                            "<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
                    writer.println("</tr>");
                } else {
                    writer.println("<tr>");
                    writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
                    writer.println("<td class='level'>" + formatNullable(item.getData().getLevel()) + "</td>");
                    writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
                    writer.println("<td class='weight'>" + item.getWeight() + "</td>");
                    writer.println(
                            "<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
                    writer.println("</tr>");
                }
            }
        }

        // Output the total weight for the container
        writer.println("<tr class='containerTotalWeight'>");
        writer.println("<th colspan='5'>Total Bulk</th>");
        writer.println("<td class='totalWeight'>"
                + EquipmentSet.getTotalWeight(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED), locationsMap)
                + "</td>");
        writer.println("</tr>");

        // Close the location container
        writer.println("</table>");
        writer.println("</div>");
    }

    /**
     * Write the details of a container of equipment.
     * 
     * @param writer The writer.
     * @param name The name of the container.
     * @param locationsMap The map of equipment by location.
     */
    private void writeEquipmentContainer(PrintWriter writer,
            String name,
            Map<String, Set<AbstractEquipment<?>>> locationsMap){
        writer.println("<div class='location'>");
        writer.println("<h3>" + name + "</h3>");
        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='name'>Item</th>");
        writer.println("<th class='level'>Level</th>");
        writer.println("<th class='quantity'>Qty</th>");
        writer.println("<th class='weight'>Bulk</th>");
        writer.println("<th class='totalWeight'>Total Bulk</th>");
        writer.println("</tr>");

        // Iterate through items in the location
        for (AbstractEquipment<?> item : locationsMap.get(name)) {
            writer.println("<tr>");
            writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
            writer.println("<td class='level'>" + formatNullable(item.getData().getLevel()) + "</td>");
            writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
            writer.println("<td class='weight'>" + item.getWeight() + "</td>");
            writer.println("<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
            writer.println("</tr>");
        }

        // Output the total weight for the container
        writer.println("<tr class='containerTotalWeight'>");
        writer.println("<th colspan='4'>Total Bulk</th>");
        writer.println("<td class='totalWeight'>" + EquipmentSet.getTotalWeight(locationsMap.get(name), locationsMap)
                + "</td>");
        writer.println("</tr>");

        // Close the location container
        writer.println("</table>");
        writer.println("</div>");
    }
}
