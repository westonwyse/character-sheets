/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;
import java.util.Collection;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;

/**
 * Contains the character-specific details of a feat.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class FeatWrapper extends SpecialAbility3E {
	private static final String LOADER_NAME = "feat";

	/**
	 * Determines if the given ID is valid.
	 * 
	 * @param id   The ID
	 * @param root The resource root
	 * @return TRUE is the ID is a known feat; FALSE if not.
	 */
	public static boolean isValid(String id, File root) {
		return isValid(LOADER_NAME, id, root);
	}

	/**
	 * @param root The resource root
	 * @return All valid IDs.
	 */
	public static Collection<String> getKnownKeys(File root) {
		return getKnownKeys(LOADER_NAME, root);
	}

	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public FeatWrapper(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	/**
	 * @param id        The ID
	 * @param rulesRoot The ruleset root directory
	 */
	public FeatWrapper(String id, File rulesRoot) {
		super(id, ResourceFactory.rootForFile(rulesRoot));
	}

	@Override
	public String getLoaderId() {
		return LOADER_NAME;
	}
}
