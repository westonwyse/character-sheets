package com.westpalmetto.gaming.character.swade.model;

import com.westpalmetto.gaming.character.collection.KeyedSet;
import com.westpalmetto.gaming.character.swade.model.Attribute.AttributeName;

/**
 * AttributeSet is and implementation of KeyedSet to contain a character's
 * Attribute objects.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class AttributeSet extends KeyedSet<AttributeName, Attribute> {
	/* (non-Javadoc)
	 * @see com.westaplmetto.gaming.character.collection.KeyedSet#getKey(java.lang.Object)
	 */
	@Override
	public AttributeName getKey(Object target) {
		AttributeName key = null;
		
		if(target instanceof Attribute){
			key = ((Attribute)target).getName();
		} else {
			//TODO: Exceptions
			throw new RuntimeException("Unrecognized object type");
		}
		
		return key;
	}
	
	@Override
	public Attribute get(AttributeName key){
		if(!containsKey(key)){
			add(new Attribute(key, 1));
		}
		
		return super.get(key);
	}
}
