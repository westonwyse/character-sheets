/**
 * ModifierTypePredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;

/**
 * Predicate to match a given ModifierType.  Evaluates to true if an object
 * is a Modifier and matches the specified type.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class ModifierTypePredicate implements Predicate {
	private static Map<ModifierType, ModifierTypePredicate> instances = new HashMap<ModifierType, ModifierTypePredicate>();
	
	private ModifierType type;
	
	private ModifierTypePredicate(ModifierType type){
		this.type = type;
	}
	
	public static ModifierTypePredicate getInstance(ModifierType type){
		//Get from map
		ModifierTypePredicate predicate = instances.get(type);
		
		//Create if not found
		if(predicate == null){
			predicate = new ModifierTypePredicate(type);
			instances.put(type, predicate);
		}
		
		return predicate;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Modifier){
			Modifier Modifier = (Modifier)object;
			matches = (Modifier.getType() == type);
		}
		
		return matches;
	}
}
