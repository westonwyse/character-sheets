package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;

public class StaticClassLoader extends OverridableStaticObjectLoader3E<StaticClass> {
	private static final Map<File,StaticClassLoader> INSTANCES = new HashMap<>();
	
	public static StaticClassLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticClassLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticClassLoader(File root) {
		super("class", StaticClass.class, root);
	}

	@Override
	public StaticClass getClonedObject(StaticClass original) {
		return new StaticClass(original);
	}
}
