/**
 * EquipmentSet
 * 
 * Copyright � 2012 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 30, 2012
 */
package com.westpalmetto.gaming.character.collection;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.Charge;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment.Slot;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Ammunition;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Coins;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Equipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.predicate.ArmorAttackPredicate;
import com.westpalmetto.gaming.character.model.predicate.WeaponCombatTypePredicate;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.equipment.SFArmor;
import com.westpalmetto.gaming.character.util.ClassMatchingPredicate;

/**
 * EquipmentSet is and implementation of KeyedSet to contain a character's
 * Equipment objects.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class EquipmentSet extends BonusProviderSet<AbstractEquipment<?>> {
	@Override
	public String getKey(Object target) {
		String key = null;

		if (target instanceof AbstractEquipment) {
			key = ((AbstractEquipment<?>) target).getDisplayName();
		} else {
			// TODO: Exceptions
			throw new RuntimeException("Unrecognized object type: " + target);
		}

		return key;
	}

	/**
	 * Gets the set of equipment that contains only basic equipment.
	 * 
	 * @return A Set containing only basic equipment.
	 */
	public Set<Equipment> getBasicSet() {
		Set<Equipment> basic = new HashSet<Equipment>();
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(Equipment.class), basic);
		return basic;
	}

	/**
	 * Gets the set of equipment that contains only weapons.
	 * 
	 * @return A Set containing only weapons.
	 */
	public Set<Weapon> getWeaponSet() {
		Set<Weapon> weapons = new KeyedSet<String, Weapon>() {
			@Override
			public String getKey(Object target) {
				String key = null;

				if (target instanceof Weapon) {
					key = ((Weapon) target).getDisplayName();
				} else {
					// TODO: Exceptions
					throw new RuntimeException("Unrecognized object type");
				}

				return key;
			}
		};
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(Weapon.class), weapons);

		// Also account for AC items that can be used to attack
		Set<Armor> armors = new HashSet<Armor>();
		CollectionUtils.select(this, ArmorAttackPredicate.getInstance(), armors);
		for (Armor armor : armors) {
			weapons.add(armor.getAttack());
		}

		return weapons;
	}

	/**
	 * Gets the set of equipment that contains only melee weapons.
	 * 
	 * @return A Set containing only melee weapons.
	 */
	public Set<Weapon> getMeleeWeaponSet() {
		Set<Weapon> weapons = getWeaponSet();
		Set<Weapon> meleeWeapons = new HashSet<Weapon>();
		CollectionUtils.select(weapons, WeaponCombatTypePredicate.MELEE, meleeWeapons);
		return meleeWeapons;
	}

	/**
	 * Gets the set of equipment that contains only ammunition.
	 * 
	 * @return A Set containing only ammunition.
	 */
	public Set<Ammunition> getAmmunitionSet() {
		Set<Ammunition> ammo = new KeyedSet<String, Ammunition>() {
			@Override
			public String getKey(Object target) {
				String key = null;

				if (target instanceof Ammunition) {
					key = ((Ammunition) target).getDisplayName();
				} else {
					// TODO: Exceptions
					throw new RuntimeException("Unrecognized object type");
				}

				return key;
			}
		};
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(Ammunition.class), ammo);

		return ammo;
	}

	/**
	 * Gets the set of equipment that contains only armor.
	 * 
	 * @return A Set containing only armor.
	 */
	public Set<Armor> getArmorSet() {
		Set<Armor> armor = new KeyedSet<String, Armor>() {
			@Override
			public String getKey(Object target) {
				String key = null;

				if (target instanceof Armor) {
					key = ((Armor) target).getDisplayName();
				} else {
					// TODO: Exceptions
					throw new RuntimeException("Unrecognized object type");
				}

				return key;
			}
		};
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(Armor.class), armor);
		return armor;
	}

	/**
	 * Gets the set of equipment that contains only Starfinder armor.
	 * 
	 * @return A Set containing only SF armor.
	 */
	public Set<SFArmor> getSFArmorSet() {
		Set<SFArmor> armor = new KeyedSet<String, SFArmor>() {
			@Override
			public String getKey(Object target) {
				String key = null;

				if (target instanceof SFArmor) {
					key = ((SFArmor) target).getDisplayName();
				} else {
					// TODO: Exceptions
					throw new RuntimeException("Unrecognized object type");
				}

				return key;
			}
		};
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(SFArmor.class), armor);
		return armor;
	}

	/**
	 * Gets the set of equipment that contains only coins.
	 * 
	 * @return A Set containing only coins.
	 */
	public Coins getCoins() {
		Set<Coins> coins = new HashSet<Coins>();
		CollectionUtils.select(this, ClassMatchingPredicate.getInstance(Coins.class), coins);
		return Coins.sum(coins);
	}

	/**
	 * Gets the set of equipment that contains only charged items.
	 * 
	 * @return A Set containing only charged items.
	 */
	public Set<AbstractEquipment<?>> getChargedItems() {
		Set<AbstractEquipment<?>> charged = new HashSet<AbstractEquipment<?>>();
		CollectionUtils.select(this, Charge.PREDICATE, charged);
		return charged;
	}

	/**
	 * Returns a Map of equipment location to equipment carried at that location.
	 * 
	 * @return A Map of location to equipment
	 */
	public Map<String, Set<AbstractEquipment<?>>> getEquipmentByLocation() {
		Map<String, Set<AbstractEquipment<?>>> locations = new TreeMap<String, Set<AbstractEquipment<?>>>(
				COMPARATOR_CONTAINER);

		// Cycle through all equipment
		for (AbstractEquipment<?> item : this) {
			// Use unequipped location if none exists
			String location = item.getLocation();
			if (StringUtils.isBlank(location)) {
				location = AbstractEquipment.LOCATION_UNEQUIPPED;
				item.setLocation(location);
			}

			// Create location set if it doesn't exist
			Set<AbstractEquipment<?>> locationSet = locations.get(location);
			if (locationSet == null) {
				locationSet = new TreeSet<AbstractEquipment<?>>(COMPARATOR_EQUIPMENT);
			}

			// Add item to location set
			locationSet.add(item);

			// Update map
			locations.put(location, locationSet);
		}

		return locations;
	}

	/**
	 * Sorts the given set of equipment by slot.
	 * 
	 * @param equipment The equipment to sort.
	 * @return The given equipment, sorted by slot.
	 */
	public static Map<Slot, Set<AbstractEquipment<?>>> bySlot(Set<AbstractEquipment<?>> equipment) {
		Map<Slot, Set<AbstractEquipment<?>>> map = new TreeMap<Slot, Set<AbstractEquipment<?>>>(COMPARATOR_SLOT);

		for (AbstractEquipment<?> item : equipment) {
			// Ensure the set exists
			if (map.get(item.getData().getSlot()) == null) {
				map.put(item.getData().getSlot(), new TreeSet<AbstractEquipment<?>>(COMPARATOR_EQUIPMENT));
			}

			// Add the item
			map.get(item.getData().getSlot()).add(item);
		}

		return map;
	}

	/**
	 * Calculates the total weight for a given Set of equipment for an equipped
	 * location.
	 * 
	 * @param locationSet  The Set to inspect
	 * @param locationsMap A map of all locations (containers) to their contents
	 * @return The total weight of all items in the given Set.
	 */
	public static BigDecimal getTotalWeight(Set<AbstractEquipment<?>> locationSet,
			Map<String, Set<AbstractEquipment<?>>> locationsMap) {
		BigDecimal weight = new BigDecimal(0);

		if (locationSet != null) {
			for (AbstractEquipment<?> item : locationSet) {
				weight = weight.add(getTotalWeight(item, locationsMap));
			}
		}

		return weight;
	}

	/**
	 * Calculates the total weight of a given item and any possible contents of the
	 * item. Determines if the requested item contains other items and returns a
	 * total of its weight and its contents weight if so; returns the items weight
	 * alone if it does not contain other items.
	 * 
	 * @param item         The requested item
	 * @param locationsMap A map of all locations (containers) to their contents
	 * @return The total weight of the requested item and its contents.
	 */
	public static BigDecimal getTotalWeight(AbstractEquipment<?> item,
			Map<String, Set<AbstractEquipment<?>>> locationsMap) {
		BigDecimal weight = item.getTotalWeight();
		Set<AbstractEquipment<?>> locationSet = locationsMap.get(item.getName());

		if (locationSet != null) {
			weight = weight.add(getTotalWeight(locationSet, locationsMap));
		}

		return weight;
	}

	private static class EquipmentComparator implements Comparator<AbstractEquipment<?>> {
		@Override
		public int compare(AbstractEquipment<?> item1, AbstractEquipment<?> item2) {
			return item1.getDisplayName().compareToIgnoreCase(item2.getDisplayName());
		}
	}

	private static final EquipmentComparator COMPARATOR_EQUIPMENT = new EquipmentComparator();

	protected static class SlotComparator implements Comparator<Slot> {
		@Override
		public int compare(Slot slot1, Slot slot2) {
			int comparison;

			if (Slot.SLOTLESS.equals(slot1)) {
				if (Slot.SLOTLESS.equals(slot2)) {
					comparison = 0;
				} else {
					comparison = 1;
				}
			} else if (Slot.SLOTLESS.equals(slot2)) {
				comparison = -1;
			} else {
				comparison = slot1.getName().compareToIgnoreCase(slot2.getName());
			}

			return comparison;
		}
	}

	protected static final SlotComparator COMPARATOR_SLOT = new SlotComparator();

	private static class ContainerComparator implements Comparator<String> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(String name1, String name2) {
			int comparison;

			if (AbstractEquipment.LOCATION_EQUIPPED.equals(name1)) {
				if (AbstractEquipment.LOCATION_EQUIPPED.equals(name2)) {
					comparison = 0;
				} else {
					comparison = -1;
				}
			} else if (AbstractEquipment.LOCATION_UNEQUIPPED.equals(name1)) {
				if (AbstractEquipment.LOCATION_EQUIPPED.equals(name2)) {
					comparison = 1;
				} else if (AbstractEquipment.LOCATION_UNEQUIPPED.equals(name2)) {
					comparison = 0;
				} else {
					comparison = -1;
				}
			} else if ((AbstractEquipment.LOCATION_EQUIPPED.equals(name2))
					|| (Equipment.LOCATION_UNEQUIPPED.equals(name2))) {
				comparison = 1;
			} else {
				comparison = name1.compareToIgnoreCase(name2);
			}

			return comparison;
		}
	}

	private static final ContainerComparator COMPARATOR_CONTAINER = new ContainerComparator();
}
