/**
 * Weapon.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment;

import java.util.Collection;
import java.util.HashSet;

import com.westpalmetto.gaming.character.model.Damage;

/**
 * Class to contain stats for a weapon.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class StaticWeapon extends StaticEquipment {
	private Damage damage;
	private String threatMultiplier = "x2";
	private int threatRange = 20;
	private String damageType;
	private String rangeIncrement;
	private Proficiency proficiency;
	private CombatType combatType;
	private Heft heft;
	private boolean weaponFinesse;
	private Collection<String> weaponGroups;

	public StaticWeapon() {
	}

	public StaticWeapon(StaticWeapon original) {
		super(original);
		setDamage(original.getDamage());
		setThreatMultiplier(original.getThreatMultiplier());
		setThreatRange(original.getThreatRange());
		setDamageType(original.getDamageType());
		setRangeIncrement(original.getRangeIncrement());
		setProficiency(original.getProficiency());
		setCombatType(original.getCombatType());
		setHeft(original.getHeft());
		setWeaponFinesse(original.isWeaponFinesse());
		setWeaponGroups(original.getWeaponGroups());
	}

	/**
	 * @return the damage
	 */
	public Damage getDamage() {
		return damage;
	}

	/**
	 * @param damage the damage to set
	 */
	public void setDamage(Damage damage) {
		this.damage = damage;
	}

	/**
	 * @return the threatMultiplier
	 */
	public String getThreatMultiplier() {
		return threatMultiplier;
	}

	/**
	 * @param threatMultiplier the threatMultiplier to set
	 */
	public void setThreatMultiplier(String threatMultiplier) {
		this.threatMultiplier = threatMultiplier;
	}

	/**
	 * @return the threatRange
	 */
	public int getThreatRange() {
		return threatRange;
	}

	/**
	 * @param threatRange the threatRange to set
	 */
	public void setThreatRange(int threatRange) {
		this.threatRange = threatRange;
	}

	/**
	 * @return the damageType
	 */
	public String getDamageType() {
		return damageType;
	}

	/**
	 * @param damageType the damageType to set
	 */
	public void setDamageType(String damageType) {
		this.damageType = damageType;
	}

	/**
	 * @return the rangeIncrement
	 */
	public String getRangeIncrement() {
		return rangeIncrement;
	}

	/**
	 * @param rangeIncrement the rangeIncrement to set
	 */
	public void setRangeIncrement(String rangeIncrement) {
		this.rangeIncrement = rangeIncrement;
	}

	/**
	 * @return the proficiency
	 */
	public Proficiency getProficiency() {
		return proficiency;
	}

	/**
	 * @param proficiency the proficiency to set
	 */
	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}

	/**
	 * @return the combatType
	 */
	public CombatType getCombatType() {
		return combatType;
	}

	/**
	 * @param combatType the combatType to set
	 */
	public void setCombatType(CombatType combatType) {
		this.combatType = combatType;
	}

	/**
	 * @return the heft
	 */
	public Heft getHeft() {
		return heft;
	}

	/**
	 * @param heft the heft to set
	 */
	public void setHeft(Heft heft) {
		this.heft = heft;
	}

	/**
	 * @return the weaponFinesse
	 */
	public boolean isWeaponFinesse() {
		return weaponFinesse;
	}

	/**
	 * @param weaponFinesse the weaponFinesse to set
	 */
	public void setWeaponFinesse(boolean weaponFinesse) {
		this.weaponFinesse = weaponFinesse;
	}

	/**
	 * @return the weaponGroup
	 */
	public Collection<String> getWeaponGroups() {
		if (weaponGroups == null) {
			setWeaponGroups(new HashSet<String>());
		}
		return weaponGroups;
	}

	/**
	 * @param weaponGroups the weaponGroup to set
	 */
	public void setWeaponGroups(Collection<String> weaponGroups) {
		this.weaponGroups = weaponGroups;
	}

	@Override
	public Slot getSlot() {
		return Slot.SLOTLESS;
	}

	@Override
	public WeightModifier getWeightModifier() {
		return WeightModifier.ARMOR_WEAPON;
	}

	public enum Proficiency {
		SIMPLE("Simple"), MARTIAL("Martial"), EXOTIC("Exotic"), NATURAL("Natural"), BASIC("Basic"),
		ADVANCED("Advanced"), SMALL_ARMS("Small Arms"), LONGARMS("Longarms");

		private String displayName;

		private Proficiency(String displayName) {
			this.displayName = displayName;
		}

		public String getDisplayName() {
			return this.displayName;
		}
	}

	public enum CombatType {
		MELEE, RANGED,;
	}

	public enum Heft {
		UNARMED("U"), LIGHT("L"), ONEHAND("1H"), TWOHAND("2H"), PRIMARY("Primary"), SECONDARY("Secondary"),;

		private String abbreviation;

		private Heft(String abbreviation) {
			this.abbreviation = abbreviation;
		}

		public String getAbbreviation() {
			return this.abbreviation;
		}
	}
}
