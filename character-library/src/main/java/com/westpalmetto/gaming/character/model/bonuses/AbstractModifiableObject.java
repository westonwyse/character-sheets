package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.util.resources.model.Model;

/**
 * Model for modifiable bonus details.
 */
public abstract class AbstractModifiableObject extends Model implements ModifiableObject{
	private int applies;
	private ModifierType appliesType;
	private String appliesKey;
	private Set<String> itemKeys = new HashSet<String>();
	private boolean requireItemKey;

	/**
	 * No-argument constructor.
	 */
	public AbstractModifiableObject(){
	}
	
	/**
	 * Copy constructor.
	 * @param bonus The Bonus object to copy
	 */
	public AbstractModifiableObject(AbstractModifiableObject bonus) {
		setApplies(bonus.getApplies());
		setAppliesType(bonus.getAppliesType());
		setAppliesKey(bonus.getAppliesKey());
		setItemKeys(new HashSet<String>(bonus.getItemKeys()));
		setRequireItemKey(bonus.isRequireItemKey());
	}

	@Override
	public int getApplies() {
		return applies;
	}

	/**
	 * @param applies the applies to set
	 */
	public void setApplies(int applies) {
		this.applies = applies;
	}

	@Override
	public ModifierType getAppliesType() {
		return appliesType;
	}

	/**
	 * @param appliesType the appliesType to set
	 */
	public void setAppliesType(ModifierType appliesType) {
		this.appliesType = appliesType;
	}

	@Override
	public String getAppliesKey() {
		return appliesKey;
	}

	/**
	 * @param appliesKey the appliesKey to set
	 */
	public void setAppliesKey(String appliesKey) {
		this.appliesKey = appliesKey;
	}

	@Override
	public Set<String> getItemKeys() {
		if(itemKeys == null){
			setItemKeys(new HashSet<String>());
		}
		return itemKeys;
	}

	@Override
	public void setItemKeys(Collection<String> itemKeys) {
		this.itemKeys = new HashSet<>(itemKeys);
	}

	@Override
	public boolean isRequireItemKey() {
		return requireItemKey;
	}

	/**
	 * @param requireItemKey the requireItemKey to set
	 */
	public void setRequireItemKey(boolean requireItemKey) {
		this.requireItemKey = requireItemKey;
	}
	
	@Override
	public int compareTo(NamedObject obj){
		return getName().compareTo(obj.getName());
	}
}
