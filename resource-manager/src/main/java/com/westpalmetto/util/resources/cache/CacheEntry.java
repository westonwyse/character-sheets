package com.westpalmetto.util.resources.cache;


/**
 * A cache entry.
 *
 * @param <K> The type of the key
 * @param <V> The type of the value
 */
public class CacheEntry <K, V> {
	private final K key;
	private V value;
	private long lastUpdated;
	
	public CacheEntry(K id, V value) {
		this.key = id;
		this.value = value;
		this.lastUpdated = System.currentTimeMillis();
	}
	
	/**
	 * @return the key
	 */
	public K getKey() {
		return key;
	}
	/**
	 * @return the value
	 */
	public V getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(V value) {
		this.value = value;
		this.lastUpdated = System.currentTimeMillis();
	}
	/**
	 * @return the lastUpdated
	 */
	public long getLastUpdated() {
		return lastUpdated;
	}
}
