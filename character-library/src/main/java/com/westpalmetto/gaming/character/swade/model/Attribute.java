package com.westpalmetto.gaming.character.swade.model;

import com.westpalmetto.util.resources.model.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Attribute extends Model {
	private AttributeName name;
	private int ranks;
	
	public String getStep() {
		return Steps.getStep(getRanks());
	}

	/**
	 * An enum for valid attribute names
	 */
	@RequiredArgsConstructor
	@Getter
	public enum AttributeName{
		AGILITY("Agility"),
		SMARTS("Smarts"),
		SPIRIT("Spirit"),
		STRENGTH("Strength"),
		VIGOR("Vigor"),
		;
		
		private final String name;
	}
}
