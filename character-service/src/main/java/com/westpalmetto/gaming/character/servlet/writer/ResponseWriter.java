/**
 * Writer.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

/**
 * Defines an interface by which a requested object will be written to the
 * output stream.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface ResponseWriter {
	/**
	 * For a given path, gets the File object that should be read and written
	 * to the response.
	 * @param path The path of the request.
	 * @return The file to be read and written.
	 */
	public InputStream getFileStream(String path);
	
	/**
	 * For a given path, determines the MIME type of the response.
	 * @param path The path of the request.
	 * @return The appropriate MIME type of the response.
	 */
	public String getMimeType(String path);
	
	/**
	 * For a given file, handles the output to the response.
	 * @param path The path for which to write
	 * @param response The response for the current request
	 * @throws IOException
	 */
	public void write(String path, HttpServletResponse response) throws IOException;
}
