package com.westpalmetto.gaming.character.swade.model;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.westpalmetto.gaming.character.bonuses.ChosenBonus;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.model.CharacterModel;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.ConceptWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.GearWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.HindranceWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.PowerWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.RaceWrapper;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * The model for a SWADE character.
 */
@Getter
@Setter
@Slf4j
public class SwadeModel extends CharacterModel{
    private ConceptWrapper concept;
    private RaceWrapper race;
    private ChosenBonus attributePoints;
    private BonusProviderSet<HindranceWrapper> hindrances;
    private ChosenBonus hindrancePoints;
    private ChosenBonus skillPoints;
    private ChosenBonus bonusEdges;
    private EquipmentSet equipment;
    private List<ChosenBonus> advances;
    private Set<PowerWrapper> powers;
    private Set<GearWrapper> gear;

    public SwadeModel(File resourceRoot){
        super(resourceRoot);
    }

    public EquipmentSet getEquipment(){
        if (equipment == null) {
            setEquipment(new EquipmentSet());
        }
        return equipment;
    }

    public BonusProviderSet<HindranceWrapper> getHindrances(Collection<UnmodifiableBonus> buyoffs){
        // Disable bought-off hindrances
        BonusProviderSet<HindranceWrapper> hindrances = getHindrances();
        Set<String> names = new HashSet<String>();
        for (UnmodifiableBonus buyoff : buyoffs) {
            names.addAll(buyoff.getItemKeys());
        }
        for (String name : names) {
            HindranceWrapper hindrance = hindrances.get(name);
            if (hindrance != null) {
                hindrance.setBuyoff(true);
            } else {
                log.warn("Hindrance not found: {}", name);
            }
        }

        return hindrances;
    }
}
