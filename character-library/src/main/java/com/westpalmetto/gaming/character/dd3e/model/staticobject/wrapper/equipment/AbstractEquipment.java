/**
 * Equipment.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 3, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.StaticSpecialAbilityLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.Charge;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableObject;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.loader.StaticSpecialAbilityLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.EquipmentHelper;
import com.westpalmetto.gaming.character.util.ObjectHelper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to represent a single item in a character's equipment.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class AbstractEquipment<V extends StaticEquipment> extends AbstractBonusProviderWrapper<V>
		implements ModifiableObject {
	public static final String LOCATION_EQUIPPED = "Equipped";//For equipment carried but not in a container
	public static final String LOCATION_UNEQUIPPED = "Unequipped";//For equipment not carried and not in a container
	
	private StaticSpecialAbilityLoader abilityLoader;
	
	private String name;
	private int quantity;
	private Size size;
	private String location;
	private Hand hand;
	private BonusProviderSet<StaticSpecialAbility> abilities;
	private Collection<ModifiableBonus> bonuses;
	private Set<String> itemKeys;
	private NamedObjectSet<Charge> charges;
	
	private BonusProviderSet<StaticSpecialAbility> combinedAbilities;
	
	public static enum Hand{
		PRIMARY("Primary hand"),
		OFF("Off-hand"),
		;
		
		private final String value;
		
		private Hand(String value){
			this.value = value;
		}

		/**
		 * @return the value
		 */
		public String getValue(){
			return value;
		}
	}
	
	public AbstractEquipment(String id){
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}
	
	public DataLoader<StaticSpecialAbility> getNamedAbilityLoader() {
		if(abilityLoader == null){
			abilityLoader = StaticSpecialAbilityLoader3E.getInstance("magicItemAbility", getRoot());
		}
		return abilityLoader;
	}

	@Override
	public String getName() {
		if(StringUtils.isBlank(name)){
			setName(super.getName());
		}
		
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return The name that should be displayed for the object.
	 */
	public String getDisplayName() {
		return ObjectHelper.getDisplayName(this);
	}
	
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the size
	 */
	public Size getSize() {
		if(size == null){
			setSize(Size.M);
		}
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(Size size) {
		this.size = size;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	
	/**
	 * @return the hand
	 */
	public Hand getHand(){
		return hand;
	}

	/**
	 * @param hand the hand to set
	 */
	public void setHand(Hand hand){
		this.hand = hand;
	}
	
	/**
	 * @return The weight of the item, in pounds.
	 */
	public BigDecimal getWeight(){
		BigDecimal weight = null;
		
		if(getData().getWeight() != null){
			weight = EquipmentHelper.calculateWeight(getData().getWeight(), getData().getWeightModifier(), getSize());
		}
		
		return weight;
	}

	/**
	 * Gets the total weight of all items of this type (weight * quantity).
	 * @return The total weight of all items.
	 */
	public BigDecimal getTotalWeight(){
		BigDecimal weight = null;
		
		if(getData().getWeight() != null){
			weight = getWeight().multiply(new BigDecimal(getQuantity()));
		}
		
		return weight;
	}
	
	@Override
	public V getData(){
		V data = super.getData();
		
		//Verify the static item exists
		if(data == null){
			//TODO: Exceptions
			throw new RuntimeException("Could not load item for " + getId() + " from " + getDataLoader().getCache().keySet());
		}
		
		//Duplicate the static element & add any item keys
		V duplicate = getDataLoader().getClonedObject(data);
		duplicate.getItemKeys().addAll(getItemKeys());
		
		return duplicate;
	}

	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		
		//Name the resulting objects
		BonusProviderSet<StaticSpecialAbility> abilities = getStaticAbilities();
		Collection<ModifiableBonus> bonuses = abilities.getBonuses(modifiers);
		bonuses = BonusHelper.nameBonuses(bonuses, getName());
		bonuses.addAll(getData().getBonuses(modifiers));
		bonuses.addAll(BonusHelper.filterByApplies(this, this.bonuses, modifiers));
		bonuses.addAll(getBonusAbilities(bonuses).getBonuses(modifiers));
		
		return bonuses;
	}

	public void setBonuses(Collection<ModifiableBonus> bonuses){
		this.bonuses = bonuses;
	}
	
	public BonusProviderSet<StaticSpecialAbility> getAbilities(Collection<ModifiableBonus> bonuses) {
		BonusProviderSet<StaticSpecialAbility> abilities = new BonusProviderSet<StaticSpecialAbility>();
		abilities.addAll(getStaticAbilities());
		abilities.addAll(getBonusAbilities(bonuses));
		return abilities;
	}
	
	protected BonusProviderSet<StaticSpecialAbility> getStaticAbilities() {
		if(combinedAbilities == null){
			//Collect all the abilities
			combinedAbilities = new BonusProviderSet<StaticSpecialAbility>();
			if(abilities != null){
				combinedAbilities.addAll(abilities);
			}
			combinedAbilities.addAll(getData().getAbilities());
			
			//Replace any named abilities that should be data-loaded.
			combinedAbilities = loadNamedAbilities(combinedAbilities);
		}
		
		return combinedAbilities;
	}
	
	protected void setAbilities(BonusProviderSet<StaticSpecialAbility> abilities){
		this.abilities = abilities;
		this.combinedAbilities = null;
	}
	
	public BonusProviderSet<StaticSpecialAbility> getBonusAbilities(Collection<ModifiableBonus> bonuses) {
		BonusProviderSet<StaticSpecialAbility> abilities = new BonusProviderSet<StaticSpecialAbility>();
		
		for(ModifiableBonus bonus: bonuses){
			if(BonusType.ITEM_ABILITY.equals(bonus.getType())){
				String name = bonus.getValueType();
				
				if (StringUtils.isNotBlank(name) && (getNamedAbilityLoader().contains(name))) {
					StaticSpecialAbility staticAbility = new StaticSpecialAbility(getNamedAbilityLoader().getData(name));
					staticAbility.getItemKeys().addAll(bonus.getItemKeys());
					abilities.add(staticAbility);
				}
			}
		}
		
		return abilities;
	}
	
	protected BonusProviderSet<StaticSpecialAbility> loadNamedAbilities(
			BonusProviderSet<StaticSpecialAbility> originals){
		BonusProviderSet<StaticSpecialAbility> abilities = new BonusProviderSet<StaticSpecialAbility>();
		
		//Inspect for named bonuses
		for (StaticSpecialAbility ability : originals) {
			if (StringUtils.isNotBlank(ability.getName()) && (getNamedAbilityLoader().contains(ability.getName()))) {
				StaticSpecialAbility staticAbility = new StaticSpecialAbility(getNamedAbilityLoader().getData(ability.getName()));
				staticAbility.getItemKeys().addAll(ability.getItemKeys());
				abilities.add(staticAbility);
			} else {
				StaticSpecialAbility duplicate = new StaticSpecialAbility(ability);
				abilities.add(duplicate);
			}
		}
		
		return abilities;
	}
	
	public static enum Size {
		T,
		S,
		M,
		L;
	}

	@Override
	public Set<String> getItemKeys() {
		if(itemKeys == null){
			setItemKeys(new HashSet<String>());
		}
		return itemKeys;
	}

	@Override
	public void setItemKeys(Collection<String> itemKeys) {
		this.itemKeys = new HashSet<>(itemKeys);
	}

	@Override
	public boolean isRequireItemKey() {
		return false;
	}

	@Override
	public int getApplies() {
		return 0;
	}

	@Override
	public ModifierType getAppliesType() {
		return null;
	}

	@Override
	public String getAppliesKey() {
		return null;
	}

	/**
	 * @return the charges
	 */
	public NamedObjectSet<Charge> getCharges() {
		if(charges == null){
			setCharges(new NamedObjectSet<Charge>());
		}
		return charges;
	}

	/**
	 * @param charges the charges to set
	 */
	public void setCharges(NamedObjectSet<Charge> charges) {
		this.charges = charges;
	}

	/**
	 * Overlay the specified charges on top of the standard charges.
	 * @return the overlaid charges
	 */
	public NamedObjectSet<Charge> getSpecifiedCharges() {
		NamedObjectSet<Charge> specified = new NamedObjectSet<Charge>();
		specified.addAll(getData().getCharges());
		specified.addAll(getCharges());
		return specified;
	}
}
