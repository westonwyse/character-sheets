package com.westpalmetto.gaming.character.bonuses;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.model.bonuses.Bonus;

/**
 * Predicate to match a Bonus with a valueDie.  Evaluates to true if an object
 * is a Bonus and has a value of bonusDie.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class BonusDiePredicate implements Predicate {
	private static BonusDiePredicate instance = new BonusDiePredicate();
	
	private BonusDiePredicate(){
	}
	
	public static BonusDiePredicate getInstance(){
		return instance;
	}

	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Bonus){
			Bonus bonus = (Bonus)object;
			matches = (bonus.getValueDie() != null);
		}
		
		return matches;
	}
}
