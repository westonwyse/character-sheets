/**
 * ArmorAttackPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.predicate;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;


/**
 * Predicate to match AC items with attacks.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class ArmorAttackPredicate implements Predicate {
	private static ArmorAttackPredicate instance;
	
	private ArmorAttackPredicate(){
		//Making constructor private - singleton instance
	}
	
	public static ArmorAttackPredicate getInstance(){
		if(instance == null){
			instance = new ArmorAttackPredicate();
		}
		return instance;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Armor){
			Armor armor = (Armor)object;
			matches = (armor.getAttack() != null);
		}
		
		return matches;
	}
}
