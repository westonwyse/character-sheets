/**
 * PSRDCharacter.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.psrd;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.psrd.model.PFSCharacterModel;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

/**
 * Extension of PSRDCharacter for Pathfinder Society characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class PFSCharacter extends PSRDCharacter {
	/**
	 * Creates a new PFS character.
	 * 
	 * @param root The resource root
	 */
	public PFSCharacter() {
		this(new PFSCharacterModel(ResourceFactory.Ruleset.PSRD.getRoot()));
	}

	/**
	 * Creates a new PFSCharacter from the given character model.
	 * 
	 * @param model The character model.
	 */
	public PFSCharacter(PFSCharacterModel model) {
		super(model);
	}

	/**
	 * @param json The JSON from which to initialize this character.
	 */
	public PFSCharacter(JsonObject json) {
		this();
		initialize(json);
	}

	@Override
	public void initialize(JsonObject json) {
		setData(GsonUtils.readModel(json, PFSCharacterModel.class));
	}

	public PFSCharacterModel getPfsData() {
		return (PFSCharacterModel) getData();
	}

}
