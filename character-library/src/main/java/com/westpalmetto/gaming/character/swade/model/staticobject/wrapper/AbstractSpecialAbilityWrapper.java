package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;

public abstract class AbstractSpecialAbilityWrapper<V extends BonusProvider & StaticObject & SpecialAbility> extends
        AbstractBonusProviderWrapper<V> implements
        SpecialAbility{

    public AbstractSpecialAbilityWrapper(String id){
        super(id, ResourceFactory.Ruleset.SWADE.toString());
    }

    @Override
    public Set<String> getItemKeys(){
        return getData().getItemKeys();
    }

    @Override
    public void setItemKeys(Collection<String> keys){
        // Ignore from wrapper
    }

    @Override
    public boolean isRequireItemKey(){
        return getData().isRequireItemKey();
    }

    @Override
    public String getDescription(){
        return getData().getDescription();
    }

    @Override
    public void setDescription(String description){
        // Ignore from wrapper
    }

    @Override
    public BigDecimal getCost(){
        return getData().getCost();
    }

    @Override
    public String getRuleset(){
        return ResourceFactory.Ruleset.SWADE.name();
    }
}
