/**
 * Equipment.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 3, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment;

import java.math.BigDecimal;

import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class to represent a single item in a character's equipment.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@NoArgsConstructor
@Getter
@Setter
public class StaticEquipment extends SimpleOverridableAbilityProvider {
	private Integer level;
	private BigDecimal weight;
	private WeightModifier weightModifier;
	private Slot slot;
	private NamedObjectSet<Charge> charges;

	public StaticEquipment(StaticEquipment original) {
		super(original);
		setLevel(original.getLevel());
		setWeight(original.getWeight());
		setWeightModifier(original.getWeightModifier());
		setSlot(original.getSlot());
		setCharges(original.getCharges());
	}

	/**
	 * Gets the weight of a single item of this type.
	 * 
	 * @return the weight
	 */
	public BigDecimal getWeight() {
		if (weight == null) {
			setWeight(BigDecimal.ZERO);
		}
		return weight;
	}

	/**
	 * @return the weightModifier
	 */
	public WeightModifier getWeightModifier() {
		if (weightModifier == null) {
			setWeightModifier(WeightModifier.NONE);
		}
		return weightModifier;
	}

	/**
	 * @return the slot
	 */
	public Slot getSlot() {
		if (slot == null) {
			setSlot(Slot.SLOTLESS);
		}
		return slot;
	}

	/**
	 * @return the charges
	 */
	public NamedObjectSet<Charge> getCharges() {
		if (charges == null) {
			setCharges(new NamedObjectSet<Charge>());
		}
		return charges;
	}

	public static enum WeightModifier {
		NONE, CLOTHING_CONTAINER, ARMOR_WEAPON;
	}

	public static enum Slot {
		ARMOR("Armor"), BELT("Belt"), BODY("Body"), CHEST("Chest"), EYES("Eyes"), FEET("Feet"), HANDS("Hands"),
		HEAD("Head"), HEADBAND("Headband"), NECK("Neck"), RING("Ring", 2), SHIELD("Shield"), SHOULDERS("Shoulders"),
		WRISTS("Wrists"), SLOTLESS("Slotless", -1);

		private String name;
		private int count;

		private Slot(String name) {
			this(name, 1);
		}

		private Slot(String name, int count) {
			this.name = name;
			this.count = count;
		}

		/**
		 * @return The name of this slot.
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return The number of items of this slot type that a character may equip.
		 */
		public int getCount() {
			return count;
		}
	}
}
