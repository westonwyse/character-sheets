package com.westpalmetto.gaming.character.srd35.model;

import java.io.File;

import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Advancement;
import com.westpalmetto.gaming.character.dd3e.model.Attribute;
import com.westpalmetto.gaming.character.dd3e.model.AttributeSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;

/**
 * A character class for SRD 3.5 characters.
 */
public class SRD35CharacterModel extends DD3EModel {
	
	public SRD35CharacterModel(File resourceRoot){
		super(resourceRoot);
		
		//Setup attributes
		AttributeSet attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, 10));
		attributes.add(new Attribute(AttributeName.DEX, 10));
		attributes.add(new Attribute(AttributeName.CON, 10));
		attributes.add(new Attribute(AttributeName.INT, 10));
		attributes.add(new Attribute(AttributeName.WIS, 10));
		attributes.add(new Attribute(AttributeName.CHA, 10));
		setAttributes(attributes);
		
		//Setup Skills
		NamedObjectSet<DD3ESkill> skills = new NamedObjectSet<DD3ESkill>();
		skills.add(new DD3ESkill("Appraise", AttributeName.INT));
		skills.add(new DD3ESkill("Balance", AttributeName.DEX));
		skills.add(new DD3ESkill("Bluff", AttributeName.CHA));
		skills.add(new DD3ESkill("Climb", AttributeName.STR));
		skills.add(new DD3ESkill("Concentration", AttributeName.CON));
		skills.add(new DD3ESkill("Craft", AttributeName.INT));
		skills.add(new DD3ESkill("Decipher Script", AttributeName.INT));
		skills.add(new DD3ESkill("Diplomacy", AttributeName.INT));
		skills.add(new DD3ESkill("Disable Device", AttributeName.DEX));
		skills.add(new DD3ESkill("Disguise", AttributeName.CHA));
		skills.add(new DD3ESkill("Escape Artist", AttributeName.DEX));
		skills.add(new DD3ESkill("Forgery", AttributeName.INT));
		skills.add(new DD3ESkill("Gather Information", AttributeName.CHA));
		skills.add(new DD3ESkill("Handle Animal", AttributeName.CHA));
		skills.add(new DD3ESkill("Heal", AttributeName.WIS));
		skills.add(new DD3ESkill("Hide", AttributeName.DEX));
		skills.add(new DD3ESkill("Intimidate", AttributeName.CHA));
		skills.add(new DD3ESkill("Jump", AttributeName.STR));
		skills.add(new DD3ESkill("Knowledge", AttributeName.INT));
		skills.add(new DD3ESkill("Listen", AttributeName.WIS));
		skills.add(new DD3ESkill("Move Silently", AttributeName.DEX));
		skills.add(new DD3ESkill("Open Lock", AttributeName.DEX));
		skills.add(new DD3ESkill("Perform", AttributeName.CHA));
		skills.add(new DD3ESkill("Profession", AttributeName.WIS));
		skills.add(new DD3ESkill("Ride", AttributeName.DEX));
		skills.add(new DD3ESkill("Search", AttributeName.INT));
		skills.add(new DD3ESkill("Sense Motive", AttributeName.WIS));
		skills.add(new DD3ESkill("Sleight of Hand", AttributeName.DEX));
		skills.add(new DD3ESkill("Spellcraft", AttributeName.INT));
		skills.add(new DD3ESkill("Spot", AttributeName.WIS));
		skills.add(new DD3ESkill("Survival", AttributeName.WIS));
		skills.add(new DD3ESkill("Swim", AttributeName.STR));
		skills.add(new DD3ESkill("Tumble", AttributeName.DEX));
		skills.add(new DD3ESkill("Use Magic Device", AttributeName.CHA));
		skills.add(new DD3ESkill("Use Rope", AttributeName.DEX));
		setSkills(skills);
	}

	@Override
	public Advancement getAdvancement() {
		// TODO Auto-generated method stub
		return null;
	}
}
