package com.westpalmetto.gaming.character.dd3e.model;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Advancement;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.BoonWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FamiliarWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FeatWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.QualityWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.RaceWrapper;
import com.westpalmetto.gaming.character.model.CharacterModel;

/**
 * CharacterModel contains all of the data that defines a given character.
 */
public abstract class DD3EModel extends CharacterModel {
	private String deity;
	private Alignment alignment;
	private RaceWrapper race;
	private BonusProviderSet<ClassWrapper> classes;
	private FamiliarWrapper familiar;
	private HitPointType hpType;
	private String favoredClass;
	private AttributeSet attributes;
	private AttributeSet attributeIncreases;
	private AttributeSet inherentBonuses;
	private NamedObjectSet<DD3ESkill> skills;
	private Set<String> languages;
	private BonusProviderSet<FeatWrapper> feats;
	private EquipmentSet equipment;
	private BigDecimal experience;
	private Set<String> notes;
	private BonusProviderSet<BoonWrapper> boons;
	private BonusProviderSet<QualityWrapper> qualities;
	
	public DD3EModel(File resourceRoot) {
		super(resourceRoot);
	}
	/**
	 * @return the deity
	 */
	public String getDeity() {
		return deity;
	}
	/**
	 * @param deity the deity to set
	 */
	public void setDeity(String deity) {
		this.deity = deity;
	}
	/**
	 * @return the alignment
	 */
	public Alignment getAlignment() {
		return alignment;
	}
	/**
	 * @param alignment the alignment to set
	 */
	public void setAlignment(Alignment alignment) {
		this.alignment = alignment;
	}
	/**
	 * @return the race
	 */
	public RaceWrapper getRace() {
		return race;
	}
	/**
	 * @param race the race to set
	 */
	public void setRace(RaceWrapper race) {
		this.race = race;
	}
	/**
	 * @return the languages
	 */
	public Set<String> getLanguages() {
		if(languages == null){
			setLanguages(new HashSet<String>());
		}
		return languages;
	}
	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(Set<String> languages) {
		this.languages = languages;
	}
	/**
	 * @return the feats
	 */
	public BonusProviderSet<FeatWrapper> getFeats() {
		if (feats == null){
			feats = new BonusProviderSet<FeatWrapper>();
		}
		return feats;
	}
	/**
	 * @param feats the feats to set
	 */
	public void setFeats(BonusProviderSet<FeatWrapper> feats) {
		this.feats = feats;
	}
	/**
	 * @return the classes
	 */
	public BonusProviderSet<ClassWrapper> getClasses() {
		if (classes == null){
			classes = new BonusProviderSet<ClassWrapper>();
		}
		return classes;
	}
	/**
	 * @param classes the classes to set
	 */
	public void setClasses(BonusProviderSet<ClassWrapper> classes) {
		this.classes = classes;
	}
	/**
	 * @return the familiar
	 */
	public FamiliarWrapper getFamiliar() {
		return familiar;
	}
	/**
	 * @param familiar the familiar to set
	 */
	public void setFamiliar(FamiliarWrapper familiar) {
		this.familiar = familiar;
	}
	/**
	 * @return the hpType
	 */
	public HitPointType getHpType() {
		return hpType;
	}
	/**
	 * @param hpType the hpType to set
	 */
	public void setHpType(HitPointType hpType) {
		this.hpType = hpType;
	}
	/**
	 * @return the favoredClass
	 */
	public String getFavoredClass() {
		return favoredClass;
	}
	/**
	 * @param favoredClass the favoredClass to set
	 */
	public void setFavoredClass(String favoredClass) {
		this.favoredClass = favoredClass;
	}
	/**
	 * @return the attributes
	 */
	public AttributeSet getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(AttributeSet attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return the attributeIncreases
	 */
	public AttributeSet getAttributeIncreases() {
		if(attributeIncreases == null){
			setAttributeIncreases(new AttributeSet());
		}
		return attributeIncreases;
	}
	/**
	 * @param attributeIncreases the attributeIncreases to set
	 */
	public void setAttributeIncreases(AttributeSet attributeIncreases) {
		this.attributeIncreases = attributeIncreases;
	}
	/**
	 * @return the inherentBonuses
	 */
	public AttributeSet getInherentBonuses() {
		if(inherentBonuses == null){
			setInherentBonuses(new AttributeSet());
		}
		return inherentBonuses;
	}
	/**
	 * @param inherentBonuses the inherentBonuses to set
	 */
	public void setInherentBonuses(AttributeSet inherentBonuses) {
		this.inherentBonuses = inherentBonuses;
	}
	/**
	 * @return the skills
	 */
	public NamedObjectSet<DD3ESkill> getSkills() {
		if(skills == null){
			setSkills(new NamedObjectSet<DD3ESkill>());
		}
		return skills;
	}
	/**
	 * @param skills the skills to set
	 */
	public void setSkills(NamedObjectSet<DD3ESkill> skills) {
		this.skills = skills;
	}
	/**
	 * @return the equipment
	 */
	public EquipmentSet getEquipment(){
		if(equipment == null){
			setEquipment(new EquipmentSet());
		}
		return equipment;
	}
	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(EquipmentSet equipment) {
		this.equipment = equipment;
	}
	/**
	 * @return the experience
	 */
	public BigDecimal getExperience() {
		return experience;
	}
	/**
	 * @param experience the experience to set
	 */
	public void setExperience(BigDecimal experience) {
		this.experience = experience;
	}
	/**
	 * @return The character's advancement information.
	 */
	public abstract Advancement getAdvancement();
	/**
	 * @return the notes
	 */
	public Set<String> getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Set<String> notes) {
		this.notes = notes;
	}
	/**
	 * @return the boons
	 */
	public BonusProviderSet<BoonWrapper> getBoons() {
		if(boons == null){
			setBoons(new BonusProviderSet<BoonWrapper>());
		}
		return boons;
	}
	/**
	 * @param boons the boons to set
	 */
	public void setBoons(BonusProviderSet<BoonWrapper> boons) {
		this.boons = boons;
	}
	/**
	 * @return the qualities
	 */
	public BonusProviderSet<QualityWrapper> getQualities(){
		if(qualities == null){
			setQualities(new BonusProviderSet<QualityWrapper>());
		}
		return qualities;
	}
	/**
	 * @param qualities the qualities to set
	 */
	public void setQualities(BonusProviderSet<QualityWrapper> qualities){
		this.qualities = qualities;
	}
}
