package com.westpalmetto.gaming.character.servlet.writer.character.sfsrd;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;
import com.westpalmetto.gaming.character.sfsrd.SFCharacter;
import com.westpalmetto.gaming.character.sfsrd.SFSCharacter;

public final class SFWriterFactory {
	private static final SFCharacterWriter WRITER_SFSRD = new SFCharacterWriter();
	private static final SFSCharacterWriter WRITER_SFS = new SFSCharacterWriter();

	/**
	 * Returns the appropriate writer for the given character.
	 * 
	 * @param character The character
	 * @return The writer for the character
	 */
	public static CharacterHtmlWriter<? extends Character<?>> getWriter(SFCharacter character) {
		CharacterHtmlWriter<? extends Character<?>> writer = null;
		
		if (character instanceof SFSCharacter) {
			writer = WRITER_SFS;
		} else {
			writer = WRITER_SFSRD;
		}
		
		return writer;
	}
}
