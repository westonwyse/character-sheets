package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.loader.SimpleOverridableAbilityProviderLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Model for a familiar.
 */
public class FamiliarWrapper extends AbstractBonusProviderWrapper<SimpleOverridableAbilityProvider> {
	public FamiliarWrapper(String id){
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	@Override
	public String getName() {
		return StringUtils.substringBefore(getData().getName(), " (");
	}

	@Override
	protected DataLoader<SimpleOverridableAbilityProvider> createDataLoader() {
		return SimpleOverridableAbilityProviderLoader.getInstance("familiar", getRoot());
	}
}
