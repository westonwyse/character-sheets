/**
 * SFCharacterMapper.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.sfsrd;

import java.io.PrintWriter;

import com.westpalmetto.gaming.character.sfsrd.SFCharacter;
import com.westpalmetto.gaming.character.sfsrd.SFSCharacter;
import com.westpalmetto.gaming.character.sfsrd.model.Faction;
import com.westpalmetto.gaming.character.sfsrd.model.SFSCharacterModel;

/**
 * Handling of response writing for PSRD characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SFSCharacterWriter extends SFCharacterWriter {
	@Override
	protected void writeBasics(PrintWriter writer, SFCharacter character) {
		// Get data
		SFSCharacter sfsCharacter = (SFSCharacter) character;
		SFSCharacterModel data = sfsCharacter.getSfsData();

		// Write standard information
		super.writeBasics(writer, character);

		// Open section
		writer.println("<div id='basics-sfs'>");

		// Write SFS data, if applicable
		writeLabelValuePair(writer, "faction", "Primary Faction", sfsCharacter.getPrimaryFaction().getName());
		writeLabelValuePair(writer, "reputation", "Faction Reputation",
				sfsCharacter.getPrimaryFaction().getReputation());
		writeLabelValuePair(writer, "reputation", "Total Reputation", sfsCharacter.getTotalReputation());
		writeLabelValuePair(writer, "fame", "Fame", data.getFame());

		// Close section
		writer.println("</div>");
	}

	@Override
	protected void writeNotes(PrintWriter writer, SFCharacter character) {
		SFSCharacter sfsCharacter = (SFSCharacter) character;

		// Write reputation
		writer.println("<h2>Factions &amp; Reputation</h2>");
		writer.println("<div id='factions'>");
		for (Faction faction : sfsCharacter.getSfsData().getFactions()) {
			writeLabelValuePair(writer, "faction", faction.getName(), faction.getReputation());
		}
		writer.println("</div>");

		super.writeNotes(writer, character);
	}
}
