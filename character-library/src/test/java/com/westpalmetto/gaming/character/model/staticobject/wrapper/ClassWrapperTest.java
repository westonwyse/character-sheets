package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.util.Collection;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

/**
 * Tests of ClassWrapper.
 */
public class ClassWrapperTest {
	private ClassWrapper cls;
	
	@Before
	public void setup(){
		cls = new ClassWrapper("fighter", 2);
	}
	
	@Test
	public void testGetBonuses() {
		Collection<ModifiableBonus> bonuses = cls.getBonuses(cls.getModifiers());
		Assert.assertFalse(CollectionUtils.isEmpty(BonusHelper.filterObjectsByKey(bonuses, "Bravery")));
		Assert.assertTrue(CollectionUtils.isEmpty(BonusHelper.filterObjectsByKey(bonuses, "Armor Training")));
	}
	
	@Test
	public void testModifyBonuses() {
		Collection<UnmodifiableBonus> bonuses = cls.modifyBonuses(cls.getModifiers());
		Assert.assertFalse(CollectionUtils.isEmpty(BonusHelper.filterObjectsByKey(bonuses, "Bravery")));
		Assert.assertTrue(CollectionUtils.isEmpty(BonusHelper.filterObjectsByKey(bonuses, "Armor Training")));
	}
}
