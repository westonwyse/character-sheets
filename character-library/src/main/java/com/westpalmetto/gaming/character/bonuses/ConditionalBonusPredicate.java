/**
 * ConditionalBonusPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.model.bonuses.Bonus;


/**
 * Predicate to evaluates to true if an object is a Bonus with a condition.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class ConditionalBonusPredicate implements Predicate {
	private static ConditionalBonusPredicate instance;
	
	private ConditionalBonusPredicate(){
		//Making constructor private - singleton instance
	}
	
	public static ConditionalBonusPredicate getInstance(){
		if(instance == null){
			instance = new ConditionalBonusPredicate();
		}
		return instance;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Bonus){
			Bonus bonus = (Bonus)object;
			matches = (bonus.getCondition() != null);
		}
		
		return matches;
	}
}
