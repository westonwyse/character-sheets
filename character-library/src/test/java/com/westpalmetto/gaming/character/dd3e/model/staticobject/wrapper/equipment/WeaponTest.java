package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.util.Collection;
import java.util.HashSet;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;

public class WeaponTest {
	private Weapon weapon;
	
	@Before
	public void setUp() {
		weapon = new Weapon("Waraxe, dwarven double");
	}

	@Test
	public void testGetBonuses() {
		Collection<Modifier> modifiers = new HashSet<Modifier>();
		
		Collection<ModifiableBonus> actual = weapon.getBonuses(modifiers);
		Assert.assertEquals(1, actual.size());
	}
	
	@Test
	public void testGetAbilities() {
		//Add flaming burst
		StaticSpecialAbility flamingBurst = new StaticSpecialAbility();
		flamingBurst.setName("Flaming Burst");
		
		BonusProviderSet<StaticSpecialAbility> abilities = new BonusProviderSet<StaticSpecialAbility>();
		abilities.add(flamingBurst);
		weapon.setAbilities(abilities);
		
		//Test the results
		BonusProviderSet<StaticSpecialAbility> actual = weapon.getAbilities(weapon.getBonuses(new HashSet<Modifier>()));
		Assert.assertEquals(2, actual.size());
		Assert.assertTrue("Flaming not found in " + actual.keySet(), actual.containsKey("Flaming"));
		Assert.assertTrue("Flaming Burst not found in " + actual.keySet(), actual.containsKey("Flaming Burst"));
	}
}
