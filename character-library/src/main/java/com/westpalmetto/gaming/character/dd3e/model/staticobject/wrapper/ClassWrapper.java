/**
 * ClassLoader.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.SpellSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticClassLoader;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Contains the character-specific details of a character's class.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
@Slf4j
public class ClassWrapper extends AbstractBonusProviderWrapper<StaticClass> implements BonusProvider {
	private int levels;
	private boolean initialClass;
	private List<SpellSet> spells;
	private Map<String, String> options;
	private BonusProviderSet<TalentWrapper> talents;
	private BonusProviderSet<SchoolWrapper> schools;
	private List<Integer> hitDice;
	private AttributeName keyAttribute;

	private StaticClass data;

	/**
	 * All data is vital for operation, so require it to be set.
	 * 
	 * @param name   The ID
	 * @param levels The number of levels in this class the character has attained
	 */
	public ClassWrapper(String id, int levels) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
		this.levels = levels;
	}

	@Override
	public StaticClass getData() {
		if (data == null) {
			StaticClass original = super.getData();
			if (original == null) {
				throw new IllegalArgumentException("No class data for " + getId());
			}

			data = new StaticClass(original);
			data.setOptions(getOptions());
		}
		return data;
	}

	@Override
	protected DataLoader<StaticClass> createDataLoader() {
		return StaticClassLoader.getInstance(getRoot());
	}

	@Override
	public Collection<Modifier> getModifiers() {
		Collection<Modifier> modifiers = new HashSet<>(super.getModifiers());
		modifiers.add(getLevelModifier());
		modifiers.add(getBaseAttackModifier());
		return modifiers;
	}

	private Modifier getLevelModifier() {
		Modifier modifier = new Modifier();
		modifier.setType(ModifierType.CLASS_LEVEL);
		modifier.setValue(getLevels());
		modifier.setName(getName());
		return modifier;
	}

	private Modifier getBaseAttackModifier() {
		Modifier modifier = new Modifier();
		modifier.setType(ModifierType.BASE_ATTACK);
		modifier.setValue(getBaseAttackBonus());
		modifier.setName(getName());
		return modifier;
	}

	/**
	 * @return The base attack bonus provided by this class
	 */
	public int getBaseAttackBonus() {
		return getData().getBaseAttackBonus().getValue(getLevels());
	}

	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		Collection<ModifiableBonus> bonuses = super.getBonuses(modifiers);
		bonuses.addAll(getTalents().getBonuses(modifiers));
		bonuses.addAll(getSchools().getBonuses(modifiers));

		return bonuses;
	}

	/**
	 * @return Options to apply to this class's special abilities.
	 */
	public Map<String, String> getOptions() {
		if (options == null) {
			setOptions(new HashMap<String, String>());
		}
		return options;
	}

	/**
	 * @return the talents
	 */
	public BonusProviderSet<TalentWrapper> getTalents() {
		// Handle null returns
		if (talents == null) {
			setTalents(new BonusProviderSet<TalentWrapper>());
		}
		return talents;
	}

	/**
	 * @return the schools
	 */
	public BonusProviderSet<SchoolWrapper> getSchools() {
		if (schools == null) {
			setSchools(new BonusProviderSet<SchoolWrapper>());
		}
		return schools;
	}

	/**
	 * @return the hitDice
	 */
	public List<Integer> getHitDice() {
		if (hitDice == null) {
			return new ArrayList<Integer>();
		}
		return hitDice;
	}

	public Collection<? extends SpecialAbility> getAbilities() {
		Builder<SpecialAbility> builder = ImmutableSet.<SpecialAbility>builder().addAll(getData().getAbilities());

		return builder.build();
	}

	public int getHighestSpellLevel() {
		int classLevels = getLevels();
		Integer[] spellSet = getData().getSpellsPerDay()[classLevels - 1];
		log.info("Class {} at level {}, spells per day: {}", getName(), classLevels, spellSet);
		int spells;
		// Skip 0, because it's null for non-prepared classes
		for (spells = 1; spells < spellSet.length; spells++) {
			if (spellSet[spells] == null) {
				break;
			}
		}
		return spells - 1;
	}

	/**
	 * @return The key attribute for this class
	 */
	public AttributeName getKeyAttribute() {
		AttributeName attribute;

		if (this.keyAttribute != null) {
			attribute = this.keyAttribute;
		} else {
			attribute = getData().getKeyAttribute();
		}

		return attribute;
	}

	/**
	 * @return TRUE iff the class has a school with spells.
	 */
	public boolean hasSchoolSpells() {
		boolean hasSpells = false;
		if (!getSchools().isEmpty()) {
			for (SchoolWrapper school : getSchools()) {
				hasSpells = hasSpells || school.hasSpells();
			}
		}
		return hasSpells;
	}
}
