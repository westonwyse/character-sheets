package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticDeityLoader;

public class StaticEquipmentLoader extends OverridableStaticObjectLoader3E<StaticEquipment> {
	private static final Map<File,StaticEquipmentLoader> INSTANCES = new HashMap<>();
	
	public static StaticEquipmentLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticEquipmentLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticEquipmentLoader(File root) {
		super("equipment", StaticEquipment.class, root);
	}
	
	@Override
	public StaticEquipment getClonedObject(StaticEquipment original) {
		return new StaticEquipment(original);
	}
}