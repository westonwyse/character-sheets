package com.westpalmetto.gaming.character.swade.model.staticobject;

import com.westpalmetto.gaming.character.model.staticobject.StaticObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class StaticPower implements StaticObject{
    private String name;
    private String source;
    private String rank;
    private int powerPoints;
    private String range;
    private String duration;
    private String damage;
    private String description;

    StaticPower(String name){
        this.name = name;
    }

    public StaticPower(StaticPower original){
        this(original.getName());
        setSource(original.getSource());
        setRank(original.getRank());
        setPowerPoints(original.getPowerPoints());
        setRange(original.getRange());
        setDuration(original.getDuration());
        setDamage(original.getDamage());
        setDescription(original.getDescription());
    }
}
