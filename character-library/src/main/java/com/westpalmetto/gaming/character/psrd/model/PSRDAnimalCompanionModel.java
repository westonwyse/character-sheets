package com.westpalmetto.gaming.character.psrd.model;

import java.io.File;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.Advancement;
import com.westpalmetto.gaming.character.dd3e.SavingThrow;
import com.westpalmetto.gaming.character.dd3e.model.Attribute;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.AttributeSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticAnimalCompanion;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticAnimalCompanion.AnimalCompanionData;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticAnimalCompanionLoader;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.AnimalTrick;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.QualityWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SizeWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.loader.StaticSpecialAbilityLoader;
import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

/**
 * A model class for animal companions.
 */
public class PSRDAnimalCompanionModel extends PSRDCharacterModel {
	private static final int[] BASE_ATTACK = { 1, 2, 2, 3, 3, 4, 4, 5, 6, 6, 6, 7, 8, 9, 9, 9, 10, 11, 11, 12 };
	private static final int[] HIT_DICE = { 2, 3, 3, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 12, 12, 13, 14, 15, 15, 16 };
	private static final int[] NATURAL_ARMOR_BONUS = { 0, 0, 2, 2, 2, 4, 4, 4, 6, 6, 6, 8, 8, 8, 10, 10, 10, 12, 12,
			12 };
	private static final int[] STR_DEX_BONUS = { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6 };

	private AnimalCompanionData animalData;
	private BonusProviderSet<StaticSpecialAbility> abilities;

	private int level;
	private String animalType;
	private BonusProviderSet<AnimalTrick> tricks;

	public enum AnimalSavingThrow {
		GOOD(new int[] { 3, 3, 3, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 8, 8, 8, 9, 9, 9, 10 }),
		BAD(new int[] { 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5 }),;

		private int[] bonuses;

		private AnimalSavingThrow(int[] bonuses) {
			this.bonuses = bonuses;
		}

		/**
		 * Returns the bonus for a given companion level.
		 * 
		 * @param level The level for which to retrieve the bonus.
		 * @return The bonus for the requested level.
		 */
		public int getBonus(int level) {
			// Note that levels are 1-based, not 0-based, so subtract one before retrieval.
			return bonuses[level - 1];
		}
	}

	public PSRDAnimalCompanionModel(File resourceRoot) {
		super(resourceRoot);
	}

	/**
	 * Returns the static data for the animal companion.
	 * 
	 * @return The static animal data.
	 */
	protected AnimalCompanionData getAnimalData() {
		if (animalData == null) {
			StaticAnimalCompanion staticData = getLoader().getData(getAnimalType());
			setAnimalData(staticData.getBase());

			// Check for advancement
			if (getLevel() >= staticData.getAdvancement()) {
				setAnimalData(applyAdvancement(getAnimalData(), staticData.getAdvanced()));
			}
		}
		return animalData;
	}

	private JsonDataLoader<StaticAnimalCompanion> getLoader() {
		return StaticAnimalCompanionLoader.getInstance(ResourceFactory.Ruleset.PSRD.getRoot());
	}

	private AnimalCompanionData applyAdvancement(AnimalCompanionData base, AnimalCompanionData advanced) {
		AnimalCompanionData applied = new AnimalCompanionData(base);

		if (advanced.getSize() != null) {
			applied.setSize(advanced.getSize());
		}
		if (advanced.getBaseSpeed() > 0) {
			applied.setBaseSpeed(advanced.getBaseSpeed());
		}
		if (advanced.getNaturalArmor() > 0) {
			applied.setNaturalArmor(advanced.getNaturalArmor());
		}
		if (advanced.getAttacks() != null) {
			applied.setAttacks(advanced.getAttacks());
		}
		if (advanced.getAttributes() != null) {
			applied.setAttributes(advanced.getAttributes());
		}
		if (advanced.getSpecialQualities() != null) {
			applied.setSpecialQualities(advanced.getSpecialQualities());
		}

		return applied;
	}

	/**
	 * @param animalData the animalData to set
	 */
	protected void setAnimalData(AnimalCompanionData animalData) {
		this.animalData = animalData;
	}

	/**
	 * Returns the abilities data for the animal companion.
	 * 
	 * @return The static animal abilities data.
	 */
	public BonusProviderSet<StaticSpecialAbility> getSpecialAbilities() {
		if (abilities == null) {
			setAbilities(new BonusProviderSet<>(getAbilityLoader().getCache().values()));
		}
		return abilities;
	}

	private StaticSpecialAbilityLoader getAbilityLoader() {
		return StaticSpecialAbilityLoader.getInstance("animalCompanionAbilities",
				ResourceFactory.Ruleset.PSRD.getRoot());
	}

	/**
	 * @param abilities the abilities to set
	 */
	protected void setAbilities(Collection<StaticSpecialAbility> abilities) {
		this.abilities = new BonusProviderSet<>(abilities);
	}

	/**
	 * @return the animalCompanion
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public BigDecimal getExperience() {
		return new BigDecimal(level);
	}

	@Override
	public void setExperience(BigDecimal experience) {
		this.level = experience.intValue();
	}

	/**
	 * @return the type
	 */
	public String getAnimalType() {
		return animalType;
	}

	/**
	 * @param type the type to set
	 */
	public void setAnimalType(String type) {
		this.animalType = type;
	}

	/**
	 * @return the tricks
	 */
	public BonusProviderSet<AnimalTrick> getTricks() {
		if (tricks == null) {
			setTricks(new BonusProviderSet<AnimalTrick>());
		}
		return tricks;
	}

	/**
	 * @param tricks the tricks to set
	 */
	public void setTricks(BonusProviderSet<AnimalTrick> tricks) {
		this.tricks = tricks;
	}

	public SizeWrapper getSize() {
		return getAnimalData().getSize();
	}

	@Override
	public AttributeSet getAttributes() {
		// Get the static attributes
		AttributeSet attributes = new AttributeSet();
		attributes.addAll(getAnimalData().getAttributes());

		// Apply Str & Dex mods
		int strDexMod = getValueForLevel(STR_DEX_BONUS);
		attributes.add(new Attribute(AttributeName.STR, attributes.get(AttributeName.STR).getScore() + strDexMod));
		attributes.add(new Attribute(AttributeName.DEX, attributes.get(AttributeName.DEX).getScore() + strDexMod));

		// Return the attributes
		return attributes;
	}

	@Override
	public void setAttributes(AttributeSet attributes) {
		throw new UnsupportedOperationException("Attributes is not modifiable.");
	}

	@Override
	public Advancement getAdvancement() {
		return new AnimalAdvancement();
	}

	/**
	 * @return The base speed of the companion.
	 */
	public int getBaseSpeed() {
		return getAnimalData().getBaseSpeed();
	}

	/**
	 * @param saveType The save type to find.
	 * @return The saving throws of the companion for the type.
	 */
	public AnimalSavingThrow getBaseSave(SavingThrow saveType) {
		switch (saveType) {
		case FORTITUDE:
		case REFLEX:
			return AnimalSavingThrow.GOOD;
		case WILL:
			return AnimalSavingThrow.BAD;
		default:
			// TODO: Exceptions
			throw new RuntimeException("Unknown saving throw type: " + saveType);
		}
	}

	/**
	 * @return The base attack bonus for the companion's current level.
	 */
	public int getBaseAttack() {
		return getValueForLevel(BASE_ATTACK);
	}

	/**
	 * @return The number of hit dice for the companion's current level.
	 */
	public int getHitDice() {
		return getValueForLevel(HIT_DICE);
	}

	/**
	 * @return The natural armor bonus for the companion's level
	 */
	public int getNaturalArmorBonus() {
		return getValueForLevel(NATURAL_ARMOR_BONUS);
	}

	/**
	 * @param values The values
	 * @return The value for the companion's level.
	 */
	private int getValueForLevel(int[] values) {
		// Note that levels are 1-based, not 0-based, so subtract one before retrieval.
		return values[getLevel() - 1];
	}

	/**
	 * @return The companion's natural weapons.
	 */
	public Set<Weapon> getAttacks() {
		return getAnimalData().getAttacks();
	}

	/**
	 * @return The companion's special qualities
	 */
	public BonusProviderSet<QualityWrapper> getSpecialQualities() {
		return getAnimalData().getSpecialQualities();
	}

	public static class AnimalAdvancement implements Advancement {
		@Override
		public int getLevel(int experience) {
			return experience;
		}

		@Override
		public int getExperience(int level) {
			return level;
		}

		@Override
		public int getNextLevel(int experience) {
			return experience + 1;
		}
	}
}
