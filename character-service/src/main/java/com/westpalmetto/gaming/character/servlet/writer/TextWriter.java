/**
 * TextWriter.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import com.westpalmetto.gaming.character.util.FileHelper;

/**
 * Response writer for simple text requests.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class TextWriter implements ResponseWriter {
	private static final Pattern PATTERN_MIMETYPE = Pattern.compile(".*\\.(\\w+)");
	
	@Override
	public InputStream getFileStream(String path){
		return getClass().getResourceAsStream("/data" + path);
	}

	@Override
	public String getMimeType(String path) {
		Matcher matcher = PATTERN_MIMETYPE.matcher(path);
		String type = null;
		
		if(matcher.matches()){
			type = "text/" + matcher.group(1);
		}
		
		return type;
	}
	
	@Override
	public void write(String path, HttpServletResponse response) throws IOException {
		//Set up response
		response.setContentType("text");
		PrintWriter writer = response.getWriter();
		
		//Get file & write contents
		String contents = FileHelper.readFile(getFileStream(path));
		writer.println(contents);
        
        //Close stream
        writer.close();
	}
}
