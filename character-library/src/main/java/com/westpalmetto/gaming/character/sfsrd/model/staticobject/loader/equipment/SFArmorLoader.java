package com.westpalmetto.gaming.character.sfsrd.model.staticobject.loader.equipment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.equipment.SFStaticArmor;

public class SFArmorLoader extends OverridableStaticObjectLoader3E<SFStaticArmor>{
    private static final Map<File, SFArmorLoader> INSTANCES = new HashMap<>();

    public static SFArmorLoader getInstance(File root){
        if (!INSTANCES.containsKey(root)) {
            INSTANCES.put(root, new SFArmorLoader(root));
        }
        return INSTANCES.get(root);
    }

    private SFArmorLoader(File root){
        super("armor", SFStaticArmor.class, root);
    }

    @Override
    public SFStaticArmor getClonedObject(SFStaticArmor original){
        return new SFStaticArmor(original);
    }
}
