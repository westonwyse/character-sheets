package com.westpalmetto.gaming.character.psrd;

import org.junit.Assert;
import org.junit.Test;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;

public class PSRDCharacterTest{
	@Test
	public void testGetSkillUnlocks(){
		//Set up the rogue
		ClassWrapper unchainedRogue = new ClassWrapper("rogue-unchained", 5);
		unchainedRogue.getOptions().put("Rogue's Edge 1", "Perception");
		
		//Set up skill
		String skillName = "Perception";
		DD3ESkill perception = new DD3ESkill(skillName, AttributeName.WIS, false);
		perception.setClassSkill(true);
		perception.setRanks(5);
		
		//Set up the character
		PSRDCharacter character = new PSRDCharacter(ResourceFactory.Ruleset.PSRD.getRoot());
		PSRDCharacterModel data = character.getPsrdData();
		data.getClasses().add(unchainedRogue);
		data.getSkills().add(perception);
		
		//Test the skill unlock
		String expected = "Skill Unlock - " + skillName;
		BonusProviderSet<SimpleOverridableAbilityProvider> unlocks = character.getSkillUnlocks(character.getFixedBonusProviders());
		Assert.assertTrue("Expected '" + expected + "' in " + unlocks.keySet(), unlocks.containsKey(expected));
	}
}
