package com.westpalmetto.gaming.character.model.bonuses;

import java.util.ArrayList;
import java.util.List;

import com.westpalmetto.gaming.character.model.Damage;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Model for modifiable bonus details.
 */
@NoArgsConstructor
@Getter
@Setter
public class ModifiableBonus extends AbstractModifiableObject implements Bonus{
    private BonusType type;
    private int value;
    private Damage valueDie;
    private String valueType;
    private String valueName;
    private String name;
    private String source;
    private String condition;
    private List<Integer> conditionValues = new ArrayList<Integer>();
    private List<Integer> increases = new ArrayList<Integer>();
    private List<Integer> steps = new ArrayList<Integer>();

    /**
     * Copy constructor.
     * 
     * @param bonus The Bonus object to copy
     */
    public ModifiableBonus(ModifiableBonus bonus){
        super(bonus);
        setType(bonus.getType());
        setValue(bonus.getValue());
        setValueDie(bonus.getValueDie());
        setValueType(bonus.getValueType());
        setValueName(bonus.getValueName());
        setName(bonus.getName());
        setSource(bonus.getSource());
        setCondition(bonus.getCondition());
        setConditionValues(new ArrayList<Integer>(bonus.getConditionValues()));
        setIncreases(new ArrayList<Integer>(bonus.getIncreases()));
        setSteps(new ArrayList<Integer>(bonus.getSteps()));
    }

    /**
     * @return the conditionValues
     */
    public List<Integer> getConditionValues(){
        if (conditionValues == null) {
            conditionValues = new ArrayList<Integer>();
        }
        return conditionValues;
    }

    /**
     * @return the increases
     */
    public List<Integer> getIncreases(){
        if (increases == null) {
            increases = new ArrayList<Integer>();
        }
        return increases;
    }
}
