/**
 * RaceLoader.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticRace;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticRaceLoader;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.ObjectHelper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;

/**
 * Contains the character-specific details of a character's race.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class RaceWrapper extends AbstractBonusProviderWrapper<StaticRace>{
    private AttributeName attributeBonus;
    private String skillFocus;
    private Set<RacialTraitWrapper> alternateRacialTraits;
    private SizeWrapper size;

    /**
     * The ID is vital for operation, so require it to be set.
     * 
     * @param name The ID
     */
    public RaceWrapper(String id){
        super(id, ResourceFactory.Ruleset.PSRD.toString());
    }

    @Override
    protected DataLoader<StaticRace> createDataLoader(){
        return StaticRaceLoader.getInstance(getRoot());
    }

    /**
     * @return The data
     */
    @Override
    public StaticRace getData(){
        StaticRace original = new StaticRace(super.getData());

        // Handle alternate racial traits
        if (CollectionUtils.isNotEmpty(getAlternateRacialTraits())) {
            original.setAbilities(
                    ObjectHelper.overrideAbilities(ObjectHelper.extractStaticAbilities(getAlternateRacialTraits()),
                            original.getAbilities(),
                            original.getName()));
        }

        return original;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = super.getBonuses(modifiers);
        bonuses.addAll(getSize().getBonuses(modifiers));

        // Check if we need to add any item keys
        if ((getAttributeBonus() != null) || (getSkillFocus() != null)) {
            for (ModifiableBonus bonus : bonuses) {
                if ((getAttributeBonus() != null) && (BonusType.ATTRIBUTE_CORE.equals(bonus.getType()))) {
                    bonus.getItemKeys().add(getAttributeBonus().toString());
                }

                if ((getSkillFocus() != null) && BonusType.FEAT.equals(bonus.getType())
                        && bonus.getItemKeys().contains("Skill Focus")) {
                    bonus.getItemKeys().add(getSkillFocus());
                }
            }
        }

        return bonuses;
    }

    /**
     * @return the size
     */
    public SizeWrapper getSize(){
        if (size == null) {
            setSize(new SizeWrapper(getData().getSize()));
        }
        return size;
    }
}
