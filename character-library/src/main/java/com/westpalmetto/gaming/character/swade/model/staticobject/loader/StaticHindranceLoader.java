package com.westpalmetto.gaming.character.swade.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.model.staticobject.loader.OverridableStaticObjectLoader;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticHindrance;

public class StaticHindranceLoader extends OverridableStaticObjectLoader<StaticHindrance> {
	private static final Map<File,StaticHindranceLoader> INSTANCES = new HashMap<>();
	
	public static StaticHindranceLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticHindranceLoader(root));
		}
		return INSTANCES.get(root);
	}
	private StaticHindranceLoader(File root) {
		super("hindrance", StaticHindrance.class, root);
	}

	@Override
	public StaticHindrance getClonedObject(StaticHindrance original) {
		return new StaticHindrance(original);
	}
}
