package com.westpalmetto.gaming.character.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment.Size;


/**
 * See http://paizo.com/paizo/faq/v5748nruor1fm#v5748eaic9t3f
 */
public enum Damage{
	UNKNOWN("UNKNOWN", "UNKNOWN", "UNKNOWN"),
	NONE("-", "NONE", "NONE"),
	ONE("1", "NONE", "ONE_TWO"),
	ONE_TWO("1d2", "ONE", "ONE_THREE"),
	ONE_THREE("1d3", "ONE_TWO", "ONE_FOUR"),
	ONE_FOUR("1d4", "ONE_THREE", "ONE_SIX"),
	ONE_SIX("1d6", "ONE_FOUR", "ONE_EIGHT"),
	ONE_EIGHT("1d8", "ONE_SIX", "ONE_TEN"),
	ONE_TEN("1d10", "ONE_EIGHT", "TWO_SIX"),
	ONE_TWELVE("1d12", "ONE_TEN", "TWO_EIGHT"),
	TWO_FOUR("2d4", "ONE_SIX", "ONE_TEN"),
	TWO_SIX("2d6", "ONE_TEN", "TWO_EIGHT"),
	TWO_EIGHT("2d8", "TWO_SIX", "THREE_SIX"),
	TWO_TEN("2d10", "TWO_EIGHT", "FOUR_EIGHT"),
	THREE_SIX("3d6", "TWO_EIGHT", "THREE_EIGHT"),
	;
	
    private static final Logger LOGGER = LoggerFactory.getLogger(Damage.class);
	
	private final String display;
	private final String stepDown;
	private final String stepUp;
	
	private Damage(String display, String stepDown, String stepUp){
		this.display = display;
		this.stepDown = stepDown;
		this.stepUp = stepUp;
	}

	/**
	 * @return the string to display
	 */
	public String getDisplay(){
		return display;
	}

	/**
	 * @return the stepDown
	 */
	public Damage getStepDown(){
		return valueOf(stepDown);
	}

	/**
	 * @return the stepUp
	 */
	public Damage getStepUp(){
		return valueOf(stepUp);
	}

	public Damage getForSize(Size size){
		Damage damage;
		
		switch(size){
		case L:
			damage = getStepUp();
			break;
		case M:
			damage = this;
			break;
		case S:
			damage = getStepDown();
			break;
		case T:
			damage = getStepDown().getStepDown();
			break;
		default:
			LOGGER.error("Unknown size for damage: " + size);
			damage = UNKNOWN;
			break;
		}
		
		return damage;
	}
}

