/**
 * GsonUtil.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;

/**
 * Utilities for interacting with Gson.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class GsonUtils {
	private static Gson gsonInstance;
	
	/**
	 * @return The Gson instance to use.
	 */
	public static Gson getGsonInstance(){
		if(gsonInstance == null){
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(AbstractEquipment.class, new EquipmentDeserializer());
			gsonInstance = builder.create();
		}
		
		return gsonInstance;
	}

	/**
	 * Reads the given JSON as the given model class.
	 * 
	 * @param json The JSON to read.
	 * @param cls The class to read as.
	 * @return The resulting model.
	 */
	public static <T> T readModel(JsonObject json, Class<T> cls) {
		Gson gson = getGsonInstance();
		T model;
		try {
			model = gson.fromJson(json, cls);
		} catch (JsonSyntaxException e){
			//TODO: Exceptions
			throw e;
		}
		return model;
	}
	
	/**
	 * Converts the given string to a JsonElement.
	 * 
	 * @param json The string to convert
	 * @return The converted JsonElement object
	 */
	public static JsonElement asJsonElement(String json){
		JsonParser parser = new JsonParser();
		return parser.parse(json);
	}
	
	/**
	 * Null-safe method to convert a JsonElement to a String.
	 * 
	 * @param json The JSON element.
	 * @return The value as a string; null if the element is null.
	 */
	public static String asString(JsonElement json) {
		String string = null;
		if(json != null){
			string = json.getAsString();
		}
		return string;
	}
}
