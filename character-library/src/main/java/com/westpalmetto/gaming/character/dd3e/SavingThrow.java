package com.westpalmetto.gaming.character.dd3e;

/**
 * Types of saving throws.
 */
public enum SavingThrow{
	FORTITUDE,
	REFLEX,
	WILL,
	;
}
