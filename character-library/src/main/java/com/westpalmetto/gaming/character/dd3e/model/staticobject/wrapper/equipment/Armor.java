/**
 * Armor.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticArmor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticArmor.Proficiency;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment.StaticArmorLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to contain stats for an AC item.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class Armor extends AbstractEquipment<StaticArmor> {
	private Quality quality = Quality.NORMAL;
	private OffensiveArmor attack;
	
	public Armor(String id) {
		super(id);
	}

	@Override
	protected DataLoader<StaticArmor> createDataLoader() {
		return StaticArmorLoader.getInstance(getRoot());
	}

	@Override
	public String getDisplayName() {
		String name = super.getName();
		
		if(!(Quality.NORMAL.equals(getQuality()))){
			name = super.getName() + ", " + getQuality().getLabel();
		}
		
		return name ;
	}
	/**
	 * Calculates the armor check penalty after armor quality bonuses.
	 * @param bonus Any additional bonus to apply.
	 * @return the modified check penalty
	 */
	public int getModifiedCheckPenalty(int bonus){
		int penalty = getData().getCheckPenalty() + getQuality().getCheckBonus() + bonus;
		if (penalty > 0){ penalty = 0; }
		return penalty;
	}
	
	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		if(quality == null){
			setQuality(Quality.NORMAL);
		}
		return quality;
	}
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}
	/**
	 * @return the attack
	 */
	public OffensiveArmor getAttack() {
		//Inspect for default shield bash stats
		if ((attack == null) && getData().getProficiency().isShieldBash()) {
			setAttack(loadShieldBash(getData().getProficiency()));
		}
		
		if (attack != null) {
			//Inspect for parent reference
			if(attack.getParent() == null){
				attack.setParent(this);
			}
			
			//Set location & hand
			attack.setLocation(getLocation());
			attack.setHand(getHand());
		}
		
		return attack;
	}
	
	private OffensiveArmor loadShieldBash(Proficiency proficiency) {
		OffensiveArmor bash = null;
		
		switch(proficiency){
		case SHIELD_HEAVY:
		case SHIELD_LIGHT:
			bash = new OffensiveArmor(proficiency.getAbbreviation());
			break;
		default:
			break;
		}
		
		return bash;
	}

	/**
	 * @param attack the attack to set
	 */
	public void setAttack(OffensiveArmor attack) {
		this.attack = attack;
	}
	
	/**
	 * Calculates the AC bonus after armor quality bonuses.
	 * @return the modified AC bonus
	 */
	public int getModifiedAcBonus(){
		return getData().getAcBonus() + getQuality().getACBonus();
	}

	/**
	 * The quality of an AC item, and the bonuses that quality confers.
	 */
	public enum Quality{
		NORMAL(0, 0, ""),
		MASTERWORK(1, 0, "Masterwork"),
		PLUS1(1, 1, "+1"),
		PLUS2(1, 2, "+2"),
		PLUS3(1, 3, "+3"),
		PLUS4(1, 4, "+4"),
		PLUS5(1, 5, "+5"),
		;
		
		private int checkBonus;
		private int acBonus;
		private String label;
		
		private Quality(int checkBonus, int acBonus, String label){
			this.checkBonus = checkBonus;
			this.acBonus = acBonus;
			this.label = label;
		}

		/**
		 * @return the checkBonus
		 */
		public int getCheckBonus() {
			return checkBonus;
		}

		/**
		 * @return the acBonus
		 */
		public int getACBonus() {
			return acBonus;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}
}
