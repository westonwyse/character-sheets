package com.westpalmetto.gaming.character.model.staticobject;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

public class StaticClassTest {
	private StaticClass staticClass;
	
	@Before
	public void setup() {
		ClassWrapper chClass = new ClassWrapper("fighter", 1);
		staticClass = chClass.getData();
	}
	
//	@Test
//	public void testNewAbilitiesByLevel() {
//		SpecialAbilitySet<StaticSpecialAbility> actual6 = staticClass.getAbilitiesAtLevel(6, null);
//		Assert.assertEquals(3, actual6.get("Armor Training").getBonuses().size());
//		
//		//Gains full speed in heavy armor at 7
//		SpecialAbilitySet<StaticSpecialAbility> actual7 = staticClass.getAbilitiesAtLevel(7, null);
//		Assert.assertEquals(4, actual7.get("Armor Training").getBonuses().size());
//	}
//	
//	@Test
//	public void testBonusesByLevel() {
//		SpecialAbilitySet<StaticSpecialAbility> actual6 = staticClass.getAbilitiesAtLevel(6, null);
//		boolean found = false;
//		for (Bonus bonus : actual6.get("Armor Training").getBonuses()){
//			if (BonusType.MAX_DEX.equals(bonus.getType())) {
//				Assert.assertEquals(1, bonus.getValue());
//				found = true;
//			}
//		}
//		Assert.assertTrue("Max Dex modifier not found.", found);
//		
//		//Armor training 2 at 7th level
//		SpecialAbilitySet<StaticSpecialAbility> actual7 = staticClass.getAbilitiesAtLevel(7, null);
//		found = false;
//		for (Bonus bonus : actual7.get("Armor Training").getBonuses()){
//			if (BonusType.MAX_DEX.equals(bonus.getType())) {
//				Assert.assertEquals(2, bonus.getValue());
//				found = true;
//			}
//		}
//		Assert.assertTrue("Max Dex modifier not found.", found);
//	}
	
//	@Test
//	public void testSlayer(){
//		CharacterClass chClass = new CharacterClass("slayer", 1);
//		staticClass = chClass.getData();
//		Bonus bonus = staticClass.getAbilitiesAtLevel(1, null).get("Track (Ex)").getBonuses().iterator().next();
//		Assert.assertTrue(bonus.getCondition().contains("+1"));
//	}
	
	@Test
	public void testClericChannel(){
		ClassWrapper cleric1 = new ClassWrapper("cleric", 1);
		staticClass = cleric1.getData();
		Collection<UnmodifiableBonus> bonuses = cleric1.modifyBonuses(BonusType.ATTACK, cleric1.getModifiers());
		Bonus bonus = bonuses.iterator().next();
		Assert.assertTrue(bonus.getCondition().contains("1d6"));

		ClassWrapper cleric3 = new ClassWrapper("cleric", 3);
		staticClass = cleric3.getData();
		bonuses = cleric3.modifyBonuses(BonusType.ATTACK, cleric3.getModifiers());
		bonus = bonuses.iterator().next();
		System.out.println("Bonus: " + bonus);
		System.out.println("Modifiers: " + cleric3.getModifiers());
		Assert.assertTrue(bonus.getCondition().contains("2d6"));
	}
}
