package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.loader.StaticSpecialAbilityLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.OptionalBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

public class ConceptWrapper extends OptionalBonusProviderWrapper<StaticSpecialAbility>{
    public ConceptWrapper(String id){
        super(id, ResourceFactory.Ruleset.SWADE.toString());
    }

    @Override
    public String getName(){
        return StringUtils.substringBefore(getData().getName(), " (");
    }

    @Override
    public String getSource(){
        return getData().getSource();
    }

    @Override
    protected DataLoader<StaticSpecialAbility> createDataLoader(){
        return StaticSpecialAbilityLoader.getInstance("concept", getRoot());
    }

    @Override
    protected StaticSpecialAbility createData(){
        StaticSpecialAbility data = new StaticSpecialAbility();
        data.setName(getId());
        return data;
    }

    @Override
    public String getRuleset(){
        return ResourceFactory.Ruleset.SWADE.name();
    }
}
