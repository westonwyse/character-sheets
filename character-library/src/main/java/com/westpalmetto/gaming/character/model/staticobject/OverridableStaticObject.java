package com.westpalmetto.gaming.character.model.staticobject;

import java.util.Set;


/**
 * Common functionality for class containing modifiable static data.
 */
public interface OverridableStaticObject extends StaticObject {
	/**
	 * @return the overrides
	 */
	public Set<String> getOverrides();
	/**
	 * @param overrides the overrides to set
	 */
	public void setOverrides(Set<String> overrides);
}
