package com.westpalmetto.gaming.character.servlet.writer.character;

import java.io.PrintWriter;

import com.westpalmetto.gaming.character.Character;

/**
 * Interface to define writes that output HTML responses for a Character.
 *
 * @param <T> The type of character being written.
 */
public interface CharacterHtmlWriter<T extends Character> {
	/**
	 * Write the HTML.
	 * 
	 * @param writer The writer to which to write.
	 * @param character The character to write.
	 */
	public void writeHtml(PrintWriter writer, T character);
}
