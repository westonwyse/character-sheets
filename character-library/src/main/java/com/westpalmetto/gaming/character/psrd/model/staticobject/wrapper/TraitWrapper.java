/**
 * CharacterTrait.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision May 8, 2013
 */
package com.westpalmetto.gaming.character.psrd.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;

/**
 * Contains the character-specific details of a trait.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class TraitWrapper extends SpecialAbility3E{
	private static final String LOADER_NAME = "trait";

	/**
	 * Determines if the given ID is valid.
	 * 
	 * @param id The ID
	 * @param root The resource root
	 * @return TRUE is the ID is a known feat; FALSE if not.
	 */
	public static boolean isValid(String id, File root) {
		return isValid(LOADER_NAME, id, root);
	}
	
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public TraitWrapper(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	@Override
	public String getLoaderId() {
		return LOADER_NAME;
	}
}
