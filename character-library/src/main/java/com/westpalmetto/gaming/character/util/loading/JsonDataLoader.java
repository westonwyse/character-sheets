package com.westpalmetto.gaming.character.util.loading;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

import lombok.Getter;

@Getter
public abstract class JsonDataLoader<V> implements DataLoader<V> {
	private final Map<String, V> cache;
	private final String typeId;
	private final Class<V> clz;
	private final File root;
	
	public JsonDataLoader(String typeId, Class<V> clz, File root) {
		this.cache = new HashMap<String, V>();
		this.typeId = typeId;
		this.clz = clz;
		this.root = root;
		
		initializeCache();
	}

	protected void initializeCache() {
		SourceDataLoader sources = SourceDataLoader.getInstance(getRoot());
		
		if(!sources.contains(getTypeId())){
			throw new IllegalArgumentException("No data for type '" + getTypeId() + "': " + sources.getKnownTypes());
		}
		for(Entry<String, JsonElement> entry : sources.getData(getTypeId()).entrySet()){
			try {
				getCache().put(entry.getKey(),
						GsonUtils.getGsonInstance().fromJson(entry.getValue(), clz));
			}catch (Exception e){
				//TODO: Exceptions
				throw new RuntimeException("Error parsing " + entry.getKey(), e);
			}
		}
	}

	@Override
	public boolean contains(String id) {
		return getCache().containsKey(getKey(id));
	}

	@Override
	public V getData(String id) {
		V response = null;
		if(contains(id)){
			response = getClonedObject(getCache().get(getKey(id)));
		}
		
		return response;
	}
	
	@Override
	public String getKey(String id){
		return id;
	}
}
