/**
 * PSRDCharacterTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 19, 2013
 */
package com.westpalmetto.gaming.character.psrd.model;

import junit.framework.Assert;

import org.junit.Test;

import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel.PSRDAdvancementTable;


/**
 * Tests for PSRDCharacterModel.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class PSRDCharacterModelTest {
	@Test
	public void testAdvancement(){
		PSRDAdvancementTable table = PSRDAdvancementTable.FAST;
		Assert.assertEquals(1, table.getLevel(50));
		Assert.assertEquals(9, table.getLevel(55000));
		Assert.assertEquals(17, table.getLevel(1000000));
		
		table = PSRDAdvancementTable.SLOW;
		Assert.assertEquals(1, table.getLevel(50));
		Assert.assertEquals(7, table.getLevel(55000));
		Assert.assertEquals(15, table.getLevel(1000000));
	}
}
