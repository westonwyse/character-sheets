/**
 * ProvidesBonus.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character;

/**
 * Marker interface to guarantee naming.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface NamedObject extends Comparable<NamedObject>{
    /**
     * @return The name of this object.
     */
    public String getName();

    /**
     * @return The source of this object.
     */
    public String getSource();

    @Override
    default int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }
}
