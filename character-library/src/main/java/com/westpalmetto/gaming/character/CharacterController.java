package com.westpalmetto.gaming.character;

import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public class CharacterController {
	private static final CharacterController INSTANCE = new CharacterController();

	private ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
	private long ttl = 30 * 60 * 1000;
	private String dataRoot = "https://bitbucket.org/westonwyse/character-sheets/raw/externalData/character-data/data";

	private CharacterController(){}

	public static CharacterController getInstance() {
		return INSTANCE;
	}

	/**
	 * @return the executor
	 */
	public synchronized ListeningExecutorService getExecutor() {
		return executor;
	}

	/**
	 * @param executor the executor to set
	 */
	public void setExecutor(ListeningExecutorService executor) {
		this.executor = executor;
	}

	/**
	 * @return the ttl
	 */
	public long getTtl() {
		return ttl;
	}

	/**
	 * @param ttl the ttl to set
	 */
	public void setTtl(long ttl) {
		this.ttl = ttl;
	}

	/**
	 * @return the dataRoot
	 */
	public String getDataRoot() {
		return dataRoot;
	}

	/**
	 * @param dataRoot the dataRoot to set
	 */
	public void setDataRoot(String dataRoot) {
		this.dataRoot = dataRoot;
	}
	
}
