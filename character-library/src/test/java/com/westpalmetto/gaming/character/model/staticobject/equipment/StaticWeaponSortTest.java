package com.westpalmetto.gaming.character.model.staticobject.equipment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.TreeMap;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon;
import com.westpalmetto.gaming.character.util.FileHelper;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

@Ignore
public class StaticWeaponSortTest {
	private static final Map<String, StaticWeapon> CACHE = new TreeMap<String, StaticWeapon>();
	private static final String PATH = "/data/prd/weapons.json";

	@Before
	public void setUp() {
		CACHE.clear();
		loadStaticData(PATH);
	}
	
	@Test
	public void test() throws Exception{
		//Convert sorted cache to JSON
		Gson gson = GsonUtils.getGsonInstance();
		String json = gson.toJson(CACHE);
		
		//Open the file & write
		File file = new File("target/weapons.json");
		FileOutputStream output = new FileOutputStream(file);
		IOUtils.write(json, output);
		output.close();
	}
	
	@Test
	public void testCounts(){
		int sorted = CACHE.size();
		
		CACHE.clear();
		loadStaticData("/data/prd/weapons.bk.json");
		int original = CACHE.size();
		
		Assert.assertEquals(sorted, original);
	}

	protected Map<String, StaticWeapon> parseMapFromJson(String json) {
		Map<String, StaticWeapon> map = GsonUtils.getGsonInstance()
				.fromJson(json,
						new TypeToken<Map<String, StaticWeapon>>() {}.getType());
		return map;
	}
	
	protected void loadStaticData(String classpath){
		try {
			String json = FileHelper.readFile(classpath);
			parseFromJson(json);
		} catch (Exception e) {
			//TODO: Exceptions
			throw new RuntimeException("Exception while reading " + classpath, e);
		}
	}
	
	protected void parseFromJson(String json){
		//Fill cache
		Map<String, StaticWeapon> parsedData = parseMapFromJson(json);
		CACHE.putAll(parsedData);
	}
}
