package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.converter.SkillModifierProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticClassLoader;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FeatWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.TalentWrapper;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.model.Skill;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;

/**
 * Tests of BonusHelper.
 */
public class BonusHelperTest {
	private static final BonusProviderSet<SpecialAbility> ABILITIES = new BonusProviderSet<SpecialAbility>();
	private static final Set<Modifier> LEVEL1 = new HashSet<Modifier>();
	private static final Set<Modifier> LEVEL5 = new HashSet<Modifier>();
	private static final Set<Modifier> LEVEL10 = new HashSet<Modifier>();
	private static final Set<Modifier> LEVEL20 = new HashSet<Modifier>();

	private static StaticSpecialAbility level3;
	private static StaticSpecialAbility level7;
	private static StaticSpecialAbility level11;
	private static StaticSpecialAbility level14;
	
	@BeforeClass
	public static void setupClass(){
		//Load some fighter abilities
		StaticClassLoader classLoader = StaticClassLoader.getInstance(ResourceFactory.Ruleset.PSRD.getRoot());
		StaticClass cls = classLoader.getData("slayer");
		level3 = cls.getAbilities().get("Sneak Attack");
		level7 = cls.getAbilities().get("Stalker (Ex)");
		level11 = cls.getAbilities().get("Swift Tracker (Ex)");
		level14 = cls.getAbilities().get("Quarry (Ex)");
		
		//Put them in the set
		ABILITIES.add(level3);
		ABILITIES.add(level7);
		ABILITIES.add(level11);
		ABILITIES.add(level14);
		
		//Set up various levels
		Modifier level1 = new Modifier();
		level1.getItemKeys().add("Slayer");
		level1.setValue(1);
		level1.setType(ModifierType.CLASS_LEVEL);
		LEVEL1.add(level1);
		
		Modifier level5 = new Modifier(level1);
		level5.setValue(5);
		LEVEL5.add(level5);
		
		Modifier level10 = new Modifier(level1);
		level10.setValue(10);
		LEVEL10.add(level10);
		
		Modifier level20 = new Modifier(level1);
		level20.setValue(20);
		LEVEL20.add(level20);
	}
	
	@Test
	public void testApplies_Bonuses(){
		Assert.assertFalse(BonusHelper.applies(level3, LEVEL1));
		Assert.assertTrue(BonusHelper.applies(level3, LEVEL5));
		Assert.assertTrue(BonusHelper.applies(level3, LEVEL10));
		Assert.assertTrue(BonusHelper.applies(level3, LEVEL20));
		
		Assert.assertFalse(BonusHelper.applies(level7, LEVEL1));
		Assert.assertFalse(BonusHelper.applies(level7, LEVEL5));
		Assert.assertTrue(BonusHelper.applies(level7, LEVEL10));
		Assert.assertTrue(BonusHelper.applies(level7, LEVEL20));
		
		Assert.assertFalse(BonusHelper.applies(level11, LEVEL1));
		Assert.assertFalse(BonusHelper.applies(level11, LEVEL5));
		Assert.assertFalse(BonusHelper.applies(level11, LEVEL10));
		Assert.assertTrue(BonusHelper.applies(level11, LEVEL20));
		
		Assert.assertFalse(BonusHelper.applies(level14, LEVEL1));
		Assert.assertFalse(BonusHelper.applies(level14, LEVEL5));
		Assert.assertFalse(BonusHelper.applies(level14, LEVEL10));
		Assert.assertTrue(BonusHelper.applies(level14, LEVEL20));
	}
	
	@Test
	public void testApplies_SpecialAbilities(){
		ClassWrapper fighter = new ClassWrapper("fighter", 1);
		ClassWrapper slayer = new ClassWrapper("slayer", 3);
		BonusProviderSet<ClassWrapper> classes = new BonusProviderSet<ClassWrapper>();
		classes.add(fighter);
		classes.add(slayer);
		
		//Get abilities to test
		StaticSpecialAbility bravery = fighter.getData().getAbilities().get("Bravery");
		StaticSpecialAbility weaponTraining = fighter.getData().getAbilities().get("Weapon Training");
		StaticSpecialAbility weaponMastery = fighter.getData().getAbilities().get("Weapon Mastery");
		
		//Test before & after
		fighter.setLevels(1);
		Assert.assertFalse(BonusHelper.applies(bravery, classes.getModifiers()));
		fighter.setLevels(2);
		Assert.assertTrue(BonusHelper.applies(bravery, classes.getModifiers()));
		
		Assert.assertFalse(BonusHelper.applies(weaponTraining, classes.getModifiers()));
		fighter.setLevels(5);
		Assert.assertTrue(BonusHelper.applies(weaponTraining, classes.getModifiers()));
		
		Assert.assertFalse(BonusHelper.applies(weaponMastery, classes.getModifiers()));
		fighter.setLevels(20);
		Assert.assertTrue(BonusHelper.applies(weaponMastery, classes.getModifiers()));
	}
	
	@Test
	public void testApplies_Talents(){
		ClassWrapper fighter = new ClassWrapper("fighter", 11);
		BonusProviderSet<ClassWrapper> classes = new BonusProviderSet<ClassWrapper>();
		classes.add(fighter);
		
		//Add talents
		TalentWrapper weaponTraining1 = new TalentWrapper("Weapon Training 1");
		weaponTraining1.setClassName("fighter");
		weaponTraining1.getItemKeys().add("TARGET");
		fighter.getTalents().add(weaponTraining1);
		
		//Test before & after
		Assert.assertTrue(BonusHelper.applies(weaponTraining1, classes.getModifiers()));
		Assert.assertTrue(BonusHelper.applies(weaponTraining1.getData(), classes.getModifiers()));
	}
	
	@Test
	public void testFilterModifiers(){
		ClassWrapper fighter = new ClassWrapper("fighter", 1);
		ClassWrapper slayer = new ClassWrapper("slayer", 3);
		BonusProviderSet<ClassWrapper> classes = new BonusProviderSet<ClassWrapper>();
		classes.add(fighter);
		classes.add(slayer);
		
		//Test filter
		StaticSpecialAbility bravery = fighter.getData().getAbilities().get("Bravery");
		Assert.assertEquals(1, BonusHelper.filterModifiers(classes.getModifiers(), bravery).size());
	}
	
	@Test
	public void testCalculateBonuses_FilterByName() {
		//Set up class
		ClassWrapper fighter = new ClassWrapper("fighter", 1);
		
		//Set up modifiers
		Modifier modifier = new Modifier();
		modifier.setType(ModifierType.CLASS_LEVEL);
		modifier.getItemKeys().add("Armor Training");
		modifier.setValue(4);
		
		Collection<Modifier> modifiers = fighter.getModifiers();
		modifiers.add(modifier);
		
		//Get abilities to test
		StaticSpecialAbility armorTraining = fighter.getData().getAbilities().get("Armor Training");
		
		//Test at 1 (does not apply)
		Collection<UnmodifiableBonus> bonuses = BonusHelper.calculateBonuses(armorTraining.getBonuses(modifiers), modifiers);
		Assert.assertEquals(1, BonusHelper.filterBonusesByType(bonuses, BonusType.ARMOR_CHECK).iterator().next().getValue());
	}
	
	@Test
	public void testCalculateBonuses_SteppedBonus_ClassLevel() {
		ClassWrapper fighter = new ClassWrapper("fighter", 1);
//		ClassWrapper slayer = new ClassWrapper("slayer", 10);
		BonusProviderSet<ClassWrapper> classes = new BonusProviderSet<ClassWrapper>();
		classes.add(fighter);
//		classes.add(slayer);
		
		//Get abilities to test
		StaticSpecialAbility armorTraining = fighter.getData().getAbilities().get("Armor Training");
		
		//Test at 1 (does not apply)
		Collection<UnmodifiableBonus> bonuses = BonusHelper.calculateBonuses(armorTraining.getBonuses(classes.getModifiers()), classes.getModifiers());
		Assert.assertEquals(0, BonusHelper.filterBonusesByType(bonuses, BonusType.ARMOR_CHECK).size());
		Assert.assertEquals(0, BonusHelper.filterBonusesByType(bonuses, BonusType.MAX_DEX).size());
		Assert.assertEquals(0, BonusHelper.filterBonusesByType(bonuses, BonusType.SPEED).size());
		
		//Test at 3 (applies level)
		fighter.setLevels(3);
		bonuses = BonusHelper.calculateBonuses(armorTraining.getBonuses(classes.getModifiers()), classes.getModifiers());
		Assert.assertEquals("Incorrect Armor Check bonus.", 1, BonusHelper.filterBonusesByType(bonuses, BonusType.ARMOR_CHECK).iterator().next().getValue());
		Assert.assertEquals("Incorrect Max Dex bonus.", 1, BonusHelper.filterBonusesByType(bonuses, BonusType.MAX_DEX).iterator().next().getValue());
		Assert.assertEquals("Incorrect number of Speed bonuses.", 1, BonusHelper.filterBonusesByType(bonuses, BonusType.SPEED).size());
		
		//Test at 7 (first step & second speed bonus)
		fighter.setLevels(7);
		bonuses = BonusHelper.calculateBonuses(armorTraining.getBonuses(classes.getModifiers()), classes.getModifiers());
		Assert.assertEquals(2, BonusHelper.filterBonusesByType(bonuses, BonusType.ARMOR_CHECK).iterator().next().getValue());
		Assert.assertEquals(2, BonusHelper.filterBonusesByType(bonuses, BonusType.MAX_DEX).iterator().next().getValue());
		Assert.assertEquals(2, BonusHelper.filterBonusesByType(bonuses, BonusType.SPEED).size());
		
		//Test at 15 (third step)
		fighter.setLevels(15);
		bonuses = BonusHelper.calculateBonuses(armorTraining.getBonuses(classes.getModifiers()), classes.getModifiers());
		Assert.assertEquals(4, BonusHelper.filterBonusesByType(bonuses, BonusType.ARMOR_CHECK).iterator().next().getValue());
		Assert.assertEquals(4, BonusHelper.filterBonusesByType(bonuses, BonusType.MAX_DEX).iterator().next().getValue());
		Assert.assertEquals(2, BonusHelper.filterBonusesByType(bonuses, BonusType.SPEED).size());
	}
	
	@Test
	public void testCalculateBonuses_SteppedBonus_BAB() {
		ClassWrapper fighter = new ClassWrapper("fighter", 1);
		BonusProviderSet<ClassWrapper> classes = new BonusProviderSet<ClassWrapper>();
		classes.add(fighter);
		
		//Get abilities to test
		FeatWrapper powerAttack = new FeatWrapper("Power Attack");
		
		//Test at 1
		Collection<UnmodifiableBonus> bonuses = BonusHelper.calculateBonuses(powerAttack.getBonuses(classes.getModifiers()), classes.getModifiers());
		UnmodifiableBonus bonus = bonuses.iterator().next();
		Assert.assertTrue(bonus.getCondition().startsWith("-1"));
		
		//Test at 3
		fighter.setLevels(3);
		bonuses = BonusHelper.calculateBonuses(powerAttack.getBonuses(classes.getModifiers()), classes.getModifiers());
		bonus = bonuses.iterator().next();
		Assert.assertTrue(bonus.getCondition().startsWith("-1"));
		
		//Test at 4
		fighter.setLevels(4);
		bonuses = BonusHelper.calculateBonuses(powerAttack.getBonuses(classes.getModifiers()), classes.getModifiers());
		bonus = bonuses.iterator().next();
		Assert.assertTrue(bonus.getCondition().startsWith("-2"));
		
		//Test at 12
		fighter.setLevels(12);
		bonuses = BonusHelper.calculateBonuses(powerAttack.getBonuses(classes.getModifiers()), classes.getModifiers());
		bonus = bonuses.iterator().next();
		Assert.assertTrue(bonus.getCondition().startsWith("-4"));
	}
	
	@Test
	public void testCalculateBonuses_SteppedBonus_SkillRank() {
		String skillName = "Intimidate";
		
		//Set up abilities to test
		FeatWrapper skillFocus = new FeatWrapper("Skill Focus");
		skillFocus.getItemKeys().add(skillName);
		
		//Set up skill
		DD3ESkill skill = new DD3ESkill(skillName, AttributeName.CHA, false);
		skill.setRanks(1);
		Collection<Skill> skills = new HashSet<Skill>();
		skills.add(skill);
		
		//Set up provider
		BonusProvider provider = new SkillModifierProvider(skills);
		
		//Test with 1 rank modifier
		Collection<UnmodifiableBonus> bonuses = BonusHelper.calculateBonuses(skillFocus.getBonuses(provider.getModifiers()), provider.getModifiers());
		Bonus bonus = bonuses.iterator().next();
		Assert.assertEquals(3, bonus.getValue());
		
		//Test with 10 rank modifier
		skill.setRanks(10);
		provider = new SkillModifierProvider(skills);
		bonuses = BonusHelper.calculateBonuses(skillFocus.getBonuses(provider.getModifiers()), provider.getModifiers());
		bonus = bonuses.iterator().next();
		Assert.assertEquals(6, bonus.getValue());
	}
	
	@Test
	public void testCalculateBonus_Filtering() {
		//Get the bonuses & test
		Collection<ModifiableBonus> bonuses1 = ABILITIES.getBonuses(LEVEL1);
		Assert.assertEquals(0, bonuses1.size());
		Collection<ModifiableBonus> bonuses5 = ABILITIES.getBonuses(LEVEL5);
		Assert.assertEquals(1, bonuses5.size());
		Collection<ModifiableBonus> bonuses10 = ABILITIES.getBonuses(LEVEL10);
		Assert.assertEquals(2, bonuses10.size());
		Collection<ModifiableBonus> bonuses20 = ABILITIES.getBonuses(LEVEL20);
		Assert.assertEquals(3, bonuses20.size());//Swift Tracker has no bonuses.
	}
	
	@Test
	public void testOptional(){
		FeatWrapper powerAttack = new FeatWrapper("Power Attack");
		Assert.assertFalse(CollectionUtils.isEmpty(powerAttack.getOptional(LEVEL20).getBonuses()));
	}
}
