package com.westpalmetto.gaming.character;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Lists;

@Ignore
public class BlueRiderRoller {
	private Random generator = new Random(System.currentTimeMillis());

	@Test
	public void roll() {
		for(int set=1; set<=10; set++){
			System.out.println("Set " + set);
			for(int i=0; i<6; i++){
				rollAttribute();
			}
			System.out.println("\n");
		}
	}

	private void rollAttribute() {
		//4d6
		List<Integer> dice = Lists.newArrayList(d6(), d6(), d6(), d6());
		
		//Reroll a single 1
		if(dice.contains(1)){
			dice.add(d6());
		}
		
		System.out.println(dice + "; Score: " + bestThree(dice));
	}

	private int bestThree(List<Integer> dice) {
		ArrayList<Integer> copy = new ArrayList<Integer>(dice);
		Collections.sort(copy);
		Collections.reverse(copy);
		return copy.get(0) + copy.get(1) + copy.get(2);
	}

	private int d6() {
		return generator.nextInt(6)+1;
	}
}
