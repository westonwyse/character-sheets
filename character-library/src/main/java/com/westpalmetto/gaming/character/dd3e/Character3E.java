/**
 * Character.java
 * 
 * Copyright � 2012 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.dd3e;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.bonuses.KeyedObjectKeyPredicate;
import com.westpalmetto.gaming.character.bonuses.OptionalAttackPredicate;
import com.westpalmetto.gaming.character.bonuses.converter.SkillModifierProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.KeyedSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass.BaseSavingThrow;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.Charge;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.CombatType;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.Heft;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FeatWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SizeWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment.Hand;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Ammunition;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.OffensiveArmor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.Attack;
import com.westpalmetto.gaming.character.model.Damage;
import com.westpalmetto.gaming.character.model.Skill;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.model.predicate.EquippedItemPredicate;
import com.westpalmetto.gaming.character.model.predicate.HeldWeaponPredicate;
import com.westpalmetto.gaming.character.model.predicate.SpellcastingClassPredicate;

import lombok.extern.slf4j.Slf4j;

/**
 * Character is the mediator object for the character's data.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public abstract class Character3E extends Character<DD3EModel>{
    // Any bonus that should be applied per level instead of once.
    public static final String ITEMKEY_PERLEVEL = "PER_LEVEL";

    public static final String ITEMKEY_AC_ARMOR = "ARMOR_BONUS";
    public static final String ITEMKEY_AC_SHIELD = "SHIELD_BONUS";
    public static final String ITEMKEY_AC_NATURALARMOR = "NATURAL ARMOR";
    public static final String ITEMKEY_AC_DEFLECTION = "DEFLECTION";

    // Any misc bonus that is lost when flat-footed
    public static final String ITEMKEY_AC_TOUCH = "TOUCH";
    public static final String ITEMKEY_AC_FLATFOOTED = "FLATFOOTED";// Any misc
                                                                    // bonus
                                                                    // that is
                                                                    // lost v.
                                                                    // touch
    public static final String ITEMKEY_AC_MISC = "MISC";// Any misc bonus that
                                                        // always applies

    public static final String ITEMKEY_ATTACK_ATTACK = "ATTACK";// Any bonus
                                                                // that always
                                                                // applies to
                                                                // attacks
    public static final String ITEMKEY_ATTACK_MELEE = "MELEE";// Any bonus that
                                                              // applies to
                                                              // melee attacks
    public static final String ITEMKEY_ATTACK_RANGED = "RANGED";// Any bonus
                                                                // that applies
                                                                // to ranged
                                                                // attacks
    public static final String ITEMKEY_ATTACK_WEAPONFINESSE = "WEAPON_FINESSE";// Special
                                                                               // key
                                                                               // to
                                                                               // flag
                                                                               // if
                                                                               // the
                                                                               // character
                                                                               // has
                                                                               // the
                                                                               // Weapon
                                                                               // Finesse
                                                                               // feat.
    public static final String ITEMKEY_ATTACK_TWOWEAPON = "TWO_WEAPON_FIGHTING";// Special
                                                                                // key
                                                                                // to
                                                                                // flag
                                                                                // if
                                                                                // the
                                                                                // character
                                                                                // has
                                                                                // the
                                                                                // Two-Weapon
                                                                                // Fighting
                                                                                // feat.

    public static final String NAME_ATTACK_IMPROVEDCRITICAL = "Improved Critical";// Special
                                                                                  // key
                                                                                  // to
                                                                                  // flag
                                                                                  // if
                                                                                  // the
                                                                                  // character
                                                                                  // has
                                                                                  // the
                                                                                  // Improved
                                                                                  // Critical
                                                                                  // feat
    public static final String NAME_ATTACK_WEAPONTRAINING = "Weapon Training";// Special
                                                                              // key
                                                                              // to
                                                                              // flag
                                                                              // if
                                                                              // the
                                                                              // character
                                                                              // has
                                                                              // Fighter
                                                                              // Weapon
                                                                              // Training

    public static final String NAME_ARMOR_SHIELDFOCUS = "Shield Focus";// Special
                                                                       // key to
                                                                       // flag
                                                                       // if the
                                                                       // character
                                                                       // has
                                                                       // the
                                                                       // Shield
                                                                       // Focus
                                                                       // feat

    public static final String ITEMKEY_ARMORCHECK_ARMOR = "ARMOR";
    public static final String ITEMKEY_ARMORCHECK_SHIELD = "SHIELD";

    public static final String ITEMKEY_OTHER_IGNOREENCUMBRANCE = "IGNORE_ENCUMBRANCE";// Special
                                                                                      // key
                                                                                      // to
                                                                                      // flag
                                                                                      // if
                                                                                      // the
                                                                                      // character
                                                                                      // ignores
                                                                                      // encumbrance
                                                                                      // penalties.

    public static final String NAME_OTHER_DEIFICOBEDIENCE = "Deific Obedience";// Special
                                                                               // key
                                                                               // to
                                                                               // flag
                                                                               // if
                                                                               // the
                                                                               // character
                                                                               // gains
                                                                               // deific
                                                                               // obedience
                                                                               // bonuses.
    public static final String NAME_OTHER_SHIELDMASTER = "Shield Master";// Special
                                                                         // key
                                                                         // to
                                                                         // flag
                                                                         // if
                                                                         // the
                                                                         // character
                                                                         // has
                                                                         // the
                                                                         // Shield
                                                                         // Master
                                                                         // feat.

    public static final String ID_CLASS_CLERIC = "Cleric";// Special key to flag
                                                          // if the character
                                                          // gains cleric
                                                          // abilities.

    private static final BigDecimal[] CARRYINGCAPACITY_LIST = { new BigDecimal("0"),
            new BigDecimal("3.33333"),
            new BigDecimal("6.66666"),
            new BigDecimal("10"),
            new BigDecimal("13.33333"),
            new BigDecimal("16.66666"),
            new BigDecimal("20"),
            new BigDecimal("23.33333"),
            new BigDecimal("26.66666"),
            new BigDecimal("30"),
            new BigDecimal("33.33333"),
            new BigDecimal("38.33333"),
            new BigDecimal("43.33333"),
            new BigDecimal("50"),
            new BigDecimal("58.33333"),
            new BigDecimal("66.66666"),
            new BigDecimal("76.66666"),
            new BigDecimal("86.66666"),
            new BigDecimal("100"),
            new BigDecimal("116.66666") };
    private static final BigDecimal CARRYINGCAPACITY_MULTIPLIER_OVER = new BigDecimal(4);
    private static final BigDecimal CARRYINGCAPACITY_COMPARATOR_ROUNDUP = new BigDecimal("0.7");

    public enum CarryingCapacity{
        LIGHT(new BigDecimal(1), 0, null),
        MEDIUM(new BigDecimal(2), -3, 3),
        HEAVY(new BigDecimal(3), -6, 1),
        OVER_HEAD(new BigDecimal(3), 0, null),
        OFF_GROUND(new BigDecimal(6), 0, null),
        PUSH_DRAG(new BigDecimal(15), 0, null);

        private BigDecimal multiplier;
        private int checkPenalty;
        private Integer maxDex;

        private CarryingCapacity(BigDecimal multiplier, int checkPenalty, Integer maxDex){
            this.multiplier = multiplier;
            this.checkPenalty = checkPenalty;
            this.maxDex = maxDex;
        }

        /**
         * @return The multipler to apply to the base carrying capacity for a
         *         given STR
         */
        public BigDecimal getMultiplier(){
            return this.multiplier;
        }

        /**
         * @return the checkPenalty
         */
        public int getCheckPenalty(){
            return checkPenalty;
        }

        /**
         * @return the maxDex
         */
        public Integer getMaxDex(){
            return maxDex;
        }

        /**
         * Calculates the maximum weight for a given STR value.
         * 
         * @param str The STR value to calculate against
         * @return The maximum weight for this carrying capacity type
         */
        protected int getCarryingCapacity(int str){
            // Calculate exact raw capacity
            BigDecimal rawValue = getRawCarryingCapacity(str).multiply(getMultiplier());

            // Round as appropriate
            int intValue = rawValue.intValue();
            BigDecimal decimal = rawValue.subtract(new BigDecimal(intValue));
            if (decimal.compareTo(CARRYINGCAPACITY_COMPARATOR_ROUNDUP) >= 0) {
                intValue++;
            }

            // Return the int value
            return intValue;
        }

        /**
         * Calculates the raw carrying capacity for a given STR value.
         * 
         * @param str The STR value against which to calculate
         * @return The raw value of the light carrying capacity for this STR
         *         value
         */
        protected BigDecimal getRawCarryingCapacity(int str){
            BigDecimal result;

            // Check for values above the chart
            if (str >= CARRYINGCAPACITY_LIST.length) {
                result = CARRYINGCAPACITY_MULTIPLIER_OVER.multiply(getRawCarryingCapacity(str - 10));
            } else {
                result = CARRYINGCAPACITY_LIST[str];
            }

            return result;
        }

        /**
         * Returns the carrying capacity appropriate for the given weight and
         * strength.
         * 
         * @param weight The weight of items carried
         * @param str The character's STR value
         * @return The CarryingCapacity entry appropriate
         */
        public static CarryingCapacity valueForWeight(int weight, int str){
            int light = LIGHT.getCarryingCapacity(str);
            int medium = MEDIUM.getCarryingCapacity(str);
            CarryingCapacity capacity;

            if (weight <= light) {
                capacity = LIGHT;
            } else if (weight <= medium) {
                capacity = MEDIUM;
            } else {
                capacity = HEAVY;
            }

            return capacity;
        }
    }

    private NamedObjectSet<DD3ESkill> calculatedSkills = null;

    /**
     * @param data The CharacterModel object to use for this character
     */
    public Character3E(DD3EModel data){
        super(data);
    }

    @Override
    public File getResourceRoot(){
        return ResourceFactory.Ruleset.PSRD.getRoot();
    }

    @Override
    protected Collection<BonusProvider> getBonusProviders(){
        Collection<BonusProvider> providers = getFixedBonusProviders();

        // Add bonus feats
        providers.add(getBonusFeats(providers));

        // Add modifiers from skills
        providers.add(getSkillModifiers(providers));

        return providers;
    }

    @Override
    protected Collection<BonusProvider> getFixedBonusProviders(){
        Collection<BonusProvider> providers = new HashSet<BonusProvider>();
        providers.add(getData().getRace());
        providers.add(getData().getFeats());
        providers.add(getData().getBoons());
        providers.addAll(getData().getClasses());

        if (getData().getFamiliar() != null) {
            providers.add(getData().getFamiliar());
        }

        // Only add bonuses from equipped items
        EquipmentSet equipment = new EquipmentSet();
        CollectionUtils.select(getData().getEquipment(), EquippedItemPredicate.getInstance(), equipment);
        providers.addAll(equipment);

        return providers;
    }

    public NamedObjectSet<DD3ESkill> getSkills(){
        return getSkills(getFixedBonusProviders());
    }

    public NamedObjectSet<DD3ESkill> getSkills(Collection<BonusProvider> providers){
        if (calculatedSkills == null) {
            // Get default skills for this character
            calculatedSkills = getDefaultSkills();

            // Get class skills
            for (UnmodifiableBonus bonus : getCalculatedBonuses(BonusType.CLASS_SKILL,
                    providers,
                    getModifiers(providers))) {
                for (String name : bonus.getItemKeys()) {
                    DD3ESkill skill = calculatedSkills.get(name);
                    if (skill != null) {
                        skill.setClassSkill(true);
                    } else {
                        if (!BonusType.CLASS_SKILL.getName().equals(name)) {
                            log.warn("Could not find skill for item key '{}'", name);
                        }
                    }
                }
            }

            // Get skill ranks
            for (DD3ESkill ranks : getData().getSkills()) {
                // Handle default skills
                DD3ESkill skill = calculatedSkills.get(ranks.getName());
                if ((skill != null) && (ranks.getRanks() > 0)) {
                    skill.setRanks(ranks.getRanks());
                } else if (ranks.getRanks() > 0) {
                    // Handle multiple-choice skills, like Craft
                    if (StringUtils.contains(ranks.getName(), ":")) {
                        DD3ESkill target = calculatedSkills.get(StringUtils.substringBefore(ranks.getName(), ":"));

                        if (target != null) {
                            skill = new DD3ESkill(target);
                            skill.setName(ranks.getName());
                            skill.setRanks(ranks.getRanks());
                            calculatedSkills.add(skill);
                        } else {
                            log.warn("Could not find common skill for '{}'", ranks.getName());
                        }
                    } else {
                        log.warn("Could not find skill named '{}' for ranks {}", ranks.getName(), ranks.getRanks());
                    }
                }
            }
        }

        return calculatedSkills;
    }

    protected NamedObjectSet<DD3ESkill> getDefaultSkills(){
        return new NamedObjectSet<DD3ESkill>();
    }

    public BonusProvider getSkillModifiers(Collection<BonusProvider> providers){
        return new SkillModifierProvider(getSkills(providers));
    }

    /**
     * Finds all the character's feats.
     * 
     * @return The character's feats
     */
    public BonusProviderSet<FeatWrapper> getFeats(){
        BonusProviderSet<FeatWrapper> feats = new BonusProviderSet<FeatWrapper>();
        feats.addAll(getData().getFeats());
        feats.addAll(getBonusFeats());
        return feats;
    }

    /**
     * Finds any bonus feats for the character.
     * 
     * @return Any discovered bonus feats
     */
    private BonusProviderSet<FeatWrapper> getBonusFeats(){
        return getBonusFeats(getFixedBonusProviders());
    }

    /**
     * Finds any bonus feats contained in the given bonus providers.
     * 
     * @param providers The bonus providers.
     * @return Any discovered bonus feats
     */
    private BonusProviderSet<FeatWrapper> getBonusFeats(Collection<BonusProvider> providers){
        // Collect all of the bonus feat bonuses
        Collection<Bonus> featBonuses = collectBonuses(BonusType.FEAT, providers, null);
        log.trace("Bonus feats: {}", featBonuses);

        // Transform the bonuses into feat objects
        BonusProviderSet<FeatWrapper> feats = new BonusProviderSet<FeatWrapper>();
        for (Bonus bonus : featBonuses) {
            FeatWrapper feat = null;
            Set<String> keys = new HashSet<String>(bonus.getItemKeys());

            // Validate that keys exist
            if (CollectionUtils.isEmpty(keys)) {
                // TODO: Exceptions
                throw new RuntimeException("Item keys are empty within: " + featBonuses);
            }

            // Cycle through each of the item keys looking for a valid one
            for (String id : keys) {
                if (FeatWrapper.isValid(id, getResourceRoot())) {
                    log.trace("Found bonus feat for {} in {}", id, getResourceRoot());
                    feat = new FeatWrapper(id, getResourceRoot());
                    break;
                }
            }

            // If one wasn't found, there is a problem
            if (feat == null) {
                log.error("Could not find feat for {} in {}", keys, FeatWrapper.getKnownKeys(getResourceRoot()));
                // TODO: Exceptions
                throw new RuntimeException("Could not find feat for: '" + bonus + "' at " + getResourceRoot());
            } else {
                // Persist any additional item keys
                keys.remove(feat.getId());
                feat.setItemKeys(keys);

                // Save feat
                feats.add(feat);
            }
        }

        return feats;
    }

    /**
     * @return The total number of levels this character has
     */
    public int getCharacterLevels(){
        return getData().getAdvancement().getLevel(getData().getExperience().intValue());
    }

    /**
     * Returns the attribute bonus for a requested attribute.
     * 
     * @param attribute The attribute for which to return the bonus.
     * @return The bonus for the given attribute.
     */
    public int getAttributeBonus(AttributeName attribute){
        return getAttributeBonus(attribute, true);
    }

    /**
     * Returns the attribute bonus for a requested attribute, applying the Max.
     * Dex limit, if applicable.
     * 
     * @param attribute The attribute for which to return the bonus.
     * @param applyMaxDex Whether or not to apply the Max. Dex limit
     * @return The bonus for the given attribute.
     */
    public int getAttributeBonus(AttributeName attribute, boolean applyMaxDex){
        long start = System.currentTimeMillis();

        int score = getAttributeTotalScore(attribute);
        int bonus = getAttributeModifier(score);

        // Check for max dex
        if (applyMaxDex && AttributeName.DEX.equals(attribute)) {
            Integer maxDex = getMaxDex();
            if ((maxDex != null) && (bonus > maxDex.intValue())) {
                bonus = maxDex;
            }
        }

        log.trace("getAttributeBonus - {}: {}ms", attribute, System.currentTimeMillis() - start);
        return bonus;
    }

    public int getAttributeModifier(int score){
        return (score / 2) - 5;
    }

    public int getAttributeScore(AttributeName name){
        Attribute attribute = getData().getAttributes().get(name);
        Attribute increases = getData().getAttributeIncreases().get(name);
        Attribute inherentBonuses = getData().getInherentBonuses().get(name);
        return attribute.getScore() + increases.getScore()
                + inherentBonuses.getScore()
                + getTotalSpecialBonuses(BonusType.ATTRIBUTE_CORE, name.toString());
    }

    public int getAttributeTotalScore(AttributeName name){
        return getAttributeScore(name) + getTotalSpecialBonuses(BonusType.ATTRIBUTE, name.toString());
    }

    protected Integer getMaxDex(){
        Integer maxDex = null;
        Integer armorMax = getArmorMaxDex();
        Integer encumberanceMax = getCarryingCapacity().getMaxDex();

        // Determine the max dex
        if ((armorMax != null) && (encumberanceMax == null)) {
            maxDex = armorMax;
        } else if ((armorMax == null) && (encumberanceMax != null)) {
            maxDex = encumberanceMax;
        } else if ((armorMax != null) && (encumberanceMax != null)) {
            if (armorMax.intValue() < encumberanceMax.intValue()) {
                maxDex = armorMax;
            } else {
                maxDex = encumberanceMax;
            }
        }

        return maxDex;
    }

    /**
     * @return the hitPoints
     */
    public int getHitPoints(){
        long start = System.currentTimeMillis();

        int hp = 0;

        // Get per-level HP
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            long startClass = System.currentTimeMillis();

            int i = 0;
            for (int hd : getHitDice(cls)) {
                long startHD = System.currentTimeMillis();

                hd += getAttributeBonus(AttributeName.CON);
                hd += getTotalSpecialBonuses(BonusType.HIT_POINT, ITEMKEY_PERLEVEL);

                // Verify no negative numbers
                if (hd < 1) {
                    hd = 1;
                }

                // Add to running total
                hp += hd;

                log.trace("getHitPoints - {} - HD{}: {}ms", cls.getName(), i, System.currentTimeMillis() - startHD);
                i++;
            }

            log.trace("getHitPoints - {}: {}ms", cls.getName(), System.currentTimeMillis() - startClass);
        }

        // Get any flat-rate bonus HP
        hp += (getTotalSpecialBonuses(BonusType.HIT_POINT)
                - getTotalSpecialBonuses(BonusType.HIT_POINT, ITEMKEY_PERLEVEL));

        log.trace("getHitPoints: {}ms", System.currentTimeMillis() - start);
        return hp;
    }

    /**
     * For a given character class, determine the hit dice for that class.
     * 
     * @param cls The character class.
     * @return The hit dice for this character, for the given class.
     */
    protected List<Integer> getHitDice(ClassWrapper cls){
        List<Integer> hitDice = null;

        switch (getData().getHpType()) {
        case SPECIFIED:
            hitDice = cls.getHitDice();
            break;
        case CALCULATED:
            hitDice = createHitDice(cls.getLevels(), cls.getData().getHitDie(), cls.isInitialClass());
            break;
        }

        return hitDice;
    }

    /**
     * Creates a hit dice array for the given number of levels and die type, for
     * the average hit die result per level.
     * 
     * @param levels The number of levels to generate
     * @param hitDie The hit die to use to generate
     * @param firstLevelFull Whether or not the first level should be at full
     *        HP.
     * @return The hit dice.
     */
    protected List<Integer> createHitDice(int levels, BigDecimal hitDie, boolean firstLevelFull){
        // Calculate the average HP per level
        int average =
                hitDie.divide(new BigDecimal(2)).add(new BigDecimal(0.5)).setScale(0, RoundingMode.CEILING).intValue();

        // Create the list
        List<Integer> hitDice = new ArrayList<Integer>(levels);
        for (int i = 0; i < levels; i++) {
            if ((i == 0) && firstLevelFull) {
                hitDice.add(hitDie.intValue());
            } else {
                hitDice.add(average);
            }
        }

        // Return the result
        return hitDice;
    }

    /**
     * Calculates a skill's total bonus.
     * 
     * @param skill The skill in question.
     * @return The value of the skill modifier after all additions.
     */
    public int getSkillBonus(DD3ESkill skill){
        return getAttributeBonus(skill.getAttributeName()) + skill.getRanks() + getSkillMiscModifiers(skill);
    }

    /**
     * Calculates a skill's miscellaneous modifiers.
     * 
     * @param skill The skill in question.
     * @return The total value of the miscellaneous skill modifiers.
     */
    public int getSkillMiscModifiers(DD3ESkill skill){
        int bonus = getTotalSpecialBonuses(BonusType.SKILL, skill.getName());

        // Check for armor penalty
        AttributeName attribute = skill.getAttributeName();
        if (AttributeName.DEX.equals(attribute) || AttributeName.STR.equals(attribute)) {
            int armorPenalty = getTotalArmorPenalty();
            int carryingPenalty = getCarryingCapacityCheckPenalty();

            if (armorPenalty < carryingPenalty) {
                bonus += armorPenalty;
            } else {
                bonus += carryingPenalty;
            }
        }

        // Return total bonus
        return bonus;
    }

    /**
     * @return The total number of skill points the character has earned.
     */
    public int getEarnedSkillRanks(){
        int ranks = 0;

        // Get per-level ranks
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            int perLevel = cls.getData().getSkillRanksPerLevel() + getAttributeBonus(AttributeName.INT)
                    + getTotalSpecialBonuses(BonusType.SKILL_POINT, ITEMKEY_PERLEVEL);
            ranks += (perLevel * cls.getLevels());
        }

        // Get any flat-figure bonus ranks
        ranks += (getTotalSpecialBonuses(BonusType.SKILL_POINT)
                - getTotalSpecialBonuses(BonusType.SKILL_POINT, ITEMKEY_PERLEVEL));

        return ranks;
    }

    /**
     * @return The total number of skill points the character has assigned.
     */
    public int getUsedSkillRanks(){
        int ranks = 0;

        NamedObjectSet<DD3ESkill> skills = getData().getSkills();
        for (Skill skill : skills) {
            ranks += skill.getRanks();
        }

        return ranks;
    }

    /**
     * @return The total number of feats the character has earned.
     */
    public int getEarnedFeats(){
        int feats = 0;

        // Get per-level feats
        BigDecimal level = new BigDecimal(getData().getAdvancement().getLevel(getData().getExperience().intValue()));
        feats = level.divide(new BigDecimal(2), RoundingMode.UP).intValue();

        // Get any bonus feats
        feats += getTotalSpecialBonuses(BonusType.FEAT_COUNT);

        return feats;
    }

    /**
     * @return The total number of feats the character has assigned.
     */
    public int getUsedFeats(){
        return getFeats().size();
    }

    public int getEarnedTalents(ClassWrapper clz){
        BonusType type = BonusType.TALENT_COUNT;
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(type, ImmutableList.of(clz));
        return getTotalSpecialBonuses(type, bonuses);
    }

    public int getUsedTalents(ClassWrapper clz){
        return clz.getTalents().size();
    }

    /**
     * @return The total number of attribute increases the character has earned.
     */
    public int getEarnedAttributeIncreases(){
        int increases = 0;

        // Get per-level feats
        BigDecimal level = new BigDecimal(getData().getAdvancement().getLevel(getData().getExperience().intValue()));
        increases = level.divide(new BigDecimal(4), RoundingMode.DOWN).intValue();

        return increases;
    }

    /**
     * @return The total number of attribute increases the character has
     *         assigned.
     */
    public int getUsedAttributeIncreases(){
        return getData().getAttributeIncreases().totalScores();
    }

    /**
     * Calculates a character's armor check penalty.
     * 
     * @return the armor check penalty
     */
    public int getTotalArmorPenalty(){
        Set<Armor> armors = getData().getEquipment().getArmorSet();
        int penalty = 0;

        for (Armor armor : armors) {
            penalty += getTotalArmorPenalty(armor);
        }

        return penalty;
    }

    /**
     * Calculates an armor's check penalty.
     * 
     * @return the armor check penalty
     */
    public int getTotalArmorPenalty(Armor armor){
        int penalty;

        if (armor.getData().getProficiency().isArmor()) {
            penalty = armor.getModifiedCheckPenalty(getArmorCheckBonusArmor());
        } else {
            penalty = armor.getModifiedCheckPenalty(getArmorCheckBonusShield());
        }

        return penalty;
    }

    /**
     * @return The worst Max. Dex value for equipped armor; null if no Max. Dex
     *         to be applied
     */
    public Integer getArmorMaxDex(){
        Set<Armor> armors = getData().getEquipment().getArmorSet();
        Integer maxDex = null;

        for (Armor armor : armors) {
            Integer armorMax = getArmorMaxDex(armor);
            if ((maxDex == null) || ((armorMax != null) && (maxDex.intValue() > armorMax.intValue()))) {
                maxDex = armorMax;
            }
        }

        return maxDex;
    }

    /**
     * @param armor The armor to inspect.
     * @return The Max. Dex value for the given armor; null if no Max. Dex to be
     *         applied
     */
    public Integer getArmorMaxDex(Armor armor){
        Integer max = null;
        if (armor.getData().getMaxDex() != null) {
            max = armor.getData().getMaxDex() + getTotalSpecialBonuses(BonusType.MAX_DEX);
        }

        return max;
    }

    /**
     * Calculates a character's total initiative bonus.
     * 
     * @return The character's total initiative bonus.
     */
    public int getInitiativeBonus(){
        return getAttributeBonus(AttributeName.DEX) + getInitiativeMiscModifiers();
    }

    /**
     * Calculates miscellaneous modifiers for initiative.
     * 
     * @return The total value of the miscellaneous initiative modifiers.
     */
    public int getInitiativeMiscModifiers(){
        return getTotalSpecialBonuses(BonusType.INITIATIVE);
    }

    public int getACBonus(){
        return 10 + getACArmorBonus()
                + getACShieldBonus()
                + getAttributeBonus(AttributeName.DEX, true)
                + getSizeModifier()
                + getACNaturalArmorBonus()
                + getACDeflectionBonus()
                + getACMiscBonus();
    }

    public int getACTouchBonus(){
        return 10 + getAttributeBonus(AttributeName.DEX, true)
                + getSizeModifier()
                + getACDeflectionBonus()
                + getACTouchMiscBonus();
    }

    public int getACFlatFootedBonus(){
        return 10 + getACArmorBonus()
                + getACShieldBonus()
                + getSizeModifier()
                + getACNaturalArmorBonus()
                + getACDeflectionBonus()
                + getACFlatFootedMiscBonus();
    }

    public int getACArmorBonus(){
        int best = 0;

        // Cycle through each armor
        for (Armor armor : getData().getEquipment().getArmorSet()) {
            if (armor.getData().getProficiency().isArmor()) {
                if (best < getArmorBonus(armor)) {
                    best = getArmorBonus(armor);
                }
            }
        }

        // Return the best bonus found
        return best;
    }

    public int getACShieldBonus(){
        int best = 0;

        // Cycle through each armor
        for (Armor armor : getData().getEquipment().getArmorSet()) {
            if (armor.getData().getProficiency().isShield()) {
                if (best < getArmorBonus(armor)) {
                    best = getArmorBonus(armor);
                }
            }
        }

        // Return the best bonus found
        return best;
    }

    /**
     * Gets the armor bonus for a certain armor for the character.
     * 
     * @param armor The armor to inspect.
     * @return The character's bonus for the armor.
     */
    public int getArmorBonus(Armor armor){
        int bonus = armor.getModifiedAcBonus();

        // Add shield focus, if applicable
        if (armor.getData().getProficiency().isShield()) {
            bonus += getShieldFocusBonus();
        }

        return bonus;
    }

    public int getACNaturalArmorBonus(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_NATURALARMOR);
    }

    public int getACDeflectionBonus(){
        return getGreatestSpecialBonus(BonusType.ARMOR_CLASS, ITEMKEY_AC_DEFLECTION);
    }

    public int getACTouchMiscBonus(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_TOUCH)
                + getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_MISC);
    }

    public int getACFlatFootedMiscBonus(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_FLATFOOTED)
                + getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_MISC);
    }

    public int getACMiscBonus(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_TOUCH)
                + getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_FLATFOOTED)
                + getTotalSpecialBonuses(BonusType.ARMOR_CLASS, ITEMKEY_AC_MISC);
    }

    /**
     * @return The character's Size Modifier.
     */
    public int getSizeModifier(){
        return getTotalSpecialBonuses(BonusType.SIZE);
    }

    public int getArmorCheckBonusArmor(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CHECK, ITEMKEY_ARMORCHECK_ARMOR);
    }

    public int getArmorCheckBonusShield(){
        return getTotalSpecialBonuses(BonusType.ARMOR_CHECK, ITEMKEY_ARMORCHECK_SHIELD);
    }

    public int getShieldFocusBonus(){
        return getCalculatedBonuses(BonusType.ARMOR_CLASS, NAME_ARMOR_SHIELDFOCUS).size();
    }

    /**
     * Calculates a character's total fortitude save bonus.
     * 
     * @return The character's total fortitude save bonus.
     */
    public int getFortitudeSaveTotal(){
        return getFortitudeBaseSave() + getAttributeBonus(AttributeName.CON) + getFortitudeSaveMiscModifiers();
    }

    /**
     * Calculates the total of all base Fortitude saves for all of a character's
     * classes.
     * 
     * @return The character's total base Fortitude save.
     */
    public int getFortitudeBaseSave(){
        return getTotalBaseSave(SavingThrow.FORTITUDE);
    }

    /**
     * Calculates miscellaneous modifiers for fortitude save.
     * 
     * @return The total value of the miscellaneous fortitude save modifiers.
     */
    public int getFortitudeSaveMiscModifiers(){
        return getTotalSpecialBonuses(BonusType.SAVING_THROW, SavingThrow.FORTITUDE);
    }

    /**
     * Calculates a character's total reflex save bonus.
     * 
     * @return The character's total reflex save bonus.
     */
    public int getReflexSaveTotal(){
        return getReflexBaseSave() + getAttributeBonus(AttributeName.DEX) + getReflexSaveMiscModifiers();
    }

    /**
     * Calculates the total of all base reflex saves for all of a character's
     * classes.
     * 
     * @return The character's total base reflex save.
     */
    public int getReflexBaseSave(){
        return getTotalBaseSave(SavingThrow.REFLEX);
    }

    /**
     * Calculates miscellaneous modifiers for reflex save.
     * 
     * @return The total value of the miscellaneous reflex save modifiers.
     */
    public int getReflexSaveMiscModifiers(){
        return getTotalSpecialBonuses(BonusType.SAVING_THROW, SavingThrow.REFLEX);
    }

    /**
     * Calculates a character's total will save bonus.
     * 
     * @return The character's total will save bonus.
     */
    public int getWillSaveTotal(){
        return getWillBaseSave() + getAttributeBonus(AttributeName.WIS) + getWillSaveMiscModifiers();
    }

    /**
     * Calculates the total of all base will saves for all of a character's
     * classes.
     * 
     * @return The character's total base will save.
     */
    public int getWillBaseSave(){
        return getTotalBaseSave(SavingThrow.WILL);
    }

    /**
     * Calculates miscellaneous modifiers for will save.
     * 
     * @return The total value of the miscellaneous will save modifiers.
     */
    public int getWillSaveMiscModifiers(){
        return getTotalSpecialBonuses(BonusType.SAVING_THROW, SavingThrow.WILL);
    }

    /**
     * Gets the total base save bonus for all classes for a given type.
     * 
     * @param type The type of saving throw to retrieve.
     * @return The total base saving throw.
     */
    protected int getTotalBaseSave(SavingThrow type){
        int bonus = 0;
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            bonus += getSave(type, cls);
        }
        return bonus;
    }

    /**
     * Gets the base save bonus for the specified saving throw type for a given
     * class.
     * 
     * @param type The type of saving throw.
     * @param cls The CLassLoader for the class in question
     * @return The base saving throw for the specified type and class.
     */
    protected int getSave(SavingThrow type, ClassWrapper cls){
        BaseSavingThrow save = cls.getData().getBaseSaves().get(type);
        if (save == null) {
            // TODO: Exceptions
            throw new RuntimeException("Base save is null for type " + type.toString());
        }
        return save.getBonus(cls.getLevels());
    }

    /**
     * Gets the total base attack bonus for all classes.
     * 
     * @return The total base attack bonus.
     */
    public int getBaseAttack(){
        int bonus = 0;
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            bonus += cls.getBaseAttackBonus();
        }
        return bonus;
    }

    public int getMeleeAttackTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.STR) + getSizeModifier() + getMeleeAttackMiscBonus();
    }

    public int getWeaponFinesseTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.DEX) + getSizeModifier() + getMeleeAttackMiscBonus();
    }

    public boolean ignoresEncumbrance(){
        return (getCalculatedBonuses(BonusType.OTHER, ITEMKEY_OTHER_IGNOREENCUMBRANCE).size() > 0);
    }

    public boolean hasWeaponFinesse(){
        return (getCalculatedBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_WEAPONFINESSE).size() > 0);
    }

    public boolean hasTwoWeaponFighting(){
        return (getCalculatedBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_TWOWEAPON).size() > 0);
    }

    public int getTwoWeaponAttackPrimaryBonus(){
        Weapon weapon = getHeldWeapon(Hand.PRIMARY);
        if (weapon == null) {
            // TODO: Exceptions
            throw new RuntimeException("Cannot calculate TWF bonus; not holding a weapon.");
        }

        return getWeaponAttackBonus(weapon) + getTwoWeaponPrimaryPenalty();
    }

    public int getTwoWeaponPrimaryPenalty(){
        int penalty = -6;
        if (hasTwoWeaponFighting()) {
            penalty = -4;
        }

        // Bonus for a light offhand weapon
        if (isOffHandLight()) {
            penalty += 2;
        }

        return penalty;
    }

    public int getTwoWeaponAttackOffHandBonus(){
        Weapon weapon = getHeldWeapon(Hand.OFF);
        if (weapon == null) {
            // TODO: Exceptions
            throw new RuntimeException("Cannot calculate TWF bonus; not holding a weapon.");
        }

        return getWeaponAttackBonus(weapon) + getTwoWeaponOffHandPenalty();
    }

    public int getTwoWeaponOffHandPenalty(){
        int penalty = -10;
        if (hasTwoWeaponFighting()) {
            penalty = -4;
        }

        // Bonus for a light offhand weapon
        if (isOffHandLight()) {
            penalty += 2;
        }

        // Shield Master overrules all of this
        if (isShieldMaster() && isOffHandShieldBash()) {
            penalty = 0;
        }
        return penalty;
    }

    public boolean hasTwoWeapons(){
        return (getHeldWeapon(Hand.PRIMARY) != null) && (getHeldWeapon(Hand.OFF) != null);
    }

    public Weapon getHeldWeapon(Hand hand){
        // Select the weapon
        List<Weapon> weapons = new ArrayList<>();
        CollectionUtils.select(getWeaponSet(), HeldWeaponPredicate.getInstance(hand), weapons);

        // Extract from the list
        Weapon weapon;
        switch (weapons.size()) {
        case 0:
            weapon = null;
            break;
        case 1:
            weapon = weapons.get(0);
            break;
        default:
            // TODO: Exceptions
            throw new RuntimeException("Too many " + hand + " weapons: " + weapons);
        }

        // Return the weapon
        return weapon;
    }

    protected boolean isOffHandLight(){
        Weapon weapon = getHeldWeapon(Hand.OFF);
        return Heft.LIGHT.equals(weapon.getData().getHeft());
    }

    protected boolean isOffHandShieldBash(){
        boolean isShield = false;
        Weapon weapon = getHeldWeapon(Hand.OFF);

        if ((weapon != null) && (weapon instanceof OffensiveArmor)) {
            isShield = ((OffensiveArmor) weapon).getParent().getData().getProficiency().isShieldBash();
        }

        return isShield;
    }

    protected UnmodifiableBonus getImprovedCriticalBonus(String weaponName){
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(BonusType.ATTACK, NAME_ATTACK_IMPROVEDCRITICAL);

        List<UnmodifiableBonus> matching = new ArrayList<UnmodifiableBonus>();
        CollectionUtils.select(bonuses, KeyedObjectKeyPredicate.getInstance(weaponName), matching);

        UnmodifiableBonus bonus = null;
        if (matching.size() > 0) {
            bonus = matching.get(0);
        }
        return bonus;
    }

    public boolean hasImprovedCritical(String weaponName){
        return (getImprovedCriticalBonus(weaponName) != null);
    }

    public int getImprovedCriticalValue(String weaponName){
        return hasImprovedCritical(weaponName) ? 2 : 0;
    }

    public int getMeleeAttackMiscBonus(){
        return getTotalSpecialBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_ATTACK)
                + getTotalSpecialBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_MELEE);
    }

    public int getRangedAttackTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.DEX) + getSizeModifier() + getRangedAttackMiscBonus();
    }

    public int getRangedAttackMiscBonus(){
        return getTotalSpecialBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_ATTACK)
                + getTotalSpecialBonuses(BonusType.ATTACK, ITEMKEY_ATTACK_RANGED);
    }

    public int getCMBTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.STR) - getSizeModifier() + getCMBMiscBonus();
    }

    public int getCMBFinesseTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.DEX) - getSizeModifier() + getCMBMiscBonus();
    }

    public int getCMBMiscBonus(){
        return getTotalSpecialBonuses(BonusType.COMBAT_MANEUVER);
    }

    public int getCMDTotalBonus(){
        return 10 + getBaseAttack()
                + getAttributeBonus(AttributeName.STR)
                + getAttributeBonus(AttributeName.DEX, true)
                - getSizeModifier()
                + getCMDMiscBonus();
    }

    public int getCMDMiscBonus(){
        return getTotalSpecialBonuses(BonusType.COMBAT_DEFENSE);
    }

    public int getWeaponAttackBonus(Weapon weapon){
        CombatType type = weapon.getData().getCombatType();

        // Get the weapon's bonuses
        int bonus = weapon.getQuality().getAttackBonus() + getWeaponTrainingBonus(weapon)
                + getTotalSpecialBonuses(BonusType.ATTACK, Sets.newHashSet(weapon.getName(), weapon.getId()))
                + getTotalSpecialBonuses(BonusType.ATTACK);

        // Add the appropriate attack bonuses
        switch (type) {
        case MELEE:
            if (weapon.isWeaponFinesse() && hasWeaponFinesse()) {
                bonus += getWeaponFinesseTotalBonus();
            } else {
                bonus += getMeleeAttackTotalBonus();
            }
            break;
        case RANGED:
            bonus += getRangedAttackTotalBonus();
            break;
        default:
            // TODO: Exceptions
            throw new RuntimeException("Weapon type not recognized: " + type);
        }

        return bonus;
    }

    public String getWeaponDamage(Weapon weapon){
        // Calculate any damage bonus steps
        Damage damage = weapon.getDamage();
        int steps = getTotalSpecialBonuses(BonusType.DAMAGE_STEP, Sets.newHashSet(weapon.getName(), weapon.getId()))
                + getTotalSpecialBonuses(BonusType.DAMAGE_STEP);

        if (steps > 0) {
            for (int i = 0; i < steps; i++) {
                damage = damage.getStepUp();
            }
        } else if (steps < 0) {
            for (int i = 0; i < steps; i++) {
                damage = damage.getStepDown();
            }
        }

        // Return the modified damage
        StringBuilder display = new StringBuilder(damage.getDisplay()).append(
                BonusHelper.getDieDescriptions(BonusHelper.getDieBonuses(weapon, getBonusProviders(), getModifiers())));

        int damageBonus = getWeaponDamageBonus(weapon);
        if (damageBonus != 0) {
            display.append(FORMATTER_MODIFIER.format(damageBonus));
        }
        return display.toString();
    }

    protected int getWeaponDamageBonus(Weapon weapon){
        int bonus = weapon.getQuality().getDamageBonus() + getWeaponTrainingBonus(weapon)
                + getTotalSpecialBonuses(BonusType.DAMAGE, Sets.newHashSet(weapon.getName(), weapon.getId()))
                + getTotalBonuses(weapon.modifyBonuses(BonusType.DAMAGE, getModifiers()));

        if (weapon.getData().getCombatType().equals(CombatType.MELEE)) {
            bonus += getAttributeBonus(AttributeName.STR);
        }
        return bonus;
    }

    public String getThreatRange(Weapon weapon){
        int range = weapon.getData().getThreatRange();
        int improvedValue = getImprovedCriticalValue(weapon.getId());
        if (improvedValue > 0) {
            int difference = 21 - range;
            range = 21 - (difference * improvedValue);
        }

        String rangeString;
        if (range < 20) {
            rangeString = range + "-20";
        } else {
            rangeString = "20";
        }

        return rangeString;
    }

    /**
     * Gets the character's weapon training bonus for the given weapon, if
     * applicable.
     * 
     * @param weapon The weapon
     * @return The applicable Weapon Training bonus
     */
    protected int getWeaponTrainingBonus(Weapon weapon){
        // Find weapon training bonuses
        Collection<UnmodifiableBonus> filteredBonuses =
                getCalculatedBonuses(BonusType.OTHER, NAME_ATTACK_WEAPONTRAINING);

        // Filter to only bonuses matching a weapon group
        Collection<UnmodifiableBonus> matchingGroup = new HashSet<UnmodifiableBonus>();
        CollectionUtils.select(filteredBonuses,
                KeyedObjectKeyPredicate.getInstance(weapon.getData().getWeaponGroups()),
                matchingGroup);

        // Return the highest value
        return getGreatestBonus(matchingGroup);
    }

    /**
     * @return The set of Weapons held by this character.
     */
    public Set<Weapon> getWeaponSet(){
        Set<Weapon> weapons = getData().getEquipment().getWeaponSet();

        // Look for enhanced shields for Shield Master
        if (isShieldMaster()) {
            for (Weapon weapon : weapons) {
                if (weapon instanceof OffensiveArmor
                        && !((OffensiveArmor) weapon).getParent().getQuality().equals(Armor.Quality.NORMAL)) {
                    weapon.setQuality(
                            Weapon.Quality.valueOf(((OffensiveArmor) weapon).getParent().getQuality().toString()));
                }
            }
        }

        // Add natural weapons
        Collection<UnmodifiableBonus> natWeapons = getCalculatedBonuses(BonusType.NATURAL_WEAPON);
        for (UnmodifiableBonus natWeapon : natWeapons) {
            String id = StringUtils.substringBefore(natWeapon.getName(), " (") + natWeapon.getValue();
            weapons.add(new Weapon(id));
        }

        return weapons;
    }

    protected boolean isShieldMaster(){
        return getFeats().containsKey(Character3E.NAME_OTHER_SHIELDMASTER);
    }

    /**
     * @return The set of Ammunition held by this character.
     */
    public Set<Ammunition> getAmmunitionSet(){
        Set<Ammunition> ammo = getData().getEquipment().getAmmunitionSet();

        return ammo;
    }

    /**
     * @return The set of charged items held by this character.
     */
    public Set<AbstractEquipment<?>> getChargedItemsSet(){
        Set<AbstractEquipment<?>> charged = getData().getEquipment().getChargedItems();

        return charged;
    }

    /**
     * @return The Charges from the character's charged items.
     */
    public Set<Charge> getCharges(){
        Set<AbstractEquipment<?>> chargedItems = getChargedItemsSet();
        Set<Charge> charges = new KeyedSet<String, Charge>(){
            @Override
            public String getKey(Object target){
                String key = null;

                if (target instanceof Charge) {
                    key = ((Charge) target).getName();
                } else {
                    // TODO: Exceptions
                    throw new RuntimeException("Unrecognized object type");
                }

                return key;
            }
        };

        // Collect the charges & add the item name to the charge name
        for (AbstractEquipment<?> item : chargedItems) {
            for (Charge charge : item.getSpecifiedCharges()) {
                if (Charge.DEFAULT_NAME.equals(charge.getName())) {
                    charge = new Charge(charge);
                    charge.setName(item.getDisplayName());
                } else {
                    charge = new Charge(charge);
                    charge.setName(item.getDisplayName() + " - " + charge.getName());
                }

                charges.add(charge);
            }
        }
        return charges;
    }

    /**
     * @return The set of Armor held by this character.
     */
    public Set<Armor> getArmorSet(){
        Set<Armor> armors = getData().getEquipment().getArmorSet();

        return armors;
    }

    /**
     * @return The maximum light load for this character.
     */
    public int getCarryingCapacityLight(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.LIGHT.getCarryingCapacity(str);
    }

    /**
     * @return The maximum medium load for this character.
     */
    public int getCarryingCapacityMedium(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.MEDIUM.getCarryingCapacity(str);
    }

    /**
     * @return The maximum heavy load for this character.
     */
    public int getCarryingCapacityHeavy(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.HEAVY.getCarryingCapacity(str);
    }

    /**
     * @return The maximum lift over head load for this character.
     */
    public int getCarryingCapacityOverHead(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.OVER_HEAD.getCarryingCapacity(str);
    }

    /**
     * @return The maximum lift off ground load for this character.
     */
    public int getCarryingCapacityOffGround(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.OFF_GROUND.getCarryingCapacity(str);
    }

    /**
     * @return The maximum push/drag load for this character.
     */
    public int getCarryingCapacityPushDrag(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return CarryingCapacity.PUSH_DRAG.getCarryingCapacity(str);
    }

    public CarryingCapacity getCarryingCapacity(){
        Map<String, Set<AbstractEquipment<?>>> locationsMap = getData().getEquipment().getEquipmentByLocation();
        BigDecimal weight =
                EquipmentSet.getTotalWeight(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED), locationsMap);
        int str = getAttributeTotalScore(AttributeName.STR);

        CarryingCapacity capacity = CarryingCapacity.valueForWeight(weight.intValue(), str);
        return capacity;
    }

    /**
     * @return The carrying capacity check penalty for a character's STR and
     *         equipped items.
     */
    public int getCarryingCapacityCheckPenalty(){
        return getCarryingCapacity().getCheckPenalty();
    }

    /**
     * @return The max dex for the character current carrying capacity band;
     *         null if no max dex is applied.
     */
    public Integer getCarryingCapacityMaxDex(){
        return getCarryingCapacity().getMaxDex();
    }

    /**
     * @return The character's base speed
     */
    public int getBaseSpeed(){
        return getData().getRace().getData().getBaseSpeed();
    }

    /**
     * @return The characters base encumbered speed
     */
    public int getBaseSpeedEncumbered(){
        return calculateEncumberedSpeed(getBaseSpeed());
    }

    protected int calculateEncumberedSpeed(int baseSpeed){
        int encumbered = baseSpeed;

        if (!ignoresEncumbrance()) {
            encumbered = new BigDecimal(baseSpeed).setScale(5)
                    .divide(new BigDecimal("5.00000"))
                    .multiply(new BigDecimal(".66666"))
                    .add(new BigDecimal("0.33333"))
                    .setScale(0, RoundingMode.HALF_UP)
                    .multiply(new BigDecimal(5))
                    .intValue();
        }

        return encumbered;
    }

    public int getRunningSpeed(){
        return getBaseSpeed() * 4;
    }

    public int getRunningSpeedEncumbered(){
        return getBaseSpeedEncumbered() * 3;
    }

    /**
     * @return The set of this character's classes, filtered to only the
     *         spellcasting classes.
     */
    public Set<ClassWrapper> getSpellcastingClasses(){
        Set<ClassWrapper> classes = new HashSet<ClassWrapper>();
        CollectionUtils.select(getData().getClasses(), SpellcastingClassPredicate.getInstance(), classes);
        return classes;
    }

    /**
     * @param classId The identifier of the class in question
     * @param spellLevel The level of the spell in question
     * @return The number of spells known for this character in the given class,
     *         for the given level
     */
    public Integer getSpellsKnown(String classId, int spellLevel){
        ClassWrapper cls = getClassLoaderForId(classId);
        int classLevels = cls.getLevels();
        Integer[][] spellsKnown = cls.getData().getSpellsKnown();
        Integer known = null;
        if (spellsKnown != null) {
            try {
                known = spellsKnown[classLevels - 1][spellLevel];
            } catch (ArrayIndexOutOfBoundsException e) {
                // Some classes have fewer than 9 spell levels; simply return
                // null
            }
        }
        return known;
    }

    /**
     * @param spellLevel The level of the spell in question
     * @param classId The identifier of the class in question
     * @return The spell save DC for this character at the given level
     */
    public int getSpellSaveDC(int spellLevel, String classId){
        ClassWrapper cls = getClassLoaderForId(classId);
        AttributeName attribute = cls.getData().getSpellcastingAttribute();
        return 10 + spellLevel + getAttributeBonus(attribute);
    }

    /**
     * @param classId The identifier of the class in question
     * @param spellLevel The level of the spell in question
     * @return The total number of spells this character can cast for the given
     *         class, for the given level
     */
    public Integer getTotalSpellsPerDay(String classId, int spellLevel){
        Integer spells = getBaseSpellsPerDay(classId, spellLevel);

        if (spells != null) {
            ClassWrapper cls = getClassLoaderForId(classId);
            int bonus = getAttributeBonus(cls.getData().getSpellcastingAttribute());
            spells = new Integer(spells.intValue() + getBonusSpells(bonus, spellLevel));
        }

        return spells;
    }

    /**
     * @param classId The identifier of the class in question
     * @param spellLevel The level of the spell in question
     * @return The base number of spells this character can cast for the given
     *         class, for the given level
     */
    public Integer getBaseSpellsPerDay(String classId, int spellLevel){
        ClassWrapper cls = getClassLoaderForId(classId);
        int classLevels = cls.getLevels();
        Integer spells = null;
        try {
            spells = cls.getData().getSpellsPerDay()[classLevels - 1][spellLevel];
        } catch (ArrayIndexOutOfBoundsException e) {
            // Some classes have fewer than 9 spell levels; simply return null
        }
        return spells;
    }

    /**
     * @param classId The identifier of the class in question
     * @param spellLevel The level of the spell in question
     * @return The number of bonus spells this character can cast for the given
     *         class, for the given level
     */
    public Integer getBonusSpells(String classId, int spellLevel){
        ClassWrapper cls = getClassLoaderForId(classId);
        int attributeBonus = getAttributeBonus(cls.getData().getSpellcastingAttribute());
        Integer bonus = new Integer(getBonusSpells(attributeBonus, spellLevel));
        if (bonus.intValue() == 0) {
            bonus = null;
        }
        return bonus;
    }

    protected int getBonusSpells(int attributeBonus, int spellLevel){
        int bonus = 0;

        if (spellLevel > 0) {
            bonus = new BigDecimal(attributeBonus - spellLevel + 1).setScale(5)
                    .divide(new BigDecimal("4.00000"))
                    .setScale(0, RoundingMode.CEILING)
                    .intValue();

            if (bonus < 0) {
                bonus = 0;
            }
        }

        return bonus;
    }

    /**
     * @param classId The identifier of the class to find
     * @return The ClassLoader object with the given key
     */
    protected ClassWrapper getClassLoaderForId(String classId){
        ClassWrapper cls;

        try {
            cls = getData().getClasses().get(classId);
        } catch (Exception e) {
            // TODO: Exceptions
            throw new RuntimeException("Exception retireving class for ID " + classId, e);
        }

        if (cls == null) {
            // TODO: Exceptions
            throw new RuntimeException("No class found for ID " + classId + " in " + getData().getClasses().keySet());
        }

        return cls;
    }

    public SizeWrapper getSize(){
        return getData().getRace().getSize();
    }

    public List<Attack> getPrimaryAttacks(){
        List<Attack> attacks = new ArrayList<>();

        // Get the weapon
        Weapon weapon = getHeldWeapon(Hand.PRIMARY);

        if (weapon != null) {
            // Get the bonus
            int bonus = getWeaponAttackBonus(weapon);

            // Calculate number of attacks based on BAB
            for (int i = 0; i * 5 <= getBaseAttack(); i++) {
                Attack attack = new Attack();
                attack.setWeapon(weapon);
                attack.setBonus(bonus - i * 5);
                attacks.add(attack);
            }
        }

        // Return the attacks
        return ImmutableList.copyOf(attacks);
    }

    public Attack getOffHandAttack(){
        Attack attack = null;

        // Get the weapon
        Weapon weapon = getHeldWeapon(Hand.OFF);

        if (weapon != null) {
            attack = new Attack();
            attack.setWeapon(weapon);
            attack.setBonus(getWeaponAttackBonus(weapon));
        }

        return attack;
    }

    public Collection<BonusProvider> getProvidersWithOptional(){
        Collection<BonusProvider> providers = getBonusProviders();
        providers = BonusHelper.flattenProviders(providers);

        Collection<BonusProvider> optionals = new HashSet<>();
        CollectionUtils.select(providers, new OptionalAttackPredicate(getModifiers()), optionals);
        return optionals;
    }

    public Map<String, Integer> getPools(){
        Map<String, Integer> pools = new HashMap<String, Integer>();
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(BonusType.POOL);
        for (UnmodifiableBonus bonus : bonuses) {
            if (!pools.containsKey(bonus.getName())) {
                pools.put(bonus.getName(), 0);
            }

            pools.put(bonus.getName(), pools.get(bonus.getName()) + bonus.getValue());
        }

        return pools;
    }
}
