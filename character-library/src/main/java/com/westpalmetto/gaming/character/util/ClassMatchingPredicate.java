/**
 * ClassMatchingPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.Predicate;


/**
 * Predicate that evaluates to true if an object is an instance
 * of exactly the specified class (.equals, not instanceof).
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class ClassMatchingPredicate implements Predicate {
	private static Map<Class<?>, ClassMatchingPredicate> instances = new HashMap<Class<?>, ClassMatchingPredicate>();
	public static ClassMatchingPredicate getInstance(Class<?> target){
		ClassMatchingPredicate predicate = instances.get(target);
		
		//Create the instance if it does not yet exist
		if(predicate == null){
			predicate = new ClassMatchingPredicate(target);
			instances.put(target, predicate);
		}
		
		return predicate;
	}
	
	private Class<?> target;
	
	private ClassMatchingPredicate(Class<?> target){
		this.target = target;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	public boolean evaluate(Object object) {
		return object.getClass().equals(target);
	}
}
