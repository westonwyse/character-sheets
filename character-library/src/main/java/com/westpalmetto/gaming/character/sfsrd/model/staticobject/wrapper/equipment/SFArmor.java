package com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.equipment;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.OffensiveArmor;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.equipment.SFStaticArmor;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.loader.equipment.SFArmorLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to contain stats for an AC item.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SFArmor extends AbstractEquipment<SFStaticArmor> {
	private Quality quality = Quality.NORMAL;
	private OffensiveArmor attack;

	public SFArmor(String id) {
		super(id);
	}

	@Override
	protected DataLoader<SFStaticArmor> createDataLoader() {
		return SFArmorLoader.getInstance(getRoot());
	}

	@Override
	public String getDisplayName() {
		String name = super.getName();

		if (!(Quality.NORMAL.equals(getQuality()))) {
			name = super.getName() + ", " + getQuality().getLabel();
		}

		return name;
	}

	/**
	 * Calculates the armor check penalty after armor quality bonuses.
	 * 
	 * @param bonus Any additional bonus to apply.
	 * @return the modified check penalty
	 */
	public int getModifiedCheckPenalty(int bonus) {
		int penalty = getData().getCheckPenalty() + getQuality().getCheckBonus() + bonus;
		if (penalty > 0) {
			penalty = 0;
		}
		return penalty;
	}

	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		if (quality == null) {
			setQuality(Quality.NORMAL);
		}
		return quality;
	}

	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	/**
	 * Calculates the EAC bonus after armor quality bonuses.
	 * 
	 * @return the modified AC bonus
	 */
	public int getModifiedEacBonus() {
		return getData().getEacBonus() + getQuality().getACBonus();
	}

	/**
	 * Calculates the KAC bonus after armor quality bonuses.
	 * 
	 * @return the modified KAC bonus
	 */
	public int getModifiedKacBonus() {
		return getData().getKacBonus() + getQuality().getACBonus();
	}

	/**
	 * The quality of an AC item, and the bonuses that quality confers.
	 */
	public enum Quality {
		NORMAL(0, 0, ""), MASTERWORK(1, 0, "Masterwork"), PLUS1(1, 1, "+1"), PLUS2(1, 2, "+2"), PLUS3(1, 3, "+3"),
		PLUS4(1, 4, "+4"), PLUS5(1, 5, "+5"),;

		private int checkBonus;
		private int acBonus;
		private String label;

		private Quality(int checkBonus, int acBonus, String label) {
			this.checkBonus = checkBonus;
			this.acBonus = acBonus;
			this.label = label;
		}

		/**
		 * @return the checkBonus
		 */
		public int getCheckBonus() {
			return checkBonus;
		}

		/**
		 * @return the acBonus
		 */
		public int getACBonus() {
			return acBonus;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}
}
