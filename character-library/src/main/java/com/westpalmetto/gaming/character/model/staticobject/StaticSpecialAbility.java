/**
 * StaticSpecialAbility.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 3, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import com.westpalmetto.gaming.character.bonuses.AbstractBonusProvider;
import com.westpalmetto.gaming.character.model.SpecialAbility;

import lombok.Getter;
import lombok.Setter;

/**
 * Definition of basic functionality needed for an ability/feat/trait/etc.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class StaticSpecialAbility extends AbstractBonusProvider implements OverridableStaticObject, SpecialAbility{
    private String name;
    private String source;
    private Set<String> overrides;
    private String description;
    private BigDecimal cost;
    private boolean display = true;

    /**
     * No-argument constructor.
     */
    public StaticSpecialAbility(){}

    public StaticSpecialAbility(StaticSpecialAbility original){
        super(original);
        setName(original.getName());
        setSource(original.getSource());
        setOverrides(new HashSet<>(original.getOverrides()));
        setDescription(original.getDescription());
        setCost(original.getCost());
        setDisplay(original.isDisplay());
    }

    @Override
    public Set<String> getOverrides(){
        if (overrides == null) {
            setOverrides(new HashSet<String>());
        }
        return overrides;
    }
}
