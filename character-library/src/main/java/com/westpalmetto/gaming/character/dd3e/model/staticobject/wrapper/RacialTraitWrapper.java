/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;

/**
 * Contains the character-specific details of an alternate racial trait.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class RacialTraitWrapper extends SpecialAbility3E{
	private String raceName;
	
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public RacialTraitWrapper(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	/**
	 * @return the raceName
	 */
	public String getRaceName() {
		return raceName;
	}

	/**
	 * @param raceName the raceName to set
	 */
	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}
	
	@Override
	public String getKey(){
		return getRaceName() + "-" + getId();
	}

	@Override
	public String getLoaderId() {
		return "racialAbility";
	}
}
