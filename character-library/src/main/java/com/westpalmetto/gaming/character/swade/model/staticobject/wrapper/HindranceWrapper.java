package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import java.util.Collection;
import java.util.HashSet;

import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticHindrance;
import com.westpalmetto.gaming.character.swade.model.staticobject.loader.StaticHindranceLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HindranceWrapper extends AbstractSpecialAbilityWrapper<StaticHindrance>{
    private boolean buyoff = false;

    public HindranceWrapper(String id){
        super(id);
    }

    @Override
    protected DataLoader<StaticHindrance> createDataLoader(){
        return StaticHindranceLoader.getInstance(getRoot());
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = super.getBonuses(modifiers);
        if (isBuyoff()) {
            bonuses = new HashSet<ModifiableBonus>();
        }

        return bonuses;
    }
}
