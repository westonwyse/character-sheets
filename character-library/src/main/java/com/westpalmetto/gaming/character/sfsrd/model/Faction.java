package com.westpalmetto.gaming.character.sfsrd.model;

import com.westpalmetto.util.resources.model.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Faction extends Model {
	private String name;
	private int reputation;
}
