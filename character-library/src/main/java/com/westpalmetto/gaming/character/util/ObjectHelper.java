package com.westpalmetto.gaming.character.util;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableSet;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.KeyedObject;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.StaticObjectWrapper;

/**
 * Utilities for basic object types (models/POJOs)
 */
public class ObjectHelper {

	/**
	 * For a given collection of abilities, adds the name to each object if the
	 * object has no name, or to the item keys if the object already has a name.
	 * 
	 * @param objects The objects to which to add the name.
	 * @param name The name.
	 * @return The input collection.
	 */
	public static <T extends Collection<V>, V extends StaticObject & KeyedObject> T keyObjects(
			T objects, String name) {
		for(V ability : objects){
			if(StringUtils.isBlank(ability.getName())){
				ability.setName(name);
			}else if(!StringUtils.equals(name, ability.getName())){
				HashSet<String> keys = new HashSet<>(ability.getItemKeys());
				keys.add(name);
				ability.setItemKeys(keys);
			}
		}
		
		return objects;
	}

	/**
	 * Adds the given item keys to the given special abilities.
	 * 
	 * @param objects The abilities to which to add the keys.
	 * @param itemKeys The keys to add.
	 * @return The modified collection.
	 */
	public static <T extends Collection<V>, V extends StaticObject & KeyedObject> T addKeys(
			T objects,
			Collection<String> itemKeys) {
		for(V ability : objects){
			HashSet<String> keys = new HashSet<>(ability.getItemKeys());
			keys.addAll(itemKeys);
			ability.setItemKeys(keys);
		}
		
		return objects;
	}
	
	/**
	 * For a given KeyedObject, determines the display name based on the given
	 * name and the item keys.
	 * 
	 * @param object The object.
	 * @return The display name.
	 */
	public static String getDisplayName(KeyedObject object){
		String name = object.getName();
		
		if(!object.getItemKeys().isEmpty()){
			name = name + ", " + StringUtils.join(object.getItemKeys(), ", ");
		}
		
		return name;
	}

	/**
	 * Determines the abilities from an original object with templated data.
	 * 
	 * @param template The template to apply.
	 * @param original The original object.
	 * @param originalName The name of the original object.
	 * @return The overridden abilities.
	 */
	public static BonusProviderSet<StaticSpecialAbility> overrideAbilities(
			BonusProviderSet<StaticSpecialAbility> template,
			BonusProviderSet<StaticSpecialAbility> original,
			String originalName) {
		BonusProviderSet<StaticSpecialAbility> originalAbilities =
				new BonusProviderSet<>(original);
		BonusProviderSet<StaticSpecialAbility> newAbilities = new BonusProviderSet<>();
		
		for (StaticSpecialAbility rawAbility : template){
			StaticSpecialAbility ability = new StaticSpecialAbility(rawAbility);
			if(CollectionHelper.isNotEmpty(ability.getOverrides())
					&& originalAbilities.containsAllKeys(ability.getOverrides())){
				originalAbilities.removeAllKeys(ability.getOverrides());
			}else if(CollectionHelper.isNotEmpty(ability.getOverrides())){
				throw new RuntimeException("Cannot find ability to override: " + ability + " in " + originalAbilities.keySet());
			}
		}
		
		//Add the resulting abilities together
		addCloneAll(newAbilities, template);
		addCloneAll(newAbilities, originalAbilities);
		
		//Remove the original name's item keys
		removeItemKeys(newAbilities, originalName);
		
		//Return the abilities
		return newAbilities;
	}

	public static void addCloneAll(
			Collection<StaticSpecialAbility> target,
			Collection<StaticSpecialAbility> toAdds) {
		for(StaticSpecialAbility toAdd : ImmutableSet.copyOf(toAdds)){
			target.add(new StaticSpecialAbility(toAdd));
		}
	}
	
	/**
	 * For the given abilities, removes all item keys matching the given key.
	 * @param abilities The abilities to modify.
	 * @param key The item key to remove.
	 */
	public static void removeItemKeys(
			Collection<? extends SpecialAbility> abilities,
			String key) {
		for(SpecialAbility ability : abilities){
			ability.getItemKeys().remove(key);
		}
	}

	public static BonusProviderSet<StaticSpecialAbility> extractStaticAbilities(
			Collection<? extends StaticObjectWrapper<StaticSpecialAbility>> wrappers){
		BonusProviderSet<StaticSpecialAbility> providers = new BonusProviderSet<>();
		
		for(StaticObjectWrapper<StaticSpecialAbility> wrapper : wrappers){
			providers.add(wrapper.getData());
		}
		
		return providers;
	}
}
