package com.westpalmetto.gaming.character.swade.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Die steps for attributes and skills.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Steps{
	private final static String[] STEPS = {
			"-",
			"d4",
			"d6",
			"d8",
			"d10",
			"d12"
	};
	
	private int ranks;
	
	@Override
	public String toString() {
		return getStep(getRanks());
	}

	/**
	 * Returns the String representation of the step.
	 * @param ranks The number of ranks
	 * @return The string
	 */
	public static String getStep(int ranks){
		String tos;
		if(ranks < STEPS.length) {
			tos = STEPS[ranks];
		}else {
			tos = "d12+" + (ranks-5);
		}
		
		return tos;
	}
}
