package com.westpalmetto.gaming.character.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class CollectionHelper {
	public static boolean isEqual(Collection<?> col1, Collection<?> col2) {
		boolean equals = false;

		if ((col1 == null) && (col2 == null)) {
			equals = true;
		} else if ((col1 != null) && (col2 != null)) {
			equals = org.apache.commons.collections.CollectionUtils.isEqualCollection(col1, col2);
		}

		return equals;
	}

	public static <T> HashSet<T> newHashSet(
			Collection<? extends T>... collections) {
		HashSet<T> set = new HashSet<T>();

		for (Collection<? extends T> collection : collections) {
			set.addAll(collection);
		}

		return set;
	}

	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null) || collection.isEmpty();
	}

	public static boolean isNotEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}

	public static <T> T get(Collection<T> collection, int i) {
		return new ArrayList<>(collection).get(i);
	}
}
