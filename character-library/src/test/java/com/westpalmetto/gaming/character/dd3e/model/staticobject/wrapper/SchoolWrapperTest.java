package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSchool;

/**
 * Tests of SchoolWrapper.
 */
public class SchoolWrapperTest {
	private SchoolWrapper wrapper;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLoading() {
		wrapper = new SchoolWrapper("Healer");
		wrapper.setClassName("mystic");
		wrapper.setRuleset("SFSRD");
		StaticSchool data = wrapper.getData();
		System.out.println(data);
		Assert.assertNotNull(data.getAbilities());
	}
}
