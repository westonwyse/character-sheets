/**
 * CharacterServlet.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.westpalmetto.gaming.character.servlet.writer.ResponseWriter;
import com.westpalmetto.gaming.character.servlet.writer.WriterFactory;

/**
 * Servlet class for serving character sheets and other data.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class CharacterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException{
		String path = request.getPathInfo().replace(" ", "");
		ResponseWriter writer = WriterFactory.getWriter(path);
		response.setContentType(writer.getMimeType(path));
		writer.write(path, response);
	}
}
