/**
 * Character.java
 * 
 * Copyright � 2012 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.bonuses.ConditionalBonusPredicate;
import com.westpalmetto.gaming.character.bonuses.KeyedObjectKeyPredicate;
import com.westpalmetto.gaming.character.model.CharacterModel;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.util.resources.model.Model;

import lombok.extern.slf4j.Slf4j;

/**
 * Character is the mediator object for the character's data.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public abstract class Character<T extends CharacterModel> extends Model{
    public static final String ITEMKEY_ALL = "ALL";// Any bonus that should be
                                                   // applied to all entries of
                                                   // a given type

    public static final String KEY_TYPENAME = "type";

    public static final DecimalFormat FORMATTER_THOUSANDS = new DecimalFormat("#,##0.##");
    public static final DecimalFormat FORMATTER_MODIFIER = new DecimalFormat("+##0;-##0");

    private T data;
    private String type;
    private Map<Object, Collection<UnmodifiableBonus>> bonusCache = new HashMap<>();

    /**
     * @param data The CharacterModel object to use for this character
     */
    public Character(T data){
        setData(data);
    }

    /**
     * Updates the character with the given character model JSON.
     * 
     * @param json The JSON of the character model.
     */
    public abstract void initialize(JsonObject json);

    /**
     * @return the data
     */
    public T getData(){
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(T data){
        this.data = data;
    }

    /**
     * @return the type
     */
    public String getType(){
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type){
        this.type = type;
    }

    /**
     * @return The directory that contains all of the necessary resources.
     */
    abstract public File getResourceRoot();

    /**
     * Collects all known bonus providers.
     * 
     * @return All bonus providers.
     */
    protected abstract Collection<BonusProvider> getBonusProviders();

    /**
     * Collects all known fixed bonus providers. Does not include calculated
     * providers, such as bonus feats.
     * 
     * @return All fixed bonus providers.
     */
    protected abstract Collection<BonusProvider> getFixedBonusProviders();

    /**
     * @return All bonus modifiers for this character.
     */
    public Collection<Modifier> getModifiers(){
        return getModifiers(getBonusProviders());
    }

    /**
     * @return All bonus modifiers for this character.
     */
    public Collection<Modifier> getModifiers(Collection<BonusProvider> providers){
        // Add modifiers from all providers
        Collection<Modifier> modifiers = new HashSet<Modifier>();
        for (BonusProvider provider : providers) {
            if (provider != null) {
                modifiers.addAll(provider.getModifiers());
            }
        }

        return modifiers;
    }

    /**
     * Collects bonuses from all known bonus providers for a given bonus type
     * (SKILL, INTIIATIVE, etc.).
     * 
     * @param type The type of bonus to collect.
     * @return All discovered matching bonuses
     */
    protected final Collection<UnmodifiableBonus> getCalculatedBonuses(BonusType type){
        return getCalculatedBonuses(type, getBonusProviders());
    }

    /**
     * Collects bonuses from the given bonus providers for a given bonus type
     * (SKILL, INTIIATIVE, etc.).
     * 
     * @param type The type of bonus to collect.
     * @param providers The bonus providers.
     * @return All discovered matching bonuses
     */
    protected final Collection<UnmodifiableBonus> getCalculatedBonuses(BonusType type,
            Collection<BonusProvider> providers){
        return getCalculatedBonuses(type, providers, getModifiers());
    }

    /**
     * Collects bonuses from the given bonus providers for a given bonus type
     * (SKILL, INTIIATIVE, etc.).
     * 
     * @param type The type of bonus to collect.
     * @param providers The bonus providers.
     * @return All discovered matching bonuses
     */
    protected final Collection<UnmodifiableBonus>
            getCalculatedBonuses(BonusType type, Collection<BonusProvider> providers, Collection<Modifier> modifiers){
        if (!bonusCache.containsKey(type)) {
            Collection<UnmodifiableBonus> bonuses = new ArrayList<UnmodifiableBonus>();
            for (BonusProvider provider : providers) {
                addBonuses(provider, bonuses, type, modifiers);
            }
            bonusCache.put(type, bonuses);
        }

        return bonusCache.get(type);
    }

    /**
     * For a given collection, adds all bonuses of the given type.
     * 
     * @param provider The bonus provider from which to add.
     * @param bonuses The bonuses collection to which to add.
     * @param type The type of bonuses to add.
     */
    protected void addBonuses(BonusProvider provider, Collection<UnmodifiableBonus> bonuses, BonusType type){
        addBonuses(provider, bonuses, type, getModifiers());
    }

    /**
     * For a given collection, adds all bonuses of the given type.
     * 
     * @param provider The bonus provider from which to add.
     * @param bonuses The bonuses collection to which to add.
     * @param type The type of bonuses to add.
     */
    protected void addBonuses(BonusProvider provider,
            Collection<UnmodifiableBonus> bonuses,
            BonusType type,
            Collection<Modifier> modifiers){
        if (provider != null) {
            Collection<UnmodifiableBonus> modified = provider.modifyBonuses(type, modifiers);
            log.trace("{} from {}: {}", type, provider, modified);
            bonuses.addAll(modified);
        }
    }

    /**
     * Collects bonuses from all known bonus providers for a given bonus type
     * (SKILL, INITIATIVE, etc.) and key ("Stealth", "Ranged", etc.).
     * 
     * @param type The type of bonus to collect.
     * @param key The key of the specific bonus to collect, either itemKey or
     *        name.
     * @return All discovered matching bonuses
     */
    protected Collection<UnmodifiableBonus> getCalculatedBonuses(BonusType type, Object key){
        Object cacheKey = new ImmutablePair<BonusType, Object>(type, key);
        if (!bonusCache.containsKey(cacheKey)) {
            // Collect all bonuses
            Collection<UnmodifiableBonus> bonuses = new ArrayList<UnmodifiableBonus>();
            CollectionUtils.select(getCalculatedBonuses(type), KeyedObjectKeyPredicate.getInstance(key), bonuses);
            CollectionUtils
                    .select(getCalculatedBonuses(type), KeyedObjectKeyPredicate.getInstance(ITEMKEY_ALL), bonuses);

            // Handle named bonuses not stacking
            Collection<UnmodifiableBonus> finalized = new ArrayList<UnmodifiableBonus>();
            Map<String, UnmodifiableBonus> named = new HashMap<String, UnmodifiableBonus>();

            for (UnmodifiableBonus bonus : bonuses) {
                if (StringUtils.isBlank(bonus.getValueName())) {
                    finalized.add(bonus);
                } else {
                    UnmodifiableBonus existing = named.get(bonus.getValueName());
                    if ((existing == null) || (existing.getValue() < bonus.getValue())) {
                        named.put(bonus.getValueName(), bonus);
                    }
                }
            }
            finalized.addAll(named.values());

            // Cache bonuses
            bonusCache.put(cacheKey, finalized);
        }

        return bonusCache.get(cacheKey);
    }

    /**
     * Collects bonuses from all known bonus providers for a given bonus type
     * (SKILL, INTIIATIVE, etc.).
     * 
     * @param type The type of bonus to collect.
     * @return Total value of all discovered matching bonuses
     */
    protected int getTotalSpecialBonuses(BonusType type){
        return getTotalSpecialBonuses(type, getCalculatedBonuses(type));
    }

    protected int getTotalSpecialBonuses(BonusType type, Collection<UnmodifiableBonus> bonuses){
        int modifiers = 0;
        for (UnmodifiableBonus bonus : bonuses) {
            if (bonus != null) {
                modifiers += bonus.getValue();
            }
        }
        return modifiers;
    }

    /**
     * Collects bonuses from all known bonus providers for a given bonus type
     * (SKILL, INITIATIVE, etc.) and key ("Stealth", "Ranged", etc.).
     * 
     * @param type The type of bonus to collect.
     * @param key The key of the specific bonus to collect.
     * @return Total value of all discovered matching bonuses
     */
    protected int getTotalSpecialBonuses(BonusType type, Object key){
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(type, key);
        log.trace("Bonuses for {}, {}: {}", type, key, bonuses);
        return getTotalBonuses(bonuses);
    }

    /**
     * Sums all bonus values for the given collection of Bonus objects.
     * 
     * @param bonuses The Bonus objects.
     * @return The sum of the values.
     */
    protected int getTotalBonuses(Collection<UnmodifiableBonus> bonuses){
        int modifiers = 0;
        for (UnmodifiableBonus bonus : bonuses) {
            modifiers += bonus.getValue();
        }
        return modifiers;
    }

    /**
     * Collects bonuses from all known bonus providers for a given bonus type
     * (SKILL, INTIIATIVE, etc.) and key ("Stealth", "Ranged", etc.) and returns
     * only the largest value.
     * 
     * @param type The type of bonus to collect.
     * @param key The key of the specific bonus to collect.
     * @return The largest of the discovered matching bonuses
     */
    protected int getGreatestSpecialBonus(BonusType type, Object key){
        Collection<UnmodifiableBonus> bonuses = new HashSet<UnmodifiableBonus>();
        CollectionUtils.select(getCalculatedBonuses(type), KeyedObjectKeyPredicate.getInstance(key), bonuses);
        return getGreatestBonus(bonuses);
    }

    /**
     * Returns the largest value from the provided bonuses.
     * 
     * @param bonuses The bonuses to inspect.
     * @return The largest of the discovered matching bonuses
     */
    protected int getGreatestBonus(Collection<UnmodifiableBonus> bonuses){
        int greatestBonus = 0;
        for (UnmodifiableBonus bonus : bonuses) {
            if (bonus.getValue() > greatestBonus) {
                greatestBonus = bonus.getValue();
            }
        }

        return greatestBonus;
    }

    /**
     * Fetches and returns a list of all conditional bonuses for a given bonus
     * type.
     * 
     * @return Conditional bonuses of the given type.
     */
    public Collection<UnmodifiableBonus> getConditionalModifiers(BonusType bonusType){
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(bonusType);
        Collection<UnmodifiableBonus> conditional = new HashSet<UnmodifiableBonus>();
        CollectionUtils.select(bonuses, ConditionalBonusPredicate.getInstance(), conditional);
        return conditional;
    }

    /**
     * Collects bonuses from bonus providers.
     * 
     * @param type The type of bonueses to collect
     * @param providers The bonus providers
     * @param modifiers Any modifiers to apply (may be null)
     * @return The collected bonuses
     */
    protected Collection<Bonus>
            collectBonuses(BonusType type, Collection<BonusProvider> providers, Collection<Modifier> modifiers){
        Collection<Bonus> bonuses = new HashSet<Bonus>();
        for (BonusProvider provider : providers) {
            if (provider != null) {
                bonuses.addAll(provider.getBonuses(type, modifiers));
            }
        }

        return bonuses;
    }
}
