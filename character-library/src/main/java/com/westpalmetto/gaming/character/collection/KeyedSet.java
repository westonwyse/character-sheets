/**
 * KeyedSet.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * KeyedSet is a map implementation that ties keying to a specific key
 * property of a complex object, rather than taking an unrelated key value.
 * It is similar to a set, except that equality is checked against only the key
 * property, like a map, rather than the whole object.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class KeyedSet<K, V> implements Set<V> {
	private SortedMap<K, V> data;
	
	public KeyedSet(){
		data = new TreeMap<K, V>();
	}
	
	public KeyedSet(Collection<V> original) {
		this();
		
		for(V entry : original){
			add(entry);
		}
	}

	public abstract K getKey(Object target);
	
	/**
	 * @return The Map that contains the data backing this implementation.
	 */
	protected SortedMap<K, V> getData() {
		return data;
	}

	/**
	 * @param data The Map that contains the data backing this Set implementation.
	 */
	protected void setData(SortedMap<K, V> data) {
		this.data = data;
	}
	
	/**
	 * For the given key, returns the appropriate object from the set.
	 * @param key The key to retrieve
	 * @return The corresponding object, or null if not found.
	 */
	public V get(K key){
		return getData().get(key);
	}

	@Override
	public int size() {
		return getData().size();
	}

	@Override
	public boolean isEmpty() {
		return getData().isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return containsKey(getKey(o));
	}

	/**
	 * @param key The key for which to check
	 * @return TRUE if the key is found; FALSE otherwise
	 */
	public boolean containsKey(K key) {
		boolean contains = false;
		if(key != null){
			contains = getData().containsKey(key);
		}
		return contains;
	}

	@Override
	public Iterator<V> iterator() {
		return getData().values().iterator();
	}

	@Override
	public Object[] toArray() {
		return getData().values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return getData().values().toArray(a);
	}

	@Override
	public boolean add(V e) {
		//Verify key exists
		K key = getKey(e);
		if(key == null){
			//TODO: Exceptions
			throw new NullPointerException("Could not create key for item " + e);
		}
		
		//Add item
		getData().put(key, e);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		return removeKey(getKey(o));
	}

	public boolean removeKey(K key) {
		return (getData().remove(key) != null);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		boolean found = true;
		
		for(Object obj : c){
			if(!contains(obj)){
				found = false;
				break;
			}
		}
		
		return found;
	}

	public boolean containsAllKeys(Collection<K> keys) {
		boolean found = true;
		
		for(K key : keys){
			if(!containsKey(key)){
				found = false;
				break;
			}
		}
		
		return found;
	}

	@Override
	public boolean addAll(Collection<? extends V> c) {
		for (V value : c) {
			getData().put(getKey(value), value);
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		//TODO Implement remaining methods
		throw new UnsupportedOperationException("Method not yet implemented.");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		//TODO Implement remaining methods
		throw new UnsupportedOperationException("Method not yet implemented.");
	}

	public void removeAllKeys(Collection<K> keys) {
		for(K key : keys){
			removeKey(key);
		}
	}

	@Override
	public void clear() {
		getData().clear();
	}
	
	/**
	 * For a given collection, adds all of the members of that collection to this set,
	 * overriding any existing matching values.
	 * 
	 * @param c The collection of objects to add.
	 */
	public void overrideAll(Collection<? extends V> c) {
		for (V v : c) {
			if(this.contains(v)){
				this.remove(v);
			}
			
			this.add(v);
		}
	}
	
	public Set<K> keySet() {
		return getData().keySet();
	}
	
	public Collection<V> values() {
		return getData().values();
	}
	
	@Override
	public String toString(){
		return getData().toString();
	}
	
	@Override
	public boolean equals(Object o){
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
