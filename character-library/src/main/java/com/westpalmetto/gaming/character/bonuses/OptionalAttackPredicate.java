package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;

/**
 * Predicate that evaluates to true if an object is a BonusProvider with an
 * optional attack defined.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class OptionalAttackPredicate implements Predicate {
	private final Collection<Modifier> modifiers;
	
	public OptionalAttackPredicate(Collection<Modifier> modifiers){
		this.modifiers = modifiers;
	}
	
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof BonusProvider){
			BonusProvider provider = (BonusProvider)object;
			OptionalBonuses optional = provider.getOptional(getModifiers());
			matches = (optional != null) && CollectionUtils.isNotEmpty(optional.getBonuses());
		}
		
		return matches;
	}

	private Collection<Modifier> getModifiers(){
		return modifiers;
	}
}
