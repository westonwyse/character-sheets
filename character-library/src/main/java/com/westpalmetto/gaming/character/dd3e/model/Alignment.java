package com.westpalmetto.gaming.character.dd3e.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Alignment {
	public enum LawChaosAxis {
		LAWFUL("Lawful"),
		NEUTRAL("Neutral"),
		CHAOTIC("Chaotic");
		
		private String name;
		private LawChaosAxis(String name){
			this.name = name;
		}
		public String getName(){
			return this.name;
		}
	}
	public enum GoodEvilAxis {
		GOOD("Good"),
		NEUTRAL("Neutral"),
		EVIL("Evil");
		
		private String name;
		private GoodEvilAxis(String name){
			this.name = name;
		}
		public String getName(){
			return this.name;
		}
	}
	
	private LawChaosAxis law;
	private GoodEvilAxis good;
	
	/**
	 * Creates an Alignment object with the given Lawful/Good combination.
	 * 
	 * @param law
	 * @param good
	 */
	public Alignment(LawChaosAxis law, GoodEvilAxis good){
		this.law = law;
		this.good = good;
	}
	
	/**
	 * Returns the long-form name of this alignment.
	 * @return The long-form name of this alignment.
	 */
	public String getLongForm(){
		String name = law.getName() + " " + good.getName();
		if(name.equals("Neutral Neutral")){
			name = "Neutral";
		}
		return name;
	}
	
	/**
	 * Returns the abbreviated form of this alignment.
	 * @return The abbreviated form of this alignment.
	 */
	public String getAbbreviation(){
		char[] letters = {law.toString().charAt(0), good.toString().charAt(0)};
		String abbreviation = new String(letters);
		if(abbreviation.equals("NN")){
			abbreviation = "N";
		}
		return abbreviation;
	}
	
	@Override
	public String toString(){
		return getLongForm();
	}
	
	@Override
	public boolean equals(Object o){
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
