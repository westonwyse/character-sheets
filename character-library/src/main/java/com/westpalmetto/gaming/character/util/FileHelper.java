/**
 * FileUtils.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;

/**
 * Utilities to handle Files.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class FileHelper {
	/**
	 * Reads a given classpath resource to a String.
	 * 
	 * @param classpath The classpath of the resource to read
	 * @return The contents of the resource as a string.
	 * @throws IOException if there is an error reading the file.
	 */
	public static String readFile(String classpath) throws IOException{
		InputStream inStream = FileHelper.class.getResourceAsStream(classpath);
		if(inStream == null){
			//TODO: Exceptions
			throw new RuntimeException("Could not locate resource " + classpath);
		}
		
	    return IOUtils.toString(inStream);
	}
	
	/**
	 * Reads a given classpath resource to a String.
	 * 
	 * @param inStream The InputStream of the resource to read
	 * @return The contents of the resource as a string.
	 * @throws IOException if there is an error reading the file.
	 * @deprecated Use IOUtils.toString(InputStream)
	 */
	public static String readFile(InputStream inStream) throws IOException{
		if(inStream == null){
			//TODO: Exceptions
			throw new RuntimeException("Input stream is null.");
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
	    StringBuilder contents = new StringBuilder();
	    String line;
	    
	    //Cycle through file, reading, while there are bytes to read
	    while ((line = reader.readLine()) != null) {
	    	contents.append(line);
	    }
	    
	    //Close & return
	    reader.close();
	    return contents.toString();
	}
}
