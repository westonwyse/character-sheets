/**
 * PSRDCharacter.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.sfsrd;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.sfsrd.model.Faction;
import com.westpalmetto.gaming.character.sfsrd.model.SFSCharacterModel;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

/**
 * Extension of PSRDCharacter for Pathfinder Society characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SFSCharacter extends SFCharacter {
	/**
	 * Creates a new SFS character.
	 */
	public SFSCharacter() {
		this(new SFSCharacterModel(ResourceFactory.Ruleset.SFSRD.getRoot()));
	}

	/**
	 * Creates a new PFSCharacter from the given character model.
	 * 
	 * @param model The character model.
	 */
	public SFSCharacter(SFSCharacterModel model) {
		super(model);
	}

	/**
	 * @param json The JSON from which to initialize this character.
	 */
	public SFSCharacter(JsonObject json) {
		this();
		initialize(json);
	}

	@Override
	public void initialize(JsonObject json) {
		setData(GsonUtils.readModel(json, SFSCharacterModel.class));
	}

	public SFSCharacterModel getSfsData() {
		return (SFSCharacterModel) getData();
	}

	/**
	 * @return The Faction with the most reputation
	 */
	public Faction getPrimaryFaction() {
		Faction primary = getSfsData().getFactions().get(0);

		for (Faction faction : getSfsData().getFactions()) {
			if (faction.getReputation() > primary.getReputation()) {
				primary = faction;
			}
		}

		return primary;
	}

	/**
	 * @return The total reputation among all factions
	 */
	public int getTotalReputation() {
		int total = 0;

		for (Faction faction : getSfsData().getFactions()) {
			total += faction.getReputation();
		}

		return total;
	}

}
