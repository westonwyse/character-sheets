/**
 * GsonUtil.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.util.resources.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

/**
 * Utilities for interacting with Gson.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class GsonHelper {
	private final Gson gson;
	
	private GsonHelper(){
		this.gson = createGsonInstance();
	}
	
	/**
	 * @return The Gson instance to use.
	 */
	protected Gson createGsonInstance(){
		GsonBuilder builder = new GsonBuilder();
		return builder.create();
	}

	/**
	 * Reads the given JSON as the given model class.
	 * 
	 * @param json The JSON to read.
	 * @param cls The class to read as.
	 * @return The resulting model.
	 */
	public <T> T readModel(JsonObject json, Class<T> cls) {
		T model;
		try {
			model = getGson().fromJson(json, cls);
		} catch (JsonSyntaxException e){
			//TODO: Exceptions
			throw e;
		}
		return model;
	}
	
	/**
	 * Null-safe method to convert a JsonElement to a String.
	 * 
	 * @param json The JSON element.
	 * @return The value as a string; null if the element is null.
	 */
	public String asString(JsonElement json) {
		String string = null;
		if(json != null){
			string = json.getAsString();
		}
		return string;
	}

	/**
	 * @return the gson
	 */
	protected Gson getGson() {
		return gson;
	}
}
