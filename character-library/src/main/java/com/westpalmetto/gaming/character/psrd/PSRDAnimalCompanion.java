package com.westpalmetto.gaming.character.psrd;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.dd3e.SavingThrow;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SizeWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.psrd.model.PSRDAnimalCompanionModel;
import com.westpalmetto.gaming.character.psrd.model.PSRDAnimalCompanionModel.AnimalSavingThrow;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

/**
 * Model for an animal companion.
 */
public class PSRDAnimalCompanion extends PSRDCharacter implements BonusProvider{
    private static final int[] FEAT_PROGRESION = new int[] { 1, 2, 5, 8, 10, 13, 16, 18 };

    /**
     * Creates a new PSRD character.
     * 
     * @param root The resource root
     */
    public PSRDAnimalCompanion(){
        this(new PSRDAnimalCompanionModel(ResourceFactory.Ruleset.PSRD.getRoot()));
    }

    /**
     * Creates a new PSRDCharacter from the given character model.
     * 
     * @param model The character model.
     */
    public PSRDAnimalCompanion(PSRDAnimalCompanionModel model){
        super(model);
    }

    /**
     * @param json The JSON from which to initialize this character.
     * @param root The resource root
     */
    public PSRDAnimalCompanion(JsonObject json){
        this();
        initialize(json);
    }

    @Override
    public void initialize(JsonObject json){
        setData(GsonUtils.readModel(json, PSRDAnimalCompanionModel.class));
    }

    public PSRDAnimalCompanionModel getPsrdAnimalCompanionData(){
        return (PSRDAnimalCompanionModel) getData();
    }

    @Override
    public String getName(){
        return getPsrdAnimalCompanionData().getName();
    }

    @Override
    public String getSource(){
        return getPsrdAnimalCompanionData().getSource();
    }

    @Override
    protected Collection<BonusProvider> getFixedBonusProviders(){
        Collection<BonusProvider> bonuses = super.getFixedBonusProviders();
        bonuses.add(this);
        return bonuses;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = new HashSet<ModifiableBonus>();
        bonuses.add(getNaturalArmorBonus());
        bonuses.addAll(getPsrdAnimalCompanionData().getSpecialQualities().getBonuses(modifiers));
        return bonuses;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(BonusType type, Collection<Modifier> modifiers){
        return BonusHelper.filterBonusesByType(getBonuses(modifiers), type);
    }

    private ModifiableBonus getNaturalArmorBonus(){
        ModifiableBonus bonus = new ModifiableBonus();
        bonus.setType(BonusType.ARMOR_CLASS);
        bonus.getItemKeys().add(ITEMKEY_AC_NATURALARMOR);
        bonus.setValue(getPsrdAnimalCompanionData().getNaturalArmorBonus());
        return bonus;
    }

    @Override
    public int getBaseSpeed(){
        return getPsrdAnimalCompanionData().getBaseSpeed();
    }

    @Override
    protected int getTotalBaseSave(SavingThrow type){
        AnimalSavingThrow save = getPsrdAnimalCompanionData().getBaseSave(type);
        return save.getBonus(getPsrdAnimalCompanionData().getLevel());
    }

    @Override
    public int getBaseAttack(){
        return getPsrdAnimalCompanionData().getBaseAttack();
    }

    @Override
    public int getHitPoints(){
        // Get base HP
        int hitDice = getPsrdAnimalCompanionData().getHitDice();
        int hp = (int) (hitDice * 4.5);

        // Add Con bonus
        int con = getAttributeTotalScore(AttributeName.CON);
        int conModifier = getAttributeModifier(con);
        hp += (hitDice * conModifier);

        // Return the total
        return hp;
    }

    @Override
    public Set<Weapon> getWeaponSet(){
        return getPsrdAnimalCompanionData().getAttacks();
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(modifiers), modifiers);
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(BonusType type, Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(type, modifiers), modifiers);
    }

    @Override
    public Collection<Modifier> getModifiers(){
        Collection<Modifier> modifiers = new HashSet<Modifier>();

        // Add level modifier
        Modifier levelModifier = new Modifier();
        levelModifier.setName("Animal companion level modifier");
        levelModifier.setType(ModifierType.CHARACTER_LEVEL);
        levelModifier.setValue(getPsrdAnimalCompanionData().getLevel());
        modifiers.add(levelModifier);

        return modifiers;
    }

    @Override
    public int getEarnedFeats(){
        int feats = 0;

        for (int i = FEAT_PROGRESION.length; i > 0; i--) {
            if (i >= FEAT_PROGRESION[i - 1]) {
                feats = i;
                break;
            }
        }

        return feats;
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }

    @Override
    public SizeWrapper getSize(){
        return getPsrdAnimalCompanionData().getSize();
    }

    @Override
    public OptionalBonuses getOptional(Collection<Modifier> modifiers){
        // Animal companions do not provide optional attacks.
        return null;
    }
}
