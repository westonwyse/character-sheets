/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;

/**
 * Contains the character-specific details of a character's special quality.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class QualityWrapper extends SpecialAbility3E {
	private Integer value;
	
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public QualityWrapper(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	@Override
	public String getLoaderId() {
		return "quality";
	}

	/**
	 * @return the value
	 */
	public Integer getValue(){
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Integer value){
		this.value = value;
	}
	
	@Override
	public String getName(){
		StringBuilder name = new StringBuilder(super.getName());
		
		if(getValue() != null){
			name.append(" (").append(getValue()).append(")");
		}
		
		return name.toString();
	}
}
