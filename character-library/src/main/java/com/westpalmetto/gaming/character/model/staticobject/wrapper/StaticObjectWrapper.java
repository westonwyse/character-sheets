/**
 * StaticObject.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.gaming.character.util.loading.DataLoader;


/**
 * Interface to define an object loaded as static data at runtime.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface StaticObjectWrapper<V extends StaticObject> {
	/**
	 * The ID of a given instance is what defines the data to be returned.
	 * 
	 * @return The ID of this instance
	 */
	public String getId();
	
	/**
	 * @return The DataLoader instance to load this type of data.
	 */
	public DataLoader<V> getDataLoader();

	/**
	 * @return The data
	 * @todo Implement interface instead?
	 */
	public V getData();
}
