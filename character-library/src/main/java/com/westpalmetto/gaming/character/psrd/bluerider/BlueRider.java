package com.westpalmetto.gaming.character.psrd.bluerider;

import java.util.Collection;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.bluerider.model.staticobject.wrapper.CharacterPointBonus;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

/**
 * Model for an animal companion.
 */
public class BlueRider extends PSRDCharacter {
	/**
	 * Creates a new BlueRider character.
	 */
	public BlueRider(){
		this(new BlueRiderModel(ResourceFactory.Ruleset.PSRD.getRoot()));
	}
	
	/**
	 * Creates a new BlueRider from the given character model.
	 * @param model The character model.
	 */
	public BlueRider(BlueRiderModel model) {
		super(model);
	}

	/**
	 * @param json The JSON from which to initialize this character.
	 * @param root The resource root
	 */
	public BlueRider(JsonObject json){
		this();
		initialize(json);
	}
	
	@Override
	public void initialize(JsonObject json){
		setData(GsonUtils.readModel(json, BlueRiderModel.class));
	}
	

	public BlueRiderModel getBlueRiderData() {
		return (BlueRiderModel)getData();
	}

	/**
	 * @return The number of character points the character has earned.
	 */
	public int getCharacterPoints() {
		return 8 + (getCharacterLevels() * 2);
	}

	/**
	 * @return The number of character points that have been spent
	 */
	public int getCharacterPointsSpent() {
		int spent = 0;
		
		for(CharacterPointBonus bonus : getBlueRiderData().getCharacterPoints()){
			spent += (bonus.getQuantity() * bonus.getData().getCost().intValue());
		}
		
		return spent;
	}
	
	@Override
	protected Collection<BonusProvider> getFixedBonusProviders(){
		Collection<BonusProvider> bonuses = super.getFixedBonusProviders();
		bonuses.add(getBlueRiderData().getCharacterPoints());
		return bonuses;
	}
}
