package com.westpalmetto.gaming.character.dd3e.loader;

import java.io.File;

import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

public abstract class JsonDataLoader3E<V> extends JsonDataLoader<V>{

	public JsonDataLoader3E(String typeId, Class<V> clz, File root){
		super(typeId, clz, root);
	}
}
