package com.westpalmetto.gaming.character.psrd.model;

import java.io.File;

import com.westpalmetto.gaming.character.dd3e.Advancement;


/**
 * A character class for PFS characters.
 */
public class PFSCharacterModel extends PSRDCharacterModel {
	private static final int[] LEVEL_FAME = {0, 5, 9, 13, 18, 22, 27, 31, 36, 40, 45, 49, 54, 58, 63, 67, 72, 76, 81, 85, 90, 94, 99};
	private static final int[] LEVEL_COST = {0, 500, 1500, 3000, 5250, 8000, 11750, 16500, 23000, 31000, 41000, 54000, 70000, 92500, 120000, 157500, 205000, 265000, 342500, 440000, 565000, 680000, 800000};
	
	private String faction;
	private int prestige;
	private int fame;
	
	public PFSCharacterModel(File resourceRoot){
		super(resourceRoot);
	}

	/**
	 * @return the faction
	 */
	public String getFaction() {
		return faction;
	}

	/**
	 * @param faction the faction to set
	 */
	public void setFaction(String faction) {
		this.faction = faction;
	}

	/**
	 * @return the prestige
	 */
	public int getPrestige() {
		return prestige;
	}

	/**
	 * @param prestige the prestige to set
	 */
	public void setPrestige(int prestige) {
		this.prestige = prestige;
	}

	/**
	 * @return the fame
	 */
	public int getFame() {
		return fame;
	}

	/**
	 * @param fame the fame to set
	 */
	public void setFame(int fame) {
		this.fame = fame;
	}
	
	/**
	 * @return The maximum item cost available through the character's faction.
	 */
	public int getMaxItemCost() {
		int cost = 0;
		
		for(int i=LEVEL_FAME.length-1; i>=0; i--){
			if(fame >= LEVEL_FAME[i]){
				cost = LEVEL_COST[i];
				break;
			}
		}
		
		return cost;
	}
	@Override
	public Advancement getAdvancement() {
		return ADVANCEMENT;
	}
	
	/**
	 * The PFS advancement table.
	 */
	public static class PFSAdvancement implements Advancement{
		private static int[] PFS = new int[] {0,3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57};
		
		@Override
		public int getLevel(int experience){
			int level;
			for(level = 0; level <= PFS.length; level++){
				if(experience < PFS[level]){
					break;
				}
			}
			return level;
		}

		@Override
		public int getNextLevel(int experience) {
			return PFS[getLevel(experience)];
		}

		@Override
		public int getExperience(int level) {
			return PFS[level-1];
		}
	}
	private static final PFSAdvancement ADVANCEMENT = new PFSAdvancement();
}
