/**
 * StaticSpell.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Contains the static details of a spell.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class StaticSpell extends Model implements StaticObject{
    /**
     * Defines the functionality needed for a spell's range.
     */
    @Getter
    public enum SpellRange{
        SELF("Personal", 0, 0), TOUCH("Touch", 0, 0), CLOSE("Close", 25, 5){
            @Override
            public int getRange(int classLevels){
                int multiplier = new BigDecimal(classLevels).setScale(5)
                        .divide(new BigDecimal("2.00000"))
                        .setScale(0, RoundingMode.DOWN)
                        .intValue();
                int extra = getPerLevel() * multiplier;
                return getBase() + extra;
            }
        },
        MEDIUM("Medium", 100, 10),
        LONG("Long", 400, 40);

        private String name;
        private int base;
        private int perLevel;

        /**
         * @param name The name of this spell range type
         * @param base The base number of feet for this spell range
         * @param perLevel The number of feet per level of this spell range
         */
        private SpellRange(String name, int base, int perLevel){
            this.name = name;
            this.base = base;
            this.perLevel = perLevel;
        }

        /**
         * @param classLevels The number of levels the character has in the
         *        appropriate class
         * @return The total range for this SpellRange at the given class level
         */
        public int getRange(int classLevels){
            return base + (perLevel * classLevels);
        }
    }

    private String name;
    private String source;
    private Map<String, Integer> applicableLevels;
    private String castingTime;
    private String save;
    private String duration;
    private String range;
    private String components;
    private boolean spellResistance;
    private String school;
    private String target;
    private String description;

    /**
     * No-argument constructor.
     */
    public StaticSpell(){}

    /**
     * Copy constructor.
     * 
     * @param original
     */
    public StaticSpell(StaticSpell original){
        setName(original.getName());
        setSource(original.getSource());
        setApplicableLevels(original.getApplicableLevels());
        setCastingTime(original.getCastingTime());
        setSave(original.getSave());
        setDuration(original.getDuration());
        setRange(original.getRange());
        setComponents(original.getComponents());
        setSpellResistance(original.isSpellResistance());
        setSchool(original.getSchool());
        setTarget(original.getTarget());
        setDescription(original.getDescription());
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }
}
