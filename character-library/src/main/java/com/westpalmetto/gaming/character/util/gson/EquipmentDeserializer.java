/**
 * EquipmentDeserializer.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 19, 2013
 */
package com.westpalmetto.gaming.character.util.gson;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Ammunition;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Coins;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Equipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.UserDefinedEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.equipment.SFArmor;

/**
 * GSON deserializer to handle subclasses of Equipment objects.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class EquipmentDeserializer implements JsonDeserializer<AbstractEquipment<?>>{
    @Override
    public AbstractEquipment<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException{
        Gson gson = new Gson();
        JsonObject jsonObject = json.getAsJsonObject();
        AbstractEquipment<?> item = null;

        EquipmentType type = EquipmentType.EQUIPMENT;
        if (jsonObject.has("type")) {
            type = EquipmentType.valueOf(jsonObject.get("type").getAsString());
        }

        switch (type) {
        case ARMOR:
            item = gson.fromJson(json, Armor.class);
            break;
        case SFARMOR:
            item = gson.fromJson(json, SFArmor.class);
            break;
        case WEAPON:
            item = gson.fromJson(json, Weapon.class);
            break;
        case AMMUNITION:
            item = gson.fromJson(json, Ammunition.class);
            break;
        case COINS:
            item = gson.fromJson(json, Coins.class);
            break;
        case USER_DEFINED:
            item = gson.fromJson(json, UserDefinedEquipment.class);
            break;
        default:
            item = gson.fromJson(json, Equipment.class);
            break;
        }

        return item;
    }

    public static enum EquipmentType{
        EQUIPMENT, WEAPON, ARMOR, SFARMOR, AMMUNITION, COINS, USER_DEFINED;
    }
}
