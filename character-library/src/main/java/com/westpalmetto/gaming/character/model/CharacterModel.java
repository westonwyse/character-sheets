package com.westpalmetto.gaming.character.model;

import java.io.File;

import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * CharacterModel contains all of the data that defines a given character.
 */
@RequiredArgsConstructor
@Getter
@Setter
public abstract class CharacterModel extends Model{
    public static enum HitPointType{
        CALCULATED, SPECIFIED;
    }

    private String name;
    private String source;
    private String gender;
    private String age;
    private String height;
    private String weight;
    private String hair;
    private String eyes;

    private final File resourceRoot;
}
