package com.westpalmetto.gaming.character.swade;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.bonuses.ChosenBonus;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.model.Skill;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.model.predicate.EquippedItemPredicate;
import com.westpalmetto.gaming.character.swade.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.swade.model.Steps;
import com.westpalmetto.gaming.character.swade.model.SwadeModel;
import com.westpalmetto.gaming.character.swade.model.SwadeSkill;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.EdgeWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.GearWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.HindranceWrapper;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * The controller for a SWADE character.
 */
@Slf4j
public class SwadeCharacter extends Character<SwadeModel>{
    private static final String KEY_FIGHTING = "Fighting";

    /**
     * Default constructor.
     */
    public SwadeCharacter(){
        this(new SwadeModel(ResourceFactory.Ruleset.SWADE.getRoot()));
    }

    /**
     * @param data The character model
     */
    public SwadeCharacter(SwadeModel data){
        super(data);
    }

    @Override
    public void initialize(JsonObject json){
        setData(GsonUtils.readModel(json, SwadeModel.class));
    }

    @Override
    protected Collection<BonusProvider> getBonusProviders(){
        Collection<BonusProvider> providers = new HashSet<BonusProvider>();
        providers.addAll(getFixedBonusProviders());
        providers.add(getData()
                .getHindrances(getCalculatedBonuses(BonusType.HINDRANCE_BUYOFF, providers, getModifiers(providers))));
        providers.add(getEdges(providers));

        return providers;
    }

    @Override
    protected Collection<BonusProvider> getFixedBonusProviders(){
        Collection<BonusProvider> providers = new HashSet<BonusProvider>();
        providers.add(getData().getConcept());
        providers.add(getData().getRace());
        providers.add(getData().getAttributePoints());
        providers.add(getData().getHindrancePoints());
        providers.add(getData().getSkillPoints());
        providers.add(getData().getBonusEdges());
        providers.add(new BonusProviderSet<ChosenBonus>(getData().getAdvances()));

        // Only add bonuses from equipped items
        EquipmentSet equipment = new EquipmentSet();
        CollectionUtils.select(getData().getEquipment(), EquippedItemPredicate.getInstance(), equipment);
        providers.addAll(equipment);

        // Return the combined providers
        return providers;
    }

    /**
     * @return The number of hindrance points the character has chosen
     */
    public int getHindrancePoints(){
        int points = 0;

        for (HindranceWrapper hindrance : getData().getHindrances()) {
            points += hindrance.getData().getType().getValue();
        }

        // Max is 4. More hindrances can be chosen, but they provide no points.
        if (points > 4) {
            points = 4;
        }

        return points;
    }

    public int getPace(){
        return 6 + getTotalSpecialBonuses(BonusType.PACE);
    }

    public int getParry(){
        int parry = 2;
        Skill fighting = getSkills().get(KEY_FIGHTING);
        if (fighting != null) {
            parry += fighting.getRanks() + 1;
        }
        parry += getTotalSpecialBonuses(BonusType.PARRY);

        return parry;
    }

    public int getToughness(){
        return 3 + getAttribute(AttributeName.VIGOR)
                + getArmorToughness()
                + getTotalSpecialBonuses(BonusType.TOUGHNESS);
    }

    public int getArmorToughness(){
        int best = 0;

        // Cycle through each armor
        for (Armor armor : getData().getEquipment().getArmorSet()) {
            if (armor.getData().getProficiency().isArmor()) {
                if (best < armor.getModifiedAcBonus()) {
                    best = armor.getModifiedAcBonus();
                }
            }
        }

        // Return the best bonus found
        return best;
    }

    /**
     * @param attribute The attribute to check
     * @return The total attribute ranks
     */
    public int getAttribute(AttributeName attribute){
        int ranks = 1 + getTotalSpecialBonuses(BonusType.ATTRIBUTE, attribute.getName());
        return ranks;
    }

    /**
     * @param attribute The attribute to check
     * @return The total attribute ranks
     */
    public String getAttributeDie(AttributeName attribute){
        return Steps.getStep(getAttribute(attribute));
    }

    public NamedObjectSet<SwadeSkill> getSkills(){
        NamedObjectSet<SwadeSkill> skills = getBaseSkills();
        Collection<UnmodifiableBonus> bonuses = getCalculatedBonuses(BonusType.SKILL_POINT);
        for (UnmodifiableBonus bonus : bonuses) {
            for (String key : bonus.getItemKeys()) {
                SwadeSkill skill = skills.get(key);
                if (skill != null) {
                    skill.setRanks(skill.getRanks() + bonus.getValue());
                } else {
                    log.warn("Unattached skill: " + key);
                    skill = new SwadeSkill(key, null, bonus.getValue());
                    skills.add(skill);
                }
            }
        }

        return skills;
    }

    protected NamedObjectSet<SwadeSkill> getBaseSkills(){
        NamedObjectSet<SwadeSkill> skills =
                new NamedObjectSet<SwadeSkill>(Sets.newHashSet(new SwadeSkill("Academics", AttributeName.SMARTS, 0),
                        new SwadeSkill("Athletics", AttributeName.AGILITY, 1),
                        new SwadeSkill("Battle", AttributeName.SMARTS, 0),
                        new SwadeSkill("Boating", AttributeName.AGILITY, 0),
                        new SwadeSkill("Common Knowledge", AttributeName.SMARTS, 1),
                        new SwadeSkill("Driving", AttributeName.AGILITY, 0),
                        new SwadeSkill("Electronics", AttributeName.SMARTS, 0),
                        new SwadeSkill("Faith", AttributeName.SPIRIT, 0),
                        new SwadeSkill("Fighting", AttributeName.AGILITY, 0),
                        new SwadeSkill("Focus", AttributeName.SPIRIT, 0),
                        new SwadeSkill("Gambling", AttributeName.SMARTS, 0),
                        new SwadeSkill("Hacking", AttributeName.SMARTS, 0),
                        new SwadeSkill("Healing", AttributeName.SMARTS, 0),
                        new SwadeSkill("Intimidation", AttributeName.SPIRIT, 0),
                        new SwadeSkill("Notice", AttributeName.SMARTS, 1),
                        new SwadeSkill("Occult", AttributeName.SMARTS, 0),
                        new SwadeSkill("Performance", AttributeName.SPIRIT, 0),
                        new SwadeSkill("Persuasion", AttributeName.SPIRIT, 1),
                        new SwadeSkill("Piloting", AttributeName.AGILITY, 0),
                        new SwadeSkill("Repair", AttributeName.SMARTS, 0),
                        new SwadeSkill("Research", AttributeName.SMARTS, 0),
                        new SwadeSkill("Riding", AttributeName.AGILITY, 0),
                        new SwadeSkill("Science", AttributeName.SMARTS, 0),
                        new SwadeSkill("Shooting", AttributeName.AGILITY, 0),
                        new SwadeSkill("Spellcasting", AttributeName.SMARTS, 0),
                        new SwadeSkill("Stealth", AttributeName.AGILITY, 1),
                        new SwadeSkill("Survival", AttributeName.SMARTS, 0),
                        new SwadeSkill("Taunt", AttributeName.SMARTS, 0),
                        new SwadeSkill("Thievery", AttributeName.AGILITY, 0),
                        new SwadeSkill("Weird Science", AttributeName.SMARTS, 0)));
        return skills;
    }

    private int getSkillRank(String name){
        return getSkills().get(name).getRanks();
    }

    public String getSkillDie(String name){
        return Steps.getStep(getSkillRank(name));
    }

    public Integer getSkillModifier(String name){
        int bonus = getTotalSpecialBonuses(BonusType.SKILL, name);
        if (bonus == 0) {
            return null;
        } else {
            return bonus;
        }
    }

    public String getRunningDie(){
        int rank = 2 + getTotalSpecialBonuses(BonusType.RUNNING);
        return Steps.getStep(rank);
    }

    public BonusProviderSet<EdgeWrapper> getEdges(){
        return getEdges(getFixedBonusProviders());
    }

    protected BonusProviderSet<EdgeWrapper> getEdges(Collection<BonusProvider> providers){
        // Get all earned edges
        Collection<Bonus> edgeBonuses = collectBonuses(BonusType.EDGE, providers, null);

        // Create and collect wrappers
        BonusProviderSet<EdgeWrapper> edges = new BonusProviderSet<EdgeWrapper>();
        for (Bonus bonus : edgeBonuses) {
            for (String name : bonus.getItemKeys()) {
                edges.add(new EdgeWrapper(name));
            }
        }

        // Return edges
        return edges;
    }

    public int getEarnedEdges(){
        return getEdges().size();
    }

    public String getRank(){
        return getRank(getData().getAdvances().size());
    }

    public String getRank(int num){
        String rank;

        switch (num / 4) {
        case 0:
            rank = "Novice";
            break;
        case 1:
            rank = "Seasoned";
            break;
        case 2:
            rank = "Veteran";
            break;
        case 3:
            rank = "Heroic";
            break;
        case 4:
        default:
            rank = "Legendary";
            break;
        }

        return rank;
    }

    /**
     * @return Names of hindrances that have been bought off.
     */
    public Collection<String> getHindranceBuyoffs(){
        Collection<UnmodifiableBonus> buyoffs = getCalculatedBonuses(BonusType.HINDRANCE_BUYOFF);
        List<String> names = new ArrayList<>();
        for (UnmodifiableBonus buyoff : buyoffs) {
            names.addAll(buyoff.getItemKeys());
        }
        return names;
    }

    @Override
    public File getResourceRoot(){
        return ResourceFactory.Ruleset.SWADE.getRoot();
    }

    public int getPowerPoints(){
        return getTotalSpecialBonuses(BonusType.POWER_POINT);
    }

    public Set<GearWrapper> getWeaponSet(){
        Set<GearWrapper> weapons = new HashSet<GearWrapper>();

        for (GearWrapper gear : getData().getGear()) {
            if (gear.isWeapon()) {
                weapons.add(gear);
            }
        }

        return weapons;
    }
}
