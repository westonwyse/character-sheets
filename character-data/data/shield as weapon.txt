Spiked armor","proficiency":"MARTIAL","heft":"LIGHT","combatType":"MELEE","cost":	special	1d6				special	P
Spiked shield, heavy","proficiency":"MARTIAL","heft":"ONEHAND","combatType":"MELEE","cost":	special	1d6				special	P
Spiked shield, light","proficiency":"MARTIAL","heft":"LIGHT","combatType":"MELEE","cost":	special	1d4				special	P

Weapon groups:
Close: Heavy shield, light shield, spiked shield, spiked armor