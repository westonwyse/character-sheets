/**
 * Coins.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 9, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment;

import java.math.BigDecimal;

/**
 * Default information for coinage.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class StaticCoins extends StaticEquipment{
	public static final String COINS_NAME = "Coins";
	public static final BigDecimal COINS_WEIGHT = new BigDecimal(".02");
	
	private static StaticCoins instance;
	
	private StaticCoins(){
	}
	
	public static StaticCoins getInstance(){
		if(instance == null){
			instance = new StaticCoins();
		}
		
		return instance;
	}
	
	@Override
	public String getName() {
		return COINS_NAME;
	}
	@Override
	public BigDecimal getWeight() {
		return COINS_WEIGHT;
	}
}
