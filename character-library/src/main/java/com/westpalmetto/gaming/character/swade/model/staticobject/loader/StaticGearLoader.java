package com.westpalmetto.gaming.character.swade.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.swade.model.staticobject.StaticGear;
import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

public class StaticGearLoader extends JsonDataLoader<StaticGear>{
    private static final Map<File, StaticGearLoader> INSTANCES = new HashMap<>();

    public static StaticGearLoader getInstance(File root){
        if (!INSTANCES.containsKey(root)) {
            INSTANCES.put(root, new StaticGearLoader(root));
        }
        return INSTANCES.get(root);
    }

    private StaticGearLoader(File root){
        super("gear", StaticGear.class, root);
    }

    @Override
    public StaticGear getClonedObject(StaticGear original){
        return new StaticGear(original);
    }
}
