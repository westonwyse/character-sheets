package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableSet;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.model.SpellSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSchool;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticSchoolLoader;
import com.westpalmetto.gaming.character.model.bonuses.KeyedObject;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Contains the character-specific details of a class's school-like abilities.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
@Getter
@Setter
public class SchoolWrapper extends AbstractBonusProviderWrapper<StaticSchool> implements KeyedObject{
    private String className;
    private Set<String> itemKeys;
    private boolean requireItemKey;

    private Map<String, String> options;

    private StaticSchool data;

    public SchoolWrapper(String id){
        super(id, ResourceFactory.Ruleset.PSRD.toString());
    }

    @Override
    public String getName(){
        String name = getData().getName();

        // Verify the name exists
        if (StringUtils.isBlank(name)) {
            name = getId();
        }

        // Handle any item key or ID value
        if (CollectionUtils.isNotEmpty(getItemKeys())) {
            name = name + " (" + StringUtils.join(getItemKeys(), ", ") + ")";
        }

        return name;
    }

    @Override
    public String getKey(){
        return getClassName() + "-" + getId();
    }

    @Override
    protected DataLoader<StaticSchool> createDataLoader(){
        return StaticSchoolLoader.getInstance(getRoot());
    }

    /**
     * Gets the Connection spell for a given level, taking variable-level spells
     * into account.
     * 
     * @param spellLevel The level of spell to retrieve
     * @param maxLevel The maximum spell level for this character
     * @return The SpellSet for the appropriate level
     */
    public SpellSet getSpell(int spellLevel, int maxLevel){
        SpellSet spells;
        log.trace("Levels: {} vs {}", spellLevel, maxLevel);
        log.trace("Variable spell: {}", getData().getVariableSpell());
        if (!hasSpells()) {
            log.warn("{} is not a spellcasting school!", getName());
            spells = new SpellSet();
        } else if ((spellLevel != maxLevel) || StringUtils.isBlank(getData().getVariableSpell())) {
            // Regular spell
            spells = getData().getSpells()[spellLevel - 1];
            log.trace("Returning regular spells for level {}: {}", spellLevel, spells);
        } else {
            // Variable-level spell
            String id = getData().getVariableSpell() + spellLevel;
            SpellWrapper spell = new SpellWrapper(id);
            spell.setRuleset(ResourceFactory.Ruleset.SFSRD.toString());

            spells = new SpellSet();
            spells.add(spell);
            log.trace("Returning variable-level spell for level {}: {}", spellLevel, spells);
        }
        return spells;
    }

    /**
     * @return TRUE iff this school provides spells.
     */
    public boolean hasSpells(){
        return (getData().getSpells() != null);
    }

    @Override
    public StaticSchool getData(){
        if (data == null) {
            StaticSchool original = super.getData();
            if (original == null) {
                throw new IllegalArgumentException("No class data for " + getKey());
            }

            data = new StaticSchool(original);
            data.setOptions(getOptions());
        }
        return data;
    }

    @Override
    public Set<String> getItemKeys(){
        if (itemKeys == null) {
            itemKeys = ImmutableSet.of();
        }
        return itemKeys;
    }

    @Override
    public void setItemKeys(Collection<String> keys){
        itemKeys = ImmutableSet.copyOf(keys);
    }
}
