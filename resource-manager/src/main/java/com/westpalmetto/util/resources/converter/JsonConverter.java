package com.westpalmetto.util.resources.converter;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

public class JsonConverter implements StreamConverter<JsonObject> {
	private static final JsonConverter instance = new JsonConverter();
	
	private JsonConverter(){
	}
	
	public static JsonConverter getInstance(){
		return instance;
	}
	
	@Override
	public JsonObject convert(InputStream in) throws ConvertException {
		//Read the string as JSON
		JsonParser parser = new JsonParser();
		try {
			return parser.parse(new InputStreamReader(in)).getAsJsonObject();
		}catch(JsonParseException e){
			throw new ConvertException("Error reading stream to JSON.", e);
		}
	}
}
