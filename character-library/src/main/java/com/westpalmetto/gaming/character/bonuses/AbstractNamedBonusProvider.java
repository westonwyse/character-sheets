package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Abstract bonus provider class that allows for named bonuses defined as
 * separately loaded static data.
 */
public abstract class AbstractNamedBonusProvider extends AbstractBonusProvider {
	private Collection<ModifiableBonus> loadedBonuses;

	/**
	 * @return The data loader with which to retrieve the named abilities.
	 */
	abstract public DataLoader<? extends SpecialAbility> getNamedAbilityLoader();
	
	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		if(loadedBonuses == null){
			//Get the original bonuses
			Collection<ModifiableBonus> original = super.getBonuses(modifiers);
			loadedBonuses = new HashSet<ModifiableBonus>();
			
			for (ModifiableBonus bonus : original) {
				if (StringUtils.isNotBlank(bonus.getName()) && (bonus.getType() == null)) {
					try {
						SpecialAbility staticBonus = getNamedAbilityLoader().getData(bonus.getName());
						if (staticBonus != null) {
							loadedBonuses.addAll(applyBonuses(bonus, staticBonus.getBonuses(modifiers)));
						} else {
							loadedBonuses.add(bonus);
						}
					} catch (Exception e) {
						//TODO: Exceptions
						throw new RuntimeException("Exception getting bonus for " + bonus.getName(), e);
					}
				} else {
					ModifiableBonus duplicate = new ModifiableBonus(bonus);
					duplicate.setName(createName(bonus));
					loadedBonuses.add(duplicate);
				}
			}
		}
			
		return loadedBonuses;
	}


	/**
	 * Takes the original named bonus & the static data and applies any data
	 * needed from the original to the bonuses.
	 * 
	 * @param original The original bonus.
	 * @param bonuses The static bonus data.
	 * @return The modified bonus data.
	 */
	protected Collection<ModifiableBonus> applyBonuses(ModifiableBonus original, Collection<ModifiableBonus> bonuses) {
		for (ModifiableBonus bonus : bonuses) {
			bonus.setName(createName(original));
			bonus.setItemKeys(original.getItemKeys());
			
			if(original.getValue() != 0){
				bonus.setValue(original.getValue());
			}
			
			if(StringUtils.isNotBlank(original.getCondition())){
				bonus.setCondition(original.getCondition());
			}
		}
		
		return bonuses;
	}

	/**
	 * @param bonus The bonus from which to get the name.
	 * @return The name, including any modifiers applied.
	 */
	private String createName(ModifiableBonus bonus) {
		String name = bonus.getName();
		
		if(StringUtils.isBlank(name)){
			name = getName();
		}
		
		return name;
	}
}
