/**
 * PSRDCharacterMapper.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.savageworlds;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.ChosenBonus;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.model.AbilityProvider;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;
import com.westpalmetto.gaming.character.swade.SwadeCharacter;
import com.westpalmetto.gaming.character.swade.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.swade.model.SwadeModel;
import com.westpalmetto.gaming.character.swade.model.SwadeSkill;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticGear;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticPower;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.GearWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.HindranceWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.PowerWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.wrapper.RaceWrapper;

import lombok.extern.slf4j.Slf4j;

/**
 * Handling of response writing for SWADE characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class SwadeCharacterWriter implements CharacterHtmlWriter<SwadeCharacter>{
    private static final String BOX_EMPTY = "&#x25FB;";
    private static final String BOX_MARKED = "&#x25FC;";
    private static final String BOXES = StringUtils.repeat(BOX_EMPTY, 5) + " ";

    @Override
    public void writeHtml(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        // Write the character out
        writer.println("<html><head>");
        writer.println(
                "<link rel='stylesheet' href='/character-service/styles/character/character-swade.css' type='text/css'>");
        writer.println("<title>" + character.getData().getName() + "</title>");
        writer.println("</head><body>");

        // Write character sections
        // Upper section
        writer.println("<div id='upperSection'>");
        writeBasics(writer, character);
        writer.println("</div>");

        // Middle Section
        writer.println("<table><tr>");
        writer.println("<td id='leftColumn'>");
        writeLeftColumn(writer, character);
        writer.println("</td>");

        writer.println("<td id='centerColumn'>");
        writeCenterColumn(writer, character);
        writer.println("</td>");

        writer.println("<td id='rightColumn'>");
        writeRightColumn(writer, character);
        writer.println("</td>");
        writer.println("</tr></table>");

        // Lower section
        writer.println("<div id='lowerSection'>");
        writeLowerSection(writer, character);
        writer.println("</div>");

        // Close file
        writer.println("</body></html>");
        log.trace("writeHtml: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLeftColumn(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        writeAttributes(writer, character);

        // writer.println("<td id='rightColumn'>");
        // writeHP(writer, character);
        // writeInitiative(writer, character);
        // writer.println("</td>");
        // writer.println("</tr></table>");

        writeDerived(writer, character);
        // writeSaves(writer, character);
        // writeCombat(writer, character);

        log.trace("writeLeftColumn: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeCenterColumn(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        // writeSpeed(writer, character);
        writeSkills(writer, character);
        // writeLanguages(writer, character);
        // writeOtherBonuses(writer, character);

        log.trace("writeRightColumn: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeRightColumn(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        // writeSpeed(writer, character);
        writeAdvancements(writer, character);
        // writeLanguages(writer, character);
        // writeOtherBonuses(writer, character);

        log.trace("writeRightColumn: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLowerSection(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        // Write hindrances
        writeHindrances(writer, character);

        // Write special abilites
        writer.println("<div id='abilities'>");
        writer.println("<h2>Special Abilities</h2>");
        writeSpecialAbilities(writer, character);
        writer.println("</div>");

        writePowers(writer, character);

        writeWeapons(writer, character);
        // writeAmmunition(writer, character);
        // writeChargedItems(writer, character);
        // writeArmor(writer, character);
        // writeEquipment(writer, character);
        // writeNotes(writer, character);

        log.trace("writeLowerSection: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, String value){
        writeLabelValuePair(writer, className, label, value, "div", null);
    }

    protected void writeLabelValuePair(PrintWriter writer,
            String className,
            String label,
            String value,
            String container,
            String formatting){
        // Verify input
        label = formatBlankable(label);
        value = formatBlankable(value);
        if (StringUtils.isBlank(formatting)) {
            formatting = "";
        }

        writer.println("<" + container + " class='" + className + " labelValue' " + formatting + ">");
        writer.println("<span class='value'>" + value + "</span>");
        writer.println("<span class='label'>" + label + "</span>");
        writer.println("</" + container + ">");
    }

    protected void
            writeLabelValuePair(PrintWriter writer, String className, String label, String value, String container){
        // Verify input
        label = formatBlankable(label);
        value = formatBlankable(value);

        writer.println("<" + container + " class='" + className + " labelValue'>");
        writer.println("<span class='value'>" + value + "</span>");
        writer.println("<span class='label'>" + label + "</span>");
        writer.println("</" + container + ">");
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, int value){
        writeLabelValuePair(writer, className, label, Integer.toString(value));
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, BigDecimal value){
        writeLabelValuePair(writer, className, label, value.toString());
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, int value, String container){
        writeLabelValuePair(writer, className, label, Integer.toString(value), container);
    }

    protected void
            writeLabelValuePair(PrintWriter writer, String className, String label, BigDecimal value, String container){
        writeLabelValuePair(writer, className, label, value.toString(), container);
    }

    protected void writeBasics(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        SwadeModel data = character.getData();

        // Open section
        writer.println("<div id='basics'>");

        // Write data
        writeLabelValuePair(writer, "name", "Character Name", data.getName());
        writeLabelValuePair(writer, "concept", "Concept", data.getConcept().getName());
        writeLabelValuePair(writer, "race", "Race", data.getRace().getName());
        writeLabelValuePair(writer, "experience", "Rank", character.getRank());
        writeLabelValuePair(writer, "gender", "Gender", data.getGender());
        writeLabelValuePair(writer, "age", "Age", data.getAge());
        writeLabelValuePair(writer, "height", "Height", data.getHeight());
        writeLabelValuePair(writer, "weight", "Weight", data.getWeight());
        writeLabelValuePair(writer, "hair", "Hair", data.getHair());
        writeLabelValuePair(writer, "eyes", "Eyes", data.getEyes());

        // Close section
        writer.println("</div>");

        log.trace("writeBasics: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeAttributes(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        SwadeModel data = character.getData();

        writer.println("<div id='attributes'>");
        writer.println("<h2>Attributes</h2>");

        // Open section
        writer.println("<table id='attributes'>");

        writer.println("<tr>");
        writer.println("<th class='label'>Name</th>");
        writer.println("<th class='score'>Score</th>");
        writer.println("</tr>");

        for (AttributeName attribute : AttributeName.values()) {
            // Write name
            writer.println("<tr class='" + attribute.getName() + "'>");
            writer.println("<th class='label'>" + attribute.getName() + "</th>");
            writer.println("<td class='score'>" + character.getAttributeDie(attribute) + "</td>");
            writer.println("</tr>");
        }

        // Close section
        writer.println("</table>");
        writer.println("</div>");

        // //Validate increases
        // writer.println("<ul>");
        // writer.println("<li>Earned increases: " +
        // character.getEarnedAttributeIncreases() + "</li>");
        // writer.println("<li>Used increases: " +
        // character.getUsedAttributeIncreases() + "</li>");
        // writer.println("</ul>");

        log.trace("writeAttributes: {}ms", System.currentTimeMillis() - start);
    }

    // protected void writeSpeed(PrintWriter writer, PSRDCharacter character){
    // //Open section
    // writer.println("<div id='speed'>");
    // writer.println("<span class='header'>Speed</span>");
    //
    // //Write base speed
    // writer.println("<div class='speedBlock baseSpeed'>");
    // writer.println("<div class='speedData'>");
    // writeLabelValuePair(writer, "base", "Base", character.getBaseSpeed());
    // writeLabelValuePair(writer, "encumbered", "Enc.",
    // character.getBaseSpeedEncumbered());
    // writer.println("</div>");
    // writer.println("<span class='speedType'>Base</span>");
    // writer.println("</div>");
    //
    // //Write running speed
    // writer.println("<div class='speedBlock runningSpeed'>");
    // writer.println("<div class='speedData'>");
    // writeLabelValuePair(writer, "base", "Base", character.getRunningSpeed());
    // writeLabelValuePair(writer, "encumbered", "Enc.",
    // character.getRunningSpeedEncumbered());
    // writer.println("</div>");
    // writer.println("<span class='speedType'>Running</span>");
    // writer.println("</div>");
    //
    // //Write notes
    // writer.println("<div class='speedBlock notes'>");
    // Collection<UnmodifiableBonus> conditionals =
    // character.getConditionalModifiers(BonusType.SPEED);
    // writeConditionals(writer, conditionals);
    // writer.println("</div>");
    //
    // //Close section
    // writer.println("</div>");
    // }

    protected void writeAdvancements(PrintWriter writer, SwadeCharacter character){
        // Open section
        writer.println("<div id='advances'>");
        writer.println("<h2>Advances</h2>");

        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='name'>R</th>");
        writer.println("<th class='attribute'>#</th>");
        writer.println("<th class='die'>Advance</th>");
        writer.println("</tr>");

        List<ChosenBonus> advances = character.getData().getAdvances();
        int count = 1;
        for (ChosenBonus advance : advances) {
            String marker = (count % 3 == 0) ? "marker" : "";
            writer.println("<tr class='" + marker + "'>");
            writer.println("<td>" + StringUtils.substring(character.getRank(count), 0, 1) + "</td>");
            writer.println("<td>" + count + "</td>");
            writer.println("<td>" + writeDescription(advance, character) + "</td>");
            writer.println("</tr>");
            count++;
        }

        writer.println("</table>");
        writer.println("</div>");
    }

    private String writeDescription(ChosenBonus advance, SwadeCharacter character){
        ModifiableBonus bonus = Iterables.get(advance.getBonuses(character.getModifiers()), 0);
        StringBuilder descr = new StringBuilder().append(bonus.getType().getName())
                .append(" (")
                .append(StringUtils.join(bonus.getItemKeys(), " ,"))
                .append(")");

        return descr.toString();
    }

    protected void writeSkills(PrintWriter writer, SwadeCharacter character){
        // Open section
        writer.println("<div id='skills'>");
        writer.println("<h2>Skills</h2>");

        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='name'>Skill Name</th>");
        writer.println("<th class='attribute'>Attribute</th>");
        writer.println("<th class='die'>Die</th>");
        writer.println("</tr>");

        // Write skills
        int count = 1;
        NamedObjectSet<SwadeSkill> skills = character.getSkills();
        for (SwadeSkill skill : skills) {
            AttributeName attributeName = skill.getAttributeName();
            if (attributeName == null) {
                log.warn("Bad name for " + skill.getName());
            }

            if (skill.getRanks() > 0) {
                String marker = (count % 3 == 0) ? "marker" : "";
                count++;

                // Write name
                writer.println("<tr class='" + marker + "'>");
                writer.println("<td class='name'>" + skill.getName() + "</td>");
                writer.println("<td class='attribute'>"
                        + formatNullable((attributeName != null) ? attributeName.getName() : null)
                        + "</td>");
                writer.println("<td class='die'>" + character.getSkillDie(skill.getName())
                        + formatModifier(character.getSkillModifier(skill.getName()))
                        + "</td>");
                writer.println("</tr>");
            }
        }
        writer.println("</table>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SKILL);
        writeConditionals(writer, conditionals);

        // //Validate skill points
        // writer.println("<ul>");
        // writer.println("<li>Earned skill ranks: " +
        // character.getEarnedSkillRanks() + "</li>");
        // writer.println("<li>Used skill ranks: " +
        // character.getUsedSkillRanks() + "</li>");
        // writer.println("</ul>");

        // Close section
        writer.println("</div>");
    }

    // protected void writeHP(PrintWriter writer, PSRDCharacter character){
    // long start = System.currentTimeMillis();
    //
    // //Open section
    // writer.println("<div id='hp'>");
    //
    // //Write HP
    // writer.println("<table class='hp'><tr>");
    // writer.println("<th class='name'>HP</th>");
    // writer.println("<td class='value'>" + character.getHitPoints() +
    // "</td>");
    // writer.println("</tr></table>");
    // writer.println("<div class='wounds'>");
    // writer.println("<span class='name'>Wounds</span>");
    // writer.println("<span class='value'>&nbsp;</span>");
    // writer.println("</div>");
    // writer.println("<div class='nonlethal'>");
    // writer.println("<span class='name'>Nonlethal Damage</span>");
    // writer.println("<span class='value'>&nbsp;</span>");
    // writer.println("</div>");
    //
    // //Close section
    // writer.println("</div>");
    //
    // LOGGER.trace("writeHP: {}ms", System.currentTimeMillis()-start);
    // }
    //
    // protected void writeInitiative(PrintWriter writer, PSRDCharacter
    // character){
    // long start = System.currentTimeMillis();
    //
    // //Open section
    // writer.println("<table id='initiative'><tr>");
    //
    // //Write Initiative
    // writeLabelValuePair(writer, "header", null, "Initiative", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // character.getInitiativeBonus(), "td");
    // writeLabelValuePair(writer, "dex", "Dex Bonus",
    // character.getAttributeBonus(AttributeName.DEX), "td");
    // writeLabelValuePair(writer, "misc", "Misc.",
    // character.getInitiativeMiscModifiers(), "td");
    //
    // //Close section
    // writer.println("</tr></table>");
    //
    // LOGGER.trace("writeInitiative: {}ms", System.currentTimeMillis()-start);
    // }

    protected void writeDerived(PrintWriter writer, SwadeCharacter character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<div id='derived'>");

        // Write Pace
        writeLabelValuePair(writer, "pace", "Pace", character.getPace() + " Inches", "div");
        writeLabelValuePair(writer, "running", "Running", character.getRunningDie(), "div");

        // Write Parry
        writeLabelValuePair(writer, "parry", "Parry", character.getParry(), "div");

        // Write Toughness
        writeLabelValuePair(writer, "toughness", "Toughness", character.getToughness(), "div");

        // Close section
        writer.println("</div>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = ImmutableSet.<UnmodifiableBonus> builder()
                .addAll(character.getConditionalModifiers(BonusType.PACE))
                .addAll(character.getConditionalModifiers(BonusType.RUNNING))
                .addAll(character.getConditionalModifiers(BonusType.PARRY))
                .addAll(character.getConditionalModifiers(BonusType.TOUGHNESS))
                .build();
        writeConditionals(writer, conditionals);

        log.trace("writeDerived: {}ms", System.currentTimeMillis() - start);
    }

    // protected void writeSaves(PrintWriter writer, PSRDCharacter character){
    // long start = System.currentTimeMillis();
    //
    // //Open section
    // writer.println("<div id='saves'>");
    // writer.println("<h2>Saving Throws</h2>");
    // writer.println("<table>");
    //
    // //Write Headers
    // writer.println("<tr>");
    // writer.println("<th class='name header'>Saving Throw</th>");
    // writer.println("<th class='total'>Total</th>");
    // writer.println("<th class='base'>Base Save</th>");
    // writer.println("<th class='ability'>Ability Mod.</th>");
    // writer.println("<th class='misc'>Misc. Mod.</th>");
    // writer.println("<th class='temp'>Temp. Mod.</th>");
    // writer.println("</tr>");
    //
    // //Write Fortitude
    // writer.println("<tr class='fortitude'>");
    // writer.println("<td class='name header'>Fortitude</td>");
    // writer.println("<td class='total'>" +
    // Character.FORMATTER_MODIFIER.format(character.getFortitudeSaveTotal()) +
    // "</td>");
    // writer.println("<td class='base'>" +
    // Character.FORMATTER_MODIFIER.format(character.getFortitudeBaseSave()) +
    // "</td>");
    // writer.println("<td class='ability'>" +
    // Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.CON))
    // + "</td>");
    // writer.println("<td class='misc'>" +
    // Character.FORMATTER_MODIFIER.format(character.getFortitudeSaveMiscModifiers())
    // + "</td>");
    // writer.println("<td class='temp'>&nbsp;</td>");
    // writer.println("</tr>");
    //
    // //Write Reflex
    // writer.println("<tr class='reflex'>");
    // writer.println("<td class='name header'>Reflex</td>");
    // writer.println("<td class='total'>" +
    // Character.FORMATTER_MODIFIER.format(character.getReflexSaveTotal()) +
    // "</td>");
    // writer.println("<td class='base'>" +
    // Character.FORMATTER_MODIFIER.format(character.getReflexBaseSave()) +
    // "</td>");
    // writer.println("<td class='ability'>" +
    // Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.DEX))
    // + "</td>");
    // writer.println("<td class='misc'>" +
    // Character.FORMATTER_MODIFIER.format(character.getReflexSaveMiscModifiers())
    // + "</td>");
    // writer.println("<td class='temp'>&nbsp;</td>");
    // writer.println("</tr>");
    //
    // //Write Fortitude
    // writer.println("<tr class='will'>");
    // writer.println("<td class='name header'>Will</td>");
    // writer.println("<td class='total'>" +
    // Character.FORMATTER_MODIFIER.format(character.getWillSaveTotal()) +
    // "</td>");
    // writer.println("<td class='base'>" +
    // Character.FORMATTER_MODIFIER.format(character.getWillBaseSave()) +
    // "</td>");
    // writer.println("<td class='ability'>" +
    // Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.WIS))
    // + "</td>");
    // writer.println("<td class='misc'>" +
    // Character.FORMATTER_MODIFIER.format(character.getWillSaveMiscModifiers())
    // + "</td>");
    // writer.println("<td class='temp'>&nbsp;</td>");
    // writer.println("</tr>");
    // writer.println("</table>");
    //
    // //Write conditional modifiers
    // Collection<UnmodifiableBonus> conditionals =
    // character.getConditionalModifiers(BonusType.SAVING_THROW);
    // writeConditionals(writer, conditionals);
    //
    // //Close section
    // writer.println("</div>");
    //
    // LOGGER.trace("writeSaves: {}ms", System.currentTimeMillis()-start);
    // }
    //
    // protected void writeCombat(PrintWriter writer, PSRDCharacter character){
    // long start = System.currentTimeMillis();
    //
    // writer.println("<h2>Combat</h2>");
    // writer.println("<table id='attacks'>");
    //
    // //Write Melee
    // writer.println("<tr class='melee'>");
    // writeLabelValuePair(writer, "header", null, "Melee", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // Character.FORMATTER_MODIFIER.format(character.getMeleeAttackTotalBonus()),
    // "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Str Bonus",
    // character.getAttributeBonus(AttributeName.STR), "td");
    // writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(),
    // "td");
    // writeLabelValuePair(writer, "misc", "Misc.",
    // character.getMeleeAttackMiscBonus(), "td");
    // writer.println("</tr>");
    //
    // //Write Weapon Finesse, if applicable
    // if(character.hasWeaponFinesse()){
    // writer.println("<tr class='finesse'>");
    // writeLabelValuePair(writer, "header", "Weapon Finesse", "Melee", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // Character.FORMATTER_MODIFIER.format(character.getWeaponFinesseTotalBonus()),
    // "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Dex Bonus",
    // character.getAttributeBonus(AttributeName.DEX), "td");
    // writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(),
    // "td");
    // writeLabelValuePair(writer, "misc", "Misc.",
    // character.getMeleeAttackMiscBonus(), "td");
    // writer.println("</tr>");
    // }
    //
    // //Write Ranged
    // writer.println("<tr class='ranged'>");
    // writeLabelValuePair(writer, "header", null, "Ranged", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // Character.FORMATTER_MODIFIER.format(character.getRangedAttackTotalBonus()),
    // "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Dex Bonus",
    // character.getAttributeBonus(AttributeName.DEX), "td");
    // writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(),
    // "td");
    // writeLabelValuePair(writer, "misc", "Misc.",
    // character.getRangedAttackMiscBonus(), "td");
    // writer.println("</tr>");
    //
    // //Close section
    // writer.println("</table>");
    //
    // //Write Two-Weapon Fighting, if applicable
    // if(character.hasTwoWeapons()){
    // writer.println("<table id='twoweapon'>");
    //
    // writer.println("<tr class='standard'>");
    // writeLabelValuePair(writer, "header", "", "Two-Weapon Fighting", "th");
    // writeLabelValuePair(writer, "primary", "Primary Hand",
    // Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackPrimaryBonus()),
    // "td");
    // writeLabelValuePair(writer, "offhand", "Off Hand",
    // Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackOffHandBonus()),
    // "td");
    // writer.println("</tr>");
    //
    // writer.println("</table>");
    // }
    //
    // //Open section
    // writer.println("<table id='maneuver'>");
    //
    // //Write CMB
    // writer.println("<tr class='cmb'>");
    // writeLabelValuePair(writer, "header", null, "CMB", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // Character.FORMATTER_MODIFIER.format(character.getCMBTotalBonus()), "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Str Bonus",
    // character.getAttributeBonus(AttributeName.STR), "td");
    // writeLabelValuePair(writer, "size", "Size",
    // (0-character.getSizeModifier()), "td");
    // writeLabelValuePair(writer, "misc", "Misc.", character.getCMBMiscBonus(),
    // "td");
    // writer.println("</tr>");
    //
    // //Write CMB/Weapon Finesse, if applicable
    // if(character.hasWeaponFinesse()){
    // writer.println("<tr class='cmbFinesse'>");
    // writeLabelValuePair(writer, "header", "Weapon Finesse", "CMB", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // Character.FORMATTER_MODIFIER.format(character.getCMBFinesseTotalBonus()),
    // "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Dex Bonus",
    // character.getAttributeBonus(AttributeName.DEX), "td");
    // writeLabelValuePair(writer, "size", "Size",
    // (0-character.getSizeModifier()), "td");
    // writeLabelValuePair(writer, "misc", "Misc.", character.getCMBMiscBonus(),
    // "td");
    // writer.println("</tr>");
    // }
    //
    // //Write CMD
    // writer.println("<tr class='cmd'>");
    // writeLabelValuePair(writer, "header", null, "CMD", "th");
    // writeLabelValuePair(writer, "total", "Total",
    // character.getCMDTotalBonus(), "td");
    // writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
    // writeLabelValuePair(writer, "base", "Base Attack",
    // character.getBaseAttack(), "td");
    // writeLabelValuePair(writer, "attribute", "Str Bonus",
    // character.getAttributeBonus(AttributeName.STR), "td");
    // writeLabelValuePair(writer, "attribute", "Dex Bonus",
    // character.getAttributeBonus(AttributeName.DEX, true), "td");
    // writeLabelValuePair(writer, "size", "Size", (0 -
    // character.getSizeModifier()), "td");
    // writeLabelValuePair(writer, "misc", "Misc.", character.getCMDMiscBonus(),
    // "td");
    // writer.println("</tr>");
    // writer.println("</table>");
    //
    // //Write conditional modifiers
    // Collection<UnmodifiableBonus> conditionals =
    // character.getConditionalModifiers(BonusType.ATTACK);
    // CollectionUtils.addAll(conditionals,
    // character.getConditionalModifiers(BonusType.COMBAT_MANEUVER).iterator());
    // CollectionUtils.addAll(conditionals,
    // character.getConditionalModifiers(BonusType.COMBAT_DEFENSE).iterator());
    // writeConditionals(writer, conditionals);
    //
    // LOGGER.trace("writeCombat: {}ms", System.currentTimeMillis()-start);
    // }
    //
    // protected void writeLanguages(PrintWriter writer, PSRDCharacter
    // character){
    // writer.println("<div id='languages'>");
    //
    // writer.println("<h2>Languages</h2>");
    //
    // writer.println("<ul>");
    //
    // for(String language: character.getData().getLanguages()){
    // writer.println("<li>" + language + "</li>");
    // }
    //
    // writer.println("</ul>");
    // writer.println("</div>");
    // }
    //
    // protected void writeOtherBonuses(PrintWriter writer, PSRDCharacter
    // character){
    // writer.println("<div id='otherBonuses'>");
    //
    // writer.println("<h2>Other Bonuses</h2>");
    //
    // Collection<UnmodifiableBonus> conditionals =
    // character.getConditionalModifiers(BonusType.OTHER);
    // writeConditionals(writer, conditionals);
    //
    // writer.println("</div>");
    // }

    protected void writeHindrances(PrintWriter writer, SwadeCharacter character){
        SwadeModel data = character.getData();
        Collection<Modifier> modifiers = character.getModifiers();
        Collection<String> buyoffs = character.getHindranceBuyoffs();

        Collection<HindranceWrapper> hindrances = data.getHindrances();
        if (CollectionUtils.isNotEmpty(hindrances)) {
            writer.println("<div id='hindrances'>");
            writer.println("<h2>Hindrances</h2>");
            writer.println("<ul>");
            Collection<SpecialAbility> allAbilities = new TreeSet<SpecialAbility>(gatherAbilities(hindrances));
            for (SpecialAbility ability : allAbilities) {
                writeHindrance(writer, ability, modifiers, null, buyoffs);
            }
            writer.println("</ul>");
            writer.println("</div>");
        }
    }

    protected void writeSpecialAbilities(PrintWriter writer, SwadeCharacter character){
        SwadeModel data = character.getData();
        Collection<Modifier> modifiers = character.getModifiers();

        // Write edges
        writer.println("<h3>Edges</h3>");
        writeSpecialAbilityList(character.getEdges(), writer, modifiers);

        // Validate edge count
        writer.println("<ul>");
        writer.println("<li>Earned edges: " + character.getEarnedEdges() + "</li>");
        // writer.println("<li>Used feats: " + character.getUsedEdges() +
        // "</li>");
        writer.println("</ul>");

        // Write racial abilities
        RaceWrapper race = data.getRace();
        if (race != null) {
            writer.println("<h3>" + race.getData().getName() + "</h3>");
            writeSpecialAbilityList(race.getData().getAbilities(), writer, modifiers);
        }

        // //Write other special abilities, if applicable
        // Collection<SpecialAbility> abilities = new HashSet<SpecialAbility>();
        // abilities.addAll(data.getBoons());
        // abilities.addAll(character.getSkillUnlocks());
        // abilities.addAll(character.getData().getQualities());
        // if(CollectionUtils.isNotEmpty(abilities)){
        // writer.println("<h3>Other Abilities</h3>");
        // writeSpecialAbilityList(abilities, writer, modifiers);
        // }
    }

    protected void writeSpecialAbilityList(Collection<? extends SpecialAbility> abilities,
            PrintWriter writer,
            Collection<Modifier> modifiers){
        writeSpecialAbilityList(abilities, writer, modifiers, null);
    }

    protected void writeSpecialAbilityList(Collection<? extends SpecialAbility> abilities,
            PrintWriter writer,
            Collection<Modifier> modifiers,
            Map<String, String> options){
        writer.println("<ul>");
        Collection<SpecialAbility> allAbilities = new TreeSet<SpecialAbility>(gatherAbilities(abilities));
        for (SpecialAbility ability : allAbilities) {
            writeBonusItem(writer, ability, modifiers, options);
        }
        writer.println("</ul>");
    }

    private Collection<SpecialAbility> gatherAbilities(Collection<? extends SpecialAbility> abilities){
        Collection<SpecialAbility> gathered = new HashSet<SpecialAbility>();

        for (SpecialAbility ability : abilities) {
            gathered.add(ability);

            // Also deal with any nested abilities (wizard schools, sorceror
            // bloodlines, etc.)
            if (ability instanceof AbilityProvider) {
                gathered.addAll(gatherAbilities(((AbilityProvider) ability).getAbilities()));
            }
        }

        return gathered;
    }

    protected void writeBonusItem(PrintWriter writer,
            SpecialAbility ability,
            Collection<Modifier> modifiers,
            Map<String, String> options){
        if (BonusHelper.applies(ability, modifiers)) {
            if (StringUtils.isNotBlank(ability.getDescription())) {
                // Search for option
                String title = ability.getName();
                if ((options != null) && options.containsKey(ability.getName())) {
                    title = title + " - " + options.get(ability.getName());
                }

                writer.println("<li>");
                writer.println("<span class='name'>" + title + "</span>");
                writer.println("<span class='description'>" + ability.getDescription() + "</span>");
                writer.println("</li>");
            } else {
                writer.println("<li>");
                writer.println("<span class='name'>" + ability.getName() + "</span>");
                writer.println("<span class='description'>Could not retrieve ability</span>");
                writer.println("</li>");
            }
        }
    }

    protected void writeHindrance(PrintWriter writer,
            SpecialAbility ability,
            Collection<Modifier> modifiers,
            Map<String, String> options,
            Collection<String> buyoffs){
        if (BonusHelper.applies(ability, modifiers)) {
            if (StringUtils.isNotBlank(ability.getDescription())) {
                // Search for option
                String title = ability.getName();
                if ((options != null) && options.containsKey(ability.getName())) {
                    title = title + " - " + options.get(ability.getName());
                }

                // Search for buyoff
                String buyoff = "";
                if (buyoffs.contains(ability.getName())) {
                    buyoff = "strike";
                }

                writer.println("<li class='" + buyoff + "'>");
                writer.println("<span class='name'>" + title + "</span>");
                writer.println("<span class='description'>" + ability.getDescription() + "</span>");
                writer.println("</li>");
            } else {
                writer.println("<li>");
                writer.println("<span class='name'>" + ability.getName() + "</span>");
                writer.println("<span class='description'>Could not retrieve ability</span>");
                writer.println("</li>");
            }
        }
    }

    protected void writeConditionals(PrintWriter writer, Collection<UnmodifiableBonus> conditionals){
        if (!CollectionUtils.isEmpty(conditionals)) {
            // Sort conditionals
            Collection<Bonus> sorted = new TreeSet<Bonus>(Bonus.COMPARATOR_BONUS_CONDITION);
            sorted.addAll(conditionals);

            // Write conditionals
            writer.println("<ul>");
            for (Bonus bonus : sorted) {
                writer.println("<li>");
                writer.println(bonus.getCondition());

                if (StringUtils.isNotBlank(bonus.getName())) {
                    String name = bonus.getName();

                    if (CollectionUtils.isNotEmpty(bonus.getItemKeys())) {
                        name = name + " (" + StringUtils.join(bonus.getItemKeys(), ", ") + ")";
                    }

                    writer.println("(" + name + ")");
                }

                writer.println("</li>");
            }
            writer.println("</ul>");
        }
    }

    protected void writePowers(PrintWriter writer, SwadeCharacter character){
        Set<PowerWrapper> powers = character.getData().getPowers();

        if ((powers != null) && (powers.size() > 0)) {
            writer.println("<div id='powers'>");
            writer.println("<h2>Powers</h2>");

            // Write power details
            writer.println("<table class='powerDetails'>");
            writer.println("<tr>");
            writer.println("<th class='name'>Name</th>");
            writer.println("<th class='trapping'>Trapping</th>");
            writer.println("<th class='rank'>Rank</th>");
            writer.println("<th class='powerPoints'>Power Points</th>");
            writer.println("<th class='range'>Range</th>");
            writer.println("<th class='duration'>Duration</th>");
            writer.println("<th class='damage'>Damage</th>");
            writer.println("<th class=description'>Description</th>");
            writer.println("</tr>");

            int count = 1;
            for (PowerWrapper power : powers) {
                StaticPower staticPower = power.getData();

                String marker = (count % 2 == 0) ? "marker" : "";
                count++;

                // Write name
                writer.println("<tr class='" + marker + "'>");

                writer.println("<td>" + staticPower.getName() + "</td>");
                writer.println("<td>" + power.getTrapping() + "</td>");
                writer.println("<td>" + staticPower.getRank() + "</td>");
                writer.println("<td>" + staticPower.getPowerPoints() + "</td>");
                writer.println("<td>" + staticPower.getRange() + "</td>");
                writer.println("<td>" + staticPower.getDuration() + "</td>");
                writer.println("<td>" + formatNullable(staticPower.getDamage()) + "</td>");
                writer.println("<td>" + staticPower.getDescription() + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");

            // Write power points
            writer.println("<h3>Power Points</h3>");
            writer.println("<div>Total Power Points: " + character.getPowerPoints() + "</div>");
            writer.println("<div>" + generateChargeBlocks(character.getPowerPoints()) + "</div>");
            writer.println("</div>");
        }
    }

    // private String formatSpellsPerDay(ClassWrapper characterClass, Integer
    // base, int level) {
    // String result;
    // if (StringUtils.equalsIgnoreCase(Character3E.ID_CLASS_CLERIC,
    // characterClass.getId())
    // && (base != null)){
    // result = formatNullable(base) + "+1";
    // } else {
    // result = formatNullable(base);
    // }
    // return result;
    // }
    //
    // /**
    // * Writes the details of a SpellSet.
    // *
    // * @param writer The writer.
    // * @param spellSet The SpellSet.
    // * @param levels The number of levels the character has in the applicable
    // spellcasting class(es).
    // */
    // private void writeSpellSet(PrintWriter writer, SpellSet spellSet, int
    // levels) {
    // int count = 1;
    // for(SpellWrapper characterSpell : spellSet){
    // StaticSpell spell = characterSpell.getData();
    // String marker = (count%2 == 0) ? "marker" : "";
    // count++;
    //
    // writer.println("<tr class='" + marker + "'>");
    // writer.println("<td rowspan='2' class='name'>" + spell.getName() +
    // "</td>");
    // writer.println("<td class='castingTime'>" + spell.getCastingTime() +
    // "</td>");
    // writer.println("<td class='duration'>" + spell.getDuration() + "</td>");
    // writer.println("<td class='save'>" + formatNullable(spell.getSave()) +
    // "</td>");
    // writer.println("<td class='spellResistance'>" +
    // formatBoolean(spell.isSpellResistance()) + "</td>");
    // writer.println("<td class='range'>" + formatSpellRange(spell.getRange(),
    // levels) + "</td>");
    // writer.println("<td class='target'>" + spell.getTarget() + "</td>");
    // writer.println("<td class='components'>" + spell.getComponents() +
    // "</td>");
    // writer.println("<td class='school'>" + spell.getSchool() + "</td>");
    // writer.println("</tr>");
    // writer.println("<tr class='" + marker + "'>");
    // writer.println("<td colspan='8' class='description'>" +
    // spell.getDescription() + "</td>");
    // writer.println("</tr>");
    // }
    // }
    //
    // /**
    // * @param range The spell range in question
    // * @param classLevels The number of levels the character has in the
    // approriate class
    // * @return The text to output for this spell's range
    // */
    // private String formatSpellRange(SpellRange range, int classLevels) {
    // String label = null;
    //
    // if(range != null){
    // label = range.getName();
    // int distance = range.getRange(classLevels);
    //
    // if(distance > 0){
    // label = label + " - " + distance + " ft.";
    // }
    // }
    //
    // return label;
    // }
    //
    // /**
    // * @param value The boolean value to convert
    // * @return "yes" for true; "no" for false.
    // */
    // private String formatBoolean(boolean value) {
    // return (value) ? "yes" : "no";
    // }

    protected String formatNullable(Object item){
        String result = "-";

        if ((item != null) && StringUtils.isNotBlank(item.toString())) {
            result = item.toString();
        }

        return result;
    }

    protected String formatBlankable(Object item){
        String result = null;

        if (item != null) {
            result = item.toString();
        }
        if (StringUtils.isBlank(result)) {
            result = "&nbsp;";
        }

        return result;
    }

    protected String formatModifier(Integer modifier){
        String result = "";

        if (modifier != null) {
            if (modifier < 0) {
                result = " - " + Math.abs(modifier);
            } else if (modifier > 0) {
                result = " + " + modifier;
            }
        }

        return result;
    }

    protected void writeWeapons(PrintWriter writer, SwadeCharacter character){
        Set<GearWrapper> weaponSet = character.getWeaponSet();

        if (CollectionUtils.isNotEmpty(weaponSet)) {
            // Open section
            writer.println("<div id='weapons'>");
            writer.println("<h2>Weapons/Attacks</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>Weapon</th>");
            writer.println("<th class='damage'>Damage</th>");
            writer.println("<th class='minStr'>Min. Str.</th>");
            writer.println("<th class='weight'>Weight (Lbs.)</th>");
            writer.println("<th class='cost'>Cost</th>");
            writer.println("<th class='notes'>Notes</th>");
            writer.println("</tr>");

            // Write weapons
            for (GearWrapper weapon : weaponSet) {
                StaticGear data = weapon.getData();

                writer.println("<tr>");
                writer.println("<td class='name'>" + data.getName() + "</td>");
                writer.println("<td class='damage'>" + data.getDamage() + "</td>");
                writer.println("<td class='minStr'>" + data.getMinStr() + "</td>");
                writer.println("<td class='weight'>" + data.getWeight() + "</td>");
                writer.println("<td class='cost'>" + data.getCost() + "</td>");
                writer.println("<td class='notes'>" + formatBlankable(data.getNotes()) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    // protected void writeAmmunition(PrintWriter writer, PSRDCharacter
    // character){
    // Set<Ammunition> ammoSet = character.getAmmunitionSet();
    //
    // if(CollectionUtils.isNotEmpty(ammoSet)){
    // //Open section
    // writer.println("<div id='ammo'>");
    // writer.println("<h2>Ammunition</h2>");
    // writer.println("<table>");
    //
    // writer.println("<tr>");
    // writer.println("<th class='name'>Ammunition</th>");
    // writer.println("<th class='quantity'>Quantity</th>");
    // writer.println("<th class='spent'>Spent</th>");
    // writer.println("</tr>");
    //
    // //Write weapons
    // for(Ammunition ammo : ammoSet){
    // writer.println("<tr>");
    // writer.println("<td class='name'>" + ammo.getDisplayName() + "</td>");
    // writer.println("<td class='quantity'>" +
    // Character.FORMATTER_THOUSANDS.format(ammo.getQuantity()) + "</td>");
    // writer.println("<td class='spent'>" +
    // generateChargeBlocks(ammo.getQuantity()) + "</td>");
    // writer.println("</tr>");
    // }
    //
    // //Close section
    // writer.println("</table>");
    // writer.println("</div>");
    // }
    // }
    //
    // protected void writeChargedItems(PrintWriter writer, PSRDCharacter
    // character){
    // Set<Charge> charges = character.getCharges();
    //
    // if(CollectionUtils.isNotEmpty(charges)){
    // //Open section
    // writer.println("<div id='chargedItems'>");
    // writer.println("<h2>Charged Items</h2>");
    // writer.println("<table>");
    //
    // writer.println("<tr>");
    // writer.println("<th class='name'>Name</th>");
    // writer.println("<th class='quantity'>Charges</th>");
    // writer.println("<th class='spent'>Used</th>");
    // writer.println("</tr>");
    //
    // //Write weapons
    // for(Charge charge : charges){
    // writer.println("<tr>");
    // writer.println("<td class='name'>" + charge.getName() + "</td>");
    // writer.println("<td class='quantity'>" +
    // Character.FORMATTER_THOUSANDS.format(charge.getQuantity()) + "</td>");
    // writer.println("<td class='spent'>" +
    // generateChargeBlocks(charge.getQuantity()) + "</td>");
    // writer.println("</tr>");
    // }
    //
    // //Close section
    // writer.println("</table>");
    // writer.println("</div>");
    // }
    // }

    private String generateChargeBlocks(int quantity){
        StringBuilder blocks = new StringBuilder();

        // Append full sets of 5
        int numBlocks = quantity / 5;
        blocks.append(StringUtils.repeat(BOXES, numBlocks));

        // Append remainder
        int remainder = quantity % 5;
        if (remainder > 0) {
            blocks.append(StringUtils.repeat(BOX_EMPTY, remainder));
            blocks.append(StringUtils.repeat(BOX_MARKED, 5 - remainder));
        }

        return blocks.toString();
    }

    // protected void writeArmor(PrintWriter writer, PSRDCharacter character){
    // Set<Armor> armorSet = character.getArmorSet();
    //
    // if(CollectionUtils.isNotEmpty(armorSet)){
    // //Open section
    // writer.println("<div id='armor'>");
    // writer.println("<h2>Armor</h2>");
    // writer.println("<table>");
    //
    // writer.println("<tr>");
    // writer.println("<th class='name'>AC Item</th>");
    // writer.println("<th class='bonus'>AC Bonus</th>");
    // writer.println("<th class='type'>Armor Type</th>");
    // writer.println("<th class='dex'>Max. Dex.</th>");
    // writer.println("<th class='penalty'>Check Penalty</th>");
    // writer.println("<th class='spellFailure'>Spell Failure</th>");
    // writer.println("<th class='weight'>Weight (Lbs.)</th>");
    // writer.println("<th class='notes'>Special</th>");
    // writer.println("</tr>");
    //
    // //Write armor
    // for(Armor armor : armorSet){
    // writer.println("<tr>");
    // writer.println("<td class='name'>" + armor.getDisplayName() + "</td>");
    // writer.println("<td class='bonus'>" +
    // Character.FORMATTER_MODIFIER.format(character.getArmorBonus(armor)) +
    // "</td>");
    // writer.println("<td class='type'>" +
    // armor.getData().getProficiency().getAbbreviation() + "</td>");
    // writer.println("<td class='dex'>" +
    // formatNullable(character.getArmorMaxDex(armor)) + "</td>");
    // writer.println("<td class='penalty'>" +
    // formatNullable(character.getTotalArmorPenalty(armor)) + "</td>");
    // writer.println("<td class='spellFailure'>" +
    // armor.getData().getArcaneFailure() + "%</td>");
    // writer.println("<td class='weight'>" + armor.getWeight() + "</td>");
    // writer.println("<td class='notes'>" + formatSpecialAbilities(armor,
    // character) + "</td>");
    // writer.println("</tr>");
    // }
    //
    // //Close section
    // writer.println("</table>");
    // writer.println("</div>");
    // }
    // }
    //
    // private String formatSpecialAbilities(AbstractEquipment<?> item,
    // PSRDCharacter character) {
    // Set<String> names = new HashSet<String>();
    //
    // //Add ability & bonus names
    // Collection<NamedObject> namedObjs =
    // com.westpalmetto.gaming.character.util.CollectionHelper
    // .<NamedObject>
    // newHashSet(item.getAbilities(item.getBonuses(character.getModifiers())),
    // item.getBonuses(character.getModifiers()));
    // for(NamedObject obj : namedObjs){
    // if(StringUtils.isNotBlank(obj.getName())){
    // names.add(obj.getName());
    // }
    // }
    //
    // //Add notes
    // //TODO: Notes
    //
    // //Remove name & ID
    // names.remove(item.getName());
    // names.remove(item.getId());
    //
    // return formatBlankable(StringUtils.join(names, "; "));
    // }
    //
    // protected void writeEquipment(PrintWriter writer, PSRDCharacter
    // character){
    // EquipmentSet equipment = character.getData().getEquipment();
    //
    // //Open section
    // writer.println("<div id='equipment'>");
    //
    // //Write equipment
    // Map<String, Set<AbstractEquipment<?>>> locationsMap =
    // equipment.getEquipmentByLocation();
    // writer.println("<h2>Equipment</h2>");
    //
    // //Iterate through locations
    // for(Map.Entry<String, Set<AbstractEquipment<?>>> location :
    // locationsMap.entrySet()){
    // if(AbstractEquipment.LOCATION_EQUIPPED.equals(location.getKey())){
    // writeEquipmentEquipped(writer, locationsMap);
    // } else {
    // writeEquipmentContainer(writer, location.getKey(), locationsMap);
    // }
    // }
    //
    // //Write coins
    // Coins coins = equipment.getCoins();
    // if((coins.getQuantity() > 0) || (coins.getGems() > 0)){
    // writer.println("<h2>Money</h2>");
    // writer.println("<div id='coins'>");
    // writeLabelValuePair(writer, "copper", "Copper Pieces",
    // Character.FORMATTER_THOUSANDS.format(coins.getCopper()));
    // writeLabelValuePair(writer, "silver", "Silver Pieces",
    // Character.FORMATTER_THOUSANDS.format(coins.getSilver()));
    // writeLabelValuePair(writer, "gold", "Gold Pieces",
    // Character.FORMATTER_THOUSANDS.format(coins.getGold()));
    // writeLabelValuePair(writer, "platinum", "Platinum Pieces",
    // Character.FORMATTER_THOUSANDS.format(coins.getPlatinum()));
    // writeLabelValuePair(writer, "gems", "Gems",
    // Character.FORMATTER_THOUSANDS.format(coins.getGems()));
    // writer.println("</div>");
    // }
    //
    // //Determine current load
    // CarryingCapacity current = character.getCarryingCapacity();
    // String lightClass = "light";
    // String mediumClass = "medium";
    // String heavyClass = "heavy";
    // switch (current){
    // case LIGHT:
    // lightClass = "light load";
    // break;
    // case MEDIUM:
    // mediumClass = "medium load";
    // break;
    // case HEAVY:
    // heavyClass = "heavy load";
    // break;
    // }
    //
    // //Write carrying capacity
    // writer.println("<h2>Carrying Capacity (Maximum in Lbs.)</h2>");
    // writer.println("<div id='carryingCapacity'>");
    // writeLabelValuePair(writer, lightClass, "Light Load",
    // character.getCarryingCapacityLight());
    // writeLabelValuePair(writer, mediumClass, "Medium Load",
    // character.getCarryingCapacityMedium());
    // writeLabelValuePair(writer, heavyClass, "Heavy Load",
    // character.getCarryingCapacityHeavy());
    // writeLabelValuePair(writer, "overHead", "Lift Over Head",
    // character.getCarryingCapacityOverHead());
    // writeLabelValuePair(writer, "offGround", "Lift Off Ground",
    // character.getCarryingCapacityOffGround());
    // writeLabelValuePair(writer, "drag", "Push/Drag",
    // character.getCarryingCapacityPushDrag());
    // writer.println("</div>");
    //
    // //Close section
    // writer.println("</div>");
    // }
    // /**
    // * Write the details of equipped equipment.
    // *
    // * @param writer The writer.
    // * @param locationsMap The map of equipment by location.
    // */
    // private void writeEquipmentEquipped(PrintWriter writer,
    // Map<String, Set<AbstractEquipment<?>>> locationsMap) {
    // writer.println("<div class='location' id='equipped'>");
    // writer.println("<h3>" + AbstractEquipment.LOCATION_EQUIPPED + "</h3>");
    // writer.println("<table>");
    // writer.println("<tr>");
    // writer.println("<th class='slot'>Slot</th>");
    // writer.println("<th class='name'>Item</th>");
    // writer.println("<th class='quantity'>Qty</th>");
    // writer.println("<th class='weight'>Weight</th>");
    // writer.println("<th class='totalWeight'>Total Weight</th>");
    // writer.println("</tr>");
    //
    // //Iterate through items in the location
    // Map<Slot, Set<AbstractEquipment<?>>> bySlot =
    // EquipmentSet.bySlot(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED));
    // for(Entry<Slot, Set<AbstractEquipment<?>>> slot : bySlot.entrySet()){
    // List<AbstractEquipment<?>> items = new
    // ArrayList<AbstractEquipment<?>>(slot.getValue());
    // int size = items.size();
    // for (int i=0; i<size; i++){
    // AbstractEquipment<?> item = items.get(i);
    //
    // if(i==0){
    // writer.println("<tr>");
    // writer.println("<th class='slot' rowspan='" + size + "'>" +
    // item.getData().getSlot().getName() + "</th>");
    // writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
    // writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
    // writer.println("<td class='weight'>" + item.getWeight() + "</td>");
    // writer.println("<td class='totalWeight'>" +
    // EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
    // writer.println("</tr>");
    // } else {
    // writer.println("<tr>");
    // writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
    // writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
    // writer.println("<td class='weight'>" + item.getWeight() + "</td>");
    // writer.println("<td class='totalWeight'>" +
    // EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
    // writer.println("</tr>");
    // }
    // }
    // }
    //
    // //Output the total weight for the container
    // writer.println("<tr class='containerTotalWeight'>");
    // writer.println("<th colspan='4'>Total Weight</th>");
    // writer.println("<td class='totalWeight'>" +
    // EquipmentSet.getTotalWeight(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED),
    // locationsMap) + "</td>");
    // writer.println("</tr>");
    //
    // //Close the location container
    // writer.println("</table>");
    // writer.println("</div>");
    // }
    //
    //
    // /**
    // * Write the details of a container of equipment.
    // *
    // * @param writer The writer.
    // * @param name The name of the container.
    // * @param locationsMap The map of equipment by location.
    // */
    // private void writeEquipmentContainer(PrintWriter writer,
    // String name,
    // Map<String, Set<AbstractEquipment<?>>> locationsMap) {
    // writer.println("<div class='location'>");
    // writer.println("<h3>" + name + "</h3>");
    // writer.println("<table>");
    // writer.println("<tr>");
    // writer.println("<th class='name'>Item</th>");
    // writer.println("<th class='quantity'>Qty</th>");
    // writer.println("<th class='weight'>Weight</th>");
    // writer.println("<th class='totalWeight'>Total Weight</th>");
    // writer.println("</tr>");
    //
    // //Iterate through items in the location
    // for(AbstractEquipment<?> item : locationsMap.get(name)){
    // writer.println("<tr>");
    // writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
    // writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
    // writer.println("<td class='weight'>" + item.getWeight() + "</td>");
    // writer.println("<td class='totalWeight'>" +
    // EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
    // writer.println("</tr>");
    // }
    //
    // //Output the total weight for the container
    // writer.println("<tr class='containerTotalWeight'>");
    // writer.println("<th colspan='3'>Total Weight</th>");
    // writer.println("<td class='totalWeight'>" +
    // EquipmentSet.getTotalWeight(locationsMap.get(name), locationsMap) +
    // "</td>");
    // writer.println("</tr>");
    //
    // //Close the location container
    // writer.println("</table>");
    // writer.println("</div>");
    // }
    //
    // protected void writeNotes(PrintWriter writer, PSRDCharacter character) {
    // writer.println("<h2>Notes</h2>");
    // writer.println("<ul id='notes'>");
    //
    // for (String note : character.getData().getNotes()){
    // writer.println("<li>" + note + "</li>");
    // }
    //
    // writer.println("</ul>");
    // }
}
