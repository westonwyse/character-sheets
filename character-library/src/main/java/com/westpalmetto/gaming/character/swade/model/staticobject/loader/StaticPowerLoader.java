package com.westpalmetto.gaming.character.swade.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.swade.model.staticobject.StaticPower;
import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

public class StaticPowerLoader extends JsonDataLoader<StaticPower>{
    private static final Map<File, StaticPowerLoader> INSTANCES = new HashMap<>();

    public static StaticPowerLoader getInstance(File root){
        if (!INSTANCES.containsKey(root)) {
            INSTANCES.put(root, new StaticPowerLoader(root));
        }
        return INSTANCES.get(root);
    }

    private StaticPowerLoader(File root){
        super("power", StaticPower.class, root);
    }

    @Override
    public StaticPower getClonedObject(StaticPower original){
        return new StaticPower(original);
    }
}
