/**
 * WeaponCombatTypePredicate
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.predicate;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.CombatType;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;

/**
 * Predicate to match weapons by combat type.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class WeaponCombatTypePredicate implements Predicate {
	public static final WeaponCombatTypePredicate MELEE = new WeaponCombatTypePredicate(CombatType.MELEE);
	public static final WeaponCombatTypePredicate RANGED = new WeaponCombatTypePredicate(CombatType.RANGED);
	
	private CombatType type;
	
	private WeaponCombatTypePredicate(CombatType type){
		this.type = type;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Weapon){
			Weapon weapon = (Weapon)object;
			matches = (type.equals(weapon.getData().getCombatType()));
		}
		
		return matches;
	}
}
