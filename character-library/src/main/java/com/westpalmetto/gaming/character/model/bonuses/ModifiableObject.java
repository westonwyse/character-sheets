package com.westpalmetto.gaming.character.model.bonuses;

import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;

/**
 * Interface to define an object that can be modified or limited by Modifiers.
 */
public interface ModifiableObject extends KeyedObject{
	/**
	 * @return The value at which this object is applied.
	 */
	public int getApplies();

	/**
	 * @return The type of modifier determining when this object is applied.
	 */
	public ModifierType getAppliesType();

	/**
	 * @return A key for the applies methods other than the displayed item keys.
	 */
	public String getAppliesKey();
}
