package com.westpalmetto.util.resources.loader;

import java.util.concurrent.Callable;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.westpalmetto.util.resources.ResourceLoader;
import com.westpalmetto.util.resources.model.Model;

public abstract class AbstractResourceLoader<T> extends Model
		implements ResourceLoader<T>, Callable<T> {
	private final ListeningExecutorService executor;
	
	public AbstractResourceLoader(ListeningExecutorService executor){
		this.executor = executor;
	}
	
	@Override
	public ListenableFuture<T> queueResource() {
		return getExecutor().submit(this);
	}

	@Override
	public T call() throws Exception {
		return getResource();
	}

	/**
	 * @return the executor
	 */
	public ListeningExecutorService getExecutor() {
		return executor;
	}
}
