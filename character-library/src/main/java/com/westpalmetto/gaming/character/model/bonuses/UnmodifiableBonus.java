/**
 * Bonus.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.model.Damage;
import com.westpalmetto.util.resources.model.Model;

import lombok.Value;

/**
 * Defines a mechanical bonus provided by some object.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Value
public class UnmodifiableBonus extends Model implements Bonus, KeyedObject{
    private final BonusType type;
    private final int value;
    private final Damage valueDie;
    private final String valueType;
    private final String valueName;
    private final boolean requireItemKey;
    private final String name;
    private final String source;
    private final Set<String> itemKeys;
    private final String condition;

    /**
     * Copy constructor.
     * 
     * @param bonus The Bonus object to copy
     */
    public UnmodifiableBonus(Bonus bonus){
        this.type = bonus.getType();
        this.value = bonus.getValue();
        this.valueDie = bonus.getValueDie();
        this.valueType = bonus.getValueType();
        this.valueName = bonus.getValueName();
        this.requireItemKey = bonus.isRequireItemKey();
        this.name = bonus.getName();
        this.source = bonus.getSource();
        bonus.getItemKeys().remove(null);
        this.itemKeys = ImmutableSet.copyOf(bonus.getItemKeys());
        this.condition = bonus.getCondition();

        BonusHelper.validateItemKeys(this);
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }

    @Override
    public void setItemKeys(Collection<String> keys){
        throw new UnsupportedOperationException("Object is immutable.");
    }
}
