package com.westpalmetto.gaming.character.psrd.model;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.Advancement;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.psrd.model.staticobject.wrapper.TraitWrapper;

/**
 * A character class for PSRD characters.
 */
public class PSRDCharacterModel extends DD3EModel{
    private String advancementTable;
    private FavoredClassBonuses favoredClassBonuses;
    private BonusProviderSet<TraitWrapper> traits;

    public PSRDCharacterModel(File resourceRoot){
        super(resourceRoot);
        // //Setup attributes
        // AttributeSet attributes = new AttributeSet();
        // attributes.add(new Attribute(AttributeName.DEX, 10));
        // attributes.add(new Attribute(AttributeName.STR, 10));
        // attributes.add(new Attribute(AttributeName.CON, 10));
        // attributes.add(new Attribute(AttributeName.INT, 10));
        // attributes.add(new Attribute(AttributeName.WIS, 10));
        // attributes.add(new Attribute(AttributeName.CHA, 10));
        // setAttributes(attributes);
    }

    /**
     * @return the advancementTable
     */
    public String getAdvancementTable(){
        if (StringUtils.isBlank(advancementTable)) {
            throw new NullPointerException("Advancement table is not specified.");
        }
        return advancementTable;
    }

    /**
     * @param advancementTable the advancementTable to set
     */
    public void setAdvancementTable(String advancementTable){
        this.advancementTable = advancementTable;
    }

    @Override
    public Advancement getAdvancement(){
        return PSRDAdvancementTable.valueOf(getAdvancementTable());
    }

    /**
     * @return the favoredClassBonuses
     */
    public FavoredClassBonuses getFavoredClassBonuses(){
        if (favoredClassBonuses == null) {
            setFavoredClassBonuses(new FavoredClassBonuses());
        }
        return favoredClassBonuses;
    }

    /**
     * @param favoredClassBonuses the favoredClassBonuses to set
     */
    public void setFavoredClassBonuses(FavoredClassBonuses favoredClassBonuses){
        this.favoredClassBonuses = favoredClassBonuses;
    }

    /**
     * @return the traits
     */
    public BonusProviderSet<TraitWrapper> getTraits(){
        if (traits == null) {
            setTraits(new BonusProviderSet<TraitWrapper>());
        }
        return traits;
    }

    /**
     * @param traits the traits to set
     */
    public void setTraits(BonusProviderSet<TraitWrapper> traits){
        this.traits = traits;
    }

    /**
     * The favored class bonus structure.
     */
    public static class FavoredClassBonuses{
        private int hitPoints;
        private int skillPoints;

        /**
         * @return the hitPoints
         */
        public int getHitPoints(){
            return hitPoints;
        }

        /**
         * @param hitPoints the hitPoints to set
         */
        public void setHitPoints(int hitPoints){
            this.hitPoints = hitPoints;
        }

        /**
         * @return the skillPoints
         */
        public int getSkillPoints(){
            return skillPoints;
        }

        /**
         * @param skillPoints the skillPoints to set
         */
        public void setSkillPoints(int skillPoints){
            this.skillPoints = skillPoints;
        }
    }

    /**
     * The advancement tables for PSRD characters.
     */
    public enum PSRDAdvancementTable implements Advancement{
        SLOW(new int[] { 0,
                3000,
                7500,
                14000,
                23000,
                35000,
                53000,
                77000,
                115000,
                160000,
                235000,
                330000,
                475000,
                665000,
                955000,
                1350000,
                1900000,
                2700000,
                3850000,
                5350000 }),
        MEDIUM(new int[] { 0,
                2000,
                5000,
                9000,
                15000,
                23000,
                35000,
                51000,
                75000,
                105000,
                155000,
                220000,
                315000,
                445000,
                635000,
                890000,
                1300000,
                1800000,
                2550000,
                3600000 }),
        FAST(new int[] { 0,
                1300,
                3300,
                6000,
                10000,
                15000,
                23000,
                34000,
                50000,
                71000,
                105000,
                145000,
                210000,
                295000,
                425000,
                600000,
                850000,
                1200000,
                1700000,
                2400000 }),;

        private int[] table;

        private PSRDAdvancementTable(int[] table){
            this.table = table;
        }

        @Override
        public int getLevel(int experience){
            int level;
            for (level = 0; level < table.length; level++) {
                if (experience < table[level]) {
                    break;
                }
            }
            return level;
        }

        @Override
        public int getExperience(int level){
            return table[level - 1];
        }

        @Override
        public int getNextLevel(int experience){
            return table[getLevel(experience)];
        }
    }
}
