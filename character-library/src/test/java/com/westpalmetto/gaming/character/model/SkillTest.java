package com.westpalmetto.gaming.character.model;


import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;

import junit.framework.Assert;

public class SkillTest {
	private DD3ESkill target;

	@Before
	public void setUp() throws Exception {
		target = new DD3ESkill(null, null, false);
	}

	@Test
	public void testArmorPenalty(){
		target.setAttributeName(AttributeName.STR);
		Assert.assertTrue("Armor check penalty should apply.", target.isArmorPenalty());
		target.setAttributeName(AttributeName.DEX);
		Assert.assertTrue("Armor check penalty should apply.", target.isArmorPenalty());
		target.setAttributeName(AttributeName.CON);
		Assert.assertFalse("Armor check penalty should not apply.", target.isArmorPenalty());
		target.setAttributeName(AttributeName.INT);
		Assert.assertFalse("Armor check penalty should not apply.", target.isArmorPenalty());
		target.setAttributeName(AttributeName.CHA);
		Assert.assertFalse("Armor check penalty should not apply.", target.isArmorPenalty());
		target.setAttributeName(AttributeName.WIS);
		Assert.assertFalse("Armor check penalty should not apply.", target.isArmorPenalty());
	}
}
