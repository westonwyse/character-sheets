package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Collection;
import java.util.Set;

import com.westpalmetto.gaming.character.NamedObject;

/**
 * Interface to define an object that has an item key.
 */
public interface KeyedObject extends NamedObject{
	/**
	 * @return the itemKeys
	 */
	public Set<String> getItemKeys();
	/**
	 * @param keys the itemKeys
	 */
	public void setItemKeys(Collection<String> keys);
	/**
	 * If the requireItemKey flag is true, then the itemKey must be set.
	 * @return the requiresItemKey
	 */
	public boolean isRequireItemKey();
}
