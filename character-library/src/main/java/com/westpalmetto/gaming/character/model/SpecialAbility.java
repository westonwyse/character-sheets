/**
 * SpecialAbility.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 3, 2013
 */
package com.westpalmetto.gaming.character.model;

import java.math.BigDecimal;

import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.model.bonuses.KeyedObject;

/**
 * Definition of basic functionality needed for an ability/feat/trait/etc.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface SpecialAbility extends BonusProvider, KeyedObject{
    /**
     * @return The description of this ability.
     */
    public String getDescription();

    /**
     * @param description The descriptions to set.
     */
    public void setDescription(String description);

    /**
     * @return The cost of this ability, if point-buy.
     */
    public BigDecimal getCost();

    /**
     * @return Whether or not to display this ability.
     */
    default boolean isDisplay(){
        return true;
    }
}
