package com.westpalmetto.gaming.character.swade.model;

import com.westpalmetto.gaming.character.model.Skill;
import com.westpalmetto.gaming.character.swade.model.Attribute.AttributeName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SwadeSkill extends Skill{
    private AttributeName attributeName;

    public SwadeSkill(){}

    public SwadeSkill(String name, AttributeName attribueName, int ranks){
        super(name, null, ranks);
        this.attributeName = attribueName;
    }

    public String getStep(){
        return Steps.getStep(getRanks());
    }
}
