/**
 * WeaponHeftPredicate
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.predicate;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.Heft;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;


/**
 * Predicate to match weapons by heft.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class WeaponHeftPredicate implements Predicate {
	public static final WeaponHeftPredicate LIGHT = new WeaponHeftPredicate(Heft.LIGHT);
	public static final WeaponHeftPredicate ONEHAND = new WeaponHeftPredicate(Heft.ONEHAND);
	public static final WeaponHeftPredicate TWOHAND = new WeaponHeftPredicate(Heft.TWOHAND);
	
	private Heft heft;
	
	private WeaponHeftPredicate(Heft heft){
		this.heft = heft;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Weapon){
			Weapon weapon = (Weapon)object;
			matches = (heft.equals(weapon.getData().getHeft()));
		}
		
		return matches;
	}
}
