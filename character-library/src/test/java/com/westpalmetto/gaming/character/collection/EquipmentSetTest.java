/**
 * EquipmentSetTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.collection;

import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Equipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.predicate.EquippedItemPredicate;

/**
 * Tests for EquipmentSet.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class EquipmentSetTest {
	private EquipmentSet equipment;

	@Before
	public void setup() {
		Equipment basic = new Equipment("Waterskin");
		basic.setLocation(AbstractEquipment.LOCATION_EQUIPPED);
		Weapon weapon = new Weapon("Waraxe, dwarven double");
		weapon.setLocation(AbstractEquipment.LOCATION_EQUIPPED);
		Armor armor = new Armor("Leather");
		armor.setLocation(AbstractEquipment.LOCATION_EQUIPPED);
		
		Equipment ring1 = new Equipment("Ring of Energy Resistance, Minor");
		ring1.getItemKeys().add("Acid");
		ring1.setLocation(AbstractEquipment.LOCATION_EQUIPPED);
		Equipment ring2 = new Equipment("Ring of Energy Resistance, Minor");
		ring2.getItemKeys().add("Electricity");
		ring2.setLocation(AbstractEquipment.LOCATION_EQUIPPED);

		EquipmentSet set = new EquipmentSet();
		set.add(basic);
		set.add(weapon);
		set.add(armor);
		set.add(ring1);
		set.add(ring2);
		
		equipment = new EquipmentSet();
		CollectionUtils.select(set, EquippedItemPredicate.getInstance(), equipment);
	}

	@Test
	public void testGetBasicSet() {
		Set<Equipment> basic = equipment.getBasicSet();
		Assert.assertEquals("Basic set not as expected: " + basic, 3,
				basic.size());
	}

	@Test
	public void testGetWeaponSet() {
		Set<Weapon> weapons = equipment.getWeaponSet();
		Assert.assertEquals("Weapons set not as expected: " + weapons, 1,
				weapons.size());
	}

	@Test
	public void testGetArmorSet() {
		Set<Armor> armors = equipment.getArmorSet();
		Assert.assertEquals("Armor set not as expected: " + armors, 1,
				armors.size());
	}

	@Test
	public void testDifferentQualties() {
		Weapon weapon2 = new Weapon("Net");
		weapon2.setQuality(Weapon.Quality.PLUS5);
		equipment.add(weapon2);

		Set<Weapon> weapons = equipment.getWeaponSet();
		Assert.assertEquals("Weapons set not as expected: " + weapons, 2,
				weapons.size());
	}
}
