/**
 * AbstractStaticObjectWrapper.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.gaming.character.util.loading.DataLoader;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Contains basic functionality for a StaticDataWrapper
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public abstract class AbstractStaticObjectWrapper<V extends StaticObject>
		extends Model implements StaticObjectWrapper<V>{
	private String id;
	private String ruleset;
	private DataLoader<V> dataLoader;
	
	public AbstractStaticObjectWrapper(String id, String ruleset) {
		this.id=id;
		this.ruleset=ruleset;
	}
	
	/**
	 * @return The DataLoader instance to load this type of data.
	 */
	@Override
	public DataLoader<V> getDataLoader() {
		if (dataLoader == null) {
			dataLoader = createDataLoader();
		}
		if (dataLoader == null) {
			throw new IllegalArgumentException("Created null data loader for " + getClass().getName());
		}
		return dataLoader;
	}
	
	public String getRuleset() {
		if(ruleset==null) {
			ruleset = ResourceFactory.Ruleset.PSRD.name();
		}
		return ruleset;
	}

	protected abstract DataLoader<V> createDataLoader();
	
	protected File getRoot() {
		return ResourceFactory.rootForName(getRuleset());
	}
	
	@Override
	public V getData(){
		return getDataLoader().getData(getKey());
	}

	/**
	 * @return The key to be used for getting the object from the data loader.
	 */
	protected String getKey() {
		return getId();
	}
}
