package com.westpalmetto.gaming.character.model;

import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;

/**
 * Marker interface for classes that provide special abilities.
 */
public interface AbilityProvider extends BonusProvider  {
	public BonusProviderSet<? extends SpecialAbility> getAbilities();
}
