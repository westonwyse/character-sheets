package com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Sets;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.StaticTheme;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.loader.StaticThemeLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Contains the character-specific details of a character's theme.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Value
@EqualsAndHashCode(callSuper = true)
public class ThemeWrapper extends AbstractBonusProviderWrapper<StaticTheme> implements BonusProvider {
	private AttributeName chosenAttribute;
	private String chosenSkill;

	/**
	 * @param name The ID
	 */
	public ThemeWrapper(String id) {
		super(id, ResourceFactory.Ruleset.SFSRD.toString());
		this.chosenAttribute = null;
		this.chosenSkill = null;
	}

	@Override
	protected DataLoader<StaticTheme> createDataLoader() {
		return StaticThemeLoader.getInstance(getRoot());
	}

	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		Collection<ModifiableBonus> bonuses = super.getBonuses(modifiers);
		if (getChosenAttribute() != null) {
			ModifiableBonus attrBonus = new ModifiableBonus();
			attrBonus.setType(BonusType.ATTRIBUTE_CORE);
			attrBonus.setItemKeys(Sets.newHashSet(getChosenAttribute().toString()));
			attrBonus.setValue(1);
			bonuses.add(attrBonus);
		}
		return bonuses;
	}

	/**
	 * @return The theme skill
	 */
	public String getThemeSkill() {
		String skill;

		if (StringUtils.isNotBlank(getChosenSkill())) {
			skill = getChosenSkill();
		} else {
			skill = getData().getThemeSkill();
		}

		return skill;
	}
}
