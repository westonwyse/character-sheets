/**
 * StaticClass.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.SavingThrow;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.util.AbilityHelper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Contains the static details of a class.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
public class StaticClass extends SimpleOverridableAbilityProvider{
    private BigDecimal hitDie;
    private int skillRanksPerLevel;
    private String talentsName;
    private BaseAttackBonus baseAttackBonus;
    private Map<SavingThrow, BaseSavingThrow> baseSaves = new HashMap<SavingThrow, BaseSavingThrow>();
    private Integer[][] spellsPerDay;
    private Integer[][] spellsKnown;
    private AttributeName keyAttribute;

    private Map<String, String> options;
    private BonusProviderSet<StaticSpecialAbility> optionedAbilities;

    public enum BaseAttackBonus{
        HIGH(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 }),
        MEDIUM(new int[] { 0, 1, 2, 3, 3, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 12, 12, 13, 14, 15 }),
        LOW(new int[] { 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10 });

        private int[] bonuses;

        private BaseAttackBonus(int[] bonuses){
            this.bonuses = bonuses;
        }

        /**
         * Returns the bonus for a given class level.
         * 
         * @param level The level for which to retrieve the bonus.
         * @return The bonus for the requested level.
         */
        public int getValue(int level){
            // Note that levels are 1-based, not 0-based, so subtract one before
            // retrieval.
            return bonuses[level - 1];
        }
    }

    public enum BaseSavingThrow{
        GOOD(new int[] { 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12 }),
        BAD(new int[] { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6 }),;

        private int[] bonuses;

        private BaseSavingThrow(int[] bonuses){
            this.bonuses = bonuses;
        }

        /**
         * Returns the bonus for a given class level.
         * 
         * @param level The level for which to retrieve the bonus.
         * @return The bonus for the requested level.
         */
        public int getBonus(int level){
            // Note that levels are 1-based, not 0-based, so subtract one before
            // retrieval.
            return bonuses[level - 1];
        }
    }

    /**
     * Copy constructor
     * 
     * @param original The object to copy
     */
    public StaticClass(StaticClass original){
        super(original);
        setHitDie(original.getHitDie());
        setSkillRanksPerLevel(original.getSkillRanksPerLevel());
        setTalentsName(original.getTalentsName());
        setBaseAttackBonus(original.getBaseAttackBonus());
        setBaseSaves(original.getBaseSaves());
        setSpellsPerDay(original.getSpellsPerDay());
        setSpellsKnown(original.getSpellsKnown());
        setKeyAttribute(original.getKeyAttribute());

        setOptions(original.getOptions());
    }

    /**
     * @return the spellcastingAttribute
     * @deprecated Use getKeyAttribute
     */
    @Deprecated
    public AttributeName getSpellcastingAttribute(){
        return keyAttribute;
    }

    /**
     * @return the options
     */
    public Map<String, String> getOptions(){
        if (options == null) {
            setOptions(new HashMap<String, String>());
        }
        return options;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(Map<String, String> options){
        this.options = options;
        this.optionedAbilities = null;
    }

    @Override
    public BonusProviderSet<StaticSpecialAbility> getAbilities(){
        if (optionedAbilities == null) {
            optionedAbilities = AbilityHelper.applyOptions(getOptions(), super.getAbilities());
        }

        return optionedAbilities;
    }
}
