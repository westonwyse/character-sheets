package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticArmor;

public class StaticArmorLoader extends OverridableStaticObjectLoader3E<StaticArmor> {
	private static final Map<File,StaticArmorLoader> INSTANCES = new HashMap<>();
	
	public static StaticArmorLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticArmorLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticArmorLoader(File root) {
		super("armor", StaticArmor.class, root);
	}

	@Override
	public StaticArmor getClonedObject(StaticArmor original) {
		return new StaticArmor(original);
	}
}