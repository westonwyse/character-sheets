/**
 * BonusProvidingStaticObjectWrapperDecorator.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 16, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.util.Collection;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;

/**
 * StaticObjectWrapper decorator for a class that provides bonuses.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class AbstractBonusProviderWrapper<V extends BonusProvider & StaticObject> extends
        AbstractStaticObjectWrapper<V> implements
        BonusProvider{
    private transient Collection<ModifiableBonus> namedBonuses;
    private transient Collection<Modifier> modifiers;

    /**
     * @param id The ID for this instance
     * @param ruleset The ruleset name
     */
    public AbstractBonusProviderWrapper(String id, String ruleset){
        super(id, ruleset);
    }

    @Override
    public String getName(){
        V data = getData();
        if (data == null) {
            throw new NullPointerException("No data found for " + getKey());
        }
        return data.getName();
    }

    @Override
    public String getSource(){
        return getData().getSource();
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        if ((namedBonuses == null)
                || (!com.westpalmetto.gaming.character.util.CollectionHelper.isEqual(modifiers, this.modifiers))) {
            namedBonuses = BonusHelper.nameBonuses(getData().getBonuses(modifiers), getName());
            this.modifiers = modifiers;
        }
        return namedBonuses;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(BonusType type, Collection<Modifier> modifiers){
        return BonusHelper.filterBonusesByType(getBonuses(modifiers), type);
    }

    @Override
    public OptionalBonuses getOptional(Collection<Modifier> modifiers){
        return getData().getOptional(modifiers);
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(modifiers), modifiers);
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(BonusType type, Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(type, modifiers), modifiers);
    }

    @Override
    public Collection<Modifier> getModifiers(){
        return getData().getModifiers();
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }
}
