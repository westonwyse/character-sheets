/**
 * PSRDCharacterWriter.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.psrd;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.collections.CollectionUtils;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;
import com.westpalmetto.gaming.character.psrd.model.staticobject.wrapper.TraitWrapper;
import com.westpalmetto.gaming.character.servlet.writer.character.character3e.Character3EWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * Handling of response writing for PSRD characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class PsrdCharacterWriter extends Character3EWriter<PSRDCharacter>{

    @Override
    protected void writeSkills(PrintWriter writer, PSRDCharacter character){
        // Open section
        writer.println("<div id='skills'>");
        writer.println("<h2>Skills</h2>");

        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='classSkill'>&nbsp;</th>");
        writer.println("<th class='name'>Skill Name</th>");
        writer.println("<th class='bonus'>Total Bonus</th>");
        writer.println("<th class='ability'>Ability</th>");
        writer.println("<th class='abilityMod'>Ability Mod.</th>");
        writer.println("<th class='ranks'>Ranks</th>");
        writer.println("<th class='classSkill'>Class Bonus</th>");
        writer.println("<th class='misc'>Misc.</th>");
        writer.println("</tr>");

        // Write skills
        int count = 1;
        NamedObjectSet<DD3ESkill> skills = character.getSkills();
        for (DD3ESkill skill : skills) {
            AttributeName attributeName = skill.getAttributeName();
            String classSkill = (skill.isClassSkill()) ? "classSkill" : "";
            String classSkillMarker = (skill.isClassSkill()) ? "&raquo;" : "";
            String trainedOnly = (skill.isTrainedOnly()) ? "trainedOnly" : "";
            String trainedOnlyMarker = (skill.isTrainedOnly()) ? " &Dagger;" : "";
            String totalBonus = (skill.isTrainedOnly() && (skill.getRanks() == 0)) ? "&mdash;"
                    : Character.FORMATTER_MODIFIER.format(character.getSkillBonus(skill));
            String marker = (count % 3 == 0) ? "marker" : "";
            count++;

            // Write name
            writer.println("<tr class='" + classSkill + " " + trainedOnly + " " + marker + "'>");
            writer.println("<td class='classSkill'>" + classSkillMarker + "</td>");
            writer.println("<td class='name'>" + skill.getName() + trainedOnlyMarker + "</td>");
            writer.println("<td class='bonus'>" + totalBonus + "</td>");
            writer.println("<td class='ability'>" + attributeName.getAbbreviation() + "</td>");
            writer.println("<td class='abilityMod'>" + character.getAttributeBonus(attributeName) + "</td>");
            writer.println("<td class='ranks'>" + skill.getRanks() + "</td>");
            writer.println("<td class='classSkill'>" + character.getSkillClassBonus(skill) + "</td>");
            writer.println("<td class='misc'>" + character.getSkillMiscModifiers(skill) + "</td>");
            writer.println("</tr>");
        }
        writer.println("</table>");
        writer.println("<div class='legend'>&raquo; - Class skill</div>");
        writer.println("<div class='legend'>&Dagger; - Trained only</div>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SKILL);
        writeConditionals(writer, conditionals);

        // Validate skill points
        writer.println("<ul>");
        writer.println("<li>Earned skill ranks: " + character.getEarnedSkillRanks() + "</li>");
        writer.println("<li>Used skill ranks: " + character.getUsedSkillRanks() + "</li>");
        writer.println("</ul>");

        // Close section
        writer.println("</div>");
    }

    @Override
    protected void writeSpecialAbilities(PrintWriter writer, PSRDCharacter character){
        PSRDCharacterModel data = (PSRDCharacterModel) character.getData();
        Collection<Modifier> modifiers = character.getModifiers();

        writeFeats(writer, character);
        writeObediences(writer, character);
        writeTraits(writer, character);
        writeRacialAbilities(writer, character);
        writeClassAbilities(writer, character);
        writeFamiliar(writer, character);
        writeOtherAbilities(writer, character);
        writePools(writer, character);
    }

    @Override
    protected void writeOtherAbilities(PrintWriter writer, PSRDCharacter character){
        // Write other special abilities, if applicable
        Collection<SpecialAbility> abilities = new HashSet<SpecialAbility>();
        abilities.addAll(character.getData().getBoons());
        abilities.addAll(character.getSkillUnlocks());
        abilities.addAll(character.getData().getQualities());
        if (CollectionUtils.isNotEmpty(abilities)) {
            writer.println("<h3>Other Abilities</h3>");
            writeSpecialAbilityList(abilities, writer, character.getModifiers());
        }
    }

    protected void writeTraits(PrintWriter writer, PSRDCharacter character){
        // Write traits
        BonusProviderSet<TraitWrapper> traits = character.getTraits();
        if (CollectionUtils.isNotEmpty(traits)) {
            writer.println("<h3>Traits</h3>");
            writeSpecialAbilityList(traits, writer, character.getModifiers());
        }

        // Validate trait count
        writer.println("<ul>");
        writer.println("<li>Earned trait: " + character.getEarnedTraits() + "</li>");
        writer.println("<li>Used trait: " + character.getUsedTraits() + "</li>");
        writer.println("</ul>");
    }

    protected void writeObediences(PrintWriter writer, PSRDCharacter character){
        // Write deific obediences, if applicable
        if (character.hasDeificBoons()) {
            writer.println("<h3>Deific Boons</h3>");
            writeSpecialAbilityList(character.getDeificBoons(), writer, character.getModifiers());
        }
    }
}
