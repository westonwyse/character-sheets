package com.westpalmetto.util.resources.converter;

import java.io.InputStream;

public interface StreamConverter<T> {

	T convert(InputStream in) throws ConvertException;

}
