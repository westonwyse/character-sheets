package com.westpalmetto.util.resources.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.westpalmetto.util.resources.converter.ConvertException;
import com.westpalmetto.util.resources.converter.StreamConverter;

public class UrlResourceLoader<T> extends AbstractResourceLoader<T> {
	private final URL url;
	private final StreamConverter<T> converter;

	public UrlResourceLoader(URL url, StreamConverter<T> converter, ListeningExecutorService executor) {
		super(executor);
		this.url = url;
		this.converter = converter;
	}

	@Override
	public T getResource() {
		T resource = null;
		try {
			InputStream in = getUrl().openStream();
			resource = getConverter().convert(in);
		} catch (IOException e) {
			//TODO: Exceptions
			Throwables.propagate(e);
		} catch (ConvertException e) {
			//TODO: Exceptions
			Throwables.propagate(e);
		}
		
		return resource;
	}

	/**
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * @return the converter
	 */
	public StreamConverter<T> getConverter() {
		return converter;
	}
}
