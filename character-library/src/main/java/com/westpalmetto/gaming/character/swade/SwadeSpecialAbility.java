package com.westpalmetto.gaming.character.swade;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractCharacterSpecialAbility;

public abstract class SwadeSpecialAbility extends AbstractCharacterSpecialAbility{
    public static boolean isValid(String loaderName, String id, File root){
        return isValid(loaderName, id, root);
    }

    public SwadeSpecialAbility(String id){
        super(id, ResourceFactory.Ruleset.SWADE.toString());
    }

    @Override
    public String getRuleset(){
        return ResourceFactory.Ruleset.SWADE.name();
    }
}
