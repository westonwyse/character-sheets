package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.collections.CollectionUtils;

import com.westpalmetto.util.resources.model.Model;

public class OptionalBonuses extends Model{
	private boolean enabled = false;
	private Collection<ModifiableBonus> bonuses;
	
	public OptionalBonuses(){
		
	}
	
	public OptionalBonuses(OptionalBonuses optional){
		this.enabled = optional.isEnabled();
		if(CollectionUtils.isNotEmpty(optional.getBonuses())){
			this.bonuses = new HashSet<>(optional.getBonuses());
		}
	}
	
	/**
	 * @return the enabled
	 */
	public boolean isEnabled(){
		return enabled;
	}
	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}
	/**
	 * @return the bonuses
	 */
	public Collection<ModifiableBonus> getBonuses(){
		if(bonuses == null){
			setBonuses(new HashSet<ModifiableBonus>());
		}
		return bonuses;
	}
	/**
	 * @param bonuses the bonuses to set
	 */
	public void setBonuses(Collection<ModifiableBonus> bonuses){
		this.bonuses = bonuses;
	}
}
