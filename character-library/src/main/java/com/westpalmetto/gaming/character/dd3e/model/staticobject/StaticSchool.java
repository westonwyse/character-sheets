package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import java.util.Map;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.SpellSet;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.util.AbilityHelper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Contains the static details of a class's school-like abilities.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
public class StaticSchool extends SimpleOverridableAbilityProvider{
    private SpellSet[] spells;
    private String variableSpell;
    private ModifierType spellsType;

    private Map<String, String> options;
    private BonusProviderSet<StaticSpecialAbility> optionedAbilities;

    /**
     * Copy constructor
     * 
     * @param original The object to copy
     */
    public StaticSchool(StaticSchool original){
        super(original);
        this.spells = original.getSpells();
        this.variableSpell = original.getVariableSpell();
        this.options = original.getOptions();
        this.optionedAbilities = original.getOptionedAbilities();
    }

    /**
     * @param options the options to set
     */
    public void setOptions(Map<String, String> options){
        this.options = options;
        this.optionedAbilities = null;
    }

    @Override
    public BonusProviderSet<StaticSpecialAbility> getAbilities(){
        if (optionedAbilities == null) {
            optionedAbilities = AbilityHelper.applyOptions(getOptions(), super.getAbilities());
        }

        return optionedAbilities;
    }

}
