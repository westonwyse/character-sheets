package com.westpalmetto.util.resources.function;

import com.google.common.base.Function;
import com.westpalmetto.util.resources.cache.Cache;

/**
 * 
 * @param <K> The type of the key
 * @param <V> The type of the value
 */
public class QueueUpdateFunction<K, V> implements Function<V, V> {
	private final K key;
	private final Cache<K, V> cache;

	public QueueUpdateFunction(K key, Cache<K, V> cache) {
		this.key = key;
		this.cache = cache;
	}

	@Override
	public V apply(V input) {
		cache.put(key, input);
		return input;
	}
}
