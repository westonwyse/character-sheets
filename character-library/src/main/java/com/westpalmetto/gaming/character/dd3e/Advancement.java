package com.westpalmetto.gaming.character.dd3e;

public interface Advancement {
	/**
	 * For a given amount of experience, returns the level of the character.
	 * 
	 * @param experience The character's experience total
	 * @return The character level of the character
	 */
	public int getLevel(int experience);

	/**
	 * For a given level, returns the minimum amount of experience of the character.
	 * 
	 * @param experience The character's (1-based) level
	 * @return The minimum experience of the character
	 */
	public int getExperience(int level);

	/**
	 * Returns the total amount of experience needed to reach the next level.
	 * 
	 * @param experience The character's current experience total
	 * @return The total amount of experience needed to reach the next level
	 */
	public int getNextLevel(int experience);
}
