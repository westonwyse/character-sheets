package com.westpalmetto.gaming.character.servlet.writer.character.savageworlds;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;
import com.westpalmetto.gaming.character.swade.SwadeCharacter;

public final class SavageWorldsWriterFactory {
	private static final SwadeCharacterWriter WRITER_SWADE = new SwadeCharacterWriter();

	/**
	 * Returns the appropriate writer for the given character.
	 * 
	 * @param character The character
	 * @return The writer for the character
	 */
	public static CharacterHtmlWriter<? extends Character<?>> getWriter(SwadeCharacter character) {
		CharacterHtmlWriter<? extends Character<?>> writer = null;
		
		if (character instanceof SwadeCharacter) {
			writer = WRITER_SWADE;
		} else {
			writer = WRITER_SWADE;
		}
		
		return writer;
	}
}
