/**
 * PSRDCharacter.java
 * 
 * Copyright � 2012 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.sfsrd;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Sets;
import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.CombatType;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.sfsrd.model.SFCharacterModel;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.wrapper.equipment.SFArmor;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Character is the mediator object for the character's data for PSRD
 * characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class SFCharacter extends Character3E{
    private static final BigDecimal HALF = new BigDecimal("0.5");
    private static final Object ITEMKEY_OPERATIVE = "Operative";

    private NamedObjectSet<DD3ESkill> skills;

    /**
     * Creates a new Starfinder character.
     */
    public SFCharacter(){
        this(new SFCharacterModel(ResourceFactory.Ruleset.SFSRD.getRoot()));
    }

    /**
     * Creates a new SF character.
     * 
     * @param root The resource root
     */
    public SFCharacter(File root){
        this(new SFCharacterModel(root));
    }

    /**
     * Creates a new PSRDCharacter from the given character model.
     * 
     * @param model The character model.
     */
    public SFCharacter(SFCharacterModel model){
        super(model);

        skills = new NamedObjectSet<DD3ESkill>();
        skills.add(new DD3ESkill("Acrobatics", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Appraise", AttributeName.INT, false));
        skills.add(new DD3ESkill("Bluff", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Computers", AttributeName.INT, true));
        skills.add(new DD3ESkill("Culture", AttributeName.INT, true));
        skills.add(new DD3ESkill("Diplomacy", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Disguise", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Engineering", AttributeName.INT, true));
        skills.add(new DD3ESkill("Intimidate", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Life Science", AttributeName.INT, true));
        skills.add(new DD3ESkill("Medicine", AttributeName.INT, true));
        skills.add(new DD3ESkill("Mysticism", AttributeName.WIS, true));
        skills.add(new DD3ESkill("Perception", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Physical Science", AttributeName.INT, true));
        skills.add(new DD3ESkill("Piloting", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Profession", AttributeName.WIS, true));
        skills.add(new DD3ESkill("Sense Motive", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Sleight of Hand", AttributeName.DEX, true));
        skills.add(new DD3ESkill("Stealth", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Survival", AttributeName.WIS, false));
    }

    /**
     * @param json The JSON from which to initialize this character.
     * @param root The resource root
     */
    public SFCharacter(JsonObject json, File root){
        this(root);
        initialize(json);
    }

    @Override
    public void initialize(JsonObject json){
        setData(GsonUtils.readModel(json, SFCharacterModel.class));
    }

    public SFCharacterModel getSFData(){
        return (SFCharacterModel) getData();
    }

    @Override
    public File getResourceRoot(){
        return ResourceFactory.Ruleset.SFSRD.getRoot();
    }

    @Override
    protected Collection<BonusProvider> getFixedBonusProviders(){
        Collection<BonusProvider> providers = super.getFixedBonusProviders();

        // Add theme
        providers.add(getSFData().getTheme());

        return providers;
    }

    @Override
    protected NamedObjectSet<DD3ESkill> getDefaultSkills(){
        return skills;
    }

    @Override
    public int getSkillBonus(DD3ESkill skill){
        return super.getSkillBonus(skill) + getSkillClassBonus(skill);
    }

    @Override
    public int getSkillMiscModifiers(DD3ESkill skill){
        int bonus = super.getSkillMiscModifiers(skill);

        if (skill.isClassSkill() && isThemeSkill(skill)) {
            bonus++;
        }

        return bonus;
    }

    /**
     * Calculates the class bonus for a given skill (=3 if classSkill &&
     * ranks>0).
     * 
     * @param skill The skill
     * @return The appropriate class bonus
     */
    public int getSkillClassBonus(DD3ESkill skill){
        int bonus = 0;
        if (skill.isClassSkill() && (skill.getRanks() > 0)) {
            bonus = 3;
        }
        return bonus;
    }

    public int getModifiedCheckPenalty(Armor armor){
        // Find any personal modifier
        int bonus = 0;
        if (armor.getData().getProficiency().isArmor()) {
            bonus = getArmorCheckBonusArmor();
        } else {
            bonus = getArmorCheckBonusShield();
        }

        // Apply to existing armor penalty
        return armor.getModifiedCheckPenalty(bonus);
    }

    public int getStaminaPoints(){
        int sp = 0;

        // Get per-level SP
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            sp += cls.getData().getHitDie().intValue() * cls.getLevels();
        }
        sp += getAttributeBonus(AttributeName.CON) * getCharacterLevels();

        return sp;
    }

    @Override
    public int getHitPoints(){
        // Start with racial HP
        int hp = getData().getRace().getData().getHitPoints();

        // Get per-level HP
        BonusProviderSet<ClassWrapper> classes = getData().getClasses();
        for (ClassWrapper cls : classes) {
            hp += cls.getData().getHitDie().intValue() * cls.getLevels();
        }
        hp += getTotalSpecialBonuses(BonusType.HIT_POINT, ITEMKEY_PERLEVEL) * getCharacterLevels();

        // Get any flat-rate bonus HP
        hp += (getTotalSpecialBonuses(BonusType.HIT_POINT)
                - getTotalSpecialBonuses(BonusType.HIT_POINT, ITEMKEY_PERLEVEL));
        return hp;
    }

    public int getResolvePoints(){
        // Level-based
        int level = new BigDecimal(getCharacterLevels()).setScale(5, RoundingMode.FLOOR).multiply(HALF).intValue();
        if (level < 1) {
            level = 1;
        }

        // Attribute-based
        int attribute = 0;
        for (ClassWrapper cls : getData().getClasses()) {
            if (attribute < getAttributeBonus(cls.getKeyAttribute())) {
                attribute = getAttributeBonus(cls.getKeyAttribute());
            }
        }

        return level + attribute;
    }

    public int getEACArmorBonus(){
        int best = 0;

        // Cycle through each armor
        for (SFArmor armor : getData().getEquipment().getSFArmorSet()) {
            if (armor.getData().getProficiency().isArmor()) {
                if (best < armor.getData().getEacBonus()) {
                    best = armor.getData().getEacBonus();
                }
            }
        }

        // Return the best bonus found
        return best;
    }

    public int getKACArmorBonus(){
        int best = 0;

        // Cycle through each armor
        for (SFArmor armor : getData().getEquipment().getSFArmorSet()) {
            if (armor.getData().getProficiency().isArmor()) {
                if (best < armor.getData().getKacBonus()) {
                    best = armor.getData().getKacBonus();
                }
            }
        }

        // Return the best bonus found
        return best;
    }

    public int getEACBonus(){
        return 10 + getEACArmorBonus() + getAttributeBonus(AttributeName.DEX);
    }

    public int getKACBonus(){
        return 10 + getKACArmorBonus() + getAttributeBonus(AttributeName.DEX);
    }

    @Override
    public CarryingCapacity getCarryingCapacity(){
        Map<String, Set<AbstractEquipment<?>>> locationsMap = getData().getEquipment().getEquipmentByLocation();
        BigDecimal weight =
                EquipmentSet.getTotalWeight(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED), locationsMap);

        // Determine capacity
        CarryingCapacity capacity;
        if (weight.compareTo(new BigDecimal(getCarryingCapacityLight())) <= 0) {
            capacity = CarryingCapacity.LIGHT;
        } else if (weight.compareTo(new BigDecimal(getCarryingCapacityMedium())) <= 0) {
            capacity = CarryingCapacity.MEDIUM;
        } else {
            capacity = CarryingCapacity.HEAVY;
        }
        return capacity;
    }

    @Override
    public int getCarryingCapacityLight(){
        int str = getAttributeTotalScore(AttributeName.STR);
        return new BigDecimal(str).multiply(HALF).setScale(2, RoundingMode.FLOOR).intValue();
    }

    @Override
    public int getCarryingCapacityMedium(){
        return getAttributeTotalScore(AttributeName.STR);
    }

    @Override
    public Set<Weapon> getWeaponSet(){
        return getData().getEquipment().getWeaponSet();
    }

    @Override
    public int getWeaponAttackBonus(Weapon weapon){
        CombatType type = weapon.getData().getCombatType();

        // Get the weapon's bonuses
        int bonus = weapon.getQuality().getAttackBonus()
                + getTotalSpecialBonuses(BonusType.ATTACK, Sets.newHashSet(weapon.getName(), weapon.getId()))
                + getTotalSpecialBonuses(BonusType.ATTACK, weapon.getData().getWeaponGroups());

        // Add the appropriate attack bonuses
        switch (type) {
        case MELEE:
            if (isOperativeWeapon(weapon) && useOperativeBonus()) {
                bonus += getOperativeTotalBonus();
            } else {
                bonus += getMeleeAttackTotalBonus();
            }
            break;
        case RANGED:
            bonus += getRangedAttackTotalBonus();
            break;
        default:
            // TODO: Exceptions
            throw new RuntimeException("Weapon type not recognized: " + type);
        }

        return bonus;
    }

    private boolean isOperativeWeapon(Weapon weapon){
        return weapon.getData().getItemKeys().contains(ITEMKEY_OPERATIVE);
    }

    public int getOperativeTotalBonus(){
        return getBaseAttack() + getAttributeBonus(AttributeName.DEX) + getSizeModifier() + getMeleeAttackMiscBonus();
    }

    private boolean useOperativeBonus(){
        return getOperativeTotalBonus() > getMeleeAttackTotalBonus();
    }

    public Set<SFArmor> getSFArmorSet(){
        return getData().getEquipment().getSFArmorSet();
    }

    /**
     * @param skill The skill to search for
     * @return TRUE iff the given skill is the character's theme skill
     */
    public boolean isThemeSkill(DD3ESkill skill){
        String themeSkill = getSFData().getTheme().getThemeSkill();
        // Strip names from professions
        return StringUtils.equalsIgnoreCase(StringUtils.substringBefore(skill.getName(), ":"), themeSkill);
    }
}
