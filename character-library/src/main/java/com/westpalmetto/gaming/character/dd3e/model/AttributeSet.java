/**
 * SkillSet.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 30, 2012
 */
package com.westpalmetto.gaming.character.dd3e.model;

import com.westpalmetto.gaming.character.collection.KeyedSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;

/**
 * AttributeSet is and implementation of KeyedSet to contain a character's
 * Attribute objects.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class AttributeSet extends KeyedSet<AttributeName, Attribute> {
	/* (non-Javadoc)
	 * @see com.westaplmetto.gaming.character.collection.KeyedSet#getKey(java.lang.Object)
	 */
	@Override
	public AttributeName getKey(Object target) {
		AttributeName key = null;
		
		if(target instanceof Attribute){
			key = ((Attribute)target).getName();
		} else {
			//TODO: Exceptions
			throw new RuntimeException("Unrecognized object type");
		}
		
		return key;
	}

	/**
	 * @return The total of all attribute values
	 */
	public int totalScores() {
		int total = 0;
		
		for(Attribute attribute : values()){
			total += attribute.getScore();
		}
		
		return total;
	}
	
	@Override
	public Attribute get(AttributeName key){
		if(!containsKey(key)){
			add(new Attribute(key, 0));
		}
		
		return super.get(key);
	}
}
