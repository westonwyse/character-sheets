package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment.StaticEquipmentLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

public class Equipment extends AbstractEquipment<StaticEquipment> {
	public Equipment(String id) {
		super(id);
	}

	@Override
	protected DataLoader<StaticEquipment> createDataLoader() {
		return StaticEquipmentLoader.getInstance(getRoot());
	}
}
