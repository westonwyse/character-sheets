/**
 * Armor.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment;

/**
 * Class to contain stats for an AC item.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class StaticArmor extends StaticEquipment {
	private int acBonus;
	private Proficiency proficiency;
	private Integer maxDex;
	private int checkPenalty;
	private int arcaneFailure;
	
	public StaticArmor(){
	}
	
	public StaticArmor(StaticArmor original){
		super(original);
		setAcBonus(original.getAcBonus());
		setProficiency(original.getProficiency());
		setMaxDex(original.getMaxDex());
		setCheckPenalty(original.getCheckPenalty());
		setArcaneFailure(original.getArcaneFailure());
	}
	
	/**
	 * @return the acBonus
	 */
	public int getAcBonus() {
		return acBonus;
	}
	/**
	 * @param acBonus the acBonus to set
	 */
	public void setAcBonus(int acBonus) {
		this.acBonus = acBonus;
	}
	/**
	 * @return the proficiency type
	 */
	public Proficiency getProficiency() {
		return proficiency;
	}
	/**
	 * @param proficiency the proficiency to set
	 */
	public void setProficiency(Proficiency proficiency) {
		this.proficiency = proficiency;
	}
	/**
	 * @return the maxDex; null if no max dex
	 */
	public Integer getMaxDex() {
		return maxDex;
	}
	/**
	 * @param maxDex the maxDex to set
	 */
	public void setMaxDex(Integer maxDex) {
		this.maxDex = maxDex;
	}
	/**
	 * @return the checkPenalty
	 */
	public int getCheckPenalty() {
		return checkPenalty;
	}
	/**
	 * @param checkPenalty the checkPenalty to set
	 */
	public void setCheckPenalty(int checkPenalty) {
		this.checkPenalty = checkPenalty;
	}
	/**
	 * @return the arcaneFailure
	 */
	public int getArcaneFailure() {
		return arcaneFailure;
	}
	/**
	 * @param arcaneFailure the arcaneFailure to set
	 */
	public void setArcaneFailure(int arcaneFailure) {
		this.arcaneFailure = arcaneFailure;
	}

	@Override
	public Slot getSlot() {
		return getProficiency().getSlot();
	}

	@Override
	public WeightModifier getWeightModifier() {
		return WeightModifier.ARMOR_WEAPON;
	}
	
	/**
	 * The type of AC item.
	 */
	public enum Proficiency{
		LIGHT("L", Slot.ARMOR),
		MEDIUM("M", Slot.ARMOR),
		HEAVY("H", Slot.ARMOR),
		SHIELD("Shield", Slot.SHIELD),
		SHIELD_LIGHT("Shield, light", Slot.SHIELD),
		SHIELD_HEAVY("Shield, heavy", Slot.SHIELD),
		TOWER_SHIELD("Tower Shield", Slot.SHIELD),
		;
		
		private String abbreviation;
		private Slot slot;
		
		private Proficiency(String abbreviation, Slot slot){
			this.abbreviation = abbreviation;
			this.slot = slot;
		}
		public String getAbbreviation(){
			return this.abbreviation;
		}
		public boolean isArmor(){
			return Slot.ARMOR.equals(getSlot());
		}
		public boolean isShield(){
			return Slot.SHIELD.equals(getSlot());
		}
		public boolean isShieldBash(){
			return (this == SHIELD_LIGHT) || (this == SHIELD_HEAVY);
		}
		public Slot getSlot(){
			return this.slot;
		}
	}
}
