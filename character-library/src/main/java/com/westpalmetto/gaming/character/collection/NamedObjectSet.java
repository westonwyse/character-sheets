/**
 * SkillSet.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 30, 2012
 */
package com.westpalmetto.gaming.character.collection;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.NamedObject;

/**
 * NamedObjectSet is an implementation of KeyedSet to contain any object that
 * implements NamedObject.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class NamedObjectSet<T extends NamedObject> extends KeyedSet<String, T> {
	public NamedObjectSet(){
	}
	
	public NamedObjectSet(Collection<T> original) {
		super(original);
	}

	@Override
	public String getKey(Object target) {
		String key = null;
		
		if(target instanceof NamedObject){
			key = ((NamedObject)target).getName();
			if(StringUtils.isBlank(key)){
				//TODO: Exceptions
				throw new RuntimeException("Name is required: " + target);
			}
		} else {
			//TODO: Exceptions
			throw new RuntimeException("Unrecognized object type: " + target);
		}
		
		return key;
	}

}
