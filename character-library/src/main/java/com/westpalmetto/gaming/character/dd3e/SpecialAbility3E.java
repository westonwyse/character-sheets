package com.westpalmetto.gaming.character.dd3e;

import java.io.File;

import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractCharacterSpecialAbility;

public abstract class SpecialAbility3E extends AbstractCharacterSpecialAbility{
	public SpecialAbility3E(String id, String ruleset){
		super(id, ruleset);
	}
}
