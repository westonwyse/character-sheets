package com.westpalmetto.gaming.character.dd3e.loader;

import java.io.File;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.loader.OverridableStaticObjectLoader;

public abstract class OverridableStaticObjectLoader3E<V extends SimpleOverridableAbilityProvider> extends
		OverridableStaticObjectLoader<V>{

	public OverridableStaticObjectLoader3E(String typeId, Class<V> type, File root){
		super(typeId, type, root);
	}
}
