/**
 * StaticClass.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import java.util.Set;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.Alignment;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Contains the static details of a deity.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class StaticDeity extends Model implements StaticObject{
    private String name;
    private String source;
    private Alignment alignment;
    private Set<String> domains;
    private Set<String> subdomains;
    private BonusProviderSet<StaticSpecialAbility> boons;

    /**
     * Default constructor.
     */
    public StaticDeity(){}

    /**
     * Copy constructor.
     */
    public StaticDeity(StaticDeity original){
        setName(original.getName());
        setSource(original.getSource());
        setAlignment(original.getAlignment());
        setDomains(original.getDomains());
        setSubdomains(original.getSubdomains());
        setBoons(original.getBoons());
    }
}
