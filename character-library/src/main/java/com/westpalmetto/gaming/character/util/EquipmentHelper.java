package com.westpalmetto.gaming.character.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment.WeightModifier;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment.Size;

/**
 * Utilities to handle equipment.
 */
public class EquipmentHelper {
	private static Map<Size, Map<WeightModifier, BigDecimal>> MODIFIER_MATRIX;
	
	private static Map<Size, Map<WeightModifier, BigDecimal>> getModifierMatrix(){
		if(MODIFIER_MATRIX == null){
			MODIFIER_MATRIX = new HashMap<Size, Map<WeightModifier,BigDecimal>>();
			
			//Add small
			Map<WeightModifier, BigDecimal> values = new HashMap<WeightModifier, BigDecimal>();
			values.put(WeightModifier.NONE, BigDecimal.ONE);
			values.put(WeightModifier.CLOTHING_CONTAINER, new BigDecimal("0.25"));
			values.put(WeightModifier.ARMOR_WEAPON, new BigDecimal("0.5"));
			MODIFIER_MATRIX.put(Size.S, values);
			
			//Add medium
			values = new HashMap<WeightModifier, BigDecimal>();
			values.put(WeightModifier.NONE, BigDecimal.ONE);
			values.put(WeightModifier.CLOTHING_CONTAINER, BigDecimal.ONE);
			values.put(WeightModifier.ARMOR_WEAPON, BigDecimal.ONE);
			MODIFIER_MATRIX.put(Size.M, values);
			
			//Add large
			values = new HashMap<WeightModifier, BigDecimal>();
			values.put(WeightModifier.NONE, BigDecimal.ONE);
			values.put(WeightModifier.CLOTHING_CONTAINER, BigDecimal.ONE);
			values.put(WeightModifier.ARMOR_WEAPON, new BigDecimal("2"));
			MODIFIER_MATRIX.put(Size.L, values);
		}
		return MODIFIER_MATRIX;
	}

	/**
	 * Determines the weight of a specific item.
	 * 
	 * @param original The weight of the stock item.
	 * @param weightModifier The weight modifier of the stock item.
	 * @param size The size of the specific item.
	 * @return The weight of the specific item.
	 */
	public static BigDecimal calculateWeight(
			BigDecimal original,
			WeightModifier weightModifier,
			Size size) {
		if(!getModifierMatrix().containsKey(size)){
			throw new IllegalArgumentException("No data for size " + size);
		}
		Map<WeightModifier, BigDecimal> sizeMap = getModifierMatrix().get(size);
		BigDecimal modifier = sizeMap.get(weightModifier);
		return original.multiply(modifier);
	}
}
