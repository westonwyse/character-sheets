/**
 * StaticClass.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import java.util.Set;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.model.AttributeSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.QualityWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SizeWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Contains the static details of an animal companion.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class StaticAnimalCompanion extends Model implements StaticObject{
    private String name;
    private String source;
    private int advancement;
    private AnimalCompanionData base;
    private AnimalCompanionData advanced;

    /**
     * Default constructor.
     */
    public StaticAnimalCompanion(){}

    /**
     * Copy constructor.
     */
    public StaticAnimalCompanion(StaticAnimalCompanion original){
        setName(original.getName());
        setSource(original.getSource());
        setAdvancement(original.getAdvancement());
        setBase(original.getBase());
        setAdvanced(original.getAdvanced());
    }

    public static class AnimalCompanionData extends Model{
        private SizeWrapper size;
        private int baseSpeed;
        private int naturalArmor;
        private Set<Weapon> attacks;
        private AttributeSet attributes;
        private BonusProviderSet<QualityWrapper> specialQualities;

        /**
         * Default constructor.
         */
        public AnimalCompanionData(){}

        /**
         * Copy constructor.
         */
        public AnimalCompanionData(AnimalCompanionData original){
            setSize(original.getSize());
            setBaseSpeed(original.getBaseSpeed());
            setNaturalArmor(original.getNaturalArmor());
            setAttacks(original.getAttacks());
            setAttributes(original.getAttributes());
            setSpecialQualities(original.getSpecialQualities());
        }

        /**
         * @return the size
         */
        public SizeWrapper getSize(){
            return size;
        }

        /**
         * @param size the size to set
         */
        public void setSize(SizeWrapper size){
            this.size = size;
        }

        /**
         * @return the baseSpeed
         */
        public int getBaseSpeed(){
            return baseSpeed;
        }

        /**
         * @param baseSpeed the baseSpeed to set
         */
        public void setBaseSpeed(int baseSpeed){
            this.baseSpeed = baseSpeed;
        }

        /**
         * @return the naturalArmor
         */
        public int getNaturalArmor(){
            return naturalArmor;
        }

        /**
         * @param naturalArmor the naturalArmor to set
         */
        public void setNaturalArmor(int naturalArmor){
            this.naturalArmor = naturalArmor;
        }

        /**
         * @return the attacks
         */
        public Set<Weapon> getAttacks(){
            return attacks;
        }

        /**
         * @param attacks the attacks to set
         */
        public void setAttacks(Set<Weapon> attacks){
            this.attacks = attacks;
        }

        /**
         * @return the attributes
         */
        public AttributeSet getAttributes(){
            return attributes;
        }

        /**
         * @param attributes the attributes to set
         */
        public void setAttributes(AttributeSet attributes){
            this.attributes = attributes;
        }

        /**
         * @return the specialQualities
         */
        public BonusProviderSet<QualityWrapper> getSpecialQualities(){
            return specialQualities;
        }

        /**
         * @param specialQualities the specialQualities to set
         */
        public void setSpecialQualities(BonusProviderSet<QualityWrapper> specialQualities){
            this.specialQualities = specialQualities;
        }
    }
}
