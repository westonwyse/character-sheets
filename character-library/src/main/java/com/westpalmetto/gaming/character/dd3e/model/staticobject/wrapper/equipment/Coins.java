/**
 * Coins.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Apr 9, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.util.Collection;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticCoins;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Container for all the types of coinage a character can carry.
 * 
 * Overrides getName, getQuantity, and getWeight. These items can be set (in
 * order to not cause errors when serializing), but the values of the parent
 * class are ignored when calculating the return values.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class Coins extends AbstractEquipment<StaticCoins>{
    private int copper;
    private int silver;
    private int gold;
    private int platinum;
    private int gems;

    public Coins(){
        super(StaticCoins.COINS_NAME);
    }

    @Override
    protected DataLoader<StaticCoins> createDataLoader(){
        return null;
    }

    @Override
    public StaticCoins getData(){
        return StaticCoins.getInstance();
    }

    @Override
    public String getSource(){
        return "-";
    }

    /**
     * @return the copper
     */
    public int getCopper(){
        return copper;
    }

    /**
     * @param copper the copper to set
     */
    public void setCopper(int copper){
        this.copper = copper;
    }

    /**
     * @return the silver
     */
    public int getSilver(){
        return silver;
    }

    /**
     * @param silver the silver to set
     */
    public void setSilver(int silver){
        this.silver = silver;
    }

    /**
     * @return the gold
     */
    public int getGold(){
        return gold;
    }

    /**
     * @param gold the gold to set
     */
    public void setGold(int gold){
        this.gold = gold;
    }

    /**
     * @return the platinum
     */
    public int getPlatinum(){
        return platinum;
    }

    /**
     * @param platinum the platinum to set
     */
    public void setPlatinum(int platinum){
        this.platinum = platinum;
    }

    /**
     * @return the gems
     */
    public int getGems(){
        return gems;
    }

    /**
     * @param gems the gems to set
     */
    public void setGems(int gems){
        this.gems = gems;
    }

    @Override
    public int getQuantity(){
        return getCopper() + getSilver() + getGold() + getPlatinum();
    }

    /**
     * Sums the values of a collection of Coins objects into a single Coins
     * object.
     * 
     * @param coins The collection to sum
     * @return A single Coins object containing the summation of the objects in
     *         the collection
     */
    public static Coins sum(Collection<Coins> coins){
        Coins summation = null;

        for (Coins single : coins) {
            if (summation == null) {
                summation = single;
            } else {
                summation.setCopper(summation.getCopper() + single.getCopper());
                summation.setSilver(summation.getSilver() + single.getSilver());
                summation.setGold(summation.getGold() + single.getGold());
                summation.setPlatinum(summation.getPlatinum() + single.getPlatinum());
                summation.setGems(summation.getGems() + single.getGems());
            }
        }

        return summation;
    }
}
