package com.westpalmetto.gaming.character.util.loading;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.CharacterController;
import com.westpalmetto.util.resources.ResourceLoader;
import com.westpalmetto.util.resources.ResourceManager;
import com.westpalmetto.util.resources.converter.JsonConverter;
import com.westpalmetto.util.resources.loader.FileResourceLoader;

/**
 * Loads static data from all known sources.
 */
public class SourceDataLoader implements DataLoader<Map<String, JsonElement>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceDataLoader.class);
    
	private static final Map<String, Map<String, JsonElement>> CACHE = new HashMap<String, Map<String, JsonElement>>();
	
	private static final ResourceManager<ResourceLoader<JsonObject>, JsonObject> MANAGER =
			new ResourceManager<ResourceLoader<JsonObject>, JsonObject>(
					CharacterController.getInstance().getTtl());
	
	private static final Map<File, SourceDataLoader> INSTANCES = new HashMap<File, SourceDataLoader>();

	/**
	 * Returns the loader instance for the given root. Creates the loader if it
	 * does not exist.
	 * @param root The root directory
	 * @return The instance
	 */
	public static SourceDataLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new SourceDataLoader(root));
		}
		return INSTANCES.get(root);
	}
	
	/**
	 * Creates a data loader for the sources at the given root directory.
	 * @param root The root directory. Must point to an existing directory.
	 */
	private SourceDataLoader(File root) {
		if((root==null) || !root.isDirectory()) {
			throw new IllegalArgumentException(root + " is not a directory.");
		}
		loadStaticData(root);
	}
	
	private void loadStaticData(File root) {
		LOGGER.info("Loading all from {}", root);
		
		File[] sources = root.listFiles();
		if(sources==null) {
			//TODO: Exceptions
			throw new RuntimeException("No sources found.");
		}
		
		for(File source : sources){
			if(source.isFile() && source.getAbsolutePath().endsWith(".json")){
				LOGGER.trace("Loading {}", source);
				
				JsonObject json = null;
				try {
					json = MANAGER.get(new FileResourceLoader<JsonObject>(
							source,
							JsonConverter.getInstance(),
							CharacterController.getInstance().getExecutor()));
					cacheSource(json);
				} catch (Exception e) {
					//TODO: Exceptions
					throw new RuntimeException("Exception while reading " + source + ": " + json, e);
				}
				
				LOGGER.trace("Success {}", source);
			} else if(source.isDirectory()){
				loadStaticData(source);
			}
		}

	}

	private void cacheSource(JsonObject json) {
		for(Entry<String, JsonElement> entry : json.entrySet()){
			String key = entry.getKey();
			
			//Ensure the key exists
			if(!CACHE.containsKey(key)){
				CACHE.put(key, new HashMap<String, JsonElement>());
			}
			
			//Add the data to the key
			putAll(CACHE.get(key), entry.getValue().getAsJsonObject());
		}
	}

	private void putAll(Map<String, JsonElement> map, JsonObject value) {
		for(Entry<String, JsonElement> entry : value.entrySet()){
			map.put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Map<String, Map<String, JsonElement>> getCache() {
		return CACHE;
	}

	@Override
	public boolean contains(String id) {
		return CACHE.containsKey(getKey(id));
	}

	@Override
	public Map<String, JsonElement> getData(String id) {
		return CACHE.get(getKey(id));
	}

	@Override
	public String getKey(String id) {
		return id;
	}

	@Override
	public Map<String, JsonElement> getClonedObject(Map<String, JsonElement> original) {
		return ImmutableMap.copyOf(original);
	}

	public Set<String> getKnownTypes() {
		return CACHE.keySet();
	}
}
