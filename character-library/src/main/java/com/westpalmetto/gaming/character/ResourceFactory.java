package com.westpalmetto.gaming.character;

import java.io.File;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResourceFactory{
    // public static final String PATH_DATA =
    // "/Users/weclowney/git/character-sheets/character-data/data/";
    public static final String PATH_DATA =
            "C:\\Users\\Weston\\Workspaces\\CharacterSheets\\character-sheets\\character-data\\data\\";

    private static final File DIR_PSRD = new File(PATH_DATA + "/resource/psrd");
    private static final File DIR_SFSRD = new File(PATH_DATA + "/resource/sfsrd");
    private static final File DIR_SWADE = new File(PATH_DATA + "/resource/swade");

    @AllArgsConstructor
    @Getter
    public enum Ruleset{
        PSRD(DIR_PSRD), SFSRD(DIR_SFSRD), SWADE(DIR_SWADE);

        private final File root;
    }

    /**
     * Determines the resource root by the name of a ruleset.
     * 
     * @param name The name of the ruleset
     * @return The resource root
     */
    public static File rootForName(String name){
        Ruleset ruleset = Ruleset.valueOf(name);
        if (ruleset == null) {
            throw new IllegalArgumentException("No ruleset found for " + name);
        }
        return ruleset.getRoot();
    }

	/**
	 * Determines the resource root by the name of a ruleset.
	 * 
	 * @param root The name of the ruleset
	 * @return The ruleset name
	 */
	public static String rootForFile(File root) {
		Ruleset ruleset = null;
		for (Ruleset current : Ruleset.values()) {
			if (current.getRoot().equals(root)) {
				ruleset = current;
				break;
			}
		}

		if (ruleset == null) {
			throw new IllegalArgumentException("No ruleset found for " + root);
		}
		return ruleset.toString();
	}
}
