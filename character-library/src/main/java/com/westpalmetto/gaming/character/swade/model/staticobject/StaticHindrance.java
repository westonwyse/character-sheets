package com.westpalmetto.gaming.character.swade.model.staticobject;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class StaticHindrance extends SimpleOverridableAbilityProvider{
	/**
	 * Copy constructor.
	 * @param original The original object
	 */
	public StaticHindrance(StaticHindrance original){
		super(original);
		this.type=original.getType();
	}

	private HindranceType type;
	
	@RequiredArgsConstructor
	@Getter
	public enum HindranceType{
		MAJOR(2),
		MINOR(1);
		
		private final int value;
	}
}
