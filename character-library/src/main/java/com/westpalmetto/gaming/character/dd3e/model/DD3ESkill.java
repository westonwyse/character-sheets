package com.westpalmetto.gaming.character.dd3e.model;

import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.model.Skill;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DD3ESkill extends Skill{
    private AttributeName attributeName;
    private boolean classSkill;
    private boolean trainedOnly;

    /**
     * Creates a Skill instance
     * 
     * @param name
     * @param attribute
     * @deprecated Use Skill(name, attribute, trainedOnly)
     */
    public DD3ESkill(String name, AttributeName attribute){
        this(name, attribute, false);
    }

    public DD3ESkill(String name, AttributeName attribute, boolean trainedOnly){
        this.attributeName = attribute;
        this.trainedOnly = trainedOnly;
        setName(name);
    }

    public DD3ESkill(DD3ESkill skill){
        super(skill);
        this.attributeName = skill.getAttributeName();
        this.classSkill = skill.isClassSkill();
        this.trainedOnly = skill.isTrainedOnly();
    }

    public AttributeName getAttributeName(){
        if (attributeName == null) {
            throw new NullPointerException("AttributeName is null for skill " + getName());
        }
        return attributeName;
    }

    public boolean isArmorPenalty(){
        return (AttributeName.STR.equals(getAttributeName()) || AttributeName.DEX.equals(getAttributeName()));
    }

}
