/**
 * PSRDCharacterMapper.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.servlet.writer.character.BattleMatrixWriter;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;
import com.westpalmetto.gaming.character.servlet.writer.character.psrd.PsrdWriterFactory;
import com.westpalmetto.gaming.character.servlet.writer.character.savageworlds.SavageWorldsWriterFactory;
import com.westpalmetto.gaming.character.servlet.writer.character.sfsrd.SFWriterFactory;
import com.westpalmetto.gaming.character.sfsrd.SFCharacter;
import com.westpalmetto.gaming.character.swade.SwadeCharacter;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Handling of response writing for characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class CharacterWriter implements ResponseWriter {
	private static final Pattern PATTERN_PATH = Pattern.compile("/(\\w+)/(.+)");
	private static final String PATH_CHARACTERS = ResourceFactory.PATH_DATA+"character/";
	private static final String KEY_CAPTURE_TYPE = "$1";
	private static final String KEY_CAPTURE_PATH = "$2";
	
	private enum WriterType {
		CHARACTERS,
		BATTLEMATRIX,
		;
	}
	
	@Override
	public InputStream getFileStream(String path){
		Matcher matcher = PATTERN_PATH.matcher(path);
		String characterPath = matcher.replaceAll(KEY_CAPTURE_PATH);
		String filepath = PATH_CHARACTERS + characterPath + ".json";
		File file = new File(filepath);
		log.info("Path to JSON: {}", file);
		try {
			return FileUtils.openInputStream(file);
		} catch (IOException e) {
			// TODO: Exceptions
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String getMimeType(String path){
		return "text/html";
	}
	
	@Override
	public void write(String path, HttpServletResponse response) throws IOException {
		//Open & parse the file
		JsonParser parser = new JsonParser();
		String contents = IOUtils.toString(getFileStream(path));
		JsonObject json = (JsonObject)parser.parse(contents);
		
		//Create the character object
		Character<?> character = createCharacter(json);
		
		//Determine the writer type
		Matcher matcher = PATTERN_PATH.matcher(path);
		WriterType writerType = WriterType.valueOf(StringUtils.upperCase(matcher.replaceAll(KEY_CAPTURE_TYPE)));
		if (writerType == null) {
			throw new IllegalArgumentException("Unknown writer type " + writerType);
		}
		
		//Get writers & write contents
		PrintWriter printWriter = response.getWriter();
		CharacterHtmlWriter htmlWriter = getHtmlWriter(character, writerType);
        htmlWriter.writeHtml(printWriter, character);
        
        //Close stream
        printWriter.close();
	}

	/**
	 * Creates the character object appropriate given the JSON.
	 * 
	 * @param json The JSON data
	 * @return The Character object.
	 */
	private Character<?> createCharacter(JsonObject json){
		//Get the character class
		String className = GsonUtils.asString(json.get(Character.KEY_TYPENAME));
		Class<?> cls;
		try {
			cls = Class.forName(className);
		} catch (ClassNotFoundException e) {
			//TODO: Exceptions
			throw new RuntimeException("Could not find class " + className, e);
		}
		
		//Create the character object
		Character<?> character;
		try {
			if (Character.class.isAssignableFrom(cls)){
				character = (Character<?>)cls.newInstance();
				character.initialize(json);
			} else {
				throw new IllegalArgumentException("Cannot assign " + className + " to Character.");
			}
		} catch (InstantiationException e) {
			//TODO: Exceptions
			throw new RuntimeException("Could not initialize class " + className, e);
		} catch (IllegalAccessException e) {
			//TODO: Exceptions
			throw new RuntimeException("Could not initialize class " + className, e);
		}
		
		//Return the character
		return character;
	}

	/**
	 * Factory method to determine HTML writer from the JSON input.
	 * 
	 * @param character The character to write.
	 * @param writerType The type of the writer to return
	 * @return The HTML writer to use.
	 */
	protected CharacterHtmlWriter<? extends Character<?>>
			getHtmlWriter(Character<?> character, WriterType writerType){
		CharacterHtmlWriter<? extends Character<?>> writer;
		
		//Create the writer based on type
		switch(writerType){
		case CHARACTERS:
		    if(character instanceof PSRDCharacter){
                writer = PsrdWriterFactory.getWriter((PSRDCharacter)character);
            } else if(character instanceof SFCharacter){
                writer = SFWriterFactory.getWriter((SFCharacter)character);
            } else if(character instanceof SwadeCharacter){
				writer = SavageWorldsWriterFactory.getWriter((SwadeCharacter)character);
			} else {
				throw new IllegalArgumentException("Unknown character type: "
						+ character.getClass().getName());
			}
			break;
			
		case BATTLEMATRIX:
			writer = new BattleMatrixWriter();
			break;
			
		default:
			throw new IllegalArgumentException("Unknown writer type " + writerType);
		}
		
		return writer;
	}
}
