/**
 * StaticRace.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;

/**
 * Contains the static details of a race.
 */
@Value
@NoArgsConstructor(force=true)
@EqualsAndHashCode(callSuper = true)
public class StaticRace extends SimpleOverridableAbilityProvider{
	private int baseSpeed;
	private String size;
	private Integer hitPoints;
	
	/**
	 * Copy constructor.
	 * @param original
	 */
	public StaticRace(StaticRace original) {
		super(original);
		this.baseSpeed=original.getBaseSpeed();
		this.size=original.getSize();
		this.hitPoints=original.getHitPoints();
	}
}
