/**
 * CharacterTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 10, 2013
 */
package com.westpalmetto.gaming.character.dd3e;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.Attribute;
import com.westpalmetto.gaming.character.dd3e.model.AttributeSet;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FeatWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.RaceWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel.PSRDAdvancementTable;


/**
 * Tests for Character.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class Character3ETest {
	private Character3E character;
	
	@Before
	public void setup(){
		character = new PSRDCharacter(new PSRDCharacterModel(ResourceFactory.Ruleset.PSRD.getRoot())) {};
		character.getData().setRace(new RaceWrapper("human"));
		((PSRDCharacter)character).getPsrdData().setAdvancementTable("FAST");
	}
	
	@Test
	public void testCarryingCapacity(){
		//Test manual entry - low
		int str = 1;
		AttributeSet attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, str));
		character.getData().setAttributes(attributes);
		Assert.assertEquals("Light load not as expected for " + str, 3, character.getCarryingCapacityLight());
		Assert.assertEquals("Medium load not as expected for " + str, 6, character.getCarryingCapacityMedium());
		Assert.assertEquals("Heavy load not as expected for " + str, 10, character.getCarryingCapacityHeavy());
		
		//Test manual entry - medium
		str = 11;
		attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, str));
		character.getData().setAttributes(attributes);
		Assert.assertEquals("Light load not as expected for " + str, 38, character.getCarryingCapacityLight());
		Assert.assertEquals("Medium load not as expected for " + str, 76, character.getCarryingCapacityMedium());
		Assert.assertEquals("Heavy load not as expected for " + str, 115, character.getCarryingCapacityHeavy());
		
		//Test manual entry - high
		str = 17;
		attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, str));
		character.getData().setAttributes(attributes);
		Assert.assertEquals("Light load not as expected for " + str, 86, character.getCarryingCapacityLight());
		Assert.assertEquals("Medium load not as expected for " + str, 173, character.getCarryingCapacityMedium());
		Assert.assertEquals("Heavy load not as expected for " + str, 260, character.getCarryingCapacityHeavy());
		
		//Test calculated entry - on table
		str = 25;
		attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, str));
		character.getData().setAttributes(attributes);
		Assert.assertEquals("Light load not as expected for " + str, 266, character.getCarryingCapacityLight());
		Assert.assertEquals("Medium load not as expected for " + str, 533, character.getCarryingCapacityMedium());
		Assert.assertEquals("Heavy load not as expected for " + str, 800, character.getCarryingCapacityHeavy());
		
		//Test calculated entry - extraordinary
		str = 38;
		attributes = new AttributeSet();
		attributes.add(new Attribute(AttributeName.STR, str));
		character.getData().setAttributes(attributes);
		Assert.assertEquals("Light load not as expected for " + str, 1600, character.getCarryingCapacityLight());
		Assert.assertEquals("Medium load not as expected for " + str, 3200, character.getCarryingCapacityMedium());
		Assert.assertEquals("Heavy load not as expected for " + str, 4800, character.getCarryingCapacityHeavy());
	}
	
	@Test
	@Ignore
	public void testGetBaseSpeedEncumbered(){
		character.getData().setRace(new RaceWrapper("TEST_RACE"));
		int[] speeds = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120};
		int[] expected = {5, 10, 10, 15, 20, 20, 25, 30, 30, 35, 40, 40, 45, 50, 50, 55, 60, 60, 65, 70, 70, 75, 80, 80};
		
		for(int i=0; i < 24; i++){
			int speed = speeds[i];
			Assert.assertEquals("Result not as expected for " + speed, expected[i], character.calculateEncumberedSpeed(speed));
		}
	}
	
	@Test
	public void testBonusSpells(){
		int[] bonuses = {-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10};
		
		//Level 0
		int spellLevel = 0;
		int[] expected = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		for(int i=0; i < 16; i++){
			int bonus = bonuses[i];
			Assert.assertEquals("Result not as expected for " + bonus, expected[i], character.getBonusSpells(bonus, spellLevel));
		}
		
		//Level 1
		spellLevel = 1;
		expected = new int[]{0,0,0,0,0,0,1,1,1,1,2,2,2,2,3,3};
		for(int i=0; i < 16; i++){
			int bonus = bonuses[i];
			Assert.assertEquals("Result not as expected for " + bonus, expected[i], character.getBonusSpells(bonus, spellLevel));
		}
		
		//Level 3
		spellLevel = 3;
		expected = new int[]{0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2};
		for(int i=0; i < 16; i++){
			int bonus = bonuses[i];
			Assert.assertEquals("Result not as expected for " + bonus, expected[i], character.getBonusSpells(bonus, spellLevel));
		}
	}
	
	@Test
	public void testAttributeModifier(){
		int[] expected = {-5,-4,-4,-3,-3,-2,-2,-1,-1,0,0,1,1,2,2,3,3,4,4,5,5,6,6,7};
		int[] actual = new int[24];
		AttributeName name = AttributeName.CHA;
		
		for(int i=0; i<24; i++){
//			Attribute attribute = new Attribute(name, i+1);
//			AttributeSet attributes = new AttributeSet();
//			attributes.add(attribute);
//			target.getData().setAttributes(attributes);
			actual[i] = character.getAttributeModifier(i+1);
		}
		Assert.assertEquals("Arrays are not the same length.", expected.length, actual.length);
		Assert.assertEquals("Modifiers not as expected.", Arrays.toString(expected), Arrays.toString(actual));
	}
	
	@Test
	public void testThreatRange_Normal() {
		//Test 20
		Weapon weapon = new Weapon("Net");
		String actual = character.getThreatRange(weapon);
		Assert.assertEquals("20 not returned as expected", "20", actual);
		
		//Test less than 20
		weapon = new Weapon("Scimitar");
		actual = character.getThreatRange(weapon);
		Assert.assertEquals("18-20 not returned as expected", "18-20", actual);
	}
	
	@Test
	public void testThreatRange_ImprovedCritical() {
		FeatWrapper improvedCritical = new FeatWrapper("Improved Critical");
		improvedCritical.getItemKeys().add("Scimitar");
		
		BonusProviderSet<FeatWrapper> feats = new BonusProviderSet<FeatWrapper>();
		feats.add(improvedCritical);
		character.getData().setFeats(feats);
		
		Weapon weapon = new Weapon("Scimitar");
		String actual = character.getThreatRange(weapon);
		Assert.assertEquals("Improved Critical not returned as expected", "15-20", actual);
	}
	
	@Test
	public void testCreateHitDice(){
		Assert.assertEquals(8, character.createHitDice(1, new BigDecimal(8), true).get(0).intValue());
		Assert.assertEquals(5, character.createHitDice(1, new BigDecimal(8), false).get(0).intValue());
	}
	
	@Test
	public void testArmorCheck(){
		character.getData().getClasses().add(new ClassWrapper("fighter", 10));

		Armor armor = new Armor("Half-plate");
		character.getData().getEquipment().add(armor);
		
		int actual = character.getTotalArmorPenalty();
		Assert.assertEquals(-5, actual);
	}
	
	@Test
	public void testGetEarnedFeats(){
		PSRDAdvancementTable table = PSRDAdvancementTable.FAST;
		int[] expected = new int[]{2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11};//+1 for human race
		
		for(int i=1; i <= 20; i++){
			character.getData().setExperience(new BigDecimal(table.getExperience(i)));
			
			Assert.assertEquals("Calculated level is not as expected.", i, character.getData().getAdvancement().getLevel(character.getData().getExperience().intValue()));
			Assert.assertEquals("Incorrect number of feats at level " + i, expected[i-1], character.getEarnedFeats());
		}
	}
}
