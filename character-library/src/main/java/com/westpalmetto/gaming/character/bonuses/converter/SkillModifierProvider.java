package com.westpalmetto.gaming.character.bonuses.converter;

import java.util.Collection;

import com.westpalmetto.gaming.character.bonuses.AbstractBonusProvider;
import com.westpalmetto.gaming.character.model.Skill;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;

public class SkillModifierProvider extends AbstractBonusProvider{
    private static final String NAME = "Skill Modifiers";

    /**
     * Creates a provider for the given skills.
     * 
     * @param skills The skills for which to create Modifiers.
     */
    public SkillModifierProvider(Collection<? extends Skill> skills){
        for (Skill skill : skills) {
            Modifier modifier = new Modifier();
            modifier.setType(ModifierType.SKILL_RANK);
            modifier.setValue(skill.getRanks());
            modifier.getItemKeys().add(skill.getName());

            getModifiers().add(modifier);
        }
    }

    @Override
    public String getName(){
        return NAME;
    }

    @Override
    public String getSource(){
        return null;
    }
}
