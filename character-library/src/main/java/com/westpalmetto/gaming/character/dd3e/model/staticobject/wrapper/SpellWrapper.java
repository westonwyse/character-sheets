/**
 * CharacterSpell.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSpell;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticSpellLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractStaticObjectWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Contains the character-specific details of a character's spell.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SpellWrapper extends AbstractStaticObjectWrapper<StaticSpell> {
	public SpellWrapper(String id){
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	/**
	 * @return The data
	 * @todo Implement interface instead?
	 */
	@Override
	public StaticSpell getData(){
		StaticSpell spell = super.getData();
		
		if(spell == null){
			spell = new StaticSpell();
			spell.setName(getId());
			spell.setDescription("UNKNOWN SPELL");
		}
		
		return spell;
	}

	@Override
	protected DataLoader<StaticSpell> createDataLoader() {
		return StaticSpellLoader.getInstance(getRoot());
	}
}
