/**
 * SpellcastingClassPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.predicate;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;

/**
 * Predicate to match a spellcasting classes.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SpellcastingClassPredicate implements Predicate {
	private static SpellcastingClassPredicate instance;

	private SpellcastingClassPredicate() {
		// Making constructor private - singleton instance
	}

	public static SpellcastingClassPredicate getInstance() {
		if (instance == null) {
			instance = new SpellcastingClassPredicate();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		StaticClass cls = null;

		// Investigate object
		if (object instanceof ClassWrapper) {
			cls = ((ClassWrapper) object).getData();
		} else if (object instanceof StaticClass) {
			cls = (StaticClass) object;
		}

		if (cls != null) {
			matches = (cls.getSpellsPerDay() != null);
		}

		return matches;
	}
}
