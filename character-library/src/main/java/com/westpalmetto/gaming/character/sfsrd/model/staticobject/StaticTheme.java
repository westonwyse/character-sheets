/**
 * StaticClass.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.sfsrd.model.staticobject;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Contains the static details of a class.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
public class StaticTheme extends SimpleOverridableAbilityProvider {
	private String themeSkill;

	/**
	 * Copy constructor
	 * 
	 * @param original The object to copy
	 */
	public StaticTheme(StaticTheme original) {
		super(original);
		this.themeSkill = original.getThemeSkill();
	}
}
