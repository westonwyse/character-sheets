package com.westpalmetto.gaming.character.model.staticobject.loader;

import java.io.File;
import java.util.Map.Entry;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.util.CollectionHelper;
import com.westpalmetto.gaming.character.util.ObjectHelper;
import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

public abstract class OverridableStaticObjectLoader<V extends SimpleOverridableAbilityProvider> extends
        JsonDataLoader<V>{
    public OverridableStaticObjectLoader(String typeId, Class<V> type, File root){
        super(typeId, type, root);
    }

    @Override
    protected void initializeCache(){
        // Load the data
        super.initializeCache();

        // Handle any templates
        for (Entry<String, V> entry : getCache().entrySet()) {
            // If modified, get the original & modify it.
            V template = getClonedObject(entry.getValue());
            if (CollectionHelper.isNotEmpty(template.getOverrides())) {
                if (template.getOverrides().size() != 1) {
                    // TODO: Exceptions
                    throw new RuntimeException("Too many overrides found for " + template);
                }

                // Retrieve the original
                String overrideKey = getKey(CollectionHelper.get(template.getOverrides(), 0));
                V original = getCache().get(overrideKey);
                if (original == null) {
                    // TODO: Exceptions
                    throw new RuntimeException("Could not find '" + overrideKey + "' in " + getCache().keySet());
                }
                V newObject = createNewObject(template, original);

                // Persist updates
                getCache().put(getKey(entry.getKey()), newObject);
            }
        }
    }

    /**
     * Creates the new object that applies the template overrides to the
     * original.
     * 
     * @param template The template
     * @param original The original object
     * @return The overridden object
     */
    protected V createNewObject(V template, V original){
        V newObject = getClonedObject(original);

        // Set up the name and source
        newObject.setName(template.getName() + " (" + original.getName() + ")");
        newObject.setSource(template.getSource() + " (" + original.getName() + ": " + original.getSource() + ")");

        // Apply the template
        newObject.setAbilities(
                ObjectHelper.overrideAbilities(template.getAbilities(), original.getAbilities(), original.getName()));

        return newObject;
    }
}
