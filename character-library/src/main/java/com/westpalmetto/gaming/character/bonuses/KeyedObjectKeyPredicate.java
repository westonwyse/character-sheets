/**
 * BonusKeyPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.model.bonuses.KeyedObject;

/**
 * Predicate to match an object by a given value of itemKey. Evaluates to true
 * if an object implements KeyedObject and returns one or more bonuses
 * containing the specified key.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class KeyedObjectKeyPredicate implements Predicate {
	private static final Map<Object, KeyedObjectKeyPredicate> CACHE = new HashMap<Object, KeyedObjectKeyPredicate>();

	private Object key;

	public static KeyedObjectKeyPredicate getInstance(Object key) {
		if (!CACHE.containsKey(key)) {
			CACHE.put(key, new KeyedObjectKeyPredicate(key));
		}

		return CACHE.get(key);
	}

	/**
	 * Creates a predicate object for the given key. The value of key cannot be
	 * null.
	 * 
	 * @param key The key to match against.
	 */
	private KeyedObjectKeyPredicate(Object key) {
		this.key = key;
	}

	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;

		// Investigate object, if it is a KeyedObject
		if (object instanceof KeyedObject) {
			KeyedObject keyed = (KeyedObject) object;

			// Slightly different comparisons, if the key is a Collection
			if (key instanceof Collection) {
				matches = evaluateCollection((Collection<?>) key, keyed);
			} else {
				matches = evaluateObject(key, keyed);
			}
		}

		return matches;
	}

	private boolean evaluateCollection(Collection<?> keyCollection, KeyedObject keyed) {
		boolean matches = false;

		if (CollectionUtils.isEmpty(keyCollection)) {
			// If there is no key, always return true
			matches = true;
		} else {
			// Matches if any key exists in the item keys
			matches = CollectionUtils.containsAny(keyCollection, BonusHelper.validateItemKeys(keyed));

			// If not in the item keys, try the name
			if (!matches) {
				matches = keyCollection.contains(keyed.getName());
			}
		}

		return matches;
	}

	private boolean evaluateObject(Object keyObj, KeyedObject keyed) {
		boolean matches = false;

		if (keyObj == null) {
			// If there is no key, always return true
			matches = true;
		} else {
			// Matches if the key exists in the item keys
			matches = BonusHelper.validateItemKeys(keyed).contains(keyObj.toString());

			// If not in the item keys, try the name
			if (!matches) {
				matches = StringUtils.contains(keyObj.toString(), keyed.getName());
			}
		}

		return matches;
	}
}
