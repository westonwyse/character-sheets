package com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Charge extends Model implements NamedObject{
    public static final ChargedItemPredicate PREDICATE = new ChargedItemPredicate();
    public static final String DEFAULT_NAME = "Charges";

    private String name;
    private String source;
    private int quantity;

    public Charge(){}

    public Charge(Charge original){
        setName(original.getName());
        setSource(original.getSource());
        setQuantity(original.getQuantity());
    }

    @Override
    public String getName(){
        if (StringUtils.isBlank(name)) {
            setName(DEFAULT_NAME);
        }
        return name;
    }

    public static class ChargedItemPredicate implements Predicate{

        @Override
        public boolean evaluate(Object object){
            boolean charged = false;

            if (object instanceof AbstractEquipment<?>) {
                charged = CollectionUtils.isNotEmpty(((AbstractEquipment<?>) object).getSpecifiedCharges());
            }

            return charged;
        }
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }
}
