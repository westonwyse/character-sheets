/**
 * WriterFactory.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer;

import java.util.regex.Pattern;

/**
 * Factory to determine the type of ResponseWriter needed for a given request.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class WriterFactory {
	private static final Pattern PATTERN_CHARACTER = Pattern.compile("/(characters|battleMatrix)/.+");
	private static final Pattern PATTERN_TEXT = Pattern.compile("/(styles)/.+");
	private static final Pattern PATTERN_IMAGE = Pattern.compile("/(images)/.+");
	
	private static final ResponseWriter WRITER_CHARCTER = new CharacterWriter();
	private static final ResponseWriter WRITER_TEXT = new TextWriter();
	private static final ResponseWriter WRITER_IMAGE = null;
	
	private WriterFactory(){
		throw new UnsupportedOperationException("Class cannot be instantiated.");
	}

	/**
	 * Determines the writer for the given path.
	 * 
	 * @param path The path for the request.
	 * @return An appropriate ResponseWriter object for the request.
	 */
	public static ResponseWriter getWriter(String path) {
		ResponseWriter writer = null;
		
		if(PATTERN_CHARACTER.matcher(path).matches()){
			writer = WRITER_CHARCTER;
		} else if(PATTERN_TEXT.matcher(path).matches()){
			writer = WRITER_TEXT;
		} else {
			//TODO: Exceptions
			throw new RuntimeException("Could not find writer for " + path);
		}
		
		return writer;
	}
}
