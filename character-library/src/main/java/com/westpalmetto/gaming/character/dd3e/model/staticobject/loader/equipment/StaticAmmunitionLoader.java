package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticDeityLoader;

public class StaticAmmunitionLoader extends OverridableStaticObjectLoader3E<StaticEquipment> {
	private static final Map<File,StaticAmmunitionLoader> INSTANCES = new HashMap<>();
	
	public static StaticAmmunitionLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticAmmunitionLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticAmmunitionLoader(File root) {
		super("ammunition", StaticEquipment.class, root);
	}
	
	@Override
	public StaticEquipment getClonedObject(StaticEquipment original) {
		return new StaticEquipment(original);
	}
}
