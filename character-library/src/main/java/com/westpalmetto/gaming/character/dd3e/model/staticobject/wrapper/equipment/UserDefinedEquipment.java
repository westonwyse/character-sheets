/**
 * Coins.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 9, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to allow user-defined equipment that exists outside of the static
 * equipment data.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class UserDefinedEquipment extends AbstractEquipment<StaticEquipment> {
	private StaticEquipment data;
	
	/**
	 * @param id the ID of this item
	 */
	public UserDefinedEquipment(String id) {
		super(id);
	}

	@Override
	protected DataLoader<StaticEquipment> createDataLoader() {
		return null;
	}
	
	@Override
	public StaticEquipment getData(){
		//Ensure Data exists
		if(data == null){
			setData(new StaticEquipment());
		}
		
		//Persist name, if not set
		if(StringUtils.isBlank(data.getName())){
			data.setName(getName());
		}
		//return the data
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(StaticEquipment data) {
		this.data = data;
	}
}
