package com.westpalmetto.gaming.character.servlet.writer.character;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.model.Attack;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;

public class BattleMatrixWriter implements CharacterHtmlWriter<Character3E>{
    private static final Logger LOGGER = LoggerFactory.getLogger(BattleMatrixWriter.class);
    
	@Override
	public void writeHtml(PrintWriter writer, Character3E character){
		long start = System.currentTimeMillis();
		
		//Write the heading
		writer.println("<html><head>");
		writer.println("<link rel='stylesheet' href='/character-service/styles/character/character-psrd.css' type='text/css'>");
		writer.println("<title>" + character.getData().getName() + "</title>");
		writer.println("</head><body>");
		
		//Get the attacks
		List<Attack> primary = character.getPrimaryAttacks();
		Attack offhand = character.getOffHandAttack();
		
		//Determine combined total
		List<Attack> combined = new ArrayList<Attack>(primary);
		if(offhand != null){
			combined.add(offhand);
		}
		int columns = combined.size();
		
		//Write the matrix
		openMatrix(writer, primary, offhand, columns);
		writePrimaryWeapon(writer, character, primary, offhand);
		if(offhand != null){
			writeTwoWeaponFighting(writer, character, primary, offhand);
		}
		writeOptions(writer, character, primary, offhand);
		closeMatrix(writer);
		
		//Close file
		writer.println("</body></html>");
		LOGGER.trace("writeHtml: {}ms", System.currentTimeMillis()-start);
	}

	private void openMatrix(PrintWriter writer,
			List<Attack> primary,
			Attack offhand,
			int columns){
		writer.println("<table id='matrix'>");
		
		//Write empty boxes
		int width = widthPerColumn(columns);
		writer.println("<tr id='stores'>");
		writer.println("<th rowspan='2' width='" + width + "%'>Attack</th>");
		for(int i=0; i<columns; i++){
			writer.println("<th width='" + width + "%'>&nbsp;</th>");
		}
		writer.println("</tr>");
		
		//Write weapon names
		writer.println("<tr id='weaponNames'>");
		writer.println("<th colspan='" + primary.size() + "'>" + primary.get(0).getWeapon().getDisplayName() + "</th>");
		if(offhand != null){
			writer.println("<th>" + offhand.getWeapon().getDisplayName() + "</th>");
		}
		writer.println("</tr>");
	}

	private int widthPerColumn(int columns){
		return new BigDecimal("100.00").divide(new BigDecimal(columns+1), RoundingMode.FLOOR).intValue();
	}

	private void writePrimaryWeapon(PrintWriter writer, Character3E character, List<Attack> primary, Attack offhand){
		writer.println("<tr id='primary-weapon'>");
		writer.println("<th>Primary Weapon</th>");
		for (Attack attack : primary){
			writeCell(writer, attack.getBonus(), character.getWeaponDamage(attack.getWeapon()));
		}
		
		if(offhand != null){
			writer.println("<td>-</td>");
		}
		
		writer.println("</tr>");
	}

	private void writeCell(PrintWriter writer, int bonus, String damage){
		writer.println("<td>");
		writer.println("Attack: " + Character.FORMATTER_MODIFIER.format(bonus));
		writer.println("<br/>");
		writer.println("Damage: " + damage);
		writer.println("</td>");
	}

	private void writeTwoWeaponFighting(PrintWriter writer, Character3E character, List<Attack> primary, Attack offhand){
		writer.println("<tr id='two-weapon-fighting'>");
		writer.println("<th>Two Weapon Fighting</th>");
		for (Attack attack : primary){
			writeCell(writer, attack.getBonus() + character.getTwoWeaponPrimaryPenalty(), character.getWeaponDamage(attack.getWeapon()));
		}
		writeCell(writer, offhand.getBonus() + character.getTwoWeaponOffHandPenalty(), character.getWeaponDamage(offhand.getWeapon()));
		
		writer.println("</tr>");
	}

	private void writeOptions(PrintWriter writer,
			Character3E character,
			List<Attack> primary,
			Attack offhand){
		//Make a list of combined attacks
		List<Attack> combined = new ArrayList<Attack>(primary);
		if(offhand != null){
			combined.add(offhand);
		}
		
		//Cycle through the providers
		Collection<BonusProvider> providers = character.getProvidersWithOptional();
		for (BonusProvider provider : providers){
			//Determine the enabled/disabled class
			OptionalBonuses optional = provider.getOptional(character.getModifiers());
			String className;
			if(optional.isEnabled()){
				className = "enabled";
			} else {
				className = "disabled";
			}
			
			String name = provider.getName();
			writer.println("<tr id='" + name + "-row' class='" + className +"'>");
			writer.println("<th>" + name + "</th>");
			
			//Find each bonus/penalty
			Collection<BonusType> types = BonusHelper.getTypes(optional.getBonuses());
			List<String> cellContents = new ArrayList<String>();
			for(BonusType type : types){
				Collection<ModifiableBonus> bonuses = BonusHelper.filterBonusesByType(optional.getBonuses(), type);
				int sum = BonusHelper.sumValues(BonusHelper.calculateBonuses(bonuses, character.getModifiers()));
				if(sum != 0){
					cellContents.add(type.getName() + ": " + Character.FORMATTER_MODIFIER.format(sum));
				}
			}
			Collections.sort(cellContents);
			
			//Cycle through the attacks
			for (Attack attack : combined){
				writer.println("<td>" + StringUtils.join(cellContents, "<br/>") + "</td>");
			}
			
			writer.println("</tr>");
		}
	}

	private void closeMatrix(PrintWriter writer){
		writer.println("</table>");
	}
}
