/**
 * PSRDCharacterTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 19, 2013
 */
package com.westpalmetto.gaming.character.psrd.model;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;

import com.westpalmetto.gaming.character.psrd.model.PFSCharacterModel.PFSAdvancement;


/**
 * Tests for PFSCharacterModel.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class PFSCharacterModelTest {
	@Test
	public void testAdvancement(){
		PFSAdvancement table = new PFSAdvancement();
		Assert.assertEquals(1, table.getLevel(0));
		Assert.assertEquals(7, table.getLevel(19));
		Assert.assertEquals(19, table.getLevel(55));
		Assert.assertEquals(3, table.getLevel(new BigDecimal("8.5").intValue()));
	}
}
