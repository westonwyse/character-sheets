package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.swade.SwadeSpecialAbility;

/**
 * Contains the character-specific details of an Edge.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class EdgeWrapper extends SwadeSpecialAbility{
	private static final String LOADER_NAME = "edge";

	/**
	 * Determines if the given ID is valid.
	 * 
	 * @param id The ID
     * @param root The resource root
	 * @return TRUE is the ID is a known feat; FALSE if not.
	 */
	public static boolean isValid(String id, File root) {
		return isValid(LOADER_NAME, id, root);
	}
		
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public EdgeWrapper(String id) {
		super(id);
	}

	@Override
	public String getLoaderId() {
		return LOADER_NAME;
	}
}
