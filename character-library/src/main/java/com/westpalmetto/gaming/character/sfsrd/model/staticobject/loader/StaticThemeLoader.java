package com.westpalmetto.gaming.character.sfsrd.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.sfsrd.model.staticobject.StaticTheme;

public class StaticThemeLoader extends OverridableStaticObjectLoader3E<StaticTheme> {
	private static final Map<File, StaticThemeLoader> INSTANCES = new HashMap<>();

	public static StaticThemeLoader getInstance(File root) {
		if (!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticThemeLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticThemeLoader(File root) {
		super("theme", StaticTheme.class, root);
	}

	@Override
	public StaticTheme getClonedObject(StaticTheme original) {
		return new StaticTheme(original);
	}
}
