package com.westpalmetto.gaming.character.servlet.writer.character.psrd;

import java.io.PrintWriter;

import org.apache.commons.collections.CollectionUtils;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.bluerider.BlueRider;
import com.westpalmetto.gaming.character.psrd.bluerider.model.staticobject.wrapper.CharacterPointBonus;

/**
 * HTML writer for Blue Riders characters.
 */
public class BlueRiderWriter extends PsrdCharacterWriter {
	@Override
	protected void writeSpecialAbilities(PrintWriter writer, PSRDCharacter character){
		super.writeSpecialAbilities(writer, character);
		writeCharacterPoints(writer, (BlueRider)character);
	}
	
	protected void writeCharacterPoints(PrintWriter writer, BlueRider character){
		//Open section
		writer.println("<div id='characterPoints'>");
		writer.println("<h3>Character Points</h3>");
		
		writer.println("<div><b>Earned:</b> " + character.getCharacterPoints() + "</div>");
		writer.println("<div><b>Spent:</b> " + character.getCharacterPointsSpent() + "</div>");
		
		//Write spent
		writer.println("<div class='spent'>");
		BonusProviderSet<CharacterPointBonus> spent = character.getBlueRiderData().getCharacterPoints();
		if(CollectionUtils.isNotEmpty(spent)){
			writer.println("<ul>");
			writeSpecialAbilityList(spent, writer, character.getModifiers());
			writer.println("</ul>");
		}
		
		//Close section
		writer.println("</div>");
	}
}
