package com.westpalmetto.util.resources;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.westpalmetto.util.resources.cache.Cache;
import com.westpalmetto.util.resources.function.QueueUpdateFunction;

/**
 * Manager to handle loading resources of various types and holding the data in
 * memory to transform to the desired types.
 *
 * @param <K> The type of the key
 * @param <V> The type of the value
 */
public class ResourceManager <K extends ResourceLoader<V>, V> {
	private final Cache<K, V> cache;
	private final Cache<K, ListenableFuture<V>> queue;
	
	public ResourceManager(long ttl){
		this.cache = new Cache<>(ttl);
		this.queue = new Cache<>(ttl);
	}
	
	/**
	 * Gets a value from the cache.  Will block to load if not loaded. Will
	 * queue update & return previous value if expired.
	 * 
	 * @param key The key to retrieve.
	 * @return The value of the key.
	 */
	public V get(K key) {
		V value;
		
		if(!cache.contains(key)){
			value = blockingLoad(key);
		} else {
			if(!cache.isCurrent(key)){
				queueUpdate(key);
			}
			
			value = cache.get(key);
		}
		
		return value;
	}

	/**
	 * Loads a resource.  Will block until load is completed.
	 * 
	 * @param key The key to load.
	 * @return The value returned.
	 */
	private V blockingLoad(K key) {
		V value = key.getResource();
		cache.put(key, value);
		return value;
	}

	/**
	 * Queues an update of the given key.
	 * 
	 * @param key The key to update.
	 */
	public synchronized void queueUpdate(K key) {
		if(!queue.isCurrent(key)){
			//Queue the update
			ListenableFuture<V> valueFuture = key.queueResource();
			ListenableFuture<V> queuedFuture = Futures.transform(valueFuture, new QueueUpdateFunction<K, V>(key, cache));
			queue.put(key, queuedFuture);
		}
	}
	
	/**
	 * Clears all values from the cache, and clears any queued updates.
	 */
	public synchronized void clear(){
		//Stop any incomplete queued processes.
		for(K key : queue.keySet()){
			ListenableFuture<V> future = queue.get(key);
			if(!future.isDone()){
				future.cancel(true);
			}
		}
		
		//Clear the queue and the cache.
		queue.clear();
		cache.clear();
	}
}
