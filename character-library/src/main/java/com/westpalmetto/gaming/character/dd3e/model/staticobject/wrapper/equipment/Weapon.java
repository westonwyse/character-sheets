/**
 * Weapon.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 6, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;
import java.util.Set;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon.Heft;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment.StaticWeaponLoader;
import com.westpalmetto.gaming.character.model.Damage;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to contain stats for a weapon.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class Weapon extends AbstractEquipment<StaticWeapon>{
	public static final String KEY_DEFAULT = "WEAPON";
	
	private Quality quality = Quality.NORMAL;
	private Set<Ammunition> ammunition;
	
	public Weapon(String id) {
		super(id);
	}

	@Override
	protected DataLoader<StaticWeapon> createDataLoader() {
		return StaticWeaponLoader.getInstance(getRoot());
	}
	
	@Override
	public String getDisplayName() {
		String name = getName();
		
		if(!(Quality.NORMAL.equals(getQuality()))){
			name += ", " + getQuality().getLabel();
		}
		
		return name;
	}
	
	/**
	 * @return The damage for this weapon.
	 */
	public Damage getDamage(){
		return getData().getDamage().getForSize(getSize());
	}

	/**
	 * @return TRUE if the weapon applies for the Weapon Finesse feat; FALSE if not.
	 */
	public boolean isWeaponFinesse() {
		return Heft.LIGHT.equals(getData().getHeft()) || getData().isWeaponFinesse();
	}
	
	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		if(quality == null){
			setQuality(Quality.NORMAL);
		}
		return quality;
	}
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}
	
	/**
	 * @return the ammunition
	 */
	public Set<Ammunition> getAmmunition() {
		return ammunition;
	}

	/**
	 * @param ammunition the ammunition to set
	 */
	public void setAmmunition(Set<Ammunition> ammunition) {
		this.ammunition = ammunition;
	}

	/**
	 * The quality of a weapon, and the bonuses that quality confers.
	 */
	public enum Quality{
		NORMAL(0, 0, ""),
		MASTERWORK(1, 0, "Masterwork"),
		PLUS1(1, 1, "+1"),
		PLUS2(2, 2, "+2"),
		PLUS3(3, 3, "+3"),
		PLUS4(4, 4, "+4"),
		PLUS5(5, 5, "+5"),
		;
		
		private int attackBonus;
		private int damageBonus;
		private String label;
		
		private Quality(int attackBonus, int damageBonus, String label){
			this.attackBonus = attackBonus;
			this.damageBonus = damageBonus;
			this.label = label;
		}

		/**
		 * @return the attackBonus
		 */
		public int getAttackBonus() {
			return attackBonus;
		}

		/**
		 * @return the damageBonus
		 */
		public int getDamageBonus() {
			return damageBonus;
		}

		/**
		 * @return the label
		 */
		public String getLabel() {
			return label;
		}
	}
}
