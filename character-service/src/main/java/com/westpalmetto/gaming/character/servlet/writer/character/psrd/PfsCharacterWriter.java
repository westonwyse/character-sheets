/**
 * PSRDCharacterMapper.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.psrd;

import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.psrd.PFSCharacter;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.model.PFSCharacterModel;

/**
 * Handling of response writing for PSRD characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class PfsCharacterWriter extends PsrdCharacterWriter {
	@Override
	protected void writeBasics(PrintWriter writer, PSRDCharacter character){
		//Get data
		PFSCharacter pfsCharacter = (PFSCharacter)character;
		PFSCharacterModel data = pfsCharacter.getPfsData();
		
		//Write standard information
		super.writeBasics(writer, character);
		
		//Open section
		writer.println("<div id='basics-pfs'>");
		
		//Write PFS data, if applicable
		if(StringUtils.isNotBlank(data.getFaction())){
			writeLabelValuePair(writer, "faction", "Faction", data.getFaction());
			writeLabelValuePair(writer, "fame", "Fame", data.getFame());
			writeLabelValuePair(writer, "prestige", "Prestige", data.getPrestige());
			writeLabelValuePair(writer, "buyingLimit", "Item Cost Limit", Character.FORMATTER_THOUSANDS.format(data.getMaxItemCost()));
		}
		
		//Close section
		writer.println("</div>");
	}
}
