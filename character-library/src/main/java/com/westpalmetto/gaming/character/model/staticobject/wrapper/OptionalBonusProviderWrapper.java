package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;

/**
 * Wrapper that has optional backing.  If the backing static object is null,
 * will use an empty data set, with the ID as the name.
 *
 * @param <V> The type of the bonus provider
 */
public abstract class OptionalBonusProviderWrapper<V extends BonusProvider & StaticObject>
		extends AbstractBonusProviderWrapper<V> {

	public OptionalBonusProviderWrapper(String id, String ruleset){
		super(id, ruleset);
	}
	
	@Override
	public V getData(){
		V data = super.getData();
		
		if(data==null) {
			data=createData();
		}
		
		return data;
	}

	/**
	 * @return A data set with the name equal to this wrapper's ID.
	 */
	protected abstract V createData();

}
