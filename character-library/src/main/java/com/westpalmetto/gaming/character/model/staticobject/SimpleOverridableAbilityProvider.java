package com.westpalmetto.gaming.character.model.staticobject;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.model.AbilityProvider;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.util.ObjectHelper;

/**
 * A basic overridable ability provider.
 */
public class SimpleOverridableAbilityProvider extends StaticSpecialAbility implements AbilityProvider {
	private BonusProviderSet<StaticSpecialAbility> abilities;
	
	/**
	 * Default constructor.
	 */
	public SimpleOverridableAbilityProvider(){
	}
	
	public SimpleOverridableAbilityProvider(SimpleOverridableAbilityProvider original){
		super(original);
		setAbilities(new BonusProviderSet<>(original.getAbilities()));
	}
	
	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		Collection<ModifiableBonus> bonuses = getAbilities().getBonuses(modifiers);
		bonuses.addAll(super.getBonuses(modifiers));
		return bonuses;
	}
	
	@Override
	public BonusProviderSet<StaticSpecialAbility> getAbilities() {
		if(abilities == null){
			setAbilities(new BonusProviderSet<StaticSpecialAbility>());
		}
		
		if(StringUtils.isBlank(abilities.getName())){
			abilities.setName(getName() + " Abilities");
		}
		
		setAbilities(ObjectHelper.keyObjects(abilities, getName()));
		setAbilities(ObjectHelper.addKeys(abilities, getItemKeys()));
		
		return abilities;
	}
	/**
	 * @param abilities the abilities to set
	 */
	public void setAbilities(BonusProviderSet<StaticSpecialAbility> abilities) {
		this.abilities = new BonusProviderSet<>();
		ObjectHelper.addCloneAll(this.abilities, abilities);
	}
}
