/**
 * SpellSet.java
 * 
 * Copyright � 2012 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 30, 2012
 */
package com.westpalmetto.gaming.character.dd3e.model;

import com.westpalmetto.gaming.character.collection.KeyedSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SpellWrapper;

/**
 * SpellSet is and implementation of KeyedSet to contain a character's spells.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class SpellSet extends KeyedSet<String, SpellWrapper> {
	/* (non-Javadoc)
	 * @see com.westaplmetto.gaming.character.collection.KeyedSet#getKey(java.lang.Object)
	 */
	@Override
	public String getKey(Object target) {
		String key = null;
		
		if(target instanceof SpellWrapper){
			key = ((SpellWrapper)target).getId();
		} else {
			//TODO: Exceptions
			throw new RuntimeException("Unrecognized object type");
		}
		
		return key;
	}

}
