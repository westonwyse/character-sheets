/**
 * Bonus.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Comparator;
import java.util.TreeSet;

import org.apache.commons.lang3.builder.CompareToBuilder;

import com.westpalmetto.gaming.character.model.Damage;

/**
 * Defines a mechanical bonus provided by some object.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface Bonus extends KeyedObject{
    /**
     * @return the type
     */
    public BonusType getType();

    /**
     * @return the value of a numeric bonus
     */
    public int getValue();

    /**
     * @return the value of a die-based bonus
     */
    public Damage getValueDie();

    /**
     * @return The type of the bonus (e.g. "Fire" or "Sonic")
     */
    public String getValueType();

    /**
     * @return The name of the bonus value (e.g. "insight" or "morale")
     */
    public String getValueName();

    /**
     * @return the name
     */
    @Override
    public String getName();

    /**
     * @return the description of a conditional bonus
     */
    public String getCondition();

    /**
     * The type of the bonus.
     */
    public static enum BonusType{
        ARMOR_CLASS("AC"),
        ARMOR_CHECK("Armor check penalty"),
        MAX_DEX("Max. Dex bonus"),
        ATTACK("Attack"),
        ATTRIBUTE("Attribute"),
        ATTRIBUTE_CORE("Attribute"),
        CLASS_SKILL("Class Skill"),
        COMBAT_MANEUVER("CMB"),
        COMBAT_DEFENSE("CMD"),
        DAMAGE("Damage"),
        DAMAGE_STEP("Damage Step"),
        EDGE("Edge"),
        FEAT("Bonus Feat"),
        FEAT_COUNT("Feat count"),
        HINDRANCE_BUYOFF("Hindrance Buyoff"),
        HIT_POINT("HP"),
        INITIATIVE("Initiative"),
        ITEM_ABILITY("Special ability"),
        NATURAL_WEAPON("Natural Weapon"),
        PACE("Pace"),
        PARRY("Parry"),
        POOL("Pool Count"),
        POWER_COUNT("Power Count"),
        POWER_POINT("Power Point"),
        RUNNING("Running"),
        SAVING_THROW("Save"),
        SIZE("Size"),
        SKILL("Skill"),
        SKILL_POINT("Skill rank"),
        SKILL_UNLOCK("Skill unlock"),
        SPEED("Speed"),
        SPELLCASTING("Spellcasting"),
        TALENT("Talent"),
        TALENT_COUNT("Talent count"),
        TOUGHNESS("Toughness"),
        TRAIT("Trait"),
        TRAIT_COUNT("Trait count"),
        OTHER("Other");

        private final String name;

        private BonusType(String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }
    }

    /**
     * Comparator to compare Bonuses primarily by string comparison of the
     * condition value.
     */
    public static class BonusConditionComparator implements Comparator<Bonus>{
        @Override
        public int compare(Bonus bonus1, Bonus bonus2){
            CompareToBuilder builder = new CompareToBuilder();
            builder.append(bonus1.getCondition(), bonus2.getCondition());
            builder.append(bonus1.getName(), bonus2.getName());
            builder.append(bonus1.getValue(), bonus2.getValue());
            builder.append(new TreeSet<String>(bonus1.getItemKeys()).toString(),
                    new TreeSet<String>(bonus2.getItemKeys()).toString());

            return builder.build();
        }
    }

    public static final BonusConditionComparator COMPARATOR_BONUS_CONDITION = new BonusConditionComparator();
}
