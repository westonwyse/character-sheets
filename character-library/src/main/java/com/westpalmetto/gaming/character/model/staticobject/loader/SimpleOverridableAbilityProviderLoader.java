package com.westpalmetto.gaming.character.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;

public class SimpleOverridableAbilityProviderLoader extends OverridableStaticObjectLoader<SimpleOverridableAbilityProvider> {
	private static final Map<String, SimpleOverridableAbilityProviderLoader> INSTANCES = new HashMap<>();
	
	public static SimpleOverridableAbilityProviderLoader getInstance(String typeId, File root){
		//Create the instance if it does not exist
		if(!INSTANCES.containsKey(typeId)){
			INSTANCES.put(typeId, new SimpleOverridableAbilityProviderLoader(typeId, root));
		}
		
		//Return the instance
		return INSTANCES.get(typeId);
	}
		
	private SimpleOverridableAbilityProviderLoader(String typeId, File root) {
		super(typeId, SimpleOverridableAbilityProvider.class, root);
	}

	@Override
	public SimpleOverridableAbilityProvider getClonedObject(SimpleOverridableAbilityProvider original) {
		if(original == null){
			//TODO: Exceptions
			throw new RuntimeException("Original object is required.");
		}
		return new SimpleOverridableAbilityProvider(original);
	}
}
