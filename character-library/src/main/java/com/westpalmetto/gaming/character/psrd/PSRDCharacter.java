/**
 * PSRDCharacter.java
 * 
 * Copyright � 2012 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 29, 2012
 */
package com.westpalmetto.gaming.character.psrd;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticDeity;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticDeityLoader;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.loader.SimpleOverridableAbilityProviderLoader;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;
import com.westpalmetto.gaming.character.psrd.model.staticobject.wrapper.TraitWrapper;
import com.westpalmetto.gaming.character.util.gson.GsonUtils;

/**
 * Character is the mediator object for the character's data for PSRD
 * characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class PSRDCharacter extends Character3E{
    private static final Logger LOGGER = LoggerFactory.getLogger(PSRDCharacter.class);

    // Special key to flag if the character has Rogue Finesse Training
    public static final String NAME_ATTACK_FINESSETRAINING = "Finesse Training";

    // Setup Skills
    private NamedObjectSet<DD3ESkill> skills;

    /**
     * Creates a new PSRD character.
     */
    public PSRDCharacter(){
        this(new PSRDCharacterModel(ResourceFactory.Ruleset.PSRD.getRoot()));
    }

    /**
     * Creates a new PSRD character.
     * 
     * @param root The resource root
     */
    public PSRDCharacter(File root){
        this(new PSRDCharacterModel(root));
    }

    /**
     * Creates a new PSRDCharacter from the given character model.
     * 
     * @param model The character model.
     */
    public PSRDCharacter(PSRDCharacterModel model){
        super(model);

        skills = new NamedObjectSet<DD3ESkill>();
        skills.add(new DD3ESkill("Acrobatics", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Appraise", AttributeName.INT, false));
        skills.add(new DD3ESkill("Bluff", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Climb", AttributeName.STR, false));
        skills.add(new DD3ESkill("Craft", AttributeName.INT, false));
        skills.add(new DD3ESkill("Diplomacy", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Disable Device", AttributeName.DEX, true));
        skills.add(new DD3ESkill("Disguise", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Escape Artist", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Fly", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Handle Animal", AttributeName.CHA, true));
        skills.add(new DD3ESkill("Heal", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Intimidate", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Knowledge (arcana)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (dungeoneering)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (engineering)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (geography)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (history)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (local)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (nature)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (nobility)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (planes)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Knowledge (religion)", AttributeName.INT, true));
        skills.add(new DD3ESkill("Linguistics", AttributeName.INT, true));
        skills.add(new DD3ESkill("Perception", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Perform", AttributeName.CHA, false));
        skills.add(new DD3ESkill("Profession", AttributeName.WIS, true));
        skills.add(new DD3ESkill("Ride", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Sense Motive", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Sleight of Hand", AttributeName.DEX, true));
        skills.add(new DD3ESkill("Spellcraft", AttributeName.INT, true));
        skills.add(new DD3ESkill("Stealth", AttributeName.DEX, false));
        skills.add(new DD3ESkill("Survival", AttributeName.WIS, false));
        skills.add(new DD3ESkill("Swim", AttributeName.STR, false));
        skills.add(new DD3ESkill("Use Magic Device", AttributeName.CHA, true));
    }

    /**
     * @param json The JSON from which to initialize this character.
     * @param root The resource root
     */
    public PSRDCharacter(JsonObject json, File root){
        this(root);
        initialize(json);
    }

    @Override
    public void initialize(JsonObject json){
        setData(GsonUtils.readModel(json, PSRDCharacterModel.class));
    }

    public PSRDCharacterModel getPsrdData(){
        return (PSRDCharacterModel) getData();
    }

    @Override
    public int getHitPoints(){
        long start = System.currentTimeMillis();

        int hp = super.getHitPoints() + getPsrdData().getFavoredClassBonuses().getHitPoints();

        LOGGER.trace("getHitPoints: {}ms", System.currentTimeMillis() - start);
        return hp;
    }

    @Override
    protected NamedObjectSet<DD3ESkill> getDefaultSkills(){
        return skills;
    }

    @Override
    public int getEarnedSkillRanks(){
        return super.getEarnedSkillRanks() + getPsrdData().getFavoredClassBonuses().getSkillPoints();
    }

    @Override
    protected Collection<BonusProvider> getFixedBonusProviders(){
        Collection<BonusProvider> providers = super.getFixedBonusProviders();
        providers.add(getPsrdData().getTraits());
        return providers;
    }

    @Override
    protected Collection<BonusProvider> getBonusProviders(){
        Collection<BonusProvider> providers = super.getBonusProviders();

        providers.add(getDeificBoons());
        providers.add(getSkillUnlocks(providers));
        providers.add(getBonusTraits(providers));

        return providers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.westaplmetto.gaming.character.Character#getSkillBonus(com.
     * westaplmetto. gaming.character.model.Skill)
     */
    @Override
    public int getSkillBonus(DD3ESkill skill){
        return super.getSkillBonus(skill) + getSkillClassBonus(skill);
    }

    /**
     * Calculates the class bonus for a given skill (=3 if classSkill &&
     * ranks>0).
     * 
     * @param skill The skill
     * @return The appropriate class bonus
     */
    public int getSkillClassBonus(DD3ESkill skill){
        int bonus = 0;
        if (skill.isClassSkill() && (skill.getRanks() > 0)) {
            bonus = 3;
        }
        return bonus;
    }

    public int getModifiedCheckPenalty(Armor armor){
        // Find any personal modifier
        int bonus = 0;
        if (armor.getData().getProficiency().isArmor()) {
            bonus = getArmorCheckBonusArmor();
        } else {
            bonus = getArmorCheckBonusShield();
        }

        // Apply to existing armor penalty
        return armor.getModifiedCheckPenalty(bonus);
    }

    public BonusProviderSet<StaticSpecialAbility> getDeificBoons(){
        BonusProviderSet<StaticSpecialAbility> boons = new BonusProviderSet<StaticSpecialAbility>();
        if (hasDeificBoons()) {
            StaticDeity deity = StaticDeityLoader.getInstance(getResourceRoot()).getData(getData().getDeity());
            boons = deity.getBoons();
        }

        return boons;
    }

    public boolean hasDeificBoons(){
        return getFeats().containsKey(Character3E.NAME_OTHER_DEIFICOBEDIENCE);
    }

    public BonusProviderSet<SimpleOverridableAbilityProvider> getSkillUnlocks(){
        return getSkillUnlocks(getBonusProviders());
    }

    protected BonusProviderSet<SimpleOverridableAbilityProvider> getSkillUnlocks(Collection<BonusProvider> providers){
        BonusProviderSet<SimpleOverridableAbilityProvider> unlocks =
                new BonusProviderSet<SimpleOverridableAbilityProvider>();
        SimpleOverridableAbilityProviderLoader loader =
                SimpleOverridableAbilityProviderLoader.getInstance("unlock", getResourceRoot());

        // Collect modifiers
        Collection<Modifier> modifiers = new HashSet<>();
        for (BonusProvider provider : providers) {
            if (provider != null) {
                modifiers.addAll(provider.getModifiers());
            }
        }

        // Find the bonuses
        for (BonusProvider provider : providers) {
            if (provider != null) {
                Collection<ModifiableBonus> unlockBonuses = provider.getBonuses(BonusType.SKILL_UNLOCK, modifiers);
                if (CollectionUtils.isNotEmpty(unlockBonuses)) {
                    for (ModifiableBonus bonus : unlockBonuses) {
                        for (String key : bonus.getItemKeys()) {
                            SimpleOverridableAbilityProvider unlock = loader.getData(key);
                            if (unlock != null) {
                                unlocks.add(unlock);
                            }
                        }
                    }
                }
            }
        }

        return unlocks;
    }

    @Override
    protected int getWeaponDamageBonus(Weapon weapon){
        return super.getWeaponDamageBonus(weapon) + getFinesseTrainingBonus(weapon);
    }

    /**
     * Gets the character's finesse training bonus for the given weapon, if
     * applicable.
     * 
     * @param weapon The weapon
     * @return The applicable Finesse Training bonus
     */
    private int getFinesseTrainingBonus(Weapon weapon){
        int bonus = 0;

        // Only bother to check for finessable weapons
        if (weapon.isWeaponFinesse()) {
            // Find weapon training bonuses
            Collection<UnmodifiableBonus> finesseBonuses =
                    getCalculatedBonuses(BonusType.OTHER, NAME_ATTACK_FINESSETRAINING);

            // See if the weapon type is in the bonuses
            for (UnmodifiableBonus finesse : finesseBonuses) {
                for (String key : finesse.getItemKeys()) {
                    if (StringUtils.containsIgnoreCase(weapon.getName(), key)) {
                        // Add the Dex modifier
                        bonus = getAttributeModifier(getAttributeScore(AttributeName.DEX));

                        // Remove the strength modifier ("instead of")
                        bonus -= getAttributeModifier(getAttributeScore(AttributeName.STR));
                        break;
                    }
                }

                if (bonus != 0) {
                    break;
                }
            }
        }

        return bonus;
    }

    /**
     * @return The total number of traits the character has earned.
     */
    public int getEarnedTraits(){
        // You start with two.
        int traits = 2;

        // Get any bonus traits
        traits += getTotalSpecialBonuses(BonusType.TRAIT_COUNT);

        return traits;
    }

    /**
     * @return The total number of traits the character has assigned.
     */
    public int getUsedTraits(){
        return getTraits().size();
    }

    public BonusProviderSet<TraitWrapper> getTraits(){
        BonusProviderSet<TraitWrapper> traits = new BonusProviderSet<TraitWrapper>();
        traits.addAll(getPsrdData().getTraits());
        traits.addAll(getBonusTraits(getFixedBonusProviders()));
        return traits;
    }

    private BonusProviderSet<TraitWrapper> getBonusTraits(Collection<BonusProvider> providers){
        // Collect all of the bonus trait bonuses
        Collection<Bonus> traitBonuses = collectBonuses(BonusType.TRAIT, providers, null);

        // Transform the bonuses into trait objects
        BonusProviderSet<TraitWrapper> traits = new BonusProviderSet<TraitWrapper>();
        for (Bonus bonus : traitBonuses) {
            TraitWrapper trait = null;
            Set<String> keys = new HashSet<String>(bonus.getItemKeys());

            // Validate that keys exist
            if (CollectionUtils.isEmpty(keys)) {
                // TODO: Exceptions
                throw new RuntimeException("Item keys are empty within: " + traitBonuses);
            }

            // Cycle through each of the item keys looking for a valid one
            for (String id : keys) {
                if (TraitWrapper.isValid(id, getResourceRoot())) {
                    trait = new TraitWrapper(id);
                    break;
                }
            }

            // If one wasn't found, there is a problem
            if (trait == null) {
                // TODO: Exceptions
                throw new RuntimeException("Could not find trait for: " + bonus);
            } else {
                // Persist any additional item keys
                keys.remove(trait.getId());
                trait.setItemKeys(keys);

                // Save feat
                traits.add(trait);
            }
        }

        return traits;
    }
}
