package com.westpalmetto.util.resources.converter;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class StringConverter implements StreamConverter<String> {
	private static final StringConverter instance = new StringConverter();
	
	private StringConverter(){
	}
	
	public static StringConverter getInstance(){
		return instance;
	}
	
	@Override
	public String convert(InputStream in) throws ConvertException {
		try {
			return IOUtils.toString(in);
		} catch (IOException e) {
			throw new ConvertException("Error reading stream to string.", e);
		}
	}
}
