package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Equipment;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;

public class AbstractEquipmentTest {
	private static final Collection<Modifier> MODIFIERS_EMPTY = new HashSet<Modifier>();

	@Before
	public void setUp() {
	}

	@Test
	public void testItemKeyPropigation() {
		Equipment ring = new Equipment("Ring of Energy Resistance, Minor");
		ring.getItemKeys().add("Acid");
		
		Assert.assertTrue(ring.getBonuses(MODIFIERS_EMPTY).iterator().next().getItemKeys().contains("Acid"));
	}

	@Test
	public void testSimilarAbilities() {
		Equipment ring = new Equipment("Ring of Energy Resistance, Minor");
		ring.getItemKeys().add("Acid");
		Armor shield = new Armor("Shield, heavy steel");
		StaticSpecialAbility resistElectric = new StaticSpecialAbility();
		resistElectric.setName("Energy Resistance");
		resistElectric.getItemKeys().add("Electricity");
		shield.getStaticAbilities().add(resistElectric);
		shield.setAbilities(shield.getStaticAbilities());
		
		EquipmentSet set = new EquipmentSet();
		set.add(ring);
		set.add(shield);
		
		Collection<ModifiableBonus> actual = set.getBonuses(MODIFIERS_EMPTY);
		Assert.assertEquals(2, actual.size());
		for(ModifiableBonus bonus : actual){
			if(bonus.getItemKeys().contains("Acid")){
				Assert.assertTrue("Found both: " + bonus, !bonus.getItemKeys().contains("Electricity"));
			} else if(bonus.getItemKeys().contains("Electricity")){
				Assert.assertTrue("Found both: " + bonus, !bonus.getItemKeys().contains("Acid"));
			}else{
				Assert.fail("Found neither: " + bonus);
			}
		}
	}
}
