package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.JsonDataLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticDeity;

public class StaticDeityLoader extends JsonDataLoader3E<StaticDeity> {
	private static final Map<File,StaticDeityLoader> INSTANCES = new HashMap<>();
	
	public static StaticDeityLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticDeityLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticDeityLoader(File root) {
		super("deity", StaticDeity.class, root);
	}

	@Override
	public StaticDeity getClonedObject(StaticDeity original) {
		return new StaticDeity(original);
	}
}
