package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticWeapon;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.StaticDeityLoader;

public class StaticWeaponLoader extends OverridableStaticObjectLoader3E<StaticWeapon> {
	private static final Map<File,StaticWeaponLoader> INSTANCES = new HashMap<>();
	
	public static StaticWeaponLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticWeaponLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticWeaponLoader(File root) {
		super("weapon", StaticWeapon.class, root);
	}
	
	@Override
	public StaticWeapon getClonedObject(StaticWeapon original) {
		return new StaticWeapon(original);
	}
}