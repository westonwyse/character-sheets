package com.westpalmetto.gaming.character.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.util.loading.JsonDataLoader;

public class StaticSpecialAbilityLoader extends JsonDataLoader<StaticSpecialAbility> {
	private static final Map<Pair<String, File>, StaticSpecialAbilityLoader> INSTANCES = new HashMap<>();

	public static StaticSpecialAbilityLoader getInstance(String typeId, File root) {
		ImmutablePair<String, File> key = new ImmutablePair<>(typeId, root);

		// Create the instance if it does not exist
		if (!INSTANCES.containsKey(key)) {
			INSTANCES.put(key, new StaticSpecialAbilityLoader(typeId, root));
		}

		// Return the instance
		return INSTANCES.get(key);
	}

	private StaticSpecialAbilityLoader(String typeId, File root) {
		super(typeId, StaticSpecialAbility.class, root);
	}

	@Override
	public StaticSpecialAbility getClonedObject(StaticSpecialAbility original) {
		return new StaticSpecialAbility(original);
	}
}