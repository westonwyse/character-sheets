/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;

/**
 * Contains the character-specific details of a trick known by an animal.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class AnimalTrick extends SpecialAbility3E{
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public AnimalTrick(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	@Override
	public String getLoaderId() {
		return "trick";
	}
}
