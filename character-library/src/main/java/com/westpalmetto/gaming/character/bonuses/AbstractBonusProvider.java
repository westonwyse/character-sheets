/**
 * AbstractBonusProvider.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;
import java.util.HashSet;

import com.westpalmetto.gaming.character.model.bonuses.AbstractModifiableObject;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

/**
 * Abstract implementation of the methods of ProvidesBonus
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class AbstractBonusProvider extends AbstractModifiableObject
		implements BonusProvider {
	private Collection<ModifiableBonus> bonuses;
	private OptionalBonuses optional;
	private Collection<Modifier> modifiers;

	private Collection<ModifiableBonus> keyedBonuses;

	/**
	 * No-argument constructor.
	 */
	public AbstractBonusProvider(){
		this.bonuses = new HashSet<ModifiableBonus>();
	}
	
	/**
	 * Copy constructor.
	 * @param original
	 */
	public AbstractBonusProvider(AbstractBonusProvider original){
		super(original);
	    this.bonuses = new HashSet<>(original.bonuses);
	    if(original.optional != null){
	    	this.optional = new OptionalBonuses(original.optional);
	    }
	    setModifiers(new HashSet<Modifier>(original.getModifiers()));
	}
	
	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		return BonusHelper.filterByApplies(this, getBonuses(), modifiers);
	}

	@Override
	public Collection<ModifiableBonus> getBonuses(BonusType type, Collection<Modifier> modifiers) {
		return BonusHelper.filterBonusesByType(getBonuses(modifiers), type);
	}

	protected Collection<ModifiableBonus> getBonuses(){
		if(keyedBonuses == null){
			keyedBonuses = BonusHelper.nameBonuses(bonuses, getName());
			keyedBonuses = BonusHelper.keyBonuses(keyedBonuses, getItemKeys());
		}
		return keyedBonuses;
	}
	
	@Override
	public OptionalBonuses getOptional(Collection<Modifier> modifiers) {
		OptionalBonuses modified = new OptionalBonuses(getOptional());
		if(modified != null){
			modified.setBonuses(BonusHelper.filterByApplies(this, modified.getBonuses(), modifiers));
		}
		return modified;
	}

	protected OptionalBonuses getOptional(){
		if(optional == null){
			optional = new OptionalBonuses();
		}
		return optional;
	}

	@Override
	public Collection<Modifier> getModifiers() {
		if(modifiers == null){
			modifiers = new HashSet<Modifier>();
		}
		return modifiers;
	}

	/**
	 * @param modifiers the modifiers to set
	 */
	public void setModifiers(Collection<Modifier> modifiers) {
		this.modifiers = new HashSet<Modifier>(modifiers.size());
	    for(Modifier modifier : modifiers){
	    	if(modifier != null){
	    		this.modifiers.add(new Modifier(modifier));
	    	}
	    }
	}

	@Override
	public Collection<UnmodifiableBonus> modifyBonuses(Collection<Modifier> modifiers) {
		return modifyBonuses(getBonuses(modifiers), modifiers);
	}

	@Override
	public Collection<UnmodifiableBonus> modifyBonuses(BonusType type, Collection<Modifier> modifiers) {
		return modifyBonuses(getBonuses(type, modifiers), modifiers);
	}

	/**
	 * Modifies the given bonuses, with the given modifiers.
	 * 
	 * @param bonuses The bonuses.
	 * @param modifiers The modifiers.
	 * @return The modified bonuses.
	 */
	private Collection<UnmodifiableBonus> modifyBonuses(
			Collection<ModifiableBonus> bonuses,
			Collection<Modifier> modifiers) {
		Collection<UnmodifiableBonus> modified = new HashSet<UnmodifiableBonus>();
		
		for(ModifiableBonus bonus : bonuses){
			modified.add(BonusHelper.calculateBonus(bonus, modifiers));
		}
		
		return modified;
	}
}
