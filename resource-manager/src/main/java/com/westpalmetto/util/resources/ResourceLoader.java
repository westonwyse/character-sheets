package com.westpalmetto.util.resources;

import com.google.common.util.concurrent.ListenableFuture;

/**
 * Definition of methods for classes that load resources from some source.
 *
 * @param <T> The type of resource being loaded.
 */
public interface ResourceLoader<T> {
	T getResource();
	ListenableFuture<T> queueResource();
}
