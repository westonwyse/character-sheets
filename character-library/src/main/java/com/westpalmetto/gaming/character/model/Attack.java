package com.westpalmetto.gaming.character.model;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.util.resources.model.Model;

public class Attack extends Model{
	private Weapon weapon;
	private int bonus;
	
	/**
	 * @return the weapon
	 */
	public Weapon getWeapon(){
		return weapon;
	}
	/**
	 * @param weapon the weapon to set
	 */
	public void setWeapon(Weapon weapon){
		this.weapon = weapon;
	}
	/**
	 * @return the bonus
	 */
	public int getBonus(){
		return bonus;
	}
	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(int bonus){
		this.bonus = bonus;
	}
}
