package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticRace;

public class StaticRaceLoader extends OverridableStaticObjectLoader3E<StaticRace> {
	private static final Map<File,StaticRaceLoader> INSTANCES = new HashMap<>();
	
	public static StaticRaceLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticRaceLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticRaceLoader(File root) {
		super("race", StaticRace.class, root);
	}

	@Override
	public StaticRace getClonedObject(StaticRace original) {
		return new StaticRace(original);
	}
}
