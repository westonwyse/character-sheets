package com.westpalmetto.util.resources.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.westpalmetto.util.resources.converter.ConvertException;
import com.westpalmetto.util.resources.converter.StreamConverter;

public class FileResourceLoader<T> extends AbstractResourceLoader<T> {
	private final File file;
	private final StreamConverter<T> converter;

	public FileResourceLoader(File file, StreamConverter<T> converter, ListeningExecutorService executor) {
		super(executor);
		this.file = file;
		this.converter = converter;
	}

	@Override
	public T getResource() {
		T resource = null;
		try {
			InputStream in = new FileInputStream(getFile());
			resource = getConverter().convert(in);
		} catch (IOException e) {
			//TODO: Exceptions
			Throwables.propagate(e);
		} catch (ConvertException e) {
			//TODO: Exceptions
			Throwables.propagate(e);
		}
		
		return resource;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @return the converter
	 */
	public StreamConverter<T> getConverter() {
		return converter;
	}
}
