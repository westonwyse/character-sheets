package com.westpalmetto.gaming.character.sfsrd.model.staticobject.equipment;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticArmor;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
public class SFStaticArmor extends StaticArmor{
    private int eacBonus;
    private int kacBonus;
    private Integer speedAdjustment;
    private int upgradeSlots;

    public SFStaticArmor(SFStaticArmor original){
        super(original);
        this.eacBonus = original.getEacBonus();
        this.kacBonus = original.getKacBonus();
        this.speedAdjustment = original.getSpeedAdjustment();
        this.upgradeSlots = original.getUpgradeSlots();
    }
}
