/**
 * CharacterTrait.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision May 8, 2013
 */
package com.westpalmetto.gaming.character.psrd.bluerider.model.staticobject.wrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.SpecialAbility3E;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;

/**
 * Contains the character-specific details of a character point bonus.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class CharacterPointBonus extends SpecialAbility3E{
	private int quantity = 1;
	
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public CharacterPointBonus(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		if(quantity == 0){
			quantity = 1;
		}
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String getName(){
		String name = super.getName();
		
		if(getQuantity() > 1){
			name = name + " x" + getQuantity();
		}
		
		return name;
	}
	
	@Override
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers) {
		Collection<ModifiableBonus> bonuses;
		
		if(getQuantity() > 1){
			bonuses = new ArrayList<ModifiableBonus>();
			
			for(ModifiableBonus bonus : super.getBonuses(modifiers)){
				ModifiableBonus duplicate = new ModifiableBonus(bonus);
				duplicate.setValue(duplicate.getValue() * getQuantity());
				bonuses.add(duplicate);
			}
		} else {
			bonuses = super.getBonuses(modifiers);
		}
		
		return bonuses;
	}

	@Override
	public String getLoaderId() {
		return "characterPointBonus";
	}
}
