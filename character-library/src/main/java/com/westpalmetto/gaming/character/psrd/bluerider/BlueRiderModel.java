package com.westpalmetto.gaming.character.psrd.bluerider;

import java.io.File;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.psrd.bluerider.model.staticobject.wrapper.CharacterPointBonus;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;

/**
 * A model class for animal companions.
 */
public class BlueRiderModel extends PSRDCharacterModel {
	private BonusProviderSet<CharacterPointBonus> characterPoints;

	public BlueRiderModel(File resourceRoot) {
		super(resourceRoot);
	}

	/**
	 * @return the characterPoints
	 */
	public BonusProviderSet<CharacterPointBonus> getCharacterPoints() {
		if(characterPoints == null){
			setCharacterPoints(new BonusProviderSet<CharacterPointBonus>());
		}
		return characterPoints;
	}
	/**
	 * @param characterPoints the characterPoints to set
	 */
	public void setCharacterPoints(BonusProviderSet<CharacterPointBonus> characterPoints) {
		this.characterPoints = characterPoints;
	}
}
