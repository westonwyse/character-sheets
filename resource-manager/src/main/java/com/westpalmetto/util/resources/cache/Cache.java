package com.westpalmetto.util.resources.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A cache.
 *
 * @param <K> The type of the key
 * @param <V> The type of the value
 */
public class Cache <K, V> {
	private final Map<K, CacheEntry<K, V>> cache = new HashMap<>();
	private final long ttl;
	
	public Cache(long ttl){
		this.ttl = ttl;
	}
	
	public void put(K key, V value){
		cache.put(key, new CacheEntry<>(key, value));
	}
	
	public V get(K key){
		V value = null;
		
		if(contains(key)){
			value = cache.get(key).getValue();
		}
		
		return value;
	}
	
	public boolean contains(K key){
		return cache.containsKey(key);
	}
	
	/**
	 * Determines if the given key exists and is not past the TTl.
	 * 
	 * @param key The key
	 * @return TRUE if the key exists and is not past the TTL; FALSE if not found or if past TTL
	 */
	public boolean isCurrent(K key){
		boolean isCurrent = false;
		
		if(contains(key)){
			isCurrent = (cache.get(key).getLastUpdated() + ttl) < System.currentTimeMillis();
		}
		
		return isCurrent;
	}
	
	public void clear(){
		cache.clear();
	}

	public Set<K> keySet() {
		return cache.keySet();
	}
}
