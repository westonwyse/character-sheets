package com.westpalmetto.gaming.character.util;

import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;

public class AbilityHelper {

	/**
	 * Applies options to the given abilities
	 * 
	 * @param options   The options to apply
	 * @param abilities The abilities
	 * @return The abilities with options applied.
	 */
	public static BonusProviderSet<StaticSpecialAbility> applyOptions(Map<String, String> options,
			BonusProviderSet<StaticSpecialAbility> abilities) {
		BonusProviderSet<StaticSpecialAbility> optioned = new BonusProviderSet<>();

		if (MapUtils.isNotEmpty(options)) {
			// For each parent ability, look for & assign options
			for (StaticSpecialAbility ability : abilities) {
				StaticSpecialAbility newAbility = new StaticSpecialAbility(ability);

				if (options.containsKey(newAbility.getName())) {
					newAbility.getItemKeys().add(options.get(newAbility.getName()));
				}

				optioned.add(newAbility);
			}
		} else {
			optioned = abilities;
		}
		return optioned;
	}

}
