package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.dd3e.loader.OverridableStaticObjectLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.SpellSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSchool;

public class StaticSchoolLoader extends OverridableStaticObjectLoader3E<StaticSchool> {
	private static final Map<File, StaticSchoolLoader> INSTANCES = new HashMap<>();

	public static StaticSchoolLoader getInstance(File root) {
		if (!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticSchoolLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticSchoolLoader(File root) {
		super("school", StaticSchool.class, root);
	}

	@Override
	public StaticSchool getClonedObject(StaticSchool original) {
		return new StaticSchool(original);
	}

	@Override
	protected StaticSchool createNewObject(StaticSchool template, StaticSchool original) {
		StaticSchool newSchool = super.createNewObject(template, original);

		// Handle School spell overrides
		if ((template.getSpells() != null) && (template.getSpells().length > 0)) {
			for (int i = 0; i < template.getSpells().length; i++) {
				SpellSet spell = template.getSpells()[i];
				if ((spell != null) && !spell.isEmpty()) {
					newSchool.getSpells()[i] = spell;
				}
			}
		}

		// Handle variable-level spell overrides
		if (StringUtils.isNotBlank(template.getVariableSpell())) {
			newSchool.setVariableSpell(template.getVariableSpell());
		}

		return newSchool;
	}
}
