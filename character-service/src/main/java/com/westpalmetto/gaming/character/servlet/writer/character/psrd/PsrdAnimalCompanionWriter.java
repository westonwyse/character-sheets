package com.westpalmetto.gaming.character.servlet.writer.character.psrd;

import java.io.PrintWriter;

import com.westpalmetto.gaming.character.psrd.PSRDAnimalCompanion;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.model.PSRDAnimalCompanionModel;

/**
 * Extension of PsrdCharacterWriter to handle animal companions.
 */
public class PsrdAnimalCompanionWriter extends PsrdCharacterWriter {
	@Override
	protected void writeRightColumn(PrintWriter writer, PSRDCharacter character) {
		super.writeRightColumn(writer, character);
		writeNaturalAttacks(writer, character);
		writeTricks(writer, character);
	}

	@Override
	protected void writeBasics(PrintWriter writer, PSRDCharacter character){
		PSRDAnimalCompanion companion = (PSRDAnimalCompanion)character;
		PSRDAnimalCompanionModel data = companion.getPsrdAnimalCompanionData();
		
		//Open section
		writer.println("<div id='basics' class='companion'>");
		
		//Write data
		writeLabelValuePair(writer, "name", "Companion Name", data.getName());
		writeLabelValuePair(writer, "type", "Type", data.getAnimalType());
		writeLabelValuePair(writer, "level", "Level", data.getLevel());
		writeLabelValuePair(writer, "size", "Size", data.getSize().getData().getName());
		writeLabelValuePair(writer, "gender", "Gender", data.getGender());
		writeLabelValuePair(writer, "age", "Age", data.getAge());
		writeLabelValuePair(writer, "height", "Height", data.getHeight());
		writeLabelValuePair(writer, "weight", "Weight", data.getWeight());
		writeLabelValuePair(writer, "hair", "Hair", data.getHair());
		writeLabelValuePair(writer, "eyes", "Eyes", data.getEyes());
		
		//Close section
		writer.println("</div>");
	}
	
	@Override
	protected void writeLanguages(PrintWriter writer, PSRDCharacter character){
		//No languages
	}
	
	@Override
	protected void writeSpecialAbilities(PrintWriter writer, PSRDCharacter character){
		PSRDAnimalCompanion companion = (PSRDAnimalCompanion)character;
		PSRDAnimalCompanionModel data = companion.getPsrdAnimalCompanionData();
		
		super.writeSpecialAbilities(writer, companion);
		
		//Write special qualities
		writer.println("<h3>Animal Companion Abilities</h3>");
		writeSpecialAbilityList(data.getSpecialAbilities(), writer, companion.getModifiers());
		
		//Write special qualities
		writer.println("<h3>Special Qualities</h3>");
		writeSpecialAbilityList(data.getSpecialQualities(), writer, companion.getModifiers());
	}
	
	@Override
	protected void writeWeapons(PrintWriter writer, PSRDCharacter character){
		//No weapons
	}
	
	private void writeNaturalAttacks(PrintWriter writer, PSRDCharacter character) {
		super.writeWeapons(writer, character);
	}

	private void writeTricks(PrintWriter writer, PSRDCharacter character) {
		PSRDAnimalCompanion companion = (PSRDAnimalCompanion)character;
		PSRDAnimalCompanionModel data = companion.getPsrdAnimalCompanionData();
		
		//Write special qualities
		writer.println("<h2>Tricks</h2>");
		writer.println("<ul>");
		writeSpecialAbilityList(data.getTricks(), writer, character.getModifiers());
		writer.println("</ul>");
	}
}
