package com.westpalmetto.gaming.character.swade.model.staticobject;

import com.westpalmetto.gaming.character.model.staticobject.StaticObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class StaticGear implements StaticObject{
    private String name;
    private String source;
    private int cost;
    private int weight;
    private String damage;
    private String minStr;
    private String notes;

    StaticGear(String name){
        this.name = name;
    }

    public StaticGear(StaticGear original){
        this(original.getName());
        setSource(original.getSource());
        setCost(original.getCost());
        setWeight(original.getWeight());
        setDamage(original.getDamage());
        setMinStr(original.getMinStr());
        setNotes(original.getNotes());
    }
}
