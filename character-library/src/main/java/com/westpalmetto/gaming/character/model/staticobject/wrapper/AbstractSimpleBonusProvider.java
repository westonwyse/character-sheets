/**
 * AbstractBonusProvider.java
 * 
 * Copyright � 2014 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Dec 30, 2014
 */
package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.staticobject.StaticObject;

/**
 * Contains the character-specific details of a simple bonus provider (such as a
 * feat), handling generic concerns such as data loading & caching.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class AbstractSimpleBonusProvider<V extends SpecialAbility & StaticObject> extends
        AbstractBonusProviderWrapper<V> implements
        SpecialAbility{
    private Set<String> itemKeys;
    private transient Collection<ModifiableBonus> itemKeyBonuses;
    private transient Collection<Modifier> modifiers;
    private boolean requireItemKey;

    /**
     * The ID is vital for operation, so require it to be set.
     * 
     * @param name The ID
     * @param ruleset The ruleset name
     */
    public AbstractSimpleBonusProvider(String id, String ruleset){
        super(id, ruleset);
    }

    @Override
    public V getData(){
        V provider = super.getData();

        if (provider == null) {
            provider = createBlankProvider();
            provider.setName(getId());
            provider.setDescription("UNKNOWN ABILITY");
        }

        return provider;
    }

    protected abstract V createBlankProvider();

    @Override
    public String getName(){
        String name = getData().getName();

        // Verify the name exists
        if (StringUtils.isBlank(name)) {
            name = getId();
        }

        // Handle any item key or ID value
        if (CollectionUtils.isNotEmpty(getItemKeys())) {
            name = name + " (" + StringUtils.join(getItemKeys(), ", ") + ")";
        }

        return name;
    }

    @Override
    public String getSource(){
        return getData().getSource();
    }

    @Override
    public String getDescription(){
        return getData().getDescription();
    }

    @Override
    public void setDescription(String description){
        // TODO: Logging
        // WARN: Cannot set description
    }

    @Override
    public BigDecimal getCost(){
        return null;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        if ((itemKeyBonuses == null)
                || (!com.westpalmetto.gaming.character.util.CollectionHelper.isEqual(modifiers, this.modifiers))) {
            ArrayList<ModifiableBonus> bonuses = new ArrayList<ModifiableBonus>();
            for (ModifiableBonus bonus : super.getBonuses(modifiers)) {
                ModifiableBonus duplicate = new ModifiableBonus(bonus);

                // Add item key, if exists
                if (CollectionUtils.isNotEmpty(getItemKeys())) {
                    duplicate.getItemKeys().addAll(getItemKeys());
                }

                bonuses.add(duplicate);
            }
            itemKeyBonuses = bonuses;
            this.modifiers = modifiers;
        }
        return itemKeyBonuses;
    }

    @Override
    public Collection<Modifier> getModifiers(){
        return getData().getModifiers();
    }

    @Override
    public Set<String> getItemKeys(){
        if (itemKeys == null) {
            setItemKeys(new HashSet<String>());
        }
        return itemKeys;
    }

    @Override
    public void setItemKeys(Collection<String> itemKeys){
        // Store the value
        this.itemKeys = new HashSet<>(itemKeys);

        // Clear any updated bonuses
        this.itemKeyBonuses = null;
    }

    @Override
    public boolean isRequireItemKey(){
        return requireItemKey;
    }

    /**
     * @param requireItemKey the requiresItemKey to set
     */
    public void setRequireItemKey(boolean requireItemKey){
        this.requireItemKey = requireItemKey;
    }
}
