package com.westpalmetto.gaming.character.dd3e.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Attribute {
	/**
	 * An enum for valid attribute names
	 */
	public enum AttributeName{
		STR("Strength", "Str"),
		DEX("Dexterity", "Dex"),
		CON("Constitution", "Con"),
		INT("Intelligence", "Int"),
		WIS("Wisdom", "Wis"),
		CHA("Charisma", "Cha"),
		;
		
		private String name;
		private String abbreviation;
		private AttributeName(String name, String abbreviation){
			this.name = name;
			this.abbreviation = abbreviation;
		}
		/**
		 * @return The standard name for this attribute.
		 */
		public String getName(){
			return name;
		}
		/**
		 * @return The standard abbreviation for this attribute.
		 */
		public String getAbbreviation(){
			return abbreviation;
		}
	}

	private AttributeName name;
	private int score;
	
	public Attribute(AttributeName name, int score){
		this.name = name;
		this.score = score;
	}

	/**
	 * @return the name
	 */
	public AttributeName getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(AttributeName name) {
		this.name = name;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	@Override
	public String toString(){
		return ReflectionToStringBuilder.toString(this);
	}
	
	@Override
	public boolean equals(Object o){
		return EqualsBuilder.reflectionEquals(this, o);
	}
	
	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
