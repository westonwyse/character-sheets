package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.loader.equipment.StaticAmmunitionLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Class to contain stats for ammunition.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class Ammunition extends AbstractEquipment<StaticEquipment> {
	public Ammunition(String id) {
		super(id);

	}

	@Override
	protected DataLoader<StaticEquipment> createDataLoader() {
		return StaticAmmunitionLoader.getInstance(getRoot());
	}
}
