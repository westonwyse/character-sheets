/**
 * ArmorAttackPredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.predicate;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;


/**
 * Predicate to match items that are equipped.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class EquippedItemPredicate implements Predicate {
	private static EquippedItemPredicate instance;
	
	private EquippedItemPredicate(){
		//Making constructor private - singleton instance
	}
	
	public static EquippedItemPredicate getInstance(){
		if(instance == null){
			instance = new EquippedItemPredicate();
		}
		return instance;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof AbstractEquipment<?>){
			AbstractEquipment<?> item = (AbstractEquipment<?>)object;
			matches = StringUtils.equalsIgnoreCase(AbstractEquipment.LOCATION_EQUIPPED, item.getLocation());
		}
		
		return matches;
	}
}
