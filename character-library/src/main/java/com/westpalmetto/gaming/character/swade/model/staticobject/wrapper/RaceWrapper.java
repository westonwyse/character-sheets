package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.loader.SimpleOverridableAbilityProviderLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractBonusProviderWrapper;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

public class RaceWrapper extends AbstractBonusProviderWrapper<SimpleOverridableAbilityProvider>{

    public RaceWrapper(String id){
        super(id, ResourceFactory.Ruleset.SWADE.toString());
    }

    @Override
    protected DataLoader<SimpleOverridableAbilityProvider> createDataLoader(){
        return SimpleOverridableAbilityProviderLoader.getInstance("race", getRoot());
    }

    @Override
    public String getRuleset(){
        return ResourceFactory.Ruleset.SWADE.name();
    }
}
