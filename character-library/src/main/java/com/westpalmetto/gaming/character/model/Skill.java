package com.westpalmetto.gaming.character.model;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.util.resources.model.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Skill extends Model implements NamedObject{
    private String name;
    private String source;
    private int ranks;

    public Skill(DD3ESkill skill){
        this.name = skill.getName();
        this.source = skill.getSource();
        this.ranks = skill.getRanks();
    }
}
