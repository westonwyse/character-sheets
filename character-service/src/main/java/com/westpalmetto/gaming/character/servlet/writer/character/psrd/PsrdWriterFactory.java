package com.westpalmetto.gaming.character.servlet.writer.character.psrd;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.psrd.PFSCharacter;
import com.westpalmetto.gaming.character.psrd.PSRDAnimalCompanion;
import com.westpalmetto.gaming.character.psrd.PSRDCharacter;
import com.westpalmetto.gaming.character.psrd.bluerider.BlueRider;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;

public final class PsrdWriterFactory {
	private static final PsrdCharacterWriter WRITER_PSRD = new PsrdCharacterWriter();
	private static final PfsCharacterWriter WRITER_PFS = new PfsCharacterWriter();
	private static final PsrdAnimalCompanionWriter WRITER_ANIMAL_COMPANION = new PsrdAnimalCompanionWriter();
	private static final BlueRiderWriter WRITER_BLUE_RIDER = new  BlueRiderWriter();

	/**
	 * Returns the appropriate writer for the given character.
	 * 
	 * @param character The character
	 * @return The writer for the character
	 */
	public static CharacterHtmlWriter<? extends Character<?>> getWriter(PSRDCharacter character) {
		CharacterHtmlWriter<? extends Character<?>> writer = null;
		
		if (character instanceof PSRDAnimalCompanion) {
			writer = WRITER_ANIMAL_COMPANION;
		} else if (character instanceof PFSCharacter) {
			writer = WRITER_PFS;
		} else if (character instanceof BlueRider) {
			writer = WRITER_BLUE_RIDER;
		} else {
			writer = WRITER_PSRD;
		}
		
		return writer;
	}
}
