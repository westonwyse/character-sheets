/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.StaticSpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.loader.StaticSpecialAbilityLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Contains the character-specific details of a StaticSpecialAbility wrapper.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public abstract class AbstractCharacterSpecialAbility extends AbstractSimpleBonusProvider<StaticSpecialAbility>
		implements SpecialAbility {
	private static final Map<Pair<String, File>, DataLoader<StaticSpecialAbility>> LOADERS = new HashMap<>();

	/**
	 * Determines if the given ID is valid for the given loader.
	 * 
	 * @param loader The ID of the loader
	 * @param name   The ID of the ability
	 * @param root   The root directory of sources
	 * @return TRUE if the ID exists in the loader; FALSE if not.
	 */
	protected static boolean isValid(String loader, String id, File root) {
		ImmutablePair<String, File> key = new ImmutablePair<String, File>(loader, root);
		if (!LOADERS.containsKey(key)) {
			// First, create the missing loader
			createLoader(loader, root);
		}

		return LOADERS.get(key).contains(id);
	}

	private static void createLoader(String loaderId, File root) {
		ImmutablePair<String, File> key = new ImmutablePair<String, File>(loaderId, root);
		if (!LOADERS.containsKey(key)) {
			LOADERS.put(key, StaticSpecialAbilityLoader.getInstance(loaderId, root));
		}
	}

	/**
	 * @param loader The ID of the loader
	 * @param root   The root directory of sources
	 * @return All known IDs for the loader
	 */
	protected static Collection<String> getKnownKeys(String loader, File root) {
		ImmutablePair<String, File> key = new ImmutablePair<String, File>(loader, root);
		if (!LOADERS.containsKey(key)) {
			createLoader(loader, root);
		}

		return LOADERS.get(key).getCache().keySet();
	}

	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param id      The ID of the ability
	 * @param ruleset The ruleset name
	 */
	public AbstractCharacterSpecialAbility(String id, String ruleset) {
		super(id, ruleset);
	}

	@Override
	protected DataLoader<StaticSpecialAbility> createDataLoader() {
		String loaderId = getLoaderId();
		ImmutablePair<String, File> key = new ImmutablePair<String, File>(loaderId, getRoot());
		createLoader(loaderId, getRoot());
		return LOADERS.get(key);
	}

	@Override
	protected StaticSpecialAbility createBlankProvider() {
		return new StaticSpecialAbility();
	}

	/**
	 * @return the loader ID
	 */
	public abstract String getLoaderId();
}
