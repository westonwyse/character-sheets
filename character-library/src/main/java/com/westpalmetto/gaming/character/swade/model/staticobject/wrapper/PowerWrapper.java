package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import com.westpalmetto.gaming.character.ResourceFactory.Ruleset;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractStaticObjectWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticPower;
import com.westpalmetto.gaming.character.swade.model.staticobject.loader.StaticPowerLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PowerWrapper extends AbstractStaticObjectWrapper<StaticPower>{
    private String trapping;

    public PowerWrapper(String id){
        super(id, Ruleset.SWADE.name());
    }

    @Override
    protected DataLoader<StaticPower> createDataLoader(){
        return StaticPowerLoader.getInstance(getRoot());
    }
}
