package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment;

import java.io.File;

/**
 * Model to define enhancements to standard weapons for the offensive
 * capabilities of armor items.
 */
public class OffensiveArmor extends Weapon {
	private transient Armor parent;
	
	public OffensiveArmor(String id) {
		super(id);
	}

	/**
	 * @return the parent
	 */
	public Armor getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Armor parent) {
		this.parent = parent;
	}
}
