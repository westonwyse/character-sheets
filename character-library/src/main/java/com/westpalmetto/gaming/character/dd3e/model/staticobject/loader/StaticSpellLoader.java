package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.JsonDataLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSpell;

public class StaticSpellLoader extends JsonDataLoader3E<StaticSpell> {
	private static final Map<File,StaticSpellLoader> INSTANCES = new HashMap<>();
	
	public static StaticSpellLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticSpellLoader(root));
		}
		return INSTANCES.get(root);
	}

	private StaticSpellLoader(File root) {
		super("spell", StaticSpell.class, root);
	}
	
	@Override
	public StaticSpell getClonedObject(StaticSpell original) {
		return new StaticSpell(original);
	}
}
