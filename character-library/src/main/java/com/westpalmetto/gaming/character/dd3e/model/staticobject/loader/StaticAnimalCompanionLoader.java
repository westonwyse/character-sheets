package com.westpalmetto.gaming.character.dd3e.model.staticobject.loader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.westpalmetto.gaming.character.dd3e.loader.JsonDataLoader3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticAnimalCompanion;

public class StaticAnimalCompanionLoader extends JsonDataLoader3E<StaticAnimalCompanion> {
	private static final Map<File, StaticAnimalCompanionLoader> INSTANCES = new HashMap<>();
	
	public static StaticAnimalCompanionLoader getInstance(File root){
		if(!INSTANCES.containsKey(root)) {
			INSTANCES.put(root, new StaticAnimalCompanionLoader(root));
		}
		return INSTANCES.get(root);
	}
	
	private StaticAnimalCompanionLoader(File root) {
		super("animalCompanion", StaticAnimalCompanion.class, root);
	}

	@Override
	public StaticAnimalCompanion getClonedObject(StaticAnimalCompanion original) {
		return new StaticAnimalCompanion(original);
	}
}
