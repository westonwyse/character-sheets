/**
 * GsonTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.util.gson;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;
import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.Alignment;
import com.westpalmetto.gaming.character.dd3e.model.Alignment.GoodEvilAxis;
import com.westpalmetto.gaming.character.dd3e.model.Alignment.LawChaosAxis;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.RaceWrapper;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.psrd.model.PSRDCharacterModel;

/**
 * Tests of Gson functionality
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class GsonUtilTest {
	@Test
	public void testGson(){
		//Test to JSON
		DD3EModel expected = new PSRDCharacterModel(ResourceFactory.Ruleset.PSRD.getRoot());
		expected.setName("Zarkuzet Menellon");
		expected.setAlignment(new Alignment(LawChaosAxis.CHAOTIC, GoodEvilAxis.GOOD));
		expected.setRace(new RaceWrapper("gnome"));
		Gson gson = GsonUtils.getGsonInstance();
		String json = gson.toJson(expected);
		
		//Test from JSON
		DD3EModel actual = gson.fromJson(json, PSRDCharacterModel.class);
		Assert.assertEquals("Models do not match.", expected, actual);
	}

}
