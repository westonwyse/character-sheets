/**
 * CharacterFeat.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Apr 15, 2013
 */
package com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper;

import java.io.File;

import com.westpalmetto.gaming.character.ResourceFactory;
import com.westpalmetto.gaming.character.collection.BonusProviderSet;
import com.westpalmetto.gaming.character.model.AbilityProvider;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.staticobject.SimpleOverridableAbilityProvider;
import com.westpalmetto.gaming.character.model.staticobject.loader.SimpleOverridableAbilityProviderLoader;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractSimpleBonusProvider;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

/**
 * Contains the character-specific details of a class-specific talent (rogue
 * talent, paladin mercy, etc.).
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class TalentWrapper extends AbstractSimpleBonusProvider<SimpleOverridableAbilityProvider>
		implements SpecialAbility, AbilityProvider {
	private String className;
	
	/**
	 * The ID is vital for operation, so require it to be set.
	 * 
	 * @param name The ID
	 */
	public TalentWrapper(String id) {
		super(id, ResourceFactory.Ruleset.PSRD.toString());
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	
	@Override
	protected DataLoader<SimpleOverridableAbilityProvider> createDataLoader() {
		return SimpleOverridableAbilityProviderLoader.getInstance("talent", getRoot());
	}
	
	@Override
	public String getKey(){
		return getClassName() + "-" + getId();
	}

	@Override
	protected SimpleOverridableAbilityProvider createBlankProvider() {
		return new SimpleOverridableAbilityProvider();
	}

	@Override
	public BonusProviderSet<? extends SpecialAbility> getAbilities() {
		return getData().getAbilities();
	}
}
