package com.westpalmetto.gaming.character.model.staticobject.wrapper;

import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.FamiliarWrapper;
import com.westpalmetto.gaming.character.util.CollectionHelper;

/**
 * Tests of FamiliarWrapper.
 */
public class FamiliarWrapperTest {
	private FamiliarWrapper familiar;
	
	@Before
	public void setUp() {
		familiar = new FamiliarWrapper("Raven");
	}

	@Test
	public void testGetName() {
		Assert.assertFalse(familiar.getName().contains("("));
	}

	@Test
	public void testItemKeys() {
		Set<String> keys = familiar.getData().getAbilities().get("Alertness (Ex)").getItemKeys();
		Assert.assertEquals("Too many keys found: " + keys, 1, keys.size());
		Assert.assertEquals("Key not as expected.", "Raven (Familiar)", CollectionHelper.get(keys, 0));
	}
}
