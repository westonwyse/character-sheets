package com.westpalmetto.util.resources.converter;


/**
 * Exception thrown when an InputStream cannot be converted to a specified
 * type.
 */
public class ConvertException extends Exception {
	private static final long serialVersionUID = 1L;

	public ConvertException(String message, Throwable cause) {
		super(message, cause);
	}
}
