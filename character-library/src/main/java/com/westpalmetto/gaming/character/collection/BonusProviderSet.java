/**
 * BonusProviderSet.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.collection;

import java.util.Collection;
import java.util.HashSet;

import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.bonuses.BonusProvider;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

import lombok.Getter;
import lombok.Setter;

/**
 * Set implementation for objects that implement BonusProvider.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class BonusProviderSet<V extends BonusProvider> extends NamedObjectSet<V> implements BonusProvider{
    private String name;
    private String source;

    public BonusProviderSet(){}

    public BonusProviderSet(Collection<V> original){
        super(original);
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = new HashSet<ModifiableBonus>();

        // Cycle through contained bonuses
        for (V provider : this) {
            Collection<ModifiableBonus> providerBonuses = provider.getBonuses(modifiers);
            providerBonuses = BonusHelper.nameBonuses(providerBonuses, provider.getName());
            bonuses.addAll(providerBonuses);
        }

        return bonuses;
    }

    @Override
    public Collection<ModifiableBonus> getBonuses(BonusType type, Collection<Modifier> modifiers){
        Collection<ModifiableBonus> bonuses = new HashSet<ModifiableBonus>();

        // Cycle through contained bonuses
        for (V provider : this) {
            bonuses.addAll(provider.getBonuses(type, modifiers));
        }

        return bonuses;
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(modifiers), modifiers);
    }

    @Override
    public Collection<UnmodifiableBonus> modifyBonuses(BonusType type, Collection<Modifier> modifiers){
        return BonusHelper.calculateBonuses(getBonuses(type, modifiers), modifiers);
    }

    @Override
    public Collection<Modifier> getModifiers(){
        Collection<Modifier> modifiers = new HashSet<Modifier>();

        // Cycle through contained bonuses
        for (V provider : this) {
            modifiers.addAll(provider.getModifiers());
        }

        return modifiers;
    }

    @Override
    public OptionalBonuses getOptional(Collection<Modifier> modifiers){
        // Optional attacks must be extracted from the individual bonus
        // providers.
        return null;
    }
}
