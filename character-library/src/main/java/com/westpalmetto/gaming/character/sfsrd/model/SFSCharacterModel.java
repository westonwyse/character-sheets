package com.westpalmetto.gaming.character.sfsrd.model;

import java.io.File;
import java.util.List;

import com.westpalmetto.gaming.character.dd3e.Advancement;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * A character class for PFS characters.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SFSCharacterModel extends SFCharacterModel {
	private int fame;
	private List<Faction> factions;

	public SFSCharacterModel(File resourceRoot) {
		super(resourceRoot);
	}

	@Override
	public Advancement getAdvancement() {
		return ADVANCEMENT;
	}

	/**
	 * The PFS advancement table.
	 */
	public static class SFSAdvancement implements Advancement {
		private static int[] SFS = new int[] { 0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54,
				57 };

		@Override
		public int getLevel(int experience) {
			int level;
			for (level = 0; level <= SFS.length; level++) {
				if (experience < SFS[level]) {
					break;
				}
			}
			return level;
		}

		@Override
		public int getNextLevel(int experience) {
			return SFS[getLevel(experience)];
		}

		@Override
		public int getExperience(int level) {
			return SFS[level - 1];
		}
	}

	private static final SFSAdvancement ADVANCEMENT = new SFSAdvancement();
}
