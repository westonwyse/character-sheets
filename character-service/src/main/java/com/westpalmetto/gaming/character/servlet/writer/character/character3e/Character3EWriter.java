/**
 * Character3EWriter.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 16, 2013
 */
package com.westpalmetto.gaming.character.servlet.writer.character.character3e;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.Character;
import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.bonuses.BonusHelper;
import com.westpalmetto.gaming.character.collection.EquipmentSet;
import com.westpalmetto.gaming.character.collection.NamedObjectSet;
import com.westpalmetto.gaming.character.dd3e.Character3E;
import com.westpalmetto.gaming.character.dd3e.Character3E.CarryingCapacity;
import com.westpalmetto.gaming.character.dd3e.model.Attribute;
import com.westpalmetto.gaming.character.dd3e.model.Attribute.AttributeName;
import com.westpalmetto.gaming.character.dd3e.model.DD3EModel;
import com.westpalmetto.gaming.character.dd3e.model.DD3ESkill;
import com.westpalmetto.gaming.character.dd3e.model.SpellSet;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticClass;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSpell;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSpell.SpellRange;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.Charge;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.equipment.StaticEquipment.Slot;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.ClassWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.RaceWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SchoolWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.SpellWrapper;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.AbstractEquipment;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Ammunition;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Armor;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Coins;
import com.westpalmetto.gaming.character.dd3e.model.staticobject.wrapper.equipment.Weapon;
import com.westpalmetto.gaming.character.model.AbilityProvider;
import com.westpalmetto.gaming.character.model.SpecialAbility;
import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.Modifier.ModifierType;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;
import com.westpalmetto.gaming.character.servlet.writer.character.CharacterHtmlWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * Handling of response writing for PSRD characters.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Slf4j
public class Character3EWriter<C extends Character3E> implements CharacterHtmlWriter<C>{
    private static final String BOX_EMPTY = "&#x25FB;";
    private static final String BOX_MARKED = "&#x25FC;";
    private static final String BOXES = StringUtils.repeat(BOX_EMPTY, 5) + " ";

    @Override
    public void writeHtml(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Write the character out
        writer.println("<html><head>");
        writer.println(
                "<link rel='stylesheet' href='/character-service/styles/character/character-psrd.css' type='text/css'>");
        writer.println("<title>" + character.getData().getName() + "</title>");
        writer.println("</head><body>");

        // Write character sections
        // Upper section
        writer.println("<div id='upperSection'>");
        writeBasics(writer, character);
        writer.println("</div>");

        // Middle Section
        writer.println("<table><tr>");
        writer.println("<td id='leftColumn'>");
        writeLeftColumn(writer, character);
        writer.println("</td>");

        writer.println("<td id='rightColumn'>");
        writeRightColumn(writer, character);
        writer.println("</td>");
        writer.println("</tr></table>");

        // Lower section
        writer.println("<div id='lowerSection'>");
        writeLowerSection(writer, character);
        writer.println("</div>");

        // Close file
        writer.println("</body></html>");
        log.trace("writeHtml: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLeftColumn(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        writer.println("<table><tr>");
        writer.println("<td id='leftColumn'>");
        writeAttributes(writer, character);
        writer.println("</td>");

        writer.println("<td id='rightColumn'>");
        writeHP(writer, character);
        writeInitiative(writer, character);
        writer.println("</td>");
        writer.println("</tr></table>");

        writeAC(writer, character);
        writeSaves(writer, character);
        writeCombat(writer, character);

        log.trace("writeLeftColumn: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeRightColumn(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        writeSpeed(writer, character);
        writeSkills(writer, character);
        writeLanguages(writer, character);
        writeOtherBonuses(writer, character);

        log.trace("writeRightColumn: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLowerSection(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Write special abilites
        writer.println("<div id='abilities'>");
        writer.println("<h2>Special Abilities</h2>");
        writeSpecialAbilities(writer, character);
        writer.println("</div>");
        ;
        writeSpells(writer, character);
        writeWeapons(writer, character);
        writeAmmunition(writer, character);
        writeChargedItems(writer, character);
        writeArmor(writer, character);
        writeEquipment(writer, character);
        writeNotes(writer, character);

        log.trace("writeLowerSection: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, String value){
        writeLabelValuePair(writer, className, label, value, "div", null);
    }

    protected void writeLabelValuePair(PrintWriter writer,
            String className,
            String label,
            String value,
            String container,
            String formatting){
        // Verify input
        label = formatBlankable(label);
        value = formatBlankable(value);
        if (StringUtils.isBlank(formatting)) {
            formatting = "";
        }

        writer.println("<" + container + " class='" + className + " labelValue' " + formatting + ">");
        writer.println("<span class='value'>" + value + "</span>");
        writer.println("<span class='label'>" + label + "</span>");
        writer.println("</" + container + ">");
    }

    protected void
            writeLabelValuePair(PrintWriter writer, String className, String label, String value, String container){
        // Verify input
        label = formatBlankable(label);
        value = formatBlankable(value);

        writer.println("<" + container + " class='" + className + " labelValue'>");
        writer.println("<span class='value'>" + value + "</span>");
        writer.println("<span class='label'>" + label + "</span>");
        writer.println("</" + container + ">");
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, int value){
        writeLabelValuePair(writer, className, label, Integer.toString(value));
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, BigDecimal value){
        writeLabelValuePair(writer, className, label, value.toString());
    }

    protected void writeLabelValuePair(PrintWriter writer, String className, String label, int value, String container){
        writeLabelValuePair(writer, className, label, Integer.toString(value), container);
    }

    protected void
            writeLabelValuePair(PrintWriter writer, String className, String label, BigDecimal value, String container){
        writeLabelValuePair(writer, className, label, value.toString(), container);
    }

    protected void writeBasics(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        DD3EModel data = character.getData();

        // Gather class names
        Iterator<ClassWrapper> classes = data.getClasses().iterator();
        StringBuilder classNames = new StringBuilder();
        while (classes.hasNext()) {
            ClassWrapper cls = classes.next();
            classNames.append(cls.getData().getName() + " " + cls.getLevels());
            if (classes.hasNext()) {
                classNames.append(" / ");
            }
        }

        // Open section
        writer.println("<div id='basics'>");

        // Write data
        writeLabelValuePair(writer, "name", "Character Name", data.getName());
        writeLabelValuePair(writer, "alignment", "Alignment", data.getAlignment().toString());
        writeLabelValuePair(writer, "class", "Class", classNames.toString());
        writeLabelValuePair(writer, "race", "Race", data.getRace().getData().getName());
        writeLabelValuePair(writer, "favoredClass", "Favored Class", data.getFavoredClass());
        writeLabelValuePair(writer, "deity", "Deity", data.getDeity());
        writeLabelValuePair(writer,
                "experience",
                "Experience",
                Character.FORMATTER_THOUSANDS.format(data.getExperience()));
        writeLabelValuePair(writer,
                "nextLevel",
                "Next Level",
                Character.FORMATTER_THOUSANDS
                        .format(data.getAdvancement().getNextLevel(data.getExperience().intValue())));
        writeLabelValuePair(writer, "size", "Size", character.getSize().getData().getName());
        writeLabelValuePair(writer, "gender", "Gender", data.getGender());
        writeLabelValuePair(writer, "age", "Age", data.getAge());
        writeLabelValuePair(writer, "height", "Height", data.getHeight());
        writeLabelValuePair(writer, "weight", "Weight", data.getWeight());
        writeLabelValuePair(writer, "hair", "Hair", data.getHair());
        writeLabelValuePair(writer, "eyes", "Eyes", data.getEyes());

        // Close section
        writer.println("</div>");

        log.trace("writeBasics: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeAttributes(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        DD3EModel data = character.getData();

        // Open section
        writer.println("<table id='attributes'>");

        writer.println("<tr>");
        writer.println("<th class='label'>Name</th>");
        writer.println("<th class='score'>Score</th>");
        writer.println("<th class='modifier'>Mod.</th>");
        writer.println("<th class='tempValue'>Temp. Score</th>");
        writer.println("<th class='tempModifier'>Temp. Mod.</th>");
        writer.println("</tr>");

        for (Attribute attribute : data.getAttributes()) {
            // Write name
            writer.println("<tr class='" + attribute.getName().getAbbreviation() + "'>");
            writer.println("<th class='label'>" + attribute.getName().getAbbreviation() + "</th>");
            writer.println("<td class='score'>" + character.getAttributeScore(attribute.getName()) + "</td>");
            writer.println("<td class='modifier'>"
                    + Character.FORMATTER_MODIFIER
                            .format(character.getAttributeModifier(character.getAttributeScore(attribute.getName())))
                    + "</td>");

            int totalScore = character.getAttributeTotalScore(attribute.getName());
            if (totalScore != character.getAttributeScore(attribute.getName())) {
                writer.println("<td class='tempValue'>" + totalScore + "</td>");
                writer.println("<td class='tempModifier'>"
                        + Character.FORMATTER_MODIFIER.format(character.getAttributeModifier(totalScore))
                        + "</td>");
            } else {
                writer.println("<td class='tempValue'>&nbsp;</td>");
                writer.println("<td class='tempModifier'>&nbsp;</td>");
            }
            writer.println("</tr>");
        }

        // Close section
        writer.println("</table>");

        // Validate increases
        writer.println("<ul>");
        writer.println("<li>Earned increases: " + character.getEarnedAttributeIncreases() + "</li>");
        writer.println("<li>Used increases: " + character.getUsedAttributeIncreases() + "</li>");
        writer.println("</ul>");

        log.trace("writeAttributes: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeSpeed(PrintWriter writer, C character){
        // Open section
        writer.println("<div id='speed'>");
        writer.println("<span class='header'>Speed</span>");

        // Write base speed
        writer.println("<div class='speedBlock baseSpeed'>");
        writer.println("<div class='speedData'>");
        writeLabelValuePair(writer, "base", "Base", character.getBaseSpeed());
        writeLabelValuePair(writer, "encumbered", "Enc.", character.getBaseSpeedEncumbered());
        writer.println("</div>");
        writer.println("<span class='speedType'>Base</span>");
        writer.println("</div>");

        // Write running speed
        writer.println("<div class='speedBlock runningSpeed'>");
        writer.println("<div class='speedData'>");
        writeLabelValuePair(writer, "base", "Base", character.getRunningSpeed());
        writeLabelValuePair(writer, "encumbered", "Enc.", character.getRunningSpeedEncumbered());
        writer.println("</div>");
        writer.println("<span class='speedType'>Running</span>");
        writer.println("</div>");

        // Write notes
        writer.println("<div class='speedBlock notes'>");
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SPEED);
        writeConditionals(writer, conditionals);
        writer.println("</div>");

        // Close section
        writer.println("</div>");
    }

    protected void writeSkills(PrintWriter writer, C character){
        DD3EModel data = character.getData();

        // Open section
        writer.println("<div id='skills'>");
        writer.println("<h2>Skills</h2>");

        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='classSkill'>&nbsp;</th>");
        writer.println("<th class='name'>Skill Name</th>");
        writer.println("<th class='bonus'>Total Bonus</th>");
        writer.println("<th class='ability'>Ability</th>");
        writer.println("<th class='abilityMod'>Ability Mod.</th>");
        writer.println("<th class='ranks'>Ranks</th>");
        writer.println("<th class='misc'>Misc.</th>");
        writer.println("</tr>");

        // Write skills
        int count = 1;
        NamedObjectSet<DD3ESkill> skills = data.getSkills();
        for (DD3ESkill skill : skills) {
            AttributeName attributeName = skill.getAttributeName();
            String classSkill = (skill.isClassSkill()) ? "classSkill" : "";
            String classSkillMarker = (skill.isClassSkill()) ? "&raquo;" : "";
            String trainedOnly = (skill.isTrainedOnly()) ? "trainedOnly" : "";
            String trainedOnlyMarker = (skill.isTrainedOnly()) ? " &Dagger;" : "";
            String totalBonus = (skill.isTrainedOnly() && (skill.getRanks() == 0)) ? "&mdash;"
                    : Character.FORMATTER_MODIFIER.format(character.getSkillBonus(skill));
            String marker = (count % 3 == 0) ? "marker" : "";
            count++;

            // Write name
            writer.println("<tr class='" + classSkill + " " + trainedOnly + " " + marker + "'>");
            writer.println("<td class='classSkill'>" + classSkillMarker + "</td>");
            writer.println("<td class='name'>" + skill.getName() + trainedOnlyMarker + "</td>");
            writer.println("<td class='bonus'>" + totalBonus + "</td>");
            writer.println("<td class='ability'>" + attributeName.getAbbreviation() + "</td>");
            writer.println("<td class='abilityMod'>" + character.getAttributeBonus(attributeName) + "</td>");
            writer.println("<td class='ranks'>" + skill.getRanks() + "</td>");
            writer.println("<td class='misc'>" + character.getSkillMiscModifiers(skill) + "</td>");
            writer.println("</tr>");
        }
        writer.println("</table>");
        writer.println("<div class='legend'>&raquo; - Class skill</div>");
        writer.println("<div class='legend'>&Dagger; - Trained only</div>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SKILL);
        writeConditionals(writer, conditionals);

        // Validate skill points
        writer.println("<ul>");
        writer.println("<li>Earned skill ranks: " + character.getEarnedSkillRanks() + "</li>");
        writer.println("<li>Used skill ranks: " + character.getUsedSkillRanks() + "</li>");
        writer.println("</ul>");

        // Close section
        writer.println("</div>");
    }

    protected void writeHP(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<div id='hp'>");

        // Write HP
        writer.println("<table class='hp'><tr>");
        writer.println("<th class='name'>HP</th>");
        writer.println("<td class='value'>" + character.getHitPoints() + "</td>");
        writer.println("</tr></table>");
        writer.println("<div class='wounds'>");
        writer.println("<span class='name'>Wounds</span>");
        writer.println("<span class='value'>&nbsp;</span>");
        writer.println("</div>");
        writer.println("<div class='nonlethal'>");
        writer.println("<span class='name'>Nonlethal Damage</span>");
        writer.println("<span class='value'>&nbsp;</span>");
        writer.println("</div>");

        // Close section
        writer.println("</div>");

        log.trace("writeHP: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeInitiative(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<table id='initiative'><tr>");

        // Write Initiative
        writeLabelValuePair(writer, "header", null, "Initiative", "th");
        writeLabelValuePair(writer, "total", "Total", character.getInitiativeBonus(), "td");
        writeLabelValuePair(writer, "dex", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getInitiativeMiscModifiers(), "td");

        // Close section
        writer.println("</tr></table>");

        log.trace("writeInitiative: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeAC(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<table id='ac'>");

        // Write AC
        writer.println("<tr class='ac'>");
        writeLabelValuePair(writer, "header", null, "AC", "th");
        writeLabelValuePair(writer, "total", "Total", character.getACBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "armor", "Armor", character.getACArmorBonus(), "td");
        writeLabelValuePair(writer, "shield", "Shield", character.getACShieldBonus(), "td");
        writeLabelValuePair(writer, "dex", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX, true), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "natural", "Natural Armor", character.getACNaturalArmorBonus(), "td");
        writeLabelValuePair(writer, "deflection", "Deflection", character.getACDeflectionBonus(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getACMiscBonus(), "td");
        writer.println("</tr>");

        // Write Touch
        writer.println("<tr id='touch'>");
        writeLabelValuePair(writer, "header", "Armor Class", "Touch", "th", "colspan='2'");
        writeLabelValuePair(writer, "total", "Total", character.getACTouchBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "dex", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX, true), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "deflection", "Deflection", character.getACDeflectionBonus(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getACTouchMiscBonus(), "td");
        writer.println("</tr>");

        // Write Flat-footed
        writer.println("<tr id='flatfooted'>");
        writeLabelValuePair(writer, "header", "Armor Class", "Flat-Footed", "th", "colspan='2'");
        writeLabelValuePair(writer, "total", "Total", character.getACFlatFootedBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "armor", "Armor", character.getACArmorBonus(), "td");
        writeLabelValuePair(writer, "shield", "Shield", character.getACShieldBonus(), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "natural", "Natural Armor", character.getACNaturalArmorBonus(), "td");
        writeLabelValuePair(writer, "deflection", "Deflection", character.getACDeflectionBonus(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getACFlatFootedMiscBonus(), "td");
        writer.println("</tr>");

        // Close section
        writer.println("</table>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.ARMOR_CLASS);
        writeConditionals(writer, conditionals);

        log.trace("writeAC: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeSaves(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        // Open section
        writer.println("<div id='saves'>");
        writer.println("<h2>Saving Throws</h2>");
        writer.println("<table>");

        // Write Headers
        writer.println("<tr>");
        writer.println("<th class='name header'>Saving Throw</th>");
        writer.println("<th class='total'>Total</th>");
        writer.println("<th class='base'>Base Save</th>");
        writer.println("<th class='ability'>Ability Mod.</th>");
        writer.println("<th class='misc'>Misc. Mod.</th>");
        writer.println("<th class='temp'>Temp. Mod.</th>");
        writer.println("</tr>");

        // Write Fortitude
        writer.println("<tr class='fortitude'>");
        writer.println("<td class='name header'>Fortitude</td>");
        writer.println("<td class='total'>" + Character.FORMATTER_MODIFIER.format(character.getFortitudeSaveTotal())
                + "</td>");
        writer.println(
                "<td class='base'>" + Character.FORMATTER_MODIFIER.format(character.getFortitudeBaseSave()) + "</td>");
        writer.println("<td class='ability'>"
                + Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.CON))
                + "</td>");
        writer.println(
                "<td class='misc'>" + Character.FORMATTER_MODIFIER.format(character.getFortitudeSaveMiscModifiers())
                        + "</td>");
        writer.println("<td class='temp'>&nbsp;</td>");
        writer.println("</tr>");

        // Write Reflex
        writer.println("<tr class='reflex'>");
        writer.println("<td class='name header'>Reflex</td>");
        writer.println(
                "<td class='total'>" + Character.FORMATTER_MODIFIER.format(character.getReflexSaveTotal()) + "</td>");
        writer.println(
                "<td class='base'>" + Character.FORMATTER_MODIFIER.format(character.getReflexBaseSave()) + "</td>");
        writer.println("<td class='ability'>"
                + Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.DEX))
                + "</td>");
        writer.println("<td class='misc'>" + Character.FORMATTER_MODIFIER.format(character.getReflexSaveMiscModifiers())
                + "</td>");
        writer.println("<td class='temp'>&nbsp;</td>");
        writer.println("</tr>");

        // Write Fortitude
        writer.println("<tr class='will'>");
        writer.println("<td class='name header'>Will</td>");
        writer.println(
                "<td class='total'>" + Character.FORMATTER_MODIFIER.format(character.getWillSaveTotal()) + "</td>");
        writer.println(
                "<td class='base'>" + Character.FORMATTER_MODIFIER.format(character.getWillBaseSave()) + "</td>");
        writer.println("<td class='ability'>"
                + Character.FORMATTER_MODIFIER.format(character.getAttributeBonus(AttributeName.WIS))
                + "</td>");
        writer.println("<td class='misc'>" + Character.FORMATTER_MODIFIER.format(character.getWillSaveMiscModifiers())
                + "</td>");
        writer.println("<td class='temp'>&nbsp;</td>");
        writer.println("</tr>");
        writer.println("</table>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.SAVING_THROW);
        writeConditionals(writer, conditionals);

        // Close section
        writer.println("</div>");

        log.trace("writeSaves: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeCombat(PrintWriter writer, C character){
        long start = System.currentTimeMillis();

        writer.println("<h2>Combat</h2>");
        writer.println("<table id='attacks'>");

        // Write Melee
        writer.println("<tr class='melee'>");
        writeLabelValuePair(writer, "header", null, "Melee", "th");
        writeLabelValuePair(writer,
                "total",
                "Total",
                Character.FORMATTER_MODIFIER.format(character.getMeleeAttackTotalBonus()),
                "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Str Bonus", character.getAttributeBonus(AttributeName.STR), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getMeleeAttackMiscBonus(), "td");
        writer.println("</tr>");

        // Write Weapon Finesse, if applicable
        if (character.hasWeaponFinesse()) {
            writer.println("<tr class='finesse'>");
            writeLabelValuePair(writer, "header", "Weapon Finesse", "Melee", "th");
            writeLabelValuePair(writer,
                    "total",
                    "Total",
                    Character.FORMATTER_MODIFIER.format(character.getWeaponFinesseTotalBonus()),
                    "td");
            writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
            writeLabelValuePair(writer, "attribute", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX), "td");
            writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
            writeLabelValuePair(writer, "misc", "Misc.", character.getMeleeAttackMiscBonus(), "td");
            writer.println("</tr>");
        }

        // Write Ranged
        writer.println("<tr class='ranged'>");
        writeLabelValuePair(writer, "header", null, "Ranged", "th");
        writeLabelValuePair(writer,
                "total",
                "Total",
                Character.FORMATTER_MODIFIER.format(character.getRangedAttackTotalBonus()),
                "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX), "td");
        writeLabelValuePair(writer, "size", "Size", character.getSizeModifier(), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getRangedAttackMiscBonus(), "td");
        writer.println("</tr>");

        // Close section
        writer.println("</table>");

        // Write Two-Weapon Fighting, if applicable
        if (character.hasTwoWeapons()) {
            writer.println("<table id='twoweapon'>");

            writer.println("<tr class='standard'>");
            writeLabelValuePair(writer, "header", "", "Two-Weapon Fighting", "th");
            writeLabelValuePair(writer,
                    "primary",
                    "Primary Hand",
                    Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackPrimaryBonus()),
                    "td");
            writeLabelValuePair(writer,
                    "offhand",
                    "Off Hand",
                    Character.FORMATTER_MODIFIER.format(character.getTwoWeaponAttackOffHandBonus()),
                    "td");
            writer.println("</tr>");

            writer.println("</table>");
        }

        // Open section
        writer.println("<table id='maneuver'>");

        // Write CMB
        writer.println("<tr class='cmb'>");
        writeLabelValuePair(writer, "header", null, "CMB", "th");
        writeLabelValuePair(writer,
                "total",
                "Total",
                Character.FORMATTER_MODIFIER.format(character.getCMBTotalBonus()),
                "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Str Bonus", character.getAttributeBonus(AttributeName.STR), "td");
        writeLabelValuePair(writer, "size", "Size", (0 - character.getSizeModifier()), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getCMBMiscBonus(), "td");
        writer.println("</tr>");

        // Write CMB/Weapon Finesse, if applicable
        if (character.hasWeaponFinesse()) {
            writer.println("<tr class='cmbFinesse'>");
            writeLabelValuePair(writer, "header", "Weapon Finesse", "CMB", "th");
            writeLabelValuePair(writer,
                    "total",
                    "Total",
                    Character.FORMATTER_MODIFIER.format(character.getCMBFinesseTotalBonus()),
                    "td");
            writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
            writeLabelValuePair(writer, "attribute", "Dex Bonus", character.getAttributeBonus(AttributeName.DEX), "td");
            writeLabelValuePair(writer, "size", "Size", (0 - character.getSizeModifier()), "td");
            writeLabelValuePair(writer, "misc", "Misc.", character.getCMBMiscBonus(), "td");
            writer.println("</tr>");
        }

        // Write CMD
        writer.println("<tr class='cmd'>");
        writeLabelValuePair(writer, "header", null, "CMD", "th");
        writeLabelValuePair(writer, "total", "Total", character.getCMDTotalBonus(), "td");
        writeLabelValuePair(writer, "ten", null, "= 10 +", "td");
        writeLabelValuePair(writer, "base", "Base Attack", character.getBaseAttack(), "td");
        writeLabelValuePair(writer, "attribute", "Str Bonus", character.getAttributeBonus(AttributeName.STR), "td");
        writeLabelValuePair(writer,
                "attribute",
                "Dex Bonus",
                character.getAttributeBonus(AttributeName.DEX, true),
                "td");
        writeLabelValuePair(writer, "size", "Size", (0 - character.getSizeModifier()), "td");
        writeLabelValuePair(writer, "misc", "Misc.", character.getCMDMiscBonus(), "td");
        writer.println("</tr>");
        writer.println("</table>");

        // Write conditional modifiers
        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.ATTACK);
        CollectionUtils.addAll(conditionals, character.getConditionalModifiers(BonusType.COMBAT_MANEUVER).iterator());
        CollectionUtils.addAll(conditionals, character.getConditionalModifiers(BonusType.COMBAT_DEFENSE).iterator());
        writeConditionals(writer, conditionals);

        log.trace("writeCombat: {}ms", System.currentTimeMillis() - start);
    }

    protected void writeLanguages(PrintWriter writer, C character){
        writer.println("<div id='languages'>");

        writer.println("<h2>Languages</h2>");

        writer.println("<ul>");

        for (String language : character.getData().getLanguages()) {
            writer.println("<li>" + language + "</li>");
        }

        writer.println("</ul>");
        writer.println("</div>");
    }

    protected void writeOtherBonuses(PrintWriter writer, C character){
        writer.println("<div id='otherBonuses'>");

        writer.println("<h2>Other Bonuses</h2>");

        Collection<UnmodifiableBonus> conditionals = character.getConditionalModifiers(BonusType.OTHER);
        writeConditionals(writer, conditionals);

        writer.println("</div>");
    }

    protected void writeSpecialAbilities(PrintWriter writer, C character){
        writeFeats(writer, character);
        writeRacialAbilities(writer, character);
        writeClassAbilities(writer, character);
        writeFamiliar(writer, character);
        writeOtherAbilities(writer, character);
        writePools(writer, character);
    }

    protected void writePools(PrintWriter writer, C character){
        // Write pools, if applicable
        Map<String, Integer> pools = character.getPools();
        if (!pools.isEmpty()) {
            for (Entry<String, Integer> pool : pools.entrySet()) {
                writer.println("<h3 class='pool'>" + pool.getKey() + "</h3>");
                writer.println("<div class='pool'>" + generateChargeBlocks(pool.getValue()) + "</td>");
            }
        }
    }

    protected void writeOtherAbilities(PrintWriter writer, C character){
        // Write other special abilities, if applicable
        Collection<SpecialAbility> abilities = new HashSet<SpecialAbility>();
        abilities.addAll(character.getData().getBoons());
        abilities.addAll(character.getData().getQualities());
        if (CollectionUtils.isNotEmpty(abilities)) {
            writer.println("<h3>Other Abilities</h3>");
            writeSpecialAbilityList(abilities, writer, character.getModifiers());
        }
    }

    protected void writeFamiliar(PrintWriter writer, C character){
        DD3EModel data = character.getData();
        Collection<Modifier> modifiers = character.getModifiers();

        // Check for familiar
        if (data.getFamiliar() != null) {
            writer.println("<h3>" + data.getFamiliar().getData().getName() + "</h3>");
            if (data.getFamiliar().getData().getSource() != null) {
                writer.println("<p>Source: " + data.getFamiliar().getData().getSource() + "</p>");
            }

            // Write familiar abilities
            writeSpecialAbilityList(data.getFamiliar().getData().getAbilities(), writer, modifiers);
        }
    }

    protected void writeClassAbilities(PrintWriter writer, C character){
        DD3EModel data = character.getData();
        Collection<Modifier> modifiers = character.getModifiers();

        // Write class abilities
        for (ClassWrapper clz : data.getClasses()) {
            Map<String, String> options = clz.getOptions();

            // Write header & Source
            writer.println("<h3>" + clz.getData().getName() + "</h3>");
            if (clz.getData().getSource() != null) {
                writer.println("<p>Source: " + clz.getData().getSource() + "</p>");
            }

            // Write standard class abilities
            writeSpecialAbilityList(clz.getData().getAbilities(), writer, modifiers, options);

            // Check for talents
            if (!clz.getTalents().isEmpty()) {
                if (StringUtils.isNotBlank(clz.getData().getTalentsName())) {
                    writer.println("<h3>" + clz.getData().getTalentsName() + "</h3>");
                }
                // Write standard class abilities
                writeSpecialAbilityList(clz.getTalents(), writer, modifiers, options);

                // Validate talent count
                if (StringUtils.isNotBlank(clz.getData().getTalentsName())) {
                    writer.println("<ul>");
                    writer.println("<li>Earned talents: " + character.getEarnedTalents(clz) + "</li>");
                    writer.println("<li>Used talents: " + character.getUsedTalents(clz) + "</li>");
                    writer.println("</ul>");
                }
            }

            // Check for schools
            if (!clz.getSchools().isEmpty()) {
                for (SchoolWrapper school : clz.getSchools()) {
                    writer.println("<h3>" + school.getName() + "</h3>");
                    if (school.getData().getSource() != null) {
                        writer.println("<p>Source: " + school.getData().getSource() + "</p>");
                    }

                    // Write school abilities
                    writeSpecialAbilityList(school.getData().getAbilities(), writer, modifiers, options);
                }
            }
        }
    }

    protected void writeRacialAbilities(PrintWriter writer, C character){
        // Write racial abilities
        RaceWrapper race = character.getData().getRace();
        if (race != null) {
            writer.println("<h3>" + race.getData().getName() + "</h3>");
            if (race.getData().getSource() != null) {
                writer.println("<p>Source: " + race.getData().getSource() + "</p>");
            }

            writeSpecialAbilityList(race.getData().getAbilities(), writer, character.getModifiers());
        }
    }

    protected void writeFeats(PrintWriter writer, C character){
        // Write feats
        writer.println("<h3>Feats</h3>");
        writeSpecialAbilityList(character.getFeats(), writer, character.getModifiers());

        // Validate feat count
        writer.println("<ul>");
        writer.println("<li>Earned feats: " + character.getEarnedFeats() + "</li>");
        writer.println("<li>Used feats: " + character.getUsedFeats() + "</li>");
        writer.println("</ul>");
    }

    protected void writeSpecialAbilityList(Collection<? extends SpecialAbility> abilities,
            PrintWriter writer,
            Collection<Modifier> modifiers){
        writeSpecialAbilityList(abilities, writer, modifiers, null);
    }

    protected void writeSpecialAbilityList(Collection<? extends SpecialAbility> abilities,
            PrintWriter writer,
            Collection<Modifier> modifiers,
            Map<String, String> options){
        writer.println("<ul>");
        Collection<SpecialAbility> allAbilities = new TreeSet<SpecialAbility>(gatherAbilities(abilities));
        for (SpecialAbility ability : allAbilities) {
            writeBonusItem(writer, ability, modifiers, options);
        }
        writer.println("</ul>");
    }

    private Collection<SpecialAbility> gatherAbilities(Collection<? extends SpecialAbility> abilities){
        Collection<SpecialAbility> gathered = new HashSet<SpecialAbility>();

        for (SpecialAbility ability : abilities) {
            gathered.add(ability);

            /*
             * Also deal with any nested abilities (wizard schools, sorcerer
             * bloodlines, etc.)
             */
            if (ability instanceof AbilityProvider) {
                gathered.addAll(gatherAbilities(((AbilityProvider) ability).getAbilities()));
            }
        }

        return gathered;
    }

    protected void writeBonusItem(PrintWriter writer,
            SpecialAbility ability,
            Collection<Modifier> modifiers,
            Map<String, String> options){
        if (BonusHelper.applies(ability, modifiers)) {
            if (ability.isDisplay()) {
                if (StringUtils.isNotBlank(ability.getDescription())) {
                    // Search for option
                    String title = ability.getName();
                    if ((options != null) && options.containsKey(ability.getName())) {
                        title = title + " - " + options.get(ability.getName());
                    }

                    writer.println("<li>");
                    writer.println("<span class='name'>" + title + "</span>");

                    if (ability.getSource() != null) {
                        writer.println("<span class='source'>(Source: " + ability.getSource() + ")</span>");
                    }

                    writer.println("<span class='description'>" + ability.getDescription() + "</span>");
                    writer.println("</li>");
                } else {
                    writer.println("<li>");
                    writer.println("<span class='name'>" + ability.getName() + "</span>");
                    writer.println("<span class='description'>Could not retrieve ability description</span>");
                    writer.println("</li>");
                }
            }
        }
    }

    protected void writeConditionals(PrintWriter writer, Collection<UnmodifiableBonus> conditionals){
        // Sort conditionals
        Collection<Bonus> sorted = new TreeSet<Bonus>(Bonus.COMPARATOR_BONUS_CONDITION);
        sorted.addAll(conditionals);

        // Write conditionals
        writer.println("<ul>");
        for (Bonus bonus : sorted) {
            writer.println("<li>");
            writer.println(bonus.getCondition());

            if (StringUtils.isNotBlank(bonus.getName())) {
                String name = bonus.getName();

                if (CollectionUtils.isNotEmpty(bonus.getItemKeys())) {
                    name = name + " (" + StringUtils.join(bonus.getItemKeys(), ", ") + ")";
                }

                writer.println("(" + name + ")");
            }

            writer.println("</li>");
        }
        writer.println("</ul>");
    }

    protected void writeSpells(PrintWriter writer, C character){
        Set<ClassWrapper> classes = character.getSpellcastingClasses();

        if (classes.size() > 0) {
            for (ClassWrapper characterClass : classes) {
                StaticClass staticClass = characterClass.getData();
                // Open section
                writer.println("<div id='spells-" + characterClass.getId() + "'>");

                // Write Spells known/save/per day
                writer.println("<h2>" + staticClass.getName() + " Spells</h2>");
                writer.println("<table class='spellStats'>");
                writer.println("<tr>");
                writer.println("<th class='known'>Spells Known</th>");
                writer.println("<th class='dc'>Save DC</th>");
                writer.println("<th class='level'>Level</th>");
                writer.println("<th class='total'>Total Spells per Day</th>");
                writer.println("<th class='base'>Base Spells per Day</th>");
                writer.println("<th class='bonus'>Bonus Spells</th>");
                writer.println("</tr>");

                // Cycle through spell levels
                int numLevels = staticClass.getSpellsPerDay()[characterClass.getLevels()].length;
                for (int i = 0; i < numLevels; i++) {
                    writer.println("<tr>");
                    writer.println(
                            "<td class='known'>" + formatNullable(character.getSpellsKnown(characterClass.getName(), i))
                                    + "</td>");
                    writer.println("<td class='dc'>" + character.getSpellSaveDC(i, characterClass.getName()) + "</td>");
                    writer.println("<td class='level'>" + i + "</td>");
                    writer.println(
                            "<td class='total'>"
                                    + formatSpellsPerDay(characterClass,
                                            character.getTotalSpellsPerDay(characterClass.getName(), i),
                                            i)
                                    + "</td>");
                    writer.println(
                            "<td class='base'>"
                                    + formatSpellsPerDay(characterClass,
                                            character.getBaseSpellsPerDay(characterClass.getName(), i),
                                            i)
                                    + "</td>");
                    writer.println(
                            "<td class='bonus'>" + formatNullable(character.getBonusSpells(characterClass.getName(), i))
                                    + "</td>");
                    writer.println("</tr>");
                }
                writer.println("</table>");

                if ((characterClass.getSpells() != null) || characterClass.hasSchoolSpells()) {
                    // Write spell details
                    writer.println("<table class='spellDetails'>");
                    writer.println("<tr>");
                    writer.println("<th class='name'>Name</th>");
                    writer.println("<th class='castingTime'>Casting Time</th>");
                    writer.println("<th class='duration'>Duration</th>");
                    writer.println("<th class='save'>Save</th>");
                    writer.println("<th class='spellResistance'>Spell Resistance</th>");
                    writer.println("<th class='range'>Range</th>");
                    writer.println("<th class='target'>Target / Area</th>");
                    writer.println("<th class='components'>Components</th>");
                    writer.println("<th class='school'>School</th>");
                    writer.println("<th class='source'>Source</th>");
                    writer.println("</tr>");

                    // Cycle through spell levels
                    if (characterClass.getSpells() != null) {
                        numLevels = CollectionUtils.size(characterClass.getSpells());
                        for (int classLevel = 0; classLevel < numLevels; classLevel++) {
                            writer.println("<tr>");
                            writer.println("<th colspan='10' class='subheading'>Level " + classLevel + "</td>");
                            writer.println("</tr>");

                            writeSpellSet(writer,
                                    characterClass.getSpells().get(classLevel),
                                    characterClass.getLevels());
                        }
                    }

                    // Check for schools
                    if (characterClass.hasSchoolSpells()) {
                        for (SchoolWrapper school : characterClass.getSchools()) {
                            log.trace("School {} has spells: {}", school.getName(), school.hasSpells());
                            if (school.hasSpells()) {
                                writer.println("<tr>");
                                writer.println("<th colspan='10' class='subheading'>" + school.getName() + "</td>");
                                writer.println("</tr>");

                                // Write school spells (1-based data in a
                                // 0-based array)
                                log.trace("Getting school spells for {}...", school.getName());
                                if (ModifierType.CLASS_LEVEL.equals(school.getData().getSpellsType())) {
                                    // School spells are bonuses at class levels
                                    int maxLevel = characterClass.getLevels();
                                    for (int spellLevel = 0; spellLevel < maxLevel; spellLevel++) {
                                        if (!school.getSpell(spellLevel + 1, maxLevel).isEmpty()) {
                                            writeSpellSet(writer,
                                                    school.getSpell(spellLevel + 1, maxLevel),
                                                    characterClass.getLevels());
                                        }
                                    }
                                } else {
                                    // School spells are listed by spell level
                                    int maxLevel = characterClass.getHighestSpellLevel();
                                    for (int spellLevel = 0; spellLevel < maxLevel; spellLevel++) {
                                        writeSpellSet(writer,
                                                school.getSpell(spellLevel + 1, maxLevel),
                                                characterClass.getLevels());
                                    }
                                }
                            }
                        }
                    }

                    writer.println("</table>");
                }

                // Close section
                writer.println("</div>");
            }
        }
    }

    private String formatSpellsPerDay(ClassWrapper characterClass, Integer base, int level){
        String result;
        if (StringUtils.equalsIgnoreCase(Character3E.ID_CLASS_CLERIC, characterClass.getId()) && (base != null)
                && (level > 0)) {
            result = formatNullable(base) + "+1";
        } else {
            result = formatNullable(base);
        }
        return result;
    }

    /**
     * Writes the details of a SpellSet.
     * 
     * @param writer The writer.
     * @param spellSet The SpellSet.
     * @param levels The number of levels the character has in the applicable
     *        spellcasting class(es).
     */
    private void writeSpellSet(PrintWriter writer, SpellSet spellSet, int levels){
        int count = 1;
        for (SpellWrapper characterSpell : spellSet) {
            StaticSpell spell = characterSpell.getData();
            String marker = (count % 2 == 0) ? "marker" : "";
            count++;

            writer.println("<tr class='" + marker + "'>");
            writer.println("<td rowspan='2' class='name'>" + spell.getName() + "</td>");
            writer.println("<td class='castingTime'>" + spell.getCastingTime() + "</td>");
            writer.println("<td class='duration'>" + spell.getDuration() + "</td>");
            writer.println("<td class='save'>" + formatNullable(spell.getSave()) + "</td>");
            writer.println("<td class='spellResistance'>" + formatBoolean(spell.isSpellResistance()) + "</td>");
            writer.println("<td class='range'>" + formatSpellRange(spell.getRange(), levels) + "</td>");
            writer.println("<td class='target'>" + spell.getTarget() + "</td>");
            writer.println("<td class='components'>" + spell.getComponents() + "</td>");
            writer.println("<td class='school'>" + spell.getSchool() + "</td>");
            writer.println("<td class='source'>" + spell.getSource() + "</td>");
            writer.println("</tr>");
            writer.println("<tr class='" + marker + "'>");
            writer.println("<td colspan='9' class='description'>" + spell.getDescription() + "</td>");
            writer.println("</tr>");
        }
    }

    /**
     * @param range The spell range in question
     * @param classLevels The number of levels the character has in the
     *        appropriate class
     * @return The text to output for this spell's range
     */
    private String formatSpellRange(String range, int classLevels){
        String label = range;

        SpellRange spellRange;
        try {
            spellRange = SpellRange.valueOf(label);
        } catch (IllegalArgumentException e) {
            spellRange = null;
        } catch (NullPointerException e) {
            spellRange = null;
            label = "NULL";
        }

        if (spellRange != null) {
            label = spellRange.getName();
            int distance = spellRange.getRange(classLevels);

            if (distance > 0) {
                label = label + " - " + distance + " ft.";
            }
        }

        return label;
    }

    /**
     * @param value The boolean value to convert
     * @return "yes" for true; "no" for false.
     */
    private String formatBoolean(boolean value){
        return (value) ? "yes" : "no";
    }

    protected String formatNullable(Object item){
        String result = "-";

        if ((item != null) && StringUtils.isNotBlank(item.toString())) {
            result = item.toString();
        }

        return result;
    }

    protected String formatBlankable(Object item){
        String result = null;

        if (item != null) {
            result = item.toString();
        }
        if (StringUtils.isBlank(result)) {
            result = "&nbsp;";
        }

        return result;
    }

    protected void writeWeapons(PrintWriter writer, C character){
        Set<Weapon> weaponSet = character.getWeaponSet();

        if (CollectionUtils.isNotEmpty(weaponSet)) {
            // Open section
            writer.println("<div id='weapons'>");
            writer.println("<h2>Weapons/Attacks</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>Weapon</th>");
            writer.println("<th class='bonus'>Atk. Bonus</th>");
            writer.println("<th class='damage'>Dam.</th>");
            writer.println("<th class='critical'>Critical</th>");
            writer.println("<th class='type'>Dam. Type</th>");
            writer.println("<th class='range'>Range</th>");
            writer.println("<th class='size'>Size</th>");
            writer.println("<th class='weight'>Weight (Lbs.)</th>");
            writer.println("<th class='category'>Class</th>");
            writer.println("<th class='notes'>Special</th>");
            writer.println("</tr>");

            // Write weapons
            for (Weapon weapon : weaponSet) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + weapon.getDisplayName() + "</td>");
                writer.println("<td class='bonus'>"
                        + Character.FORMATTER_MODIFIER.format(character.getWeaponAttackBonus(weapon))
                        + "</td>");
                writer.println("<td class='damage'>" + character.getWeaponDamage(weapon) + "</td>");
                writer.println("<td class='critical'>" + character.getThreatRange(weapon)
                        + "/"
                        + weapon.getData().getThreatMultiplier()
                        + "</td>");
                writer.println("<td class='type'>" + weapon.getData().getDamageType() + "</td>");
                writer.println("<td class='range'>" + formatNullable(weapon.getData().getRangeIncrement()) + "</td>");
                writer.println("<td class='size'>" + formatNullable(weapon.getSize()) + "</td>");
                writer.println("<td class='weight'>" + formatNullable(weapon.getWeight()) + "</td>");
                writer.println("<td class='category'>" + weapon.getData().getProficiency().getDisplayName()
                        + ", "
                        + weapon.getData().getHeft().getAbbreviation()
                        + "</td>");
                writer.println("<td class='notes'>" + formatSpecialAbilities(weapon, character) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    protected void writeAmmunition(PrintWriter writer, C character){
        Set<Ammunition> ammoSet = character.getAmmunitionSet();

        if (CollectionUtils.isNotEmpty(ammoSet)) {
            // Open section
            writer.println("<div id='ammo'>");
            writer.println("<h2>Ammunition</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>Ammunition</th>");
            writer.println("<th class='quantity'>Quantity</th>");
            writer.println("<th class='spent'>Spent</th>");
            writer.println("</tr>");

            // Write weapons
            for (Ammunition ammo : ammoSet) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + ammo.getDisplayName() + "</td>");
                writer.println(
                        "<td class='quantity'>" + Character.FORMATTER_THOUSANDS.format(ammo.getQuantity()) + "</td>");
                writer.println("<td class='spent'>" + generateChargeBlocks(ammo.getQuantity()) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    protected void writeChargedItems(PrintWriter writer, C character){
        Set<Charge> charges = character.getCharges();

        if (CollectionUtils.isNotEmpty(charges)) {
            // Open section
            writer.println("<div id='chargedItems'>");
            writer.println("<h2>Charged Items</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>Name</th>");
            writer.println("<th class='quantity'>Charges</th>");
            writer.println("<th class='spent'>Used</th>");
            writer.println("</tr>");

            // Write weapons
            for (Charge charge : charges) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + charge.getName() + "</td>");
                writer.println(
                        "<td class='quantity'>" + Character.FORMATTER_THOUSANDS.format(charge.getQuantity()) + "</td>");
                writer.println("<td class='spent'>" + generateChargeBlocks(charge.getQuantity()) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    protected String generateChargeBlocks(int quantity){
        StringBuilder blocks = new StringBuilder();

        // Append full sets of 5
        int numBlocks = quantity / 5;
        blocks.append(StringUtils.repeat(BOXES, numBlocks));

        // Append remainder
        int remainder = quantity % 5;
        if (remainder > 0) {
            blocks.append(StringUtils.repeat(BOX_EMPTY, remainder));
            blocks.append(StringUtils.repeat(BOX_MARKED, 5 - remainder));
        }

        return blocks.toString();
    }

    protected void writeArmor(PrintWriter writer, C character){
        Set<Armor> armorSet = character.getArmorSet();

        if (CollectionUtils.isNotEmpty(armorSet)) {
            // Open section
            writer.println("<div id='armor'>");
            writer.println("<h2>Armor</h2>");
            writer.println("<table>");

            writer.println("<tr>");
            writer.println("<th class='name'>AC Item</th>");
            writer.println("<th class='bonus'>AC Bonus</th>");
            writer.println("<th class='type'>Armor Type</th>");
            writer.println("<th class='dex'>Max. Dex.</th>");
            writer.println("<th class='penalty'>Check Penalty</th>");
            writer.println("<th class='spellFailure'>Spell Failure</th>");
            writer.println("<th class='weight'>Weight (Lbs.)</th>");
            writer.println("<th class='notes'>Special</th>");
            writer.println("</tr>");

            // Write armor
            for (Armor armor : armorSet) {
                writer.println("<tr>");
                writer.println("<td class='name'>" + armor.getDisplayName() + "</td>");
                writer.println(
                        "<td class='bonus'>" + Character.FORMATTER_MODIFIER.format(character.getArmorBonus(armor))
                                + "</td>");
                writer.println("<td class='type'>" + armor.getData().getProficiency().getAbbreviation() + "</td>");
                writer.println("<td class='dex'>" + formatNullable(character.getArmorMaxDex(armor)) + "</td>");
                writer.println(
                        "<td class='penalty'>" + formatNullable(character.getTotalArmorPenalty(armor)) + "</td>");
                writer.println("<td class='spellFailure'>" + armor.getData().getArcaneFailure() + "%</td>");
                writer.println("<td class='weight'>" + armor.getWeight() + "</td>");
                writer.println("<td class='notes'>" + formatSpecialAbilities(armor, character) + "</td>");
                writer.println("</tr>");
            }

            // Close section
            writer.println("</table>");
            writer.println("</div>");
        }
    }

    protected String formatSpecialAbilities(AbstractEquipment<?> item, C character){
        Set<String> names = new HashSet<String>();

        // Add ability & bonus names
        Collection<NamedObject> namedObjs = com.westpalmetto.gaming.character.util.CollectionHelper
                .<NamedObject> newHashSet(item.getAbilities(item.getBonuses(character.getModifiers())),
                        item.getBonuses(character.getModifiers()));
        for (NamedObject obj : namedObjs) {
            if (StringUtils.isNotBlank(obj.getName())) {
                names.add(obj.getName());
            }
        }

        // Add notes
        // TODO: Notes

        // Add item keys
        names.addAll(item.getData().getItemKeys());

        // Remove name & ID
        names.remove(item.getName());
        names.remove(item.getId());

        return formatBlankable(StringUtils.join(names, "; "));
    }

    protected void writeEquipment(PrintWriter writer, C character){
        EquipmentSet equipment = character.getData().getEquipment();

        // Open section
        writer.println("<div id='equipment'>");

        // Write equipment
        Map<String, Set<AbstractEquipment<?>>> locationsMap = equipment.getEquipmentByLocation();
        writer.println("<h2>Equipment</h2>");

        // Iterate through locations
        for (Map.Entry<String, Set<AbstractEquipment<?>>> location : locationsMap.entrySet()) {
            if (AbstractEquipment.LOCATION_EQUIPPED.equals(location.getKey())) {
                writeEquipmentEquipped(writer, locationsMap);
            } else {
                writeEquipmentContainer(writer, location.getKey(), locationsMap);
            }
        }

        // Write coins
        writeCoins(writer, character);

        // Determine current load
        writeLoad(writer, character);

        // Close section
        writer.println("</div>");
    }

    protected void writeCoins(PrintWriter writer, C character){
        Coins coins = character.getData().getEquipment().getCoins();
        if ((coins != null) && ((coins.getQuantity() > 0) || (coins.getGems() > 0))) {
            writer.println("<h2>Money</h2>");
            writer.println("<div id='coins'>");
            writeLabelValuePair(writer,
                    "copper",
                    "Copper Pieces",
                    Character.FORMATTER_THOUSANDS.format(coins.getCopper()));
            writeLabelValuePair(writer,
                    "silver",
                    "Silver Pieces",
                    Character.FORMATTER_THOUSANDS.format(coins.getSilver()));
            writeLabelValuePair(writer, "gold", "Gold Pieces", Character.FORMATTER_THOUSANDS.format(coins.getGold()));
            writeLabelValuePair(writer,
                    "platinum",
                    "Platinum Pieces",
                    Character.FORMATTER_THOUSANDS.format(coins.getPlatinum()));
            writeLabelValuePair(writer, "gems", "Gems", Character.FORMATTER_THOUSANDS.format(coins.getGems()));
            writer.println("</div>");
        }
    }

    protected void writeLoad(PrintWriter writer, C character){
        CarryingCapacity current = character.getCarryingCapacity();
        String lightClass = "light";
        String mediumClass = "medium";
        String heavyClass = "heavy";
        switch (current) {
        case LIGHT:
            lightClass = "light load";
            break;
        case MEDIUM:
            mediumClass = "medium load";
            break;
        case HEAVY:
            heavyClass = "heavy load";
            break;
        }

        // Write carrying capacity
        writer.println("<h2>Carrying Capacity (Maximum in Lbs.)</h2>");
        writer.println("<div id='carryingCapacity'>");
        writeLabelValuePair(writer, lightClass, "Light Load", character.getCarryingCapacityLight());
        writeLabelValuePair(writer, mediumClass, "Medium Load", character.getCarryingCapacityMedium());
        writeLabelValuePair(writer, heavyClass, "Heavy Load", character.getCarryingCapacityHeavy());
        writeLabelValuePair(writer, "overHead", "Lift Over Head", character.getCarryingCapacityOverHead());
        writeLabelValuePair(writer, "offGround", "Lift Off Ground", character.getCarryingCapacityOffGround());
        writeLabelValuePair(writer, "drag", "Push/Drag", character.getCarryingCapacityPushDrag());
        writer.println("</div>");
    }

    /**
     * Write the details of equipped equipment.
     * 
     * @param writer The writer.
     * @param locationsMap The map of equipment by location.
     */
    private void writeEquipmentEquipped(PrintWriter writer, Map<String, Set<AbstractEquipment<?>>> locationsMap){
        writer.println("<div class='location' id='equipped'>");
        writer.println("<h3>" + AbstractEquipment.LOCATION_EQUIPPED + "</h3>");
        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='slot'>Slot</th>");
        writer.println("<th class='name'>Item</th>");
        writer.println("<th class='source'>Source</th>");
        writer.println("<th class='quantity'>Qty</th>");
        writer.println("<th class='weight'>Weight</th>");
        writer.println("<th class='totalWeight'>Total Weight</th>");
        writer.println("</tr>");

        // Iterate through items in the location
        Map<Slot, Set<AbstractEquipment<?>>> bySlot =
                EquipmentSet.bySlot(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED));
        for (Entry<Slot, Set<AbstractEquipment<?>>> slot : bySlot.entrySet()) {
            List<AbstractEquipment<?>> items = new ArrayList<AbstractEquipment<?>>(slot.getValue());
            int size = items.size();
            for (int i = 0; i < size; i++) {
                AbstractEquipment<?> item = items.get(i);

                if (i == 0) {
                    writer.println("<tr>");
                    writer.println(
                            "<th class='slot' rowspan='" + size + "'>" + item.getData().getSlot().getName() + "</th>");
                    writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
                    writer.println("<td class='source'>" + item.getSource() + "</td>");
                    writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
                    writer.println("<td class='weight'>" + item.getWeight() + "</td>");
                    writer.println(
                            "<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
                    writer.println("</tr>");
                } else {
                    writer.println("<tr>");
                    writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
                    writer.println("<td class='source'>" + item.getSource() + "</td>");
                    writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
                    writer.println("<td class='weight'>" + item.getWeight() + "</td>");
                    writer.println(
                            "<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
                    writer.println("</tr>");
                }
            }
        }

        // Output the total weight for the container
        writer.println("<tr class='containerTotalWeight'>");
        writer.println("<th colspan='5'>Total Weight</th>");
        writer.println("<td class='totalWeight'>"
                + EquipmentSet.getTotalWeight(locationsMap.get(AbstractEquipment.LOCATION_EQUIPPED), locationsMap)
                + "</td>");
        writer.println("</tr>");

        // Close the location container
        writer.println("</table>");
        writer.println("</div>");
    }

    /**
     * Write the details of a container of equipment.
     * 
     * @param writer The writer.
     * @param name The name of the container.
     * @param locationsMap The map of equipment by location.
     */
    private void writeEquipmentContainer(PrintWriter writer,
            String name,
            Map<String, Set<AbstractEquipment<?>>> locationsMap){
        writer.println("<div class='location'>");
        writer.println("<h3>" + name + "</h3>");
        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th class='name'>Item</th>");
        writer.println("<th class='source'>Source</th>");
        writer.println("<th class='quantity'>Qty</th>");
        writer.println("<th class='weight'>Weight</th>");
        writer.println("<th class='totalWeight'>Total Weight</th>");
        writer.println("</tr>");

        // Iterate through items in the location
        for (AbstractEquipment<?> item : locationsMap.get(name)) {
            writer.println("<tr>");
            writer.println("<th class='name'>" + item.getDisplayName() + "</th>");
            writer.println("<td class='source'>" + item.getSource() + "</td>");
            writer.println("<td class='quantity'>" + item.getQuantity() + "</td>");
            writer.println("<td class='weight'>" + item.getWeight() + "</td>");
            writer.println("<td class='totalWeight'>" + EquipmentSet.getTotalWeight(item, locationsMap) + "</td>");
            writer.println("</tr>");
        }

        // Output the total weight for the container
        writer.println("<tr class='containerTotalWeight'>");
        writer.println("<th colspan='4'>Total Weight</th>");
        writer.println("<td class='totalWeight'>" + EquipmentSet.getTotalWeight(locationsMap.get(name), locationsMap)
                + "</td>");
        writer.println("</tr>");

        // Close the location container
        writer.println("</table>");
        writer.println("</div>");
    }

    protected void writeNotes(PrintWriter writer, C character){
        writer.println("<h2>Notes</h2>");
        writer.println("<ul id='notes'>");

        for (String note : character.getData().getNotes()) {
            writer.println("<li>" + note + "</li>");
        }

        writer.println("</ul>");
    }
}
