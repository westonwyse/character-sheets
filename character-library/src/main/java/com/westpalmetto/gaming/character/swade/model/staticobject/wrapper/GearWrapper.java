package com.westpalmetto.gaming.character.swade.model.staticobject.wrapper;

import org.apache.commons.lang3.StringUtils;

import com.westpalmetto.gaming.character.ResourceFactory.Ruleset;
import com.westpalmetto.gaming.character.model.staticobject.wrapper.AbstractStaticObjectWrapper;
import com.westpalmetto.gaming.character.swade.model.staticobject.StaticGear;
import com.westpalmetto.gaming.character.swade.model.staticobject.loader.StaticGearLoader;
import com.westpalmetto.gaming.character.util.loading.DataLoader;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GearWrapper extends AbstractStaticObjectWrapper<StaticGear>{
    private int quantity;

    public GearWrapper(String id){
        super(id, Ruleset.SWADE.name());
    }

    @Override
    protected DataLoader<StaticGear> createDataLoader(){
        return StaticGearLoader.getInstance(getRoot());
    }

    public boolean isWeapon(){
        return StringUtils.isNotBlank(getData().getDamage());
    }
}
