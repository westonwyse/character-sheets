/**
\ * ProvidesBonus.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import java.util.Collection;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;
import com.westpalmetto.gaming.character.model.bonuses.ModifiableBonus;
import com.westpalmetto.gaming.character.model.bonuses.Modifier;
import com.westpalmetto.gaming.character.model.bonuses.OptionalBonuses;
import com.westpalmetto.gaming.character.model.bonuses.UnmodifiableBonus;

/**
 * Interface that must be implemented by any class that may provide non-standard
 * bonuses to specific rolls (feats, class abilities, equipment, etc.).
 * 
 * Attribute does not implement this class because it provides a standard
 * (mathematically-determinable) bonus to various rolls.  Any roll with an
 * applied attribute bonus should apply that bonus separately.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface BonusProvider extends NamedObject {
	/**
	 * Returns all bonuses provided by this item.
	 * @param modifiers The collection of modifiers for the bonuses.
	 * @return All bonuses provided by this item.
	 */
	public Collection<UnmodifiableBonus> modifyBonuses(Collection<Modifier> modifiers);
	
	/**
	 * Returns all bonuses provided by this item of a specific bonus type.
	 * @param modifiers The collection of modifiers for the bonuses.
	 * @param type The type of bonus to retrieve.
	 * @return All applicable bonuses provided by this item.
	 */
	public Collection<UnmodifiableBonus> modifyBonuses(BonusType type, Collection<Modifier> modifiers);
	
	/**
	 * Returns all bonus modifiers provided by this item.
	 * @return All bonus modifiers provided by this item.
	 */
	public Collection<Modifier> getModifiers();
	
	/**
	 * Returns all bonuses provided by this item.
	 * 
	 * @param modifiers The collection of modifiers for the bonuses.
	 * @return All bonuses provided by this item.
	 */
	public Collection<ModifiableBonus> getBonuses(Collection<Modifier> modifiers);
	
	/**
	 * Returns all bonuses provided by this item of a specific bonus type.
	 * 
	 * @param type The type of bonus to retrieve.
	 * @param modifiers The collection of modifiers for the bonuses.
	 * @return All applicable bonuses provided by this item.
	 */
	public Collection<ModifiableBonus> getBonuses(BonusType type, Collection<Modifier> modifiers);
	
	/**
	 * Returns optional bonuses provided by this item.
	 * 
	 * @param modifiers The collection of modifiers for the bonuses.
	 * @return All applicable optional bonuses provided by this item.
	 */
	public OptionalBonuses getOptional(Collection<Modifier> modifiers);
}
