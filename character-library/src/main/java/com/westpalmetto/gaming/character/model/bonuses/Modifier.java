/**
 * Bonus.java
 * 
 * Copyright � 2013 Weston W. Clowney. All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model.bonuses;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.westpalmetto.gaming.character.NamedObject;
import com.westpalmetto.util.resources.model.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * Defines a bonus modifier provided by some object.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
@Getter
@Setter
public class Modifier extends Model implements KeyedObject{
    /**
     * The type of the modifier.
     */
    public enum ModifierType{
        ATTRIBUTE, BASE_ATTACK, CHARACTER_LEVEL, CLASS_LEVEL, HINDRANCE_BUYOFF, SKILL_RANK,;
    }

    private String name;
    private String source;
    private ModifierType type;
    private int value;
    private Set<String> itemKeys;
    private boolean requireItemKey;

    /**
     * No-argument constructor.
     */
    public Modifier(){}

    /**
     * Copy constructor.
     * 
     * @param original The object to copy
     */
    public Modifier(Modifier original){
        setName(original.getName());
        setSource(original.getSource());
        setType(original.getType());
        setValue(original.getValue());
        setItemKeys(original.getItemKeys());
        setRequireItemKey(original.isRequireItemKey());
    }

    @Override
    public Set<String> getItemKeys(){
        if (itemKeys == null) {
            setItemKeys(new HashSet<String>());
        }
        return itemKeys;
    }

    @Override
    public void setItemKeys(Collection<String> itemKeys){
        this.itemKeys = new HashSet<>(itemKeys);
    }

    @Override
    public int compareTo(NamedObject obj){
        return getName().compareTo(obj.getName());
    }
}
