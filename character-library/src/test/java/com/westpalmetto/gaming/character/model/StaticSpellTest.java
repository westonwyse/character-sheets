/**
 * StaticSpellTest.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.model;

import org.junit.Assert;
import org.junit.Test;

import com.westpalmetto.gaming.character.dd3e.model.staticobject.StaticSpell.SpellRange;


/**
 * Tests for StaticSpell. 
 */
public class StaticSpellTest {
	@Test
	public void testCloseSpellRange(){
		SpellRange range = SpellRange.CLOSE;
		int[] expected = new int[] {25,30,30,35,35,40,40,45,45,50,50,55,55,60,60,65,65,70,70,75};
		for(int level=1; level <= 20; level++){
			Assert.assertEquals(expected[level-1], range.getRange(level));
		}
	}
}
