package com.westpalmetto.gaming.character.bonuses;

import lombok.Getter;
import lombok.Setter;

/**
 * A bonus chosen during character creation or advancement. Not a wrapper.
 */
@Getter
@Setter
public class ChosenBonus extends AbstractBonusProvider{
    private String name;
    private String source;
}
