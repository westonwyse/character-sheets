/**
 * BonusTypePredicate.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision Mar 17, 2013
 */
package com.westpalmetto.gaming.character.bonuses;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.Predicate;

import com.westpalmetto.gaming.character.model.bonuses.Bonus;
import com.westpalmetto.gaming.character.model.bonuses.Bonus.BonusType;

/**
 * Predicate to match a given BonusType.  Evaluates to true if an object
 * is a Bonus and matches the specified type.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public class BonusTypePredicate implements Predicate {
	private static Map<BonusType, BonusTypePredicate> instances = new HashMap<BonusType, BonusTypePredicate>();
	
	private BonusType type;
	
	private BonusTypePredicate(BonusType type){
		this.type = type;
	}
	
	public static BonusTypePredicate getInstance(BonusType type){
		//Get from map
		BonusTypePredicate predicate = instances.get(type);
		
		//Create if not found
		if(predicate == null){
			predicate = new BonusTypePredicate(type);
			instances.put(type, predicate);
		}
		
		return predicate;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
	 */
	@Override
	public boolean evaluate(Object object) {
		boolean matches = false;
		
		//Investigate object
		if(object instanceof Bonus){
			Bonus bonus = (Bonus)object;
			matches = (bonus.getType() == type);
		}
		
		return matches;
	}
}
