/**
 * StaticObject.java
 * 
 * Copyright � 2013 Weston W. Clowney.
 * All rights reserved.
 * 
 * Initial Revision May 5, 2013
 */
package com.westpalmetto.gaming.character.model.staticobject;

import com.westpalmetto.gaming.character.NamedObject;

/**
 * Interface to mark objects that can be loaded statically through a DataLoader.
 * StaticObject objects must implement a way to create a copy of themselves;
 * copy constructors are preferred.
 * 
 * @author $Author$
 * @version $Revision$
 * @date $Date$
 */
public interface StaticObject extends NamedObject {
	/**
	 * @return The name of this object
	 */
	public void setName(String name);
}
